//
//  ViewController.m
//  ImageViewer
//
//  Created by Jimmy on 12/6/13.
//  Copyright (c) 2013 SBU. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

-(void)viewDidLoad {
    _imageView.image = [UIImage imageNamed:@"img1.jpg"];
}

-(void)playSound {
    NSError *error = nil;
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"loon" ofType:@"wav"]];
	self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    
    if(error)
        NSLog(@"Error Initiating Player: %@", error.description);
    
	if([_player play])
        NSLog(@"played");
}

-(IBAction)showImage1:(id)sender {
    [self playSound];
    [self animateImageViewToImageNamed:@"img1.jpg"];
}

-(IBAction)showImage2:(id)sender {
    [self playSound];
    [self animateImageViewToImageNamed:@"img2.jpg"];
}

-(IBAction)showImage3:(id)sender {
    [self playSound];
    [self animateImageViewToImageNamed:@"img3.jpg"];
}

-(IBAction)showImage4:(id)sender {
    [self playSound];
    [self animateImageViewToImageNamed:@"img4.jpg"];
}

-(IBAction)showImage5:(id)sender {
    [self playSound];
    [self animateImageViewToImageNamed:@"img5.jpg"];
}

-(IBAction)showImage6:(id)sender {
    [self playSound];
    [self animateImageViewToImageNamed:@"img6.jpg"];
}

#pragma mark - Helpers

-(void)animateImageViewToImageNamed:(NSString*)filename {
    UIImage * toImage = [UIImage imageNamed:filename];
    [UIView transitionWithView:_imageView duration:1.0f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        _imageView.image = toImage;
    } completion:nil];
}

@end
