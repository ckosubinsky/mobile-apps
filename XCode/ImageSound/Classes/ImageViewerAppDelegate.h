//
//  ImageViewerAppDelegate.h
//  ImageViewer
//
//  Created by teabones on 9/19/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageViewerViewController;

@interface ImageViewerAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    ImageViewerViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ImageViewerViewController *viewController;

@end

