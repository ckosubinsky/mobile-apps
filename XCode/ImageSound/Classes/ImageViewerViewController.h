//
//  ImageViewerViewController.h
//  ImageViewer
//
//  Created by teabones on 9/19/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<AVFoundation/AVAudioPlayer.h>

@interface ImageViewerViewController : UIViewController {
	
	IBOutlet UIImageView *imageView;

}

- (IBAction) showimg1;
- (IBAction) showimg2;
- (IBAction) showimg3;
- (IBAction) showimg4;
- (IBAction) showimg5;
- (IBAction) showimg6;

-(IBAction) playSound:(id)sender;

@end

