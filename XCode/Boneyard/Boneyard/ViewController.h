//
//  ViewController.h
//  Boneyard
//
//  Created by Jimmy on 1/20/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {

}

@property (nonatomic, strong) IBOutlet UIImageView *walk;
@property (nonatomic, strong) NSMutableArray *anim;

// A method to move the animated object...
-(void) move;

@end
