//
//  ViewController.m
//  Boneyard
//
//  Created by Jimmy on 1/20/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.anim = [[NSMutableArray alloc] init];
    
    // A for loop is used instead of a list of each picture in the array.
    
    for(int i=0;i<15;i++){
        NSString *pic=[NSString stringWithFormat:@"b%d.png", i];
        UIImage *img=[UIImage imageNamed:pic];
        if (img)
            [_anim addObject:img];
    }
    
    [_walk setAnimationImages:_anim];
    [_walk setAnimationDuration:.75];
    [_walk startAnimating];
}

-(void)viewWillLayoutSubviews {
    [NSTimer scheduledTimerWithTimeInterval:17 target:self selector:@selector(move) userInfo:nil repeats:YES];
    [self move];
}


-(void)move {
    _walk.frame=CGRectMake(-240,140,240,180);
    
    [UIView animateWithDuration:16.5 animations:^{
        _walk.frame=CGRectMake(self.view.frame.size.width+_walk.frame.size.width,140,240,180);
    }];
}

@end
