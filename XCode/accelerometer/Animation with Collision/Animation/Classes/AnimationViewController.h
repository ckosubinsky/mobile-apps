//
//  AnimationViewController.h
//  Animation
//
//  Created by Raman Nanda on 10/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnimationViewController : UIViewController <UIAccelerometerDelegate> {
	IBOutlet UIImageView *movement;
	NSMutableArray *anim;
	NSMutableArray *moveRight;
	NSMutableArray *moveLeft;
	NSMutableArray *standingRight;
	NSMutableArray *standingLeft;
	NSMutableArray *jumpingRight;
	NSMutableArray *jumpingLeft;
	
	IBOutlet UIImageView *collisionBox;
	
	int lastState;
	int currentState; 
	
	float oldY;
}

@property (nonatomic, retain)UIImageView *movement;
@property (nonatomic, retain)UIImageView *collisionBox;

@property (nonatomic, retain)NSMutableArray *anim;
@property (nonatomic, retain)NSMutableArray *moveRight;
@property (nonatomic, retain)NSMutableArray *moveLeft;
@property (nonatomic, retain)NSMutableArray *standingRight;
@property (nonatomic, retain)NSMutableArray *standingLeft;
@property (nonatomic, retain)NSMutableArray *jumpingRight;
@property (nonatomic, retain)NSMutableArray *jumpingLeft;


- (void) moveRight:(float)acceleration; 
- (void) moveLeft:(float)acceleration; 
- (void) standLeft;
- (void) standRight;
- (void) jumpLeft;
- (void) jumpRight;

- (void) checkCollision;

@end

