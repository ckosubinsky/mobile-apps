//
//  AnimationViewController.m
//  Animation
//
//  Created by Raman Nanda on 10/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AnimationViewController.h"

@implementation AnimationViewController

@synthesize movement;
@synthesize anim;
@synthesize moveRight;
@synthesize moveLeft;
@synthesize standingRight;
@synthesize standingLeft;
@synthesize jumpingRight;
@synthesize jumpingLeft;

@synthesize collisionBox;

// States
// Facing Left - 1
// Running Left - 2
// Facing Right - 3
// Running Right - 4
// Jumping Left - 5
// Jumping Right - 6

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	UIAccelerometer *accel = [UIAccelerometer sharedAccelerometer];
	accel.delegate = self;
	accel.updateInterval = 1.0f/60.0f;
	
    //[NSTimer scheduledTimerWithTimeInterval:17 target:self selector:@selector(move) userInfo:nil repeats:YES];
	//[self move];
	
	anim=[[NSMutableArray alloc] init];
	moveRight =[[NSMutableArray alloc] init];
	moveLeft =[[NSMutableArray alloc] init];
	standingRight =[[NSMutableArray alloc] init];
	standingLeft =[[NSMutableArray alloc] init];
	jumpingRight =[[NSMutableArray alloc] init];
	jumpingLeft =[[NSMutableArray alloc] init];
	
	for(int i=1;i<=3;i++){
		
		NSString *pic=[NSString stringWithFormat:@"LStanding%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[standingLeft addObject:img];
		
	}
	
	for(int i=1;i<=3;i++){
		
		NSString *pic=[NSString stringWithFormat:@"RStanding%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[standingRight addObject:img];
		
	}
	
	for(int i=1;i<=5;i++){
		
		NSString *pic=[NSString stringWithFormat:@"RRun%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[moveRight addObject:img];
		
	}
	
	for(int i=1;i<=5;i++){
		
		NSString *pic=[NSString stringWithFormat:@"LRun%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[moveLeft addObject:img];
		
	}
	
	for(int i=1;i<=5;i++){
		
		NSString *pic=[NSString stringWithFormat:@"RJump%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[jumpingRight addObject:img];
		
	}
	
	for(int i=1;i<=5;i++){
		
		NSString *pic=[NSString stringWithFormat:@"LJump%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[jumpingLeft addObject:img];
		
	}
	
	[movement setAnimationImages:standingRight];
	currentState = 3;
	[movement setAnimationDuration:3.0];
	[movement startAnimating];

    [super viewDidLoad];
}




// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if ((interfaceOrientation==UIInterfaceOrientationPortrait)||(interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown))
	{
		return NO;
	}
	if ((interfaceOrientation==UIInterfaceOrientationLandscapeLeft)||(interfaceOrientation==UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


- (void) moveRight:(float)acceleration 
{
	
	if(currentState != lastState)
	{
		[movement setAnimationImages:moveRight];
		[movement setAnimationDuration:.25];
		[movement startAnimating];
	}
	
	if (movement.center.x < 460) {
		int moveBy = (int)((fabs(acceleration) * 10));
		movement.center = CGPointMake((movement.center.x + moveBy), movement.center.y);
		[self checkCollision];
	}
		
}

- (void) moveLeft:(float)acceleration 
{
	if(currentState != lastState)
	{
		[movement setAnimationImages:moveLeft];
		[movement setAnimationDuration:.25];
		[movement startAnimating];
	}
	
	if (movement.center.x > 0) {
		int moveBy = (int)((fabs(acceleration) * 10));
		movement.center = CGPointMake(movement.center.x - moveBy, movement.center.y);
		[self checkCollision];
	}
	
}

- (void) standLeft
{
	if(currentState != lastState)
	{
		[movement setAnimationImages:standingLeft];
		[movement setAnimationDuration:2.5];
		[movement startAnimating];
	}
}

- (void) standRight
{
	if(currentState != lastState)
	{
		[movement setAnimationImages:standingRight];
		[movement setAnimationDuration:2.5];
		[movement startAnimating];	
	}
}

- (void)accelerometer:(UIAccelerometer *)acel didAccelerate:(UIAcceleration *)aceler {
	if(currentState != 5 || currentState != 6)
	{
		if((aceler.y > 0 && (aceler.y < .15)))
		{
			lastState = currentState;
			currentState = 1;
			[self standLeft];
			NSLog(@"Facing Left, Y: %f", aceler.y);
		}
		else if((aceler.y < 0 && (aceler.y > -.15)))
		{
			lastState = currentState;
			currentState = 3;
			[self standRight];
			NSLog(@"Facing Right, Y: %f", aceler.y);
		}
		else if((aceler.y) < -.15)
		{
			lastState = currentState;
			currentState = 4;
			[self moveRight:aceler.y];
			NSLog(@"Moving Right, Y: %f", aceler.y);
		}
		else if((aceler.y) > .15)
		{
			lastState = currentState;
			currentState = 2;
			[self moveLeft:aceler.y];
			NSLog(@"Moving Left, Y: %f", aceler.y);			
		}
	}
}

- (void) checkCollision {
	if(CGRectIntersectsRect(movement.frame, collisionBox.frame)) {
		movement.frame=CGRectMake(10,234,55,55);
		NSLog(@"*** Currently Colliding ***");
	}
	
}



@end
