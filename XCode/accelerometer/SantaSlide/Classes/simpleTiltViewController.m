//
//  simpleTiltViewController.m
//  simpleTilt
//
//  Created by JoshuaCaputo on 2/28/09.
//  Copyright JoshuaCaputo 2009. All rights reserved.
//

#import "simpleTiltViewController.h"

@implementation simpleTiltViewController
//@synthesize Object, NAME;

@synthesize stnick, walkright, walkleft;

- (void)viewDidLoad {
    [super viewDidLoad];
	
	timer = [NSTimer scheduledTimerWithTimeInterval:.05 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];

	
	[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / kAccelerometerFrequency)];
    [[UIAccelerometer sharedAccelerometer] setDelegate:self];
	
	position = CGPointMake(0,0);
    
    walkright = [[NSMutableArray alloc] init];
	walkleft = [[NSMutableArray alloc] init];
	    
	// a loop that lists the walk right images
	for (int i = 1; i < 15; i++){
		
        NSString *pic = [NSString stringWithFormat:@"santa%d.png", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [walkright addObject:img];
        
	}
    
	// a loop that lists the walk left images
	for (int i = 1; i < 15; i++){	
		
        NSString *pic = [NSString stringWithFormat:@"santaleft%d.png", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [walkleft addObject:img];
		
	}
  
	//set the default animation for the view - the walk cycle
	[stnick setAnimationImages:walkright];
	[stnick setAnimationDuration:.5];
    
	[stnick startAnimating];
	
}


// UIAccelerometerDelegate method, called when the device accelerates.
- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    // Update the accelerometer graph view
	deviceTilt.x = acceleration.x;
	deviceTilt.y = acceleration.y;
}


-(void)onTimer{
	
	int speed;
	speed = 5;
	
	stnick.center = CGPointMake(stnick.center.x+position.x,stnick.center.y+position.y);
	
	//Moves the name to wherever the Object is.
	//NAME.center = CGPointMake(Object.center.x, 141);
	
	//These two stop the Object from moving off the line.
	if (stnick.center.x > 444){stnick.center = CGPointMake(444,160);}
	if (stnick.center.x < 36){stnick.center = CGPointMake(36,160);}
	
	
		if (deviceTilt.y > 0.3f) {
            position.x = -speed;
            
            [stnick setAnimationImages:walkleft];
            [stnick setAnimationDuration:.05];
            
            [stnick startAnimating];
        }
		else if (deviceTilt.y < -0.3f) {
            position.x = speed;
            
            [stnick setAnimationImages:walkright];
            [stnick setAnimationDuration:.05];
            
            [stnick startAnimating];
        
        }
    
		else if (0.2f < deviceTilt.y > -0.2f){position.x = 0;}
	
	// These just change the image when it gets offset from the center.
	//if (Object.center.x > 242){[Object setImage:[UIImage imageNamed:@"Add.png"] forState:UIControlStateNormal];}
	//if (Object.center.x < 238){[Object setImage:[UIImage imageNamed:@"Subtract.png"] forState:UIControlStateNormal];}

	//If the Object is in the 4 center most x integers.
	/*if (Object.center.x > 238){
		if (Object.center.x < 242){[Object setImage:[UIImage imageNamed:@"Smiley.png"] forState:UIControlStateNormal];
		}
	}
	 */
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


- (void)dealloc {
    [super dealloc];
}

@end
