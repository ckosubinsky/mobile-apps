//
//  simpleTiltAppDelegate.h
//  simpleTilt
//
//  Created by JoshuaCaputo on 2/28/09.
//  Copyright JoshuaCaputo 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class simpleTiltViewController;

@interface simpleTiltAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    simpleTiltViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet simpleTiltViewController *viewController;

@end

