//
//  simpleTiltViewController.h
//  simpleTilt
//
//  Created by JoshuaCaputo on 2/28/09.
//  Copyright JoshuaCaputo 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kAccelerometerFrequency     40

@interface simpleTiltViewController : UIViewController <UIAccelerometerDelegate> {

	CGPoint deviceTilt;
	
	NSTimer *timer;
	
	//IBOutlet UIButton *Object; 
    
    
	
	//IBOutlet UILabel *NAME; 
    
    IBOutlet UIImageView *stnick; CGPoint position;//the container for the santa animations
	NSMutableArray *walkright;
	NSMutableArray *walkleft;
}

//@property (nonatomic, retain) IBOutlet UIButton *Object;

//@property (nonatomic, retain) IBOutlet UILabel *NAME;

@property (nonatomic, retain) UIImageView *stnick;
@property (nonatomic, retain) NSMutableArray *walkleft;
@property (nonatomic, retain) NSMutableArray *walkright;

@end

