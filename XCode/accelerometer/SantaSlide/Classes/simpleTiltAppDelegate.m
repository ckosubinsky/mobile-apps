//
//  simpleTiltAppDelegate.m
//  simpleTilt
//
//  Created by JoshuaCaputo on 2/28/09.
//  Copyright JoshuaCaputo 2009. All rights reserved.
//

#import "simpleTiltAppDelegate.h"
#import "simpleTiltViewController.h"

@implementation simpleTiltAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
