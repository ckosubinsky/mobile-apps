//
//  main.m
//  simpleTilt
//
//  Created by JoshuaCaputo on 2/28/09.
//  Copyright JoshuaCaputo 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
