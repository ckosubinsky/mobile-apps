//
//  ViewController.h
//  Wolfie
//
//  Created by Jimmy on 1/20/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WolfieViewController : UIViewController {
    
}

@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end
