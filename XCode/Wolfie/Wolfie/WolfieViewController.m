//
//  ViewController.m
//  Wolfie
//
//  Created by Jimmy on 1/20/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import "WolfieViewController.h"

#define PART1

@implementation WolfieViewController

-(void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidLayoutSubviews {
    srand([[NSDate date] timeIntervalSince1970]);
    [NSTimer scheduledTimerWithTimeInterval:1.6 target:self selector:@selector(move) userInfo:nil repeats:YES];
    [self move];
}

#ifdef PART1

-(void)move {
    NSInteger ran=rand()%4;
    
    NSInteger left;
    NSInteger top;
    NSInteger gotoX;
    NSInteger gotoY;
    
    if(ran==0)
    {
        left=-self.view.frame.size.width/2;
        top=-self.view.frame.size.width/2;
        gotoX=self.view.frame.size.width;
        gotoY=self.view.frame.size.height;
    }
    if(ran==1)
    {
        left=self.view.frame.size.width;
        top=-self.view.frame.size.width/2;
        gotoX=-self.view.frame.size.width/2;
        gotoY=self.view.frame.size.height;
    }
    if(ran==2)
    {
        left=self.view.frame.size.width;
        top=self.view.frame.size.height;
        gotoX=-self.view.frame.size.width/2;
        gotoY=-self.view.frame.size.width/2;
    }
    if(ran==3)
    {
        left=-self.view.frame.size.width/2;
        top=self.view.frame.size.height;
        gotoX=self.view.frame.size.width;
        gotoY=-self.view.frame.size.width/2;
    }
    
    _imageView.frame=CGRectMake(left,top,160,160);
    
    [UIView animateWithDuration:1.5 animations:^{
        _imageView.frame=CGRectMake(gotoX,gotoY,160,160);
    }];
}
#endif

#ifdef PART2

- (void) move
{
    NSInteger ran=rand()%4;
    NSInteger left;
    NSInteger top;
    if(ran==0)
    {
        left=-self.view.frame.size.width/2;
        top=self.view.frame.size.width/2;
    }
    if(ran==1)
    {
        left=self.view.frame.size.width;
        top=-self.view.frame.size.width/2;
    }
    if(ran==2)
    {
        left=self.view.frame.size.width;
        top=self.view.frame.size.height;
    }
    if(ran==3)
    {
        left=-self.view.frame.size.width/2;
        top=self.view.frame.size.height;
    }
    
    _imageView.frame=CGRectMake(left,top,160,160);
    
    [UIView animateWithDuration:.75 animations:^{
        _imageView.frame=CGRectMake(80,160,160,160);
    } completion:^(BOOL finished) {
        [self moveOff];
    }];
}

- (void) moveOff
{
    NSInteger ran=rand()%4;
    NSInteger left;
    NSInteger top;
    if(ran==0)
    {
        left=-self.view.frame.size.width/2;
        top=-self.view.frame.size.width/2;
    }
    if(ran==1)
    {
        left=self.view.frame.size.width;
        top=-self.view.frame.size.width/2;
    }
    if(ran==2)
    {
        left=self.view.frame.size.width;
        top=self.view.frame.size.height;
    }
    if(ran==3)
    {
        left=-self.view.frame.size.width/2;
        top=self.view.frame.size.height;
    }
    
    [UIView animateWithDuration:.75 animations:^{
        _imageView.frame=CGRectMake(left,top,160,160);
    }];
}

#endif

#ifdef PART3

-(void) move {
    NSInteger ran=rand()%4;
    NSInteger left;
    NSInteger top;
    if(ran==0){
        left=-160;
        top=-160;[self changeToSuperman];
    }
    if(ran==1){
        left=320;
        top=-160;
        [self changeToSuperman];
    }
    if(ran==2){
        left=320;
        top=480;
        [self changeLeftSuperman];
    }
    if(ran==3){
        left=-160;
        top=480;
        [self changeRightSuperman];
    }
    
    _imageView.frame=CGRectMake(left,top,100,100);
    [UIView animateWithDuration:1.5 animations:^{
        _imageView.frame=CGRectMake(80,160,100,100);
    } completion:^(BOOL finished) {
        [self moveOff];
    }];
}

-(void) moveOff{
    NSInteger ran=rand()%4;
    NSInteger left;
    NSInteger top;
    NSInteger w=100;
    NSInteger h=100;
    if(ran==0){
        left=10;
        top=120;
        [self changeLeftSuperman];
        w=0;
        h=0;
    }
    if(ran==1){
        left=280;
        top=120;
        [self changeRightSuperman];
        w=0;
        h=0;
    }
    if(ran==2){
        left=320;
        top=520;
        [self changeToSuperman];
    }
    if(ran==3){
        left=-160;
        top=520;
        [self changeToSuperman];
    }
    
    
    [UIView animateWithDuration:1.5 animations:^{
        _imageView.frame=CGRectMake(left,top,w,h);
    }];
}

-(void)changeLeftSuperman{
    _imageView.image=[UIImage imageNamed:@"LeftSuperman.png"];
}

-(void)changeRightSuperman{
    _imageView.image=[UIImage imageNamed:@"RightSuperman.png"];
}

-(void)changeToSuperman{
    _imageView.image=[UIImage imageNamed:@"image.png"];
}

#endif

@end
