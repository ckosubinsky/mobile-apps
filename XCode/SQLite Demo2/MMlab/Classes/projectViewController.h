//
//  projectViewController.h
//  MMlab
//
//  Created by teabones on 11/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface projectViewController : UIViewController {
	
	IBOutlet UITextView *projectDesciption;
	IBOutlet UIImageView *projectImage;

}

@property (nonatomic, retain) IBOutlet UITextView *projectDesciption;
@property (nonatomic, retain) IBOutlet UIImageView *projectImage;

@end
