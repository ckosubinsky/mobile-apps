//
//  MMlabAppDelegate.h
//  MMlab
//
//  Created by teabones on 11/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h> // Import the SQLite database framework

@interface MMlabAppDelegate : NSObject {
    
    UIWindow *window;
    UINavigationController *navigationController;
	
	// Database variables
	NSString *databaseName;
	NSString *databasePath;
	
	// Array to store the project objects
	NSMutableArray *projects;

}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic, retain) NSMutableArray *projects;

@end

