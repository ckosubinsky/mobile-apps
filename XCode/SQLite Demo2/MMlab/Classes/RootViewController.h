//
//  RootViewController.h
//  MMlab
//
//  Created by teabones on 11/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "projectViewController.h"

@interface RootViewController : UITableViewController {
	
	
	projectViewController *projectView;
}

@property(nonatomic, retain) projectViewController *projectView; 


@end
