//
//  infoViewController.m
//  Multilab
//
//  Created by Anthony Scarlatos on 10/26/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "infoViewController.h"
#import "projectViewController.h" //imports the previous view


@implementation infoViewController

@synthesize currentItem, projText;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.title = @"Info";//sets the title of the last view
	
	
	NSString *textPath = [[NSBundle mainBundle] pathForResource:currentItem ofType:@"txt"]; //this line fetches a text file whose name matches the value of currentItem  
	if (textPath) {  
		NSString *myText = [NSString stringWithContentsOfFile:textPath];// this line reads the content of the file into a string  
		if (myText) {  
			projText.text= myText;// this line sets the text of the UITextView object to the string  
		}  
	} 
}



-(IBAction)playMovie:(id)sender {
	
	UIButton *playButton = (UIButton *) sender; 
	
	NSString *filepath   =   [[NSBundle mainBundle] pathForResource:currentItem ofType:@"m4v"]; //puts the name of the project selected into a path and adds the file type
    NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
    MPMoviePlayerController *moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
	
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(moviePlaybackComplete:)
												 name:MPMoviePlayerPlaybackDidFinishNotification
											   object:moviePlayerController];
	
	[moviePlayerController.view setFrame:CGRectMake(playButton.frame.origin.x, 
													playButton.frame.origin.y, 
													playButton.frame.size.width, 
													playButton.frame.size.height)]; //sets the size of the controller to the button size (320 X 240)
    
	[self.view addSubview:moviePlayerController.view];
    //moviePlayerController.fullscreen = YES;
	
	//moviePlayerController.scalingMode = MPMovieScalingModeFill;
	
    [moviePlayerController play];
}

- (void)moviePlaybackComplete:(NSNotification *)notification {
	
    MPMoviePlayerController *moviePlayerController = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification
												  object:moviePlayerController];
	
    [moviePlayerController.view removeFromSuperview];
    [moviePlayerController release];
}

 
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    
	[super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
