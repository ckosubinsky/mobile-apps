//
//  RootViewController.h
//  Multilab
//
//  Created by Anthony Scarlatos on 10/25/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UITableViewController {
	
	NSMutableArray *projects;//a list of research projects
	NSMutableArray *dates;//a list of dates of research projects
	NSString *currentItem;//a string that holds the name of the object selected
}

@property (nonatomic, retain) NSMutableArray *projects;
@property (nonatomic, retain) NSMutableArray *dates;
@property (nonatomic, retain) NSString *currentItem;

@end
