//
//  MultilabAppDelegate.h
//  Multilab
//
//  Created by Anthony Scarlatos on 10/25/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class infoViewController; //a new class we created to show video

@interface MultilabAppDelegate : NSObject <UIApplicationDelegate> {
    
    UIWindow *window;
    UINavigationController *navigationController;
	infoViewController *viewController; //assigning a pointer to the infoViewController

}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic, retain) IBOutlet infoViewController *viewController;

@end

