//
//  RootViewController.m
//  Multilab
//
//  Created by Anthony Scarlatos on 10/25/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "RootViewController.h"
#import "projectViewController.h" //imports the next view


@implementation RootViewController

@synthesize projects, dates, currentItem;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.title = @"Research";//sets the title of the main screen
	
	projects=[[NSMutableArray alloc] initWithCapacity:0];//we manually add items to our project list
	[projects addObject:@"iSign"];
	[projects addObject:@"SmartStep"];
	[projects addObject:@"FingerSpell"];
	[projects addObject:@"WriteOn"];
	[projects addObject:@"Tower Of Hanoi"];
	[projects addObject:@"Teamagine"];
	[projects addObject:@"STEP"];
	[projects addObject:@"GuitarGenius"];
	[projects addObject:@"MusicMaker"];
	[projects addObject:@"DrumTutor"];
	
	dates=[[NSMutableArray alloc] initWithCapacity:0];//we manually add items to our dates list
	[dates addObject:@"2003"];
	[dates addObject:@"2004"];
	[dates addObject:@"2004"];
	[dates addObject:@"2005"];
	[dates addObject:@"2005"];
	[dates addObject:@"2008"];
	[dates addObject:@"2008"];
	[dates addObject:@"2009"];
	[dates addObject:@"2009"];
	[dates addObject:@"2009"];


    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

/*
 // Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
 */


#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;//default value
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [projects count];//number of items in the project list
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];//Subtitle style is used so date info displays
    }
    
	// Configure the cell.
	
	cell.textLabel.text=[projects objectAtIndex:[indexPath row]];//one line added to push data into the cell
	
	// set the subtitle text
	cell.detailTextLabel.text = [dates objectAtIndex:[indexPath row]];//pushes the dates into the subtitle for each cell
	
	// accessory type
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;//arrow is displayed as user hint that the row is linked to additional data


    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {//the method that is called when the user selects an item in the table view
    
	// Navigation logic may go here -- for example, create and push another view controller.
	projectViewController *controller = 
	[[projectViewController alloc] initWithNibName:@"projectViewController" bundle:nil];//creates an instance of the projectViewController
	
	NSString *currentItem = [projects objectAtIndex:indexPath.row];//sets the string to the object selected in the array (table view)
	
	controller.currentItem = currentItem;//passes currentItem to the next view
	
	
	[self.navigationController pushViewController:controller animated:YES];//pushes the new view onto the screen
	[controller release];
}



#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

