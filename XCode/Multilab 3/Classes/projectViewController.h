//
//  projectViewController.h
//  Multilab
//
//  Created by Anthony Scarlatos on 10/25/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface projectViewController : UIViewController {
	
	IBOutlet UILabel *message; //a label that displays the name of the project selected
	NSString *currentItem; //a string that contains the name of the project selected in the table view
	IBOutlet UIImageView *screenshot; //the image object that shows a picture of the project
	UIButton *infoButton; //a button that links to a 3rd view which displays a project video
	
}

@property (nonatomic, retain) UILabel *message;
@property (nonatomic, retain) NSString *currentItem;
@property (nonatomic, retain) IBOutlet UIImageView *screenshot;
@property (nonatomic, retain) IBOutlet UIButton *infoButton;

- (IBAction) showInfo; //the method to launch the 3rd view 


@end
