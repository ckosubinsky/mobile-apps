//
//  infoViewController.h
//  Multilab
//
//  Created by Anthony Scarlatos on 10/26/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h> //imports the MediaPlayer framework so we can access its methods


@interface infoViewController : UIViewController {
	
	NSString *currentItem; //declares the value that has been passed to the video view
	IBOutlet UITextView *projText;//displays the text about the project
	
}

@property (nonatomic, retain) NSString *currentItem;
@property (nonatomic, retain) UITextView *projText;


-(IBAction)playMovie:(id)sender; //a method to play the video


@end
