//
//  projectViewController.m
//  Multilab
//
//  Created by Anthony Scarlatos on 10/25/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "projectViewController.h"
#import "infoViewController.h" //imports the next view


@implementation projectViewController

@synthesize message, currentItem, screenshot, infoButton;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
		
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.title = @"Details"; //sets the title of the subview
	
	message.text = currentItem; //displays the name of the project that the user selected in the table view in a label
	
	NSString * imageName = [NSString stringWithFormat:@"%@.jpg", currentItem]; //assigns the name of the selection to a string and adds the file extension
	[self.screenshot setImage:[UIImage imageNamed:imageName]]; //sets the content of the image object to a file with the project's name

	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction) showInfo { //a method for the button that launches the video view

infoViewController *myController = 
	[[infoViewController alloc] initWithNibName:@"infoViewController" bundle:nil];
	
	myController.currentItem = currentItem; //passes currentItem to the video view


[self.navigationController pushViewController:myController animated:YES];
	
[myController release];
	
}

- (void)dealloc {
    [super dealloc];

}


@end
