//
//  MPHconverterAppDelegate.h
//  MPHconverter
//
//  Created by teabones on 10/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MPHconverterViewController;

@interface MPHconverterAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    MPHconverterViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet MPHconverterViewController *viewController;

@end

