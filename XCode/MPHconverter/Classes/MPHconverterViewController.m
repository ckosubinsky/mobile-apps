//
//  MPHconverterViewController.m
//  MPHconverter
//
//  Created by teabones on 10/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MPHconverterViewController.h"

@implementation MPHconverterViewController

@synthesize mph;
@synthesize kph;
@synthesize warning;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction) toMiles{
	
	//dismiss the keyboard
	[input resignFirstResponder];
	
	//Calculate kilos to miles and put that value into the output field
	float k=([input.text floatValue] /1.61);
	[output setText:[NSString stringWithFormat:@"%1.0f Miles Per Hour",k]];
	
	//Put the input value into the label on the kph button
	NSString *confirmKPH=[[NSString alloc] initWithFormat:@"%@", input.text];
	[kph setText:confirmKPH];
	
	//Clear the input and mph fields
	[mph setText:nil];
	[input setText:nil];
	
}

- (IBAction) toKilos{
	
	//dismiss the keyboard
	[input resignFirstResponder];
	
	//Calculate miles to kilos and put that value into the output field
	float m=([input.text floatValue] *1.61);
	[output setText:[NSString stringWithFormat:@"%1.0f Kilometers Per Hour",m]];
	
	//Put the input value into the label on the mph button
	NSString *confirmMPH=[[NSString alloc] initWithFormat:@"%@", input.text];
	[mph setText:confirmMPH];
	
	//Clear the input and kph fields
	[kph setText:nil];
	[input setText:nil];
}


@end
