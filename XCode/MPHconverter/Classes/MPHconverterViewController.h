//
//  MPHconverterViewController.h
//  MPHconverter
//
//  Created by teabones on 10/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPHconverterViewController : UIViewController {
	
	IBOutlet UIButton *toMiles;
	IBOutlet UIButton *toKilos;
	IBOutlet UITextField *input;
	IBOutlet UITextField *output;
	
	IBOutlet UILabel *mph;
	IBOutlet UILabel *kph;
	IBOutlet UILabel *warning;
}

- (IBAction) toMiles;
- (IBAction) toKilos;

@property (nonatomic, retain) UILabel *mph;
@property (nonatomic, retain) UILabel *kph;
@property (nonatomic, retain) UILabel *warning;

@end

