//
//  ViewController.m
//  PanicButton
//
//  Created by Jimmy on 1/15/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)showColor1:(id)sender {
    self.view.backgroundColor = [UIColor grayColor];
}

-(IBAction)showColor2:(id)sender {
    self.view.backgroundColor = [UIColor redColor];
}

@end
