//
//  AppDelegate.h
//  PanicButton
//
//  Created by Jimmy on 1/15/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+(NSManagedObjectContext*)context;

@end
