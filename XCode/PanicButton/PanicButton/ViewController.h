//
//  ViewController.h
//  PanicButton
//
//  Created by Jimmy on 1/15/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    
}

-(IBAction)showColor1:(id)sender;
-(IBAction)showColor2:(id)sender;

@end
