//
//  MasterViewController.m
//  Showcase
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import "RootViewController.h"
#import "ProjectViewController.h"

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Research";//sets the title of the main screen
    self.projects = @[@"iSign", @"SmartStep", @"FingerSpell", @"WriteOn", @"Tower Of Hanoi", @"Teamagine", @"STEP", @"GuitarGenius", @"MusicMaker", @"DrumTutor"];
    self.dates = @[@"2003", @"2004", @"2004", @"2005", @"2005", @"2008", @"2008", @"2009", @"2009", @"2009"];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _projects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    cell.textLabel.text = _projects[indexPath.row];
    cell.detailTextLabel.text = _dates[indexPath.row];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        [[segue destinationViewController] setCurrentItem:_projects[indexPath.row]];
    }
}

@end
