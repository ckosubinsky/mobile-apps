//
//  InfoViewController.m
//  Showcase
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import "InfoViewController.h"


@implementation InfoViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Info";//sets the title of the last view
    
    NSString *textPath = [[NSBundle mainBundle] pathForResource:_currentItem ofType:@"txt"]; //this line fetches a text file whose name matches the value of currentItem
    
    if (textPath) {

        // this line reads the content of the file into a string
        NSString *myText = [NSString stringWithContentsOfFile:textPath encoding:NSUTF8StringEncoding error:nil];
        
        if (myText) {
            _projText.text= myText;// this line sets the text of the UITextView object to the string
        }
    }
}

-(IBAction)playMovie:(id)sender {
    UIButton *playButton = (UIButton *) sender;
    NSString *filepath = [[NSBundle mainBundle] pathForResource:_currentItem ofType:@"m4v"]; //puts the name of the project selected into a path and adds the file type
    NSURL *fileURL = [NSURL fileURLWithPath:filepath];
    self.moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackComplete:) name:MPMoviePlayerPlaybackDidFinishNotification   object:_moviePlayerController];
    
    [_moviePlayerController.view setFrame:CGRectMake(playButton.frame.origin.x, playButton.frame.origin.y, playButton.frame.size.width, playButton.frame.size.height)]; //sets the size of the controller to the button size (320 X 240)
    
    [self.view addSubview:_moviePlayerController.view];
    [_moviePlayerController play];

}

- (void)moviePlaybackComplete:(NSNotification *)notification {
    
    MPMoviePlayerController *moviePlayerController = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification  object:moviePlayerController];
    
    [moviePlayerController.view removeFromSuperview];
}

@end
