//
//  DetailViewController.m
//  Showcase
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import "ProjectViewController.h"
#import "InfoViewController.h"

@implementation ProjectViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Details";
    
    self.message.text = self.currentItem;
    
    NSString * imageName = [NSString stringWithFormat:@"%@.jpg", _currentItem]; //assigns the name of the selection to a string and adds the file extension
    
    [self.screenshot setImage:[UIImage imageNamed:imageName]]; //sets the content of the image object to a file with the project's name
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showInfo"]) {
        InfoViewController *infoViewController = segue.destinationViewController;
        [infoViewController setCurrentItem:_currentItem];
    }
}

@end
