//
//  DetailViewController.h
//  Showcase
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectViewController : UIViewController

//a label that displays the name of the project selected
@property (nonatomic, strong) IBOutlet UILabel *message;
//a string that contains the name of the project selected in the table view
@property (nonatomic, strong) NSString *currentItem;
//the image object that shows a picture of the project
@property (nonatomic, strong) IBOutlet UIImageView *screenshot;
//a button that links to a 3rd view which displays a project video
@property (nonatomic, strong) IBOutlet UIButton *infoButton;

@end
