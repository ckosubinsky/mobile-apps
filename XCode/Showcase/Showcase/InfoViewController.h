//
//  InfoViewController.h
//  Showcase
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface InfoViewController : UIViewController

//declares the value that has been passed to the video view
@property (nonatomic, retain) NSString *currentItem;
//displays the text about the project
@property (nonatomic, retain) IBOutlet UITextView *projText;
//this is needed otherwise it will be deleted by ARC
@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;

-(IBAction)playMovie:(id)sender; //a method to play the video

@end
