//
//  MasterViewController.h
//  Showcase
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UITableViewController {
    
}

//a list of research projects
@property (nonatomic, strong) NSArray *projects;
//a list of dates of research projects
@property (nonatomic, strong) NSArray *dates;
//a string that holds the name of the object the user selected
@property (nonatomic, strong) NSString *currentItem;

@end
