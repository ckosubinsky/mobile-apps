//
//  MainViewController.h
//  graveGame
//
//  Created by teabones on 10/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> {


	IBOutlet UIImageView *bones;//the container for the skeleton animations
	NSMutableArray *walkAnim;
	NSMutableArray *jumpAnim;
	//NSMutableArray *duckAnim;
	//NSMutableArray *dieAnim;//all the different things the character does
	
	//IBOutlet UIImageView *bat;//the window for the bat animation
	//NSMutableArray *batAnim;
	
	IBOutlet UIImageView *tombs;//will switch between a couple of images
	IBOutlet UIImageView *bkgd;//will switch between a couple of background elements
	
	
	NSTimer *bkgdTimer;//moves the background image across the view
	NSTimer *tombsTimer;//moves the tombstones across the view
	//NSTimer *batTimer;//moves the bat across the screen
	
	NSTimer *tombsCollisionTimer;//checks for collision with tombs
	//NSTimer *batCollisionTimer;//checks for collision with bat
	
	AVAudioPlayer *themePlayer;//will play various theme songs
	//AVAudioPlayer *gameOverPlayer;//plays a sound when a collision is detected
}

// property declarations
@property (nonatomic, retain) UIImageView *bones;
@property (nonatomic, retain) NSMutableArray *walkAnim;
@property (nonatomic, retain) NSMutableArray *jumpAnim;
//@property (nonatomic, retain) NSMutableArray *duckAnim;
//@property (nonatomic, retain) NSMutableArray *dieAnim;

//@property (nonatomic, retain) UIImageView *bat;
//@property (nonatomic, retain) NSMutableArray *batAnim;

@property (nonatomic, retain) UIImageView *tombs;
@property (nonatomic, retain) UIImageView *bkgd;

@property (nonatomic, retain) NSTimer *bkgdTimer;
@property (nonatomic, retain) NSTimer *tombsCollisionTimer;
//@property (nonatomic, retain) NSTimer *batCollisionTimer;

// custom methods

- (IBAction) jump;

- (void) startSound;
- (void) setupArrays;
- (void) startTimers;

- (void) moveBkgd;
- (void) moveTombs;
//- (void) moveBat;

- (void) checkTombsCollision;
//- (void) checkBatCollision;

- (void) moveUp;
- (void) moveDown;
- (void) showWalk;

//- (void) gameOver;



//- (IBAction)showInfo:(id)sender;


@end
