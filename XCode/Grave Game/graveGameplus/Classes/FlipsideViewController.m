//
//  FlipsideViewController.m
//  graveGame
//
//  Created by teabones on 10/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"


@implementation FlipsideViewController

@synthesize delegate = _delegate;
@synthesize gameOverScore;
@synthesize gameOverScoreLabel;


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor viewFlipsideBackgroundColor];
    
    [gameOverScoreLabel setText: [NSString stringWithFormat:@"Score: %d", gameOverScore]];
}


- (IBAction)done:(id)sender {
	[self.delegate flipsideViewControllerDidFinish:self];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


// overrides the orientations such that the view is always fixed in horizonal position
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	
	if ((interfaceOrientation==UIInterfaceOrientationPortrait) || (interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown))
		// NO locks out the portrait mode of the iPhone. The initial orientation also has to be set in the info plist.
	{
		return NO;
	}
	if ((interfaceOrientation==UIInterfaceOrientationLandscapeLeft) || (interfaceOrientation==UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
}


- (void)dealloc {
    [super dealloc];
}


@end
