//
//  MainViewController.m
//  graveGame
//
//  Created by teabones on 10/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController

@synthesize bones;
@synthesize walkAnim;
@synthesize jumpAnim;
//@synthesize duckAnim;
//@synthesize dieAnim;

//@synthesize bat;
//@synthesize batAnim;

@synthesize tombs;
@synthesize bkgd;

@synthesize bkgdTimer;
@synthesize tombsCollisionTimer;
//@synthesize batCollisionTimer;

@synthesize score;
//@synthesize scoreLabel;


//The viewDidLoad method was uncommented to use it as a trigger for setting up the animations and loading the sound

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    /*
     UIAlertView *intro = [[UIAlertView alloc] initWithTitle:@"Welcome to the Graveyard Game!"
     message:@"Tap the skeleton to make him jump over the tombstone."
     delegate:nil cancelButtonTitle:@"I'm ready - Let's play!"
     otherButtonTitles:nil];
     [intro show];
     */
	
	[super viewDidLoad];
	
	[self startSound];//loads the theme music and other game sounds
	
	[self setupArrays];//loads the images into the arrays
	
	[self startTimers];//gets the animation and collision detection timers going
    
    score = 0;
	
}

- (void) startSound{
	
	NSString *themeSongPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/gravehop.aif"];
	NSError *err;
	
	themePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:themeSongPath] error:&err];
	themePlayer.delegate = self;
	themePlayer.currentTime = 0;
	
	[themePlayer play];
    
    
}

- (void) startTimers{
	
	bkgdTimer = [[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(moveBkgd) userInfo:nil repeats:YES] retain];
	[self moveBkgd];
	
	[bkgd startAnimating];
	
	
	tombsTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(moveTombs) userInfo:nil repeats:YES] retain];
	tombsCollisionTimer = [[NSTimer scheduledTimerWithTimeInterval:0.03 target:self selector:@selector(checkTombsCollision) userInfo:nil repeats:YES] retain];
	
	[tombs startAnimating];
	
}

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
	
	themePlayer.currentTime = 0;
	
	[themePlayer play];
    
	bkgd.frame=CGRectMake(460,0,113,163);
	
	tombs.frame=CGRectMake(600,tombs.frame.origin.y,50,78);
    
	
}

//This code was commented out - we load the flipview as a result of a collision, not pressing an info button

/*
 - (IBAction)showInfo:(id)sender {
 
 FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
 controller.delegate = self;
 
 controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
 [self presentModalViewController:controller animated:YES];
 
 [controller release];
 }
 
 */


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}



// overrides the orientations such that the view is always fixed in horizonal position
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	
	if ((interfaceOrientation==UIInterfaceOrientationPortrait) || (interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown))
		// NO locks out the portrait mode of the iPhone. The initial orientation also has to be set in the info plist.
	{
		return NO;
	}
	if ((interfaceOrientation==UIInterfaceOrientationLandscapeLeft) || (interfaceOrientation==UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
}



- (void)dealloc {
    [super dealloc];
}


- (void) setupArrays {
    
	walkAnim = [[NSMutableArray alloc] init];
	jumpAnim = [[NSMutableArray alloc] init];
	//duckAnim = [[NSMutableArray alloc] init];
	//dieAnim = [[NSMutableArray alloc] init];
    
	// a loop that lists the walk cycle images
	for (int i = 1; i < 16; i++){
		
        NSString *pic = [NSString stringWithFormat:@"w%d.png", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [walkAnim addObject:img];
        
	}
    
	// a loop that lists the jump images
	for (int i = 1; i < 22; i++){
		
        NSString *pic = [NSString stringWithFormat:@"j%d.png", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [jumpAnim addObject:img];
		
	}
    /*
     // a loop that lists the falling images
     for (int i = 1; i < 9; i++){
     
     NSString *pic = [NSString stringWithFormat:@"f%d.png", i];
     UIImage *img = [UIImage imageNamed:pic];
     if (img) [dieAnim addObject:img];
     
     }
     */
	//set the default animation for the view - the walk cycle
	[bones setAnimationImages:walkAnim];
	[bones setAnimationDuration:.5];
    
	[bones startAnimating];
	
}

// moves the background steadily across the screen
- (void) moveBkgd {
	
	bkgd.frame=CGRectMake(460,0,113,163);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:10.0];
	[UIView setAnimationDelegate:self];
	bkgd.frame=CGRectMake(-100,0,113,163);
	[UIView commitAnimations];
}

- (void) moveTombs {
    
	tombs.frame=CGRectMake(tombs.frame.origin.x,tombs.frame.origin.y,50,78);
	[UIView beginAnimations:nil context:nil];
	
	[UIView setAnimationDelegate:self];
	tombs.frame=CGRectMake(tombs.frame.origin.x - 2,tombs.frame.origin.y,50,78);
	[UIView commitAnimations];
	
	if(tombs.frame.origin.x < -100){
		
		tombs.frame=CGRectMake(600,222,50,78);
	}
}

// checks for collision and takes necessary action when a collision is detected
- (void) checkTombsCollision {
	
	if(CGRectIntersectsRect(bones.frame, tombs.frame)) {
        

		
		// stop playing the theme song
		[themePlayer stop];
        /*
         [bones setAnimationImages:dieAnim];
         [bones setAnimationDuration:1];
         [bones startAnimating];
         
         [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(gameOver)
         userInfo:nil repeats:NO];
         
         */
		// flip the view
		FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
		controller.delegate = self;
        controller.gameOverScore = score;
		
		controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
		[self presentModalViewController:controller animated:YES];
		
		[controller release];
	}
}


- (IBAction) jump{
    
	[bones setAnimationImages:jumpAnim];
	[bones setAnimationDuration:2];
	[bones startAnimating];
    
	[NSTimer scheduledTimerWithTimeInterval:1.9 target:self selector:@selector(showWalk)
                                   userInfo:nil repeats:NO];
	
	// move the skeleton up
	bones.frame = CGRectMake(bones.frame.origin.x, bones.frame.origin.y, 121, 180);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(moveDown)];
	bones.frame = CGRectMake(bones.frame.origin.x, bones.frame.origin.y - 85, 121, 180);
	[UIView commitAnimations];
    
}

- (void) showWalk{
    
	[bones setAnimationImages:walkAnim];
	[bones setAnimationDuration:.5];
	[bones startAnimating];
}

- (void) moveDown{
	
	bones.frame = CGRectMake(bones.frame.origin.x, bones.frame.origin.y, 121, 180);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:.5];
	[UIView setAnimationDelegate:self];
	bones.frame = CGRectMake(bones.frame.origin.x, bones.frame.origin.y + 85, 121, 180);
	[UIView commitAnimations];
    
    if(!CGRectIntersectsRect(bones.frame, tombs.frame)) {
        
        score += 1;
        //[scoreLabel setText: [NSString stringWithFormat:@"Score: %d", score]];
        
    }
	
}
/*
 - (void) gameOver{
 
 // flip the view
 FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
 controller.delegate = self;
 
 controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
 [self presentModalViewController:controller animated:YES];
 
 [controller release];
 
 }
 */


@end