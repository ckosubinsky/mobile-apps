//
//  FlipsideViewController.h
//  graveGame
//
//  Created by teabones on 10/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlipsideViewController;

@protocol FlipsideViewControllerDelegate

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;

@end


@interface FlipsideViewController : UIViewController {
	//id <FlipsideViewControllerDelegate> delegate;
    
    IBOutlet UILabel *gameOverScoreLabel;
    int gameOverScore;
}

@property (nonatomic, assign) id <FlipsideViewControllerDelegate> delegate;

@property (nonatomic, assign) UILabel *gameOverScoreLabel;
@property (nonatomic) int gameOverScore;

- (IBAction)done:(id)sender;

@end
