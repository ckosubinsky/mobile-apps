//
//  animDemoAppDelegate.h
//  animDemo
//
//  Created by Simon Allardice on 8/29/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class animDemoViewController;

@interface animDemoAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    animDemoViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet animDemoViewController *viewController;

@end

