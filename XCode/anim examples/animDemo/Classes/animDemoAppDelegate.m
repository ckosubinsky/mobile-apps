//
//  animDemoAppDelegate.m
//  animDemo
//
//  Created by Simon Allardice on 8/29/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import "animDemoAppDelegate.h"
#import "animDemoViewController.h"

@implementation animDemoAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
