//
//  animDemoViewController.h
//  animDemo
//
//  Created by Simon Allardice on 8/29/09.
//  Copyright __MyCompanyName__ 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface animDemoViewController : UIViewController {
	IBOutlet UIView *mover;
	IBOutlet UIView *grower;
	IBOutlet UIView *flipper;
	IBOutlet UIImageView *iv;
}
@property (nonatomic, retain) UIView *mover;
@property (nonatomic, retain) UIView *grower;
@property (nonatomic, retain) UIView *flipper;
@property (nonatomic, retain) UIImageView *iv;

-(IBAction) move;
-(IBAction) grow;
-(IBAction) flip;

@end

