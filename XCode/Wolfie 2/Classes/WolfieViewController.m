//
//  WolfieViewController.m
//  Wolfie
//
//  Created by teabones on 9/26/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WolfieViewController.h"

@implementation WolfieViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	//self.view.backgroundColor=[UIColor whiteColor];
	srand([[NSDate date] timeIntervalSince1970]);
	[NSTimer scheduledTimerWithTimeInterval:1.6 target:self selector:@selector(move)
								   userInfo:nil repeats:YES];
	[self move];
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (void) move
{
	NSInteger ran=rand()%4;
	NSInteger left;
	NSInteger top;
	if(ran==0)
	{
		left=-160;
		top=-160;
	}
	if(ran==1)
	{
		left=320;
		top=-160;
	}
	if(ran==2)
	{
		left=320;
		top=480;
	}
	if(ran==3)
	{
		left=-160;
		top=480;
	}
	imageView.frame=CGRectMake(left,top,160,160);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:.75];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(moveOff)];
	imageView.frame=CGRectMake(80,160,160,160);
	[UIView commitAnimations];
}

- (void) moveOff
{
	NSInteger ran=rand()%4;
	NSInteger left;
	NSInteger top;
	if(ran==0)
	{
		left=-160;
		top=-160;
	}
	if(ran==1)
	{
		left=320;
		top=-160;
	}
	if(ran==2)
	{
		left=320;
		top=480;
	}
	if(ran==3)
	{
		left=-160;
		top=480;
	}
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:.75];
	imageView.frame=CGRectMake(left,top,160,160);
	[UIView commitAnimations];
}

@end
