//
//  WolfieAppDelegate.h
//  Wolfie
//
//  Created by teabones on 9/26/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WolfieViewController;

@interface WolfieAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    WolfieViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet WolfieViewController *viewController;

@end

