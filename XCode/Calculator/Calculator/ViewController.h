//
//  ViewController.h
//  Calculator
//
//  Created by Jimmy on 1/20/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    
}

@property (nonatomic, strong) IBOutlet UIButton *toMiles, *toKilos;
@property (nonatomic, strong) IBOutlet UILabel *mph, *kph;
@property (nonatomic, strong) IBOutlet UITextField *input, *output;

-(IBAction)convertToMiles;
-(IBAction)convertToKilos;

@end
