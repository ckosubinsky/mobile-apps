//
//  ViewController.m
//  Calculator
//
//  Created by Jimmy on 1/20/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

-(void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

#pragma mark - IBAction

-(IBAction)convertToMiles {
    //dismiss the keyboard
    [_input resignFirstResponder];
    
    //Calculate kilos to miles and put that value into the output field
    float k=([_input.text floatValue] /1.61);
    [_output setText:[NSString stringWithFormat:@"%1.0f Miles Per Hour",k]];
    
    //Put the input value into the label on the kph button
    NSString *confirmKPH=[[NSString alloc] initWithFormat:@"%@", _input.text];
    [_kph setText:confirmKPH];
    
    //Clear the input and mph fields
    [_mph setText:nil];
    [_input setText:nil];
}

-(IBAction)convertToKilos {
    //dismiss the keyboard
    [_input resignFirstResponder];
    
    //Calculate miles to kilos and put that value into the output field
    float m=([_input.text floatValue] *1.61);
    [_output setText:[NSString stringWithFormat:@"%1.0f Kilometers Per Hour",m]];
    
    //Put the input value into the label on the mph button
    NSString *confirmMPH=[[NSString alloc] initWithFormat:@"%@", _input.text];
    [_mph setText:confirmMPH];
    
    //Clear the input and kph fields
    [_kph setText:nil];
    [_input setText:nil];

}

@end
