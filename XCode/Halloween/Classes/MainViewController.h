//
//  MainViewController.h
//  halloween
//
//  Created by teabones on 10/6/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> {

	IBOutlet UIImageView *image1;
	IBOutlet UIImageView *image2;

}

@property (nonatomic, retain) UIImageView *image1;
@property (nonatomic, retain) UIImageView *image2;

- (IBAction)showInfo:(id)sender;

- (void) checkCollision;

@end
