//
//  SecondViewController.m
//  Tabbed
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    [_webView loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString: @"http://www.mm.cs.sunysb.edu"]]];
}

@end
