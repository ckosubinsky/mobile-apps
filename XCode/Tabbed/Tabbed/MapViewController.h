//
//  MapViewController.h
//  Tabbed
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController {
    
}

@property (nonatomic, strong) IBOutlet MKMapView *mapView;

@end
