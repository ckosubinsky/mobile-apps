//
//  SecondViewController.h
//  Tabbed
//
//  Created by Jimmy on 1/23/14.
//  Copyright (c) 2014 JimmyBouker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController {
    
}

@property (nonatomic, strong) IBOutlet UIWebView *webView;

@end
