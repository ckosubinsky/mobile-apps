//
//  MainViewController.h
//  PasswordStrength
//
//  Created by RamanNanda on 10/18/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> {
	IBOutlet UIButton *generateRandomPassword;
	IBOutlet UIButton *checkPassword;
	
	IBOutlet UIImageView *starImage;
	IBOutlet UITextField *input;
	
	IBOutlet UILabel *passwordInfo;
}

- (IBAction)showInfo:(id)sender;
- (IBAction)generateRandomPassword;
- (IBAction)checkPassword;

- (IBAction)endText;

@property (nonatomic, retain) UILabel *passwordInfo;
@property (nonatomic, retain) UIImageView *starImage;

@end
