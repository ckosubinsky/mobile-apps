//
//  MainViewController.m
//  PasswordStrength
//
//  Created by RamanNanda on 10/18/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController
@synthesize passwordInfo;
@synthesize starImage;

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
}
*/


- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
}


- (IBAction)showInfo:(id)sender {    
	
	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (IBAction)endText {
	[self checkPassword];			 
}

- (void)dealloc {
    [super dealloc];
}

- (IBAction)generateRandomPassword {
	[input resignFirstResponder];
	
	CFUUIDRef theUUID = CFUUIDCreate(NULL);
	CFStringRef string = CFUUIDCreateString(NULL, theUUID);
	CFRelease(theUUID);
	[input setText: string];
	[self checkPassword];
	
}

- (IBAction)checkPassword {
	starImage.image = NULL;
	NSString *text = input.text;
	if(text.length == 0)
	{
	}
	else if(text.length <= 5)
		starImage.image = [UIImage imageNamed:@"1star.png"];
	else
	{
		if(([text rangeOfString:@"1"].location != NSNotFound ||
		   [text rangeOfString:@"2"].location != NSNotFound ||
		   [text rangeOfString:@"3"].location != NSNotFound ||
		   [text rangeOfString:@"4"].location != NSNotFound ||
		   [text rangeOfString:@"5"].location != NSNotFound ||
		   [text rangeOfString:@"6"].location != NSNotFound ||
		   [text rangeOfString:@"7"].location != NSNotFound ||
		   [text rangeOfString:@"8"].location != NSNotFound ||
		   [text rangeOfString:@"9"].location != NSNotFound ||
		   [text rangeOfString:@"0"].location != NSNotFound) &&
		   ([text rangeOfString:@"a"].location != NSNotFound ||
		   [text rangeOfString:@"b"].location != NSNotFound ||
		   [text rangeOfString:@"c"].location != NSNotFound ||
		   [text rangeOfString:@"d"].location != NSNotFound ||
		   [text rangeOfString:@"e"].location != NSNotFound ||
		   [text rangeOfString:@"f"].location != NSNotFound ||
		   [text rangeOfString:@"g"].location != NSNotFound ||
		   [text rangeOfString:@"h"].location != NSNotFound ||
		   [text rangeOfString:@"i"].location != NSNotFound ||
		   [text rangeOfString:@"j"].location != NSNotFound ||
		   [text rangeOfString:@"k"].location != NSNotFound ||
		   [text rangeOfString:@"l"].location != NSNotFound ||
		   [text rangeOfString:@"m"].location != NSNotFound ||
		   [text rangeOfString:@"n"].location != NSNotFound ||
		   [text rangeOfString:@"o"].location != NSNotFound ||
		   [text rangeOfString:@"p"].location != NSNotFound ||
		   [text rangeOfString:@"q"].location != NSNotFound ||
		   [text rangeOfString:@"s"].location != NSNotFound ||
		   [text rangeOfString:@"t"].location != NSNotFound ||
		   [text rangeOfString:@"u"].location != NSNotFound ||
		   [text rangeOfString:@"v"].location != NSNotFound ||
		   [text rangeOfString:@"w"].location != NSNotFound ||
		   [text rangeOfString:@"x"].location != NSNotFound ||
		   [text rangeOfString:@"y"].location != NSNotFound ||
		   [text rangeOfString:@"z"].location != NSNotFound) &&
		   text.length < 10
		   )
		{
			starImage.image = [UIImage imageNamed:@"3star.png"];
		}
		else {  
			starImage.image = [UIImage imageNamed:@"5star.png"];
		}
	}		
}

@end
