//
//  UserInputAppDelegate.h
//  UserInput
//
//  Created by MarkGraffia on 10/11/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserInputViewController;

@interface UserInputAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    UserInputViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UserInputViewController *viewController;

@end

