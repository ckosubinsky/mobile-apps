//
//  UserInputViewController.h
//  UserInput
//
//  Created by MarkGraffia on 10/11/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInputViewController : UIViewController {

IBOutlet UISegmentedControl *fromCurrency;
IBOutlet UISegmentedControl *toCurrency;	
IBOutlet UITextField *input;
IBOutlet UITextField *output;
	
}

- (IBAction) convert;

//@property (nonatomic, retain) UILabel *mph;
//@property (nonatomic, retain) UILabel *kph;

@end

