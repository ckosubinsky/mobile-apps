//
//  UserInputViewController.m
//  UserInput
//
//  Created by MarkGraffia on 10/11/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "UserInputViewController.h"

@implementation UserInputViewController


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction) convert {
	float in = [input.text floatValue];
	float USD = 0;
	NSString *inCurrency = nil;
	
	//Convert input to USD
	switch (fromCurrency.selectedSegmentIndex) {
		case 0:
			USD = in;
			inCurrency = "US Dollars";
			break;
		case 1:
			USD = in * 1.5934;
			inCurrency = "GB Pounds";
			break;
		case 2:
			USD = in * 1.3996;
			inCurrency = "Euros";
			break;
		case 3:
			USD = in * 0.9857;
			inCurrency = "CAN Dollars";
			break;
		case 4:
			USD = in * 0.0123;
			inCurrency = "Yen";
			break;
		default:
			break;
	}
	
	float out = 0;
	NSString *outCurrency = nil;
	
	//Convert USD to output
	switch (toCurrency.selectedSegmentIndex) {
		case 0:
			out = USD;
			outCurrency = "US Dollars";
			break;
		case 1:
			out = USD / 1.5934;
			outCurrency = "GB Pounds";
			break;
		case 2:
			out = USD / 1.3996;
			outCurrency = "Euros";
			break;
		case 3:
			out = USD / 0.9857;
			outCurrency = "CAN Dollars";
			break;
		case 4:
			out = USD / 0.0123;
			outCurrency = "Yen";
			break;
		default:
			break;
	}
	
	//Display output
	[output setText:[NSString stringWithFormat: @"%1.2f %s = %1.2f %s", 
					 in, inCurrency, out, outCurrency]];
}

@end
