//
//  ConverterAppDelegate.h
//  Converter
//
//  Created by chenghao lin on 10/17/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConverterAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@end

