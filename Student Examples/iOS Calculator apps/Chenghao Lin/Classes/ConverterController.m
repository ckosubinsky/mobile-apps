//
//  ConverterController.m
//  Converter
//
//  Created by chenghao lin on 10/17/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "ConverterController.h"
#import "SnowFallViewController.h"
#import "sunshine.h"
#import "MainViewController.h"
@implementation ConverterController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(IBAction)done{
	[textField resignFirstResponder];
	
	float temp=0.0;
	temp=[textField.text floatValue];
	float degree;	
	if (segmentedControl.selectedSegmentIndex==0) {
		degree=(temp-32.0)*5.0/9.0;
		
		if (temp<=55) {
			SnowFallViewController *snow=[[SnowFallViewController alloc]init];
			snow.title=[[NSString alloc]initWithFormat:@"%1.2f C˚",degree];
			[self.navigationController pushViewController:snow animated:YES];
			[snow release];
			
		}else if (temp>80) {
			MainViewController *fire=[[MainViewController alloc]init];
			fire.title=[[NSString alloc]initWithFormat:@"%1.2f C˚",degree];
			[self.navigationController pushViewController:fire animated:YES];
			[fire release];
		}
		else
		{
			sunshine *sunShine=[[sunshine alloc]init];
			sunShine.title=[[NSString alloc]initWithFormat:@"%1.2f C˚",degree];
			[self.navigationController pushViewController:sunShine animated:YES];
			[sunShine release];
		}
	
	}
	else
	{
		degree=temp*9.0/5.0+32.0;
		if (temp<=12.78) {
			SnowFallViewController *snow=[[SnowFallViewController alloc]init];
			snow.title=[[NSString alloc]initWithFormat:@"%1.2f F˚",degree];
			[self.navigationController pushViewController:snow animated:YES];
			[snow release];
		}else if (temp>26.67) {
			MainViewController *fire=[[MainViewController alloc]init];
			fire.title=[[NSString alloc]initWithFormat:@"%1.2f F˚",degree];
			[self.navigationController pushViewController:fire animated:YES];
			[fire release];
		}
		else {
			sunshine *sunShine=[[sunshine alloc]init];
			sunShine.title=[[NSString alloc]initWithFormat:@"%1.2f F˚",degree];
			[self.navigationController pushViewController:sunShine animated:YES];
			[sunShine release];
		}

	}
	textField.text=nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
