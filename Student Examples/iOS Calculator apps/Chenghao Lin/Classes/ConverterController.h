//
//  ConverterController.h
//  Converter
//
//  Created by chenghao lin on 10/17/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ConverterController : UIViewController {
	IBOutlet UISegmentedControl *segmentedControl;
	IBOutlet UIButton *doneButton;
	IBOutlet UITextField *textField;
}
-(IBAction)done;
@end
