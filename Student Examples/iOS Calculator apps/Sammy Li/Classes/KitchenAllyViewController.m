//
//  KitchenAllyViewController.m
//  KitchenAlly
//
//  Created by SammyLi on 10/15/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "KitchenAllyViewController.h"

@implementation KitchenAllyViewController
@synthesize help;

- (IBAction) tbl_tsp{
	
	float k=([input.text floatValue] *3);
	[output setText:[NSString stringWithFormat:@"%1.0f tsp",k]];
	
	[help setText:(@"tbl =")];
}


- (IBAction) tsp_tbl{
	float k=([input.text floatValue] /3);
	[output setText:[NSString stringWithFormat:@"%1.0f tbl",k]];
	[help setText:(@"tsp =")];
}
- (IBAction) tbl_mL{
	float k=([input.text floatValue] *15);
	[output setText:[NSString stringWithFormat:@"%1.0f mL",k]];
	[help setText:(@"tbl =")];
}
- (IBAction) mL_tbl{
	float k=([input.text floatValue] /15);
	[output setText:[NSString stringWithFormat:@"%1.0f tbl",k]];
	[help setText:(@"mL =")];
}

- (IBAction) cup_tbl{
	float k=([input.text floatValue] *16);
	[output setText:[NSString stringWithFormat:@"%1.0f tbl",k]];
	[help setText:(@"cup =")];
}


- (IBAction) tbl_cup{
	float k=([input.text floatValue] /16);
	[output setText:[NSString stringWithFormat:@"%1.0f cup",k]];
	[help setText:(@"tbl =")];
}

- (IBAction) cup_floz{
	float k=([input.text floatValue] *8);
	[output setText:[NSString stringWithFormat:@"%1.0f fl oz",k]];
	[help setText:(@"cup =")];
}

- (IBAction) floz_cup{
	float k=([input.text floatValue] /8);
	[output setText:[NSString stringWithFormat:@"%1.0f cup",k]];
	[help setText:(@"fl oz =")];
}

- (IBAction) floz_tbl{
	float k=([input.text floatValue] *2);
	[output setText:[NSString stringWithFormat:@"%1.0f tbl",k]];
	[help setText:(@"fl oz =")];
}

- (IBAction) tbl_floz{
	float k=([input.text floatValue] /2);
	[output setText:[NSString stringWithFormat:@"%1.0f fl oz",k]];
	[help setText:(@"tbl =")];
}

- (IBAction) floz_mL{
	float k=([input.text floatValue] *30);
	[output setText:[NSString stringWithFormat:@"%1.0f mL",k]];
	[help setText:(@"fl oz =")];
}

- (IBAction) mL_floz{
	float k=([input.text floatValue] /30);
	[output setText:[NSString stringWithFormat:@"%1.0f fl oz",k]];
	[help setText:(@"mL =")];
}
- (IBAction) g_floz{
	float k=([input.text floatValue] *.034);
	[output setText:[NSString stringWithFormat:@"%1.0f fl oz",k]];
	[help setText:(@"g =")];
}
- (IBAction) floz_g{
	float k=([input.text floatValue] *28.35);
	[output setText:[NSString stringWithFormat:@"%1.0f g",k]];
	[help setText:(@"fl oz =")];
}
- (IBAction) g_lb{
	float k=([input.text floatValue] *.0022);
	[output setText:[NSString stringWithFormat:@"%1.0f lb",k]];
	[help setText:(@"g =")];
}
- (IBAction) lb_g{
	float k=([input.text floatValue] *453.6);
	[output setText:[NSString stringWithFormat:@"%1.0f g",k]];
	[help setText:(@"lb =")];
}




- (void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *)event {
	for (UIView* view in self.view.subviews) {
		if ([view isKindOfClass:[UITextField class]])
			[view resignFirstResponder];
	}
}


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	output.enabled = NO;

    [super viewDidLoad];
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
