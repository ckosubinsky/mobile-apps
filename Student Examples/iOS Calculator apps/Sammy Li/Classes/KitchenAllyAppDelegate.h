//
//  KitchenAllyAppDelegate.h
//  KitchenAlly
//
//  Created by SammyLi on 10/15/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KitchenAllyViewController;

@interface KitchenAllyAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    KitchenAllyViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet KitchenAllyViewController *viewController;

@end

