//
//  ConvertLBKGViewController.m
//  ConvertLBKG
//
//  Created by WoojinShin on 10/22/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "ConvertLBKGViewController.h"

@implementation ConvertLBKGViewController

@synthesize Output;
@synthesize warning;

- (IBAction) toPound{
	
	//dismiss the keyboard
	[input resignFirstResponder];
	
	//Calculate kilos to miles and put that value into the output field
	float k=([input.text floatValue] / 0.4535);
	//Put the input value into the label on the kph button
	NSString *confirmLB=[[NSString alloc] initWithFormat:@"%@",[NSString stringWithFormat:@"%1.0flb",k]];
	
	[Output setText:confirmLB];
	
}

- (IBAction) toKilos{
	
	//dismiss the keyboard
	[input resignFirstResponder];
	
	//Calculate kilos to miles and put that value into the output field
	float m=([input.text floatValue] * 0.4535);
	//Put the input value into the label on the kph button
	NSString *confirmKG=[[NSString alloc] initWithFormat:@"%@",[NSString stringWithFormat:@"%1.0fkg",m]];
	
	[Output setText:confirmKG];
	
}

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
