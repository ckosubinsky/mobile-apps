//
//  ConvertLBKGViewController.h
//  ConvertLBKG
//
//  Created by WoojinShin on 10/22/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConvertLBKGViewController : UIViewController {
	IBOutlet UIButton *toPound;
	IBOutlet UIButton *toKilos;
	IBOutlet UITextField *input;
	IBOutlet UILabel *output;
	
	IBOutlet UILabel *warning;
}

- (IBAction) toPound;
- (IBAction) toKilos;

@property (nonatomic, retain) UILabel *Output;
@property (nonatomic, retain) UILabel *warning;

@end
