//
//  ConvertLBKGAppDelegate.h
//  ConvertLBKG
//
//  Created by WoojinShin on 10/22/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConvertLBKGViewController;

@interface ConvertLBKGAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    ConvertLBKGViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ConvertLBKGViewController *viewController;

@end

