//
//  Quarterback_RatingViewController.m
//  Quarterback Rating
//
//  Created by DayneHoffman on 10/13/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "Quarterback_RatingViewController.h"

@implementation Quarterback_RatingViewController

@synthesize comp;
@synthesize att;
@synthesize yards;
@synthesize td;
@synthesize intc;
@synthesize qbrate;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

//Action to calculate rate
- (IBAction) createrate
{
	//Dismiss the keyboard
	[comp resignFirstResponder];
	[att resignFirstResponder];
	[yards resignFirstResponder];
	[td resignFirstResponder];
	[intc resignFirstResponder];
	
	//Accept inputs
	float c=([comp.text floatValue]);
	float a=([att.text floatValue]);
	float y=([yards.text floatValue]);
	float t=([td.text floatValue]);
	float i=([intc.text floatValue]);
	
	//Calculate inputs 
	float r=(((8.4*y)+(330*t)+(100*c)-(200*i))/a);
	
	//Put float r in output field
	[qbrate setText:[NSString stringWithFormat:@"%1.0f",r]];
	
	//Clear the input fields
	[comp setText:nil];
	[att setText:nil];
	[yards setText:nil];
	[td setText:nil];
	[intc setText:nil];
}

//Play crowd cheer on click up of calculate button
-(IBAction) playSound:(id)sender{
	
	NSString *path = [[NSBundle mainBundle] pathForResource:@"Crowd" ofType:@"wav"];    
	AVAudioPlayer* theAudio=[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];        
	
	theAudio.delegate=self;   
	[theAudio play];
}


@end
