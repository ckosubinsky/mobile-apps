//
//  FlipViewController.h
//  Parallax
//
//  Created by  on 10/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FlipViewControllerDelegate;

@interface FlipViewController : UIViewController{
    id <FlipViewControllerDelegate> delegate;
    CGFloat score_;
    
    IBOutlet UILabel* scoreLabel;
}

@property(nonatomic, assign) CGFloat score;
@property(nonatomic, assign) id<FlipViewControllerDelegate> delegate;

-(id) initWithScore:(CGFloat)score;
//-(IBAction)done:(id)sender;

@end

@protocol FlipViewControllerDelegate
-(void) flipViewDone:(FlipViewController *) controller;
@end