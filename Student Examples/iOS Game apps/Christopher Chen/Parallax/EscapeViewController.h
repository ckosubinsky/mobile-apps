//
//  ParallaxViewController.h
//  Parallax
//
//  Created by  on 10/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "FlipViewController.h"
#import "Player.h"

typedef enum {
    PLAY,
    DIE,
    SCORE
} GameState;

@interface EscapeViewController : UIViewController<FlipViewControllerDelegate, AVAudioPlayerDelegate> {
    AVAudioPlayer* music;
    SystemSoundID gameover;
    NSMutableArray* coins;
    
    Player *player;
    BOOL left;
    BOOL right;
    BOOL jump;
    
    CGFloat score;
    CGFloat speed_;
    GameState state;
    float timer;
    
    IBOutletCollection(UIImageView) NSMutableArray* bgLayer1;
    IBOutletCollection(UIImageView) NSMutableArray* bgLayer2;
    IBOutletCollection(UIImageView) NSMutableArray* fgLayer;
    
    IBOutletCollection(UIImageView) NSMutableArray* platforms;
}

-(void) update;
-(IBAction) jumpDown;
-(IBAction) jumpUp;
-(IBAction) leftDown;
-(IBAction) leftUp;
-(IBAction) rightDown;
-(IBAction) rightUp;

-(void) updateLayer:(NSMutableArray*)layer withSpeed:(CGFloat)speed;
-(void) resetGame;


@end
