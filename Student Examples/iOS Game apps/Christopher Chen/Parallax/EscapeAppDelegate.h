//
//  ParallaxAppDelegate.h
//  Parallax
//
//  Created by  on 10/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EscapeViewController;

@interface EscapeAppDelegate : NSObject <UIApplicationDelegate>

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet EscapeViewController *viewController;

@end
