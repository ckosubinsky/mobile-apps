//
//  Player.m
//  Parallax
//
//  Created by  on 10/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "Player.h"

@implementation Player

@synthesize alive = alive_;

- (id)init
{
    self = [super init];
    if (self) {
        view_ = [[UIImageView alloc] initWithFrame:CGRectMake(230,237,16,16)];
        runAnim = [[NSArray arrayWithObjects:    
                                     [UIImage imageNamed:@"escapee_run0.png"],
                                     [UIImage imageNamed:@"escapee_run1.png"],
                                     [UIImage imageNamed:@"escapee_run2.png"],
                                     [UIImage imageNamed:@"escapee_run3.png"], nil] retain];
        
        jumpAnim = [[NSArray arrayWithObjects: 
                     [UIImage imageNamed:@"escapee_jump0.png"],
                     [UIImage imageNamed:@"escapee_jump1.png"],
                     [UIImage imageNamed:@"escapee_jump2.png"],
                     [UIImage imageNamed:@"escapee_fall0.png"],
                     [UIImage imageNamed:@"escapee_fall1.png"],
                     [UIImage imageNamed:@"escapee_fall2.png"],
                     [UIImage imageNamed:@"escapee_fall3.png"],
                     [UIImage imageNamed:@"escapee_fall0.png"],
                     [UIImage imageNamed:@"escapee_fall1.png"],
                     [UIImage imageNamed:@"escapee_fall2.png"],
                     [UIImage imageNamed:@"escapee_fall3.png"],nil] retain];
        
        fallAnim = [[NSArray arrayWithObjects: 
                     [UIImage imageNamed:@"escapee_fall0.png"],
                     [UIImage imageNamed:@"escapee_fall1.png"],
                     [UIImage imageNamed:@"escapee_fall2.png"],
                     [UIImage imageNamed:@"escapee_fall3.png"], nil] retain];
        
        dieAnim = [[NSArray arrayWithObjects: 
                    [UIImage imageNamed:@"escapee_die0.png"],
                    [UIImage imageNamed:@"escapee_die1.png"],
                    [UIImage imageNamed:@"escapee_die2.png"],
                    [UIImage imageNamed:@"escapee_die3.png"],
                    [UIImage imageNamed:@"escapee_die4.png"],
                    [UIImage imageNamed:@"escapee_die5.png"],
                    [UIImage imageNamed:@"escapee_die6.png"],
                    [UIImage imageNamed:@"escapee_die7.png"], nil] retain];
        
        view_.animationImages = runAnim;
        status = 0;
        view_.animationDuration = .1*4;
        view_.animationRepeatCount = 0;
        [view_ startAnimating];
        
        size_ = view_.frame.size;
        
        canJump = TRUE;
        
        //load sounds
        NSString *path = [[NSBundle mainBundle] 
                                pathForResource:@"jump" ofType:@"wav"];
        NSURL *url = [NSURL fileURLWithPath:path];
        AudioServicesCreateSystemSoundID((CFURLRef)url, &jumpSfx);
        alive_ = TRUE;
        
    }
    
    return self;
}

-(void) jump{
    if(canJump){
        dy_ -= 75;
        jumpHeight = 120;
        canJump = FALSE;
        startedJump = TRUE;
        AudioServicesPlaySystemSound(jumpSfx);
    }
    
    if(startedJump && jumpHeight > 0){
        dy_ -= 10;
        jumpHeight -= 10;
    }
}

-(void) die{
    //if(!startedDie){
        //dy_ = -200;
    //    startedDie = TRUE;
    //}
}

-(void) update:(CGFloat) dt Left:(BOOL)left Right:(BOOL)right Jump:(BOOL)jump{
    if(alive_){
        if(dy_ < 0.0f){
            if(status != 1) {
                view_.animationImages = jumpAnim;
                view_.animationDuration = .1*11;
                view_.animationRepeatCount = 0;
                [view_ startAnimating];
                status = 1;
            }
        }
        else {
            if(dy_ > 0.0f){
                if(status != 2){
                    view_.animationImages = fallAnim;
                    view_.animationDuration = .1*4;
                    view_.animationRepeatCount = 0;
                    [view_ startAnimating];
                    status = 2;
                }
            }
            else {
                if(status != 0){
                    [view_ stopAnimating];
                    view_.animationImages = runAnim;
                    view_.animationDuration = .1*4;
                    view_.animationRepeatCount = 0;
                    [view_ startAnimating];
                    status = 0;
                }
            }
        }
        
        if(!jump)
            startedJump = FALSE;
        if(jump){
            [self jump];
        }
        
        canJump = FALSE;
        
        
       
        
        //apply gravity
        dy_ += 4;
        
        //drag
        dx_ *= .9;
        
        
        if(left) dx_ = -200;
        if(right) dx_ = 200;
        
        pos_.x += dx_ * dt;
        pos_.y += dy_ * dt;
        
        view_.frame = CGRectMake(pos_.x, pos_.y, view_.frame.size.width, view_.frame.size.height);
    }else{
        if(status != 0) {
            view_.animationImages = dieAnim;
            view_.animationDuration = .1*8;
            view_.animationRepeatCount = 1;
            [view_ startAnimating];
            status = 0;
        }
        
        dy_ = 0;
        pos_.y += dy_ * dt;
        view_.frame = CGRectMake((int)pos_.x, (int)pos_.y, view_.frame.size.width, view_.frame.size.height);
    }
}

-(void) update:(CGFloat)dt{
    
}

/*
-(void) checkCollision:(CGRect) other{
    CGPoint mid1 = CGPointMake(pos_.x+view_.frame.size.width/2, pos_.y+view_.frame.size.height/2);
    CGPoint mid2 = CGPointMake(CGRectGetMidX(other), CGRectGetMidY(other));
    
    CGFloat dist = fabs(mid1.x - mid2.x);
    CGFloat mtvx = view_.frame.size.width/2 + other.size.width/2 - dist;
    CGFloat mtvy = 0;
    if(mtvx > 0){
        // x axis overlaps
        dist = fabs(mid1.y - mid2.y);
        mtvy = view_.frame.size.height/2 + other.size.height/2 - dist;
        if(mtvy > 0){
            // y axis overlaps    
            if(mtvx > mtvy){
                if(mid1.y < mid2.y)
                    mtvy = -mtvy;
                mtvx = 0;
                dy_ = 0;
                pos_.y += mtvy;
                
                // player can jump if it landed on something below it
                if(mtvy < 0)
                    canJump = TRUE;
            }else{
                if(mid1.x < mid2.x)
                    mtvx = -mtvx;
                mtvy = 0;
                dx_ = 0;
                pos_.x += mtvx;
            }
        }
    }
}
*/

-(void) collidedWith:(Entity *)other mtv:(CGPoint*)mtv{
}

-(void) collidedWithType:(EntityType)type mtv:(CGPoint*)mtv{
    if(type == PLATFORM){
        
        // mtv.y < 0 when something below is pushing the player up.
        if(mtv->y < 0)
            canJump = TRUE;
        
        //apply the smallest value
        CGFloat diff = fabs(mtv->x) - fabs(mtv->y);
        if(diff < 0){
            pos_.x += mtv->x;
            dx_ = 0;
        }
        else if(diff > 0){
            pos_.y += mtv->y;
            dy_ = 0;
        }
        else{
            pos_.x += mtv->x;
            pos_.y += mtv->y;
            dx_ = dy_ = 0;
        }
    }
}

-(void) dealloc{
    [super dealloc];
    [view_ release];
    [jumpAnim release];
    [fallAnim release];
    [dieAnim release];
    [runAnim release];
    AudioServicesDisposeSystemSoundID(jumpSfx);
}
@end
