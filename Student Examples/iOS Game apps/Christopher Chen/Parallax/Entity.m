//
//  Entity.m
//  Escape
//
//  Created by  on 11/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Entity.h"

@implementation Entity

@synthesize pos = pos_;
@synthesize size = size_;
@synthesize view = view_;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(void) update:(CGFloat)dt{}

-(BOOL) collision:(Entity*)other mtv:(CGPoint *)mtv{
    return [self collisionRect:CGRectMake(other.pos.x, other.pos.y, other.size.width, other.size.height) mtv:mtv];
}

-(BOOL) collisionRect:(CGRect)other mtv:(CGPoint *)mtv{
    CGPoint mid1 = CGPointMake(pos_.x+size_.width/2, pos_.y+size_.height/2);
    CGPoint mid2 = CGPointMake(CGRectGetMidX(other), CGRectGetMidY(other));
    
    CGFloat dist = fabs(mid1.x - mid2.x);
    CGFloat mtvx = size_.width/2 + other.size.width/2 - dist;
    CGFloat mtvy = 0;
    if(mtvx > 0){
        // x axis overlaps
        dist = fabs(mid1.y - mid2.y);
        mtvy = size_.height/2 + other.size.height/2 - dist;
        if(mtvy > 0){
            // y axis overlaps    
            if(mid1.y < mid2.y)
                mtvy = -mtvy;
            
            if(mid1.x < mid2.x)
                mtvx = -mtvx;
            
            if(mtv){
                mtv->x = mtvx;
                mtv->y = mtvy;
            }
            
            return TRUE;
        }
    }
    
    return FALSE;
}


-(void) collidedWith:(Entity*) other mtv:(CGPoint*)mtv{}
-(void) collidedWithType:(EntityType)type mtv:(CGPoint*)mtv{}

@end
