//
//  Enemy.m
//  Escape
//
//  Created by  on 11/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Enemy.h"
#import "Player.h"

@implementation Enemy

- (id)init
{
    self = [super init];
    if (self) {
        // init view stuff
        // init animations
    }
    
    return self;
}

-(void) update:(CGFloat)dt{
    if(health > 0){
        // do nothing
    }else{
        // do death animation
    }
        
}

-(void) hurt:(int)damage{
    if(damage > 0)
        health -= damage;
}

-(void) collisionWith:(Entity *)other mtv:(CGPoint *)mtv{
    if([other class] == [Player class]){
        // if jump on head, player bounce
        // if hits, player gets hurt
    }
}

-(void) collisionWithType:(EntityType)type mtv:(CGPoint*)mtv{
    if(type == PLATFORM){
        //apply the smallest value
        CGFloat diff = fabs(mtv->x) - fabs(mtv->y);
        if(diff < 0){
            pos_.x += mtv->x;
        }
        else if(diff > 0){
            pos_.y += mtv->y;
        }
        else{
            pos_.x += mtv->x;
            pos_.y += mtv->y;
        }
    }
}

@end
