//
//  Player.h
//  Parallax
//
//  Created by  on 10/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

#import "Entity.h"

@interface Player : Entity{
    SystemSoundID jumpSfx;
    
    NSArray* jumpAnim;
    NSArray* fallAnim;
    NSArray* dieAnim;
    NSArray* runAnim;
    
    CGFloat dx_;
    CGFloat dy_;
    
    int status;
    BOOL alive_;
    BOOL startedDie;
    
    CGFloat jumpHeight;
    
    BOOL canJump;
    BOOL startedJump;
}

@property(nonatomic, assign) BOOL alive;

-(void) jump;
-(void) die;
-(void) update:(CGFloat)dt Left:(BOOL)left Right:(BOOL)right Jump:(BOOL)jump;


@end
