//
//  Entity.h
//  Escape
//
//  Created by  on 11/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    PLATFORM,
    ENTITY
} EntityType;

@interface Entity : NSObject{
    CGPoint pos_;
    CGSize size_;
    
    UIImageView* view_;
}

@property(nonatomic, assign) CGPoint pos;
@property(nonatomic, assign) CGSize size;
@property(nonatomic, assign) UIImageView* view;

-(void) update:(CGFloat)dt;
-(BOOL) collision:(Entity*)other mtv:(CGPoint*)mtv;
-(BOOL) collisionRect:(CGRect)other mtv:(CGPoint*)mtv;

-(void) collidedWith:(Entity*)other mtv:(CGPoint*)mtv;
-(void) collidedWithType:(EntityType)type mtv:(CGPoint*)mtv;

@end
