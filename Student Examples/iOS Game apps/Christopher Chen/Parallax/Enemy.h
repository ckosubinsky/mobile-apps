//
//  Enemy.h
//  Escape
//
//  Created by  on 11/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Entity.h"

@interface Enemy : Entity{
    int health;
}

-(void) update:(CGFloat)dt;

-(void) hurt:(int)damage;

-(void) collisionWith:(Entity*)other mtv:(CGPoint*)mtv;
-(void) collisionWithType:(EntityType)type mtv:(CGPoint*)mtv;
@end
