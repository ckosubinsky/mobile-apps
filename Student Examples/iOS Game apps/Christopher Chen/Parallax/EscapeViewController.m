//
//  ParallaxViewController.m
//  Parallax
//
//  Created by  on 10/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EscapeViewController.h"


@implementation EscapeViewController

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // load music mp3
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"smw_castle" ofType:@"mp3"];
    NSError * error;
    music = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
    
    if(error){
        //printf("%s", [error localizedDescription]);
    } else {
        music.delegate = self;
        music.numberOfLoops = -1;
        [music prepareToPlay];
        [music play];
    }
    
    //load sounds
    NSString *path2 = [[NSBundle mainBundle] 
                      pathForResource:@"die" ofType:@"wav"];
    NSURL *url = [NSURL fileURLWithPath:path2];
    AudioServicesCreateSystemSoundID((CFURLRef)url, &gameover);
    
    
    [NSTimer scheduledTimerWithTimeInterval:1/60.0 target:self selector:@selector(update)
								   userInfo:nil repeats:YES];
    player = [[Player alloc] init];
    player.pos = CGPointMake(self.view.frame.size.height/2, self.view.frame.size.width/2);
    [self.view addSubview:player.view];
    
    left = FALSE;
    right = FALSE;
    speed_ = 1.0;
    
    state = PLAY;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [music stop];
    [music release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation ==UIInterfaceOrientationLandscapeRight);
}


- (void) update {
    if(state == PLAY){
        score += 1;
        speed_ += 0.0002;
        
        if(speed_ > 3)
            speed_ = 3;
        
        [self updateLayer:bgLayer2 withSpeed: .25*speed_];
        [self updateLayer:bgLayer1 withSpeed: .5*speed_];
        [self updateLayer:fgLayer withSpeed: 1*speed_];
        
        [player update:1/60.0 Left:left Right:right Jump:jump];
        
        // check player collision with platforms
        for (UIImageView* plat in platforms) {
            CGPoint mtv = CGPointMake(0,0);
            if([player collisionRect:plat.frame mtv:&mtv]){
                [player collidedWithType:PLATFORM mtv:&mtv];
            }
        }
        
        UIImageView* lastPlat = NULL;
        for(UIImageView* plat in platforms){
            if(lastPlat == NULL)
                lastPlat = plat;
            else{
                if(CGRectGetMaxX(plat.frame) > CGRectGetMaxX(lastPlat.frame)){
                    lastPlat = plat;
                }
            }
        }
       
        //update platform position
        for (UIImageView* plat in platforms){
            //break;
            CGFloat speed = 2*speed_;
            plat.frame = CGRectMake(plat.frame.origin.x-speed, plat.frame.origin.y, plat.frame.size.width, plat.frame.size.height);
            
            if(plat.frame.origin.x + plat.frame.size.width < 0){
                CGFloat y = lastPlat.frame.origin.y;
                int rand = arc4random() % 2;
                
                if(rand == 0 && y < 300-70){
                    y += 30;
                }
                else if(rand == 1 && y > 120+30){
                    y -= 30;
                }
                
                plat.frame = CGRectMake(CGRectGetMaxX(lastPlat.frame)+100*(speed_), y, plat.frame.size.width, plat.frame.size.height);
            }
        }
        
        // player is dead when touches lava
        if(player.pos.y > self.view.frame.size.width-40){
            //player.pos = CGPointMake(self.view.frame.size.height/2, 0);
            player.alive = FALSE;
            [player die];
            state = DIE;
            [music stop];
            AudioServicesPlaySystemSound(gameover);
        }
    }else if(state == DIE){
        [player update:1/60.0 Left:FALSE Right:FALSE Jump:FALSE];
        timer += 1/60.0;
        
        // when animation is over
        if(timer >= .1*8){
            state = SCORE;
            
            //flip over to the score view
            [music stop];
            FlipViewController* flip = [[FlipViewController alloc] initWithScore:score];
            
            flip.delegate = self;
            flip.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            [self presentModalViewController:flip animated:TRUE];
            [flip release];
        }
    }else if(state == SCORE){
        //do nothing!
    }
}

- (IBAction) jumpDown{
    jump = TRUE;
}
- (IBAction) jumpUp {
    jump = FALSE;
}
- (IBAction) leftDown{
    left = TRUE;
}
-(IBAction) leftUp{
    left = FALSE;
}
-(IBAction) rightDown{
    right = TRUE;
}
-(IBAction) rightUp{
    right = FALSE;
}

-(void) updateLayer:(NSMutableArray*)layer withSpeed:(CGFloat)speed {
    for (UIImageView* image in layer){
        image.frame = CGRectMake(image.frame.origin.x-speed, image.frame.origin.y, image.frame.size.width, image.frame.size.height);
        
        if(image.frame.origin.x + image.frame.size.width <= 0){
            image.frame = CGRectMake(480-1, image.frame.origin.y, image.frame.size.width, image.frame.size.height);
        }
    }
}

-(void) resetGame{
    timer = 0;
    music.currentTime = 0;
    [music play];
    
    score = 0;
    speed_ = 1;
    player.alive = TRUE;
    player.pos = CGPointMake(self.view.frame.size.width/2, 20);
}

-(void)flipViewDone:(FlipViewController *)controller{
    [self dismissModalViewControllerAnimated:TRUE];
    
    [self resetGame];
    state = PLAY;    
}
@end