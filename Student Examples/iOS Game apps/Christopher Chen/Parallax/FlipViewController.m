//
//  FlipViewController.m
//  Parallax
//
//  Created by  on 10/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FlipViewController.h"

@implementation FlipViewController

@synthesize score = score_;
@synthesize delegate;

- (id)initWithScore:(CGFloat)score{
    self = [super initWithNibName:@"FlipViewController" bundle:nil];
    
    if(self){
        score_ = score;
    }
    
    return self;
}

- (IBAction)done:(id)sender {
	[self.delegate flipViewDone:self];	
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(scoreLabel){
        [scoreLabel setText:[NSString stringWithFormat:@"%.0f",score_] ];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return interfaceOrientation==UIInterfaceOrientationLandscapeLeft;

}

- (void)dealloc {
    [super dealloc];
}

@end
