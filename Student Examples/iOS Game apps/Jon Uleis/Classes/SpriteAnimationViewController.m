//
//  SpriteAnimationViewController.m
//  SpriteAnimation
//
//  Created by Jon Uleis on 10/20/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "SpriteAnimationViewController.h"

@implementation SpriteAnimationViewController

@synthesize background1, background2, donkeyKong, enemy;
@synthesize dkWalk, enemyWalk, dkUpButton, dkDownButton, playAgainButton;

int dkY;
int enemyY;

int score;
int enemyspeed;

BOOL collided;

NSString *currentSound;
NSString *themeSound;
NSString *overSound;

NSTimer *colTimer;
NSTimer *bgTimer;
NSTimer *eneTimer;
NSTimer *dkTimer;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {	
	
	//Sounds
	themeSound = @"/thememusic.mp3";
	overSound = @"/gameover.mp3";
	
	currentSound = themeSound;
	[self startSound];	//Play background music

	background1.frame=CGRectMake(0,0,544,300);
	background2.frame=CGRectMake(544,0,544,300);
	
	dkY = 85;
	enemyY = 170; //Enemy begins in bottom row so player has chance to adapt
	score = 0;
	enemyspeed = 3; //Starts at -3px each frame
	collided = NO;
	
	//Background looping timer - .02 = 50fps
	bgTimer = [NSTimer scheduledTimerWithTimeInterval:.02 target:self selector:@selector(bgMove) userInfo:nil repeats:YES];
	[self bgMove];

	//Fire enemy timer
	eneTimer = [NSTimer scheduledTimerWithTimeInterval:.02 target:self selector:@selector(enemyMove) userInfo:nil repeats:YES];
	[self enemyMove];

	//Check collisions timer
	colTimer = [NSTimer scheduledTimerWithTimeInterval:.06 target:self selector:@selector(checkCol) userInfo:nil repeats:YES];
	[self checkCol];	
	
	//Donkey Kong walk animation
	dkWalk=[[NSMutableArray alloc] init];
	for(int i=0;i<15;i++){
		NSString *pic=[NSString stringWithFormat:@"Swim%i.gif", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[dkWalk addObject:img];
	}
	[donkeyKong setAnimationImages:dkWalk];
	[donkeyKong setAnimationDuration:.75];
	[donkeyKong startAnimating];
	
	//Enemy spin animation
	enemyWalk=[[NSMutableArray alloc] init];
	for(int i=0;i<8;i++){
		NSString *pic=[NSString stringWithFormat:@"Enemy%i.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[enemyWalk addObject:img];
	}
	[enemy setAnimationImages:enemyWalk];
	[enemy setAnimationDuration:.75];
	[enemy startAnimating];
	
	enemy.frame=CGRectMake(481,enemyY,55,55); //Get enemy in proper starting position
	
	[super viewDidLoad];
}

// overrides the orientations such that the view is always fixed in horizonal position
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	
	if ((interfaceOrientation==UIInterfaceOrientationPortrait) || (interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown))
		// NO locks out the portrait mode of the iPhone. The initial orientation also has to be set in the info plist.
	{
		return NO;
	}
	if ((interfaceOrientation==UIInterfaceOrientationLandscapeLeft) || (interfaceOrientation==UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
	[songPlayer release];
    [super dealloc];
}

- (void)startSound{	
	NSString *songPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], currentSound];
	songPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:songPath] error:NULL];
	songPlayer.currentTime = 0;
	if (currentSound==themeSound) {
		songPlayer.numberOfLoops = -1;
	}else{
		songPlayer.numberOfLoops = 0;
	}
	[songPlayer play];
}

- (void) bgMove //Loops background image
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	[UIView setAnimationDuration:.02];
	[UIView setAnimationDelegate:self];
	background1.frame=CGRectMake(background1.frame.origin.x-2,0,544,300);
	background2.frame=CGRectMake(background2.frame.origin.x-2,0,544,300);
	[UIView commitAnimations];
	
	if (background1.frame.origin.x==-546) {
		background1.hidden=YES; //Hide image when it is off-screen
		background1.frame=CGRectMake(542,0,544,300);
	}
	if (background2.frame.origin.x==-546) {
		background2.hidden=YES;
		background2.frame=CGRectMake(542,0,544,300);
	}	
	if (background1.frame.origin.x==480) {
		background1.hidden=NO; //Reveal image when it is about to enter screen
	}
	if (background2.frame.origin.x==480) {
		background2.hidden=NO;
	}
}

- (void) enemyMove
{	
	enemy.frame=CGRectMake(enemy.frame.origin.x-enemyspeed,enemyY,55,55);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	[UIView setAnimationDuration:.02];
	[UIView setAnimationDelegate:self];
	[UIView commitAnimations];
	
	if (enemy.frame.origin.x<=-56) {
		int randy = (arc4random() % 2); //Selects a random number from 0 to 1 (% n-1 is desired max)
		if (randy==0) {
			enemyY=80; //Enemy on top of screen
		} else {
			enemyY=170; //Enemy on bottom of screen
		}		
		enemy.frame=CGRectMake(481+150,enemyY,55,55); //150 -- estimated length of space for extra second
		if(collided==NO){
			score++;
			scoreLabel.text=[NSString stringWithFormat:@"%d", score];
			if (score%5==0 && score<16) { //Four speeds: #1 at 0-5 enemies, #2 at 5-10, #3 at 10-15, #4 at 16+
				enemyspeed = enemyspeed+1;
			}
		}
	}	
}

- (void) checkCol
{
	if(CGRectIntersectsRect(donkeyKong.frame, enemy.frame)) {
		[colTimer invalidate];
		[bgTimer invalidate];
		[eneTimer invalidate];
		[dkTimer invalidate];

		colTimer = nil;
		bgTimer = nil;
		eneTimer = nil;
		dkTimer = nil;
		
		dkUpButton.enabled = NO;
		dkDownButton.enabled = NO;
		collided = YES;
		
		currentSound = overSound;
		[songPlayer stop];
		[self startSound];
		
		[donkeyKong stopAnimating];
						
		UIImage *img=[UIImage imageNamed:@"Ouch.png"];
		[donkeyKong setImage:img];
		
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationCurve:UIViewAnimationCurveLinear];
		[UIView setAnimationDuration:0.8];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDidStopSelector:@selector(dkFall)];
		donkeyKong.frame=CGRectMake(50,donkeyKong.frame.origin.y-10,63,45);
		[UIView commitAnimations];
	}
}

- (void) dkFall
{
	UIImage *img=[UIImage imageNamed:@"Fall.png"];
	[donkeyKong setImage:img];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	[UIView setAnimationDuration:1.1];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(dkScratch)];
	donkeyKong.frame=CGRectMake(50,220,63,45);
	[UIView commitAnimations];
}

- (void) dkScratch
{
	UIImage *img=[UIImage imageNamed:@"Scratch.png"];
	[donkeyKong setImage:img];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	[UIView setAnimationDuration:1.5];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(againQ)];
	donkeyKong.frame=CGRectMake(50,221,63,45);
	[UIView commitAnimations];
}	

- (void) againQ
{
	playAgainButton.hidden = NO;
}

- (IBAction) dkUp
{
	if (dkY==175) {
		dkY = 85; //DK to top
		dkTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(dkMove) userInfo:nil repeats:YES];
		[self dkMove];
	}
}

- (IBAction) dkDown
{
	if (dkY==85) {
		dkY = 175; //DK to bottom
		dkTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(dkMove) userInfo:nil repeats:YES];
		[self dkMove];
	}
}

- (void) dkMove
{
	dkUpButton.enabled = NO;
	dkDownButton.enabled = NO;
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationCurve:UIViewAnimationCurveLinear];
	[UIView setAnimationDuration:0.1];
	[UIView setAnimationDelegate:self];

	if (dkY==85) {
		donkeyKong.frame=CGRectMake(50,donkeyKong.frame.origin.y-10,63,45);
	}else{
		donkeyKong.frame=CGRectMake(50,donkeyKong.frame.origin.y+10,63,45);
	}
	
	if (donkeyKong.frame.origin.y==85||donkeyKong.frame.origin.y==175) {
		[dkTimer invalidate];
		dkTimer = nil;
		if(collided==NO){
			dkUpButton.enabled = YES;
			dkDownButton.enabled = YES;
		}
	}
	[UIView commitAnimations];
}
	
- (IBAction) playAgain
{
	dkUpButton.enabled = YES;
	dkDownButton.enabled = YES;
	playAgainButton.hidden = YES;

	scoreLabel.text=@"0";
	
	donkeyKong.frame=CGRectMake(50,85,63,54);

	[self viewDidLoad];
}

@end