//
//  SpriteAnimationViewController.h
//  SpriteAnimation
//
//  Created by Jon Uleis on 10/20/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<AVFoundation/AVAudioPlayer.h>

@interface SpriteAnimationViewController : UIViewController {

	IBOutlet UIImageView *background1;
	IBOutlet UIImageView *background2;
	IBOutlet UIImageView *donkeyKong;
	IBOutlet UIImageView *enemy;
	NSMutableArray *dkWalk;
	NSMutableArray *enemyWalk;
	
	IBOutlet UIButton *dkUpButton;
	IBOutlet UIButton *dkDownButton;
	IBOutlet UIButton *playAgainButton;
	
	IBOutlet UILabel *scoreLabel;
	
	AVAudioPlayer *songPlayer;
}

@property (nonatomic, retain)UIImageView *background1;
@property (nonatomic, retain)UIImageView *background2;
@property (nonatomic, retain)UIImageView *donkeyKong;
@property (nonatomic, retain)UIImageView *enemy;
@property (nonatomic, retain)NSMutableArray *dkWalk;
@property (nonatomic, retain)NSMutableArray *enemyWalk;
@property (nonatomic, retain)UIButton *dkUpButton;
@property (nonatomic, retain)UIButton *dkDownButton;
@property (nonatomic, retain)UIButton *playAgainButton;

- (IBAction) dkUp;
- (IBAction) dkDown;
- (IBAction) playAgain;
- (void) startSound;
- (void) checkCol;
- (void) bgMove;
- (void) enemyMove;
- (void) dkMove;
- (void) dkFall;
- (void) dkScratch;
- (void) againQ;

@end

