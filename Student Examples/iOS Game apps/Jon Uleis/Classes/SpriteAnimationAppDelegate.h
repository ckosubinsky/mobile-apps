//
//  SpriteAnimationAppDelegate.h
//  SpriteAnimation
//
//  Created by Jon Uleis on 10/20/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SpriteAnimationViewController;

@interface SpriteAnimationAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    SpriteAnimationViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet SpriteAnimationViewController *viewController;

@end

