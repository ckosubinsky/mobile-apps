//
//  Ping_PongAppDelegate.h
//  Ping_Pong
//
//  Created by Angela Alarcon on 10/29/11.
//  Copyright 2011 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Ping_PongViewController;

@interface Ping_PongAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    Ping_PongViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Ping_PongViewController *viewController;

@end

