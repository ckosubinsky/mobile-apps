//
//  Ping_PongViewController.h
//  Ping_Pong
//
//  Created by Angela Alarcon on 10/29/11.
//  Copyright 2011 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<Foundation/Foundation.h>


@interface Ping_PongViewController : UIViewController
{
	IBOutlet UIImageView *airBall;
	IBOutlet UIImageView *playerPaddle;
	IBOutlet UIImageView *computerPaddle;
	
	IBOutlet UILabel *playerScoreText;
	
	IBOutlet UILabel *computerScoreText;
	

	CGPoint ballVelocity;
	
	NSInteger gameState;
	
	NSInteger playerScoreValue;
	NSInteger computerScoreValue;
	
	//AVAudioPlayer *themePlayer;//
	
	
}

@property(nonatomic,retain) IBOutlet UIImageView *airBall;
@property(nonatomic,retain) IBOutlet UIImageView *playerPaddle;
@property(nonatomic,retain) IBOutlet UIImageView *computerPaddle;

@property(nonatomic,retain) IBOutlet UILabel *playerScoreText;
@property(nonatomic,retain) IBOutlet UILabel *computerScoreText;




-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;



@property(nonatomic) CGPoint ballVelocity;

@property(nonatomic) NSInteger gameState;

-(void)reset:(BOOL) newGame;
-(void)startSound;


@end

