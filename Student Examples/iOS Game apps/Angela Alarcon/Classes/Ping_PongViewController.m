//
//  Ping_PongViewController.m
//  Ping_Pong
//
//  Created by Angela Alarcon on 10/29/11.
//  Copyright 2011 Stony Brook University. All rights reserved.
//

#import "Ping_PongViewController.h"
#import <AVFoundation/AVAudioPlayer.h>

@implementation Ping_PongViewController


@synthesize ballVelocity,gameState;
@synthesize airBall,playerPaddle,computerPaddle;
@synthesize playerScoreText;
@synthesize computerScoreText;
//@synthesize themePlayer;

#define kGameStateRunning 1
#define kGameStatePaused  2

#define kBallSpeedX 3
#define kBallSpeedY 4

#define kComputerMoveSpeed 3.25
#define kScoreToWin 5

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self touchesMoved:touches  withEvent:event];
	
}

//move paddles

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
	
	UITouch *touch = [[event allTouches] anyObject];
	CGPoint location = [touch locationInView: touch.view];
	
	self.gameState = kGameStateRunning;
	
	if(location.x >400)
	{
		CGPoint yLocation = CGPointMake(playerPaddle.center.x, location.y);
		playerPaddle.center = yLocation;
	}
	
}


- (void) startSound{
	
	NSString *path = [[NSBundle mainBundle] pathForResource:@"/offthepaddle" ofType:@"wav"];
	AVAudioPlayer* audio = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
	
	audio.delegate = self;
	[audio play];
	
	/*NSString *themeSongPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/offthepaddle.wav"];
	
	NSError *err;
	
	themePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:themeSongPath] error:&err];
	
	themePlayer.currentTime = 0;
	
	[themePlayer play];
	*/
}




-(void) gameLoop {
	if(gameState == kGameStateRunning) {
		
		playerScoreText.hidden = YES;
		computerScoreText.hidden = YES;
		
		
		airBall.center = CGPointMake(airBall.center.x + ballVelocity.x , airBall.center.y + ballVelocity.y);
		
		if(airBall.center.x > self.view.bounds.size.width || airBall.center.x < 0) {
			ballVelocity.x =- ballVelocity.x;
		}
		
		if(airBall.center.y > self.view.bounds.size.height || airBall.center.y < 0) {
			ballVelocity.y =- ballVelocity.y;
		}
		
		if (CGRectIntersectsRect (airBall.frame, playerPaddle.frame)) {
			CGRect frame = airBall.frame;
			frame.origin.x = playerPaddle.frame.origin.x - frame.size.height;
			airBall.frame = frame;
			ballVelocity.x =- ballVelocity.x;
		}
		
		if (CGRectIntersectsRect (airBall.frame, computerPaddle.frame)) {               
			CGRect frame = airBall.frame;
			frame.origin.x = CGRectGetMaxX(computerPaddle.frame);
			airBall.frame = frame;
			ballVelocity.x =- ballVelocity.x;
		}
		
		
		//move the computer paddle
		if(airBall.center.x <= self.view.center.x) {
			if(airBall.center.y < computerPaddle.center.y) {
				CGPoint compLocation = CGPointMake(computerPaddle.center.x, computerPaddle.center.y - kComputerMoveSpeed);
				computerPaddle.center = compLocation;
			}
			
			if(airBall.center.y > computerPaddle.center.y) {
				CGPoint compLocation = CGPointMake(computerPaddle.center.x, computerPaddle.center.y + kComputerMoveSpeed);
				computerPaddle.center = compLocation;
			}
		}
		
		//Increment the score and play sound. Check who won.
		if(airBall.center.x <= 0) 
		{
			playerScoreValue++;
			[self startSound];
			[self reset:(playerScoreValue >= kScoreToWin)];
		}
		
		if(airBall.center.x > self.view.bounds.size.width) {
			computerScoreValue++;
			[self startSound];
			[self reset:(computerScoreValue >= kScoreToWin)];
		}
	}
}

-(void)reset:(BOOL) newGame {
	self.gameState = kGameStatePaused;
	
	airBall.center = CGPointMake(241, 159);
	
	playerScoreText.hidden = NO;
	computerScoreText.hidden = NO;
	
	playerScoreText.text = [NSString stringWithFormat:@"%d",playerScoreValue];
	computerScoreText.text = [NSString stringWithFormat:@"%d",computerScoreValue];
	
	if(newGame) {
		NSString *msg = nil;
		
		if(computerScoreValue > playerScoreValue) {
			msg = @"You lose!!!!";
			UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Game Over" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			
			[alert show];
			[alert release];
			[msg release];
			
		
		} else {
			msg = @"You win!!!!";
			UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Game Over" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			
			[alert show];
			[alert release];
			[msg release];
			/*UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"You win" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
			
			[actionSheet showInView:self.view];
			[actionSheet release];
			*/
		}
		
		computerScoreValue = 0;
		playerScoreValue = 0;
		
		
	}
}


/*-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if(buttonIndex != [actionSheet cancelButtonIndex])
	{
		
	}
}*/
/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.gameState = kGameStatePaused;
	ballVelocity = CGPointMake(kBallSpeedX,kBallSpeedY);
	
	//Use a timer 
	[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(gameLoop) userInfo:nil repeats:YES];
	
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
	[super dealloc];
	
	[airBall release];
	[playerPaddle release];
	[computerPaddle release];
	[playerScoreText release];
	[computerScoreText release];

	 }
	 

@end
