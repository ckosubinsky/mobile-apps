//
//  MainViewController.m
//  animategame
//
//  Created by Tin Aung Khine on 10/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"

@implementation MainViewController

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self startSound];//loads the theme music and other game sounds
    [self setupArrays];//loads the images into the arrays
    
    health = 1.0;
    score = 0;
    animationCount = 0;
    
    // 4 different swipe events to move the sprite
    UISwipeGestureRecognizer *oneFingerSwipeUp = 
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveSpriteTop:)] autorelease];
    [oneFingerSwipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [[self view] addGestureRecognizer:oneFingerSwipeUp];
    
    UISwipeGestureRecognizer *oneFingerSwipeDown = 
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveSpriteBottom:)] autorelease];
    [oneFingerSwipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [[self view] addGestureRecognizer:oneFingerSwipeDown];
    
    UISwipeGestureRecognizer *oneFingerSwipeLeft = 
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveSpriteLeft:)] autorelease];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [[self view] addGestureRecognizer:oneFingerSwipeLeft];
    
    UISwipeGestureRecognizer *oneFingerSwipeRight = 
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveSpriteRight:)] autorelease];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:oneFingerSwipeRight];
    
    [self startTimers];
}

- (void) startTimers{
    
    leftTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(moveLeft) userInfo:nil repeats:YES] retain];
    [self moveLeft];
    [bombLeft startAnimating];
    
    rightTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(moveRight) userInfo:nil repeats:YES] retain];
    [self moveRight];
    [bombRight startAnimating];
    
    //    topTimer = [[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(moveTop) userInfo:nil repeats:YES] retain];
    //    [self moveTop];
    //    [coin startAnimating];
    
    bottomTimer = [[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(moveBottom) userInfo:nil repeats:YES] retain];
    [self moveBottom];
    [coin startAnimating];
    
    collisionTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(checkCollision) userInfo:nil repeats:YES] retain];
}

- (void) startSound
{
    NSString *songPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/bgmusic.aif"];
    NSError *err;
    themePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:songPath] error:&err];
    themePlayer.currentTime = 0;
    [themePlayer play];
    
    songPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/bomb.aif"];
    hurtSound = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:songPath] error:&err];
    hurtSound.currentTime = 0;
    
    songPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/dying.aif"];
    dyingSound = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:songPath] error:&err];
    dyingSound.currentTime = 0;
    
    songPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/scoring.aif"];
    scoringSound = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:songPath] error:&err];
    scoringSound.currentTime = 0;
    
    songPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/winning.aif"];
    winningSound = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:songPath] error:&err];
    winningSound.currentTime = 0;
    
    songPath = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/losing.aif"];
    losingSound = [[AVAudioPlayer alloc] initWithContentsOfURL: [NSURL fileURLWithPath:songPath] error:&err];
    losingSound.currentTime = 0;
}

- (void) setupArrays {
    
    spriteRightAnim = [[NSMutableArray alloc] init];
    spriteLeftAnim = [[NSMutableArray alloc] init];
    spriteBottomAnim = [[NSMutableArray alloc] init];
    spriteTopAnim = [[NSMutableArray alloc] init];
    bombLeftAnim = [[NSMutableArray alloc] init];
    bombRightAnim = [[NSMutableArray alloc] init];
    coinAnim = [[NSMutableArray alloc] init];
    
    for (int i = 1; i < 5; i++)
    {
        NSString *pic = [NSString stringWithFormat:@"sprite_left_0%d.gif", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [spriteLeftAnim addObject:img];
    }
    
    for (int i = 1; i < 5; i++)
    {
        NSString *pic = [NSString stringWithFormat:@"sprite_right_0%d.gif", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [spriteRightAnim addObject:img];
    }
    
    for (int i = 1; i < 5; i++)
    {
        NSString *pic = [NSString stringWithFormat:@"sprite_top_0%d.gif", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [spriteTopAnim addObject:img];
    }
    
    for (int i = 1; i < 5; i++)
    {
        NSString *pic = [NSString stringWithFormat:@"sprite_bottom_0%d.gif", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [spriteBottomAnim addObject:img];
    }
    
    for (int i = 1; i < 4; i++)
    {
        NSString *pic = [NSString stringWithFormat:@"bomb_0%d.png", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [bombLeftAnim addObject:img];
    }
    
    for (int i = 1; i < 4; i++)
    {
        NSString *pic = [NSString stringWithFormat:@"bomb1_0%d.png", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [bombRightAnim addObject:img];
    }
    
    for (int i = 1; i < 17; i++)
    {
        NSString *pic = [NSString stringWithFormat:@"coin_%d.png", i];
        UIImage *img = [UIImage imageNamed:pic];
        if (img) [coinAnim addObject:img];
    }
    
    //set the default animation for the view - the walk cycle
    [sprite setAnimationImages:spriteLeftAnim];
    [sprite setAnimationDuration:1];
    [sprite startAnimating];
    
    [bombLeft setAnimationImages:bombLeftAnim];
    [bombLeft setAnimationDuration:1];
    [bombLeft startAnimating];
    
    [bombRight setAnimationImages:bombRightAnim];
    [bombRight setAnimationDuration:1];
    [bombRight startAnimating];
    
    [coin setAnimationImages:coinAnim];
    [coin setAnimationDuration:1];
    [coin startAnimating];
}

- (void) moveSpriteLeft: (UISwipeGestureRecognizer *)recognizer
{
    if (sprite.frame.origin.x > GRID_SIZE) 
    {
        [sprite setAnimationImages:spriteLeftAnim];
        [sprite setAnimationDuration:1];
        [sprite startAnimating];
        
        sprite.frame = CGRectMake(sprite.frame.origin.x, sprite.frame.origin.y, SPRITE_WIDTH, SPRITE_HEIGHT);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1];
        [UIView setAnimationDelegate:self];
        sprite.frame = CGRectMake(sprite.frame.origin.x - GRID_SIZE, sprite.frame.origin.y, SPRITE_WIDTH, SPRITE_HEIGHT);
        [UIView commitAnimations];
    }
}

- (void) moveSpriteRight: (UISwipeGestureRecognizer *)recognizer
{
    if (sprite.frame.origin.x < SCREEN_WIDTH-GRID_SIZE) 
    {
        [sprite setAnimationImages:spriteRightAnim];
        [sprite setAnimationDuration:1];
        [sprite startAnimating];
        
        sprite.frame = CGRectMake(sprite.frame.origin.x, sprite.frame.origin.y, SPRITE_WIDTH, SPRITE_HEIGHT);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1];
        [UIView setAnimationDelegate:self];
        sprite.frame = CGRectMake(sprite.frame.origin.x + GRID_SIZE, sprite.frame.origin.y, SPRITE_WIDTH, SPRITE_HEIGHT);
        [UIView commitAnimations];
    }
}

- (void) moveSpriteTop: (UISwipeGestureRecognizer *)recognizer
{
    if (sprite.frame.origin.y > GRID_SIZE) 
    {
        [sprite setAnimationImages:spriteTopAnim];
        [sprite setAnimationDuration:1];
        [sprite startAnimating];
        
        sprite.frame = CGRectMake(sprite.frame.origin.x, sprite.frame.origin.y, SPRITE_WIDTH, SPRITE_HEIGHT);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1];
        [UIView setAnimationDelegate:self];
        sprite.frame = CGRectMake(sprite.frame.origin.x, sprite.frame.origin.y - GRID_SIZE, SPRITE_WIDTH, SPRITE_HEIGHT);
        [UIView commitAnimations];
    }
}

- (void) moveSpriteBottom: (UISwipeGestureRecognizer *)recognizer
{
    if (sprite.frame.origin.y < SCREEN_HEIGHT-GRID_SIZE) 
    {
        [sprite setAnimationImages:spriteBottomAnim];
        [sprite setAnimationDuration:1];
        [sprite startAnimating];
        
        sprite.frame = CGRectMake(sprite.frame.origin.x, sprite.frame.origin.y, SPRITE_WIDTH, SPRITE_HEIGHT);
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1];
        [UIView setAnimationDelegate:self];
        sprite.frame = CGRectMake(sprite.frame.origin.x, sprite.frame.origin.y + GRID_SIZE, SPRITE_WIDTH, SPRITE_HEIGHT);
        [UIView commitAnimations];
    }
}

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller 
{
    [self dismissModalViewControllerAnimated:YES];
    themePlayer.currentTime = 0;
    [themePlayer play];
}

- (IBAction)showInfo:(id)sender
{    
    FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
    controller.delegate = self;
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:controller animated:YES];
    [controller release];
}

- (void) moveTop 
{
//    coin.frame=CGRectMake(coin.frame.origin.x,coin.frame.origin.y,SPRITE_WIDTH,SPRITE_HEIGHT);
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDelegate:self];
//    coin.frame=CGRectMake(coin.frame.origin.x,coin.frame.origin.y - 2,SPRITE_WIDTH,SPRITE_HEIGHT);
//    [UIView commitAnimations];
//    
//    if(coin.frame.origin.y < 0) 
//    {
//        int ran = arc4random()%430;
//        coin.frame=CGRectMake(ran,250,SPRITE_WIDTH,SPRITE_HEIGHT);
//    }
}

- (void) moveBottom 
{
    [self animate];
    
    coin.hidden = false;
    
    coin.frame=CGRectMake(coin.frame.origin.x,coin.frame.origin.y,SPRITE_WIDTH,SPRITE_HEIGHT);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    coin.frame=CGRectMake(coin.frame.origin.x,coin.frame.origin.y + 2,SPRITE_WIDTH,SPRITE_HEIGHT);
    [UIView commitAnimations];
    
    if(coin.frame.origin.y > SCREEN_HEIGHT) 
    {
        int ran = arc4random()%8;
        coin.frame=CGRectMake(ran*GRID_SIZE,-GRID_SIZE,SPRITE_WIDTH,SPRITE_HEIGHT);
    }
}

- (void) moveLeft 
{
    bombLeft.hidden = false;
    
    bombLeft.frame=CGRectMake(bombLeft.frame.origin.x,bombLeft.frame.origin.y,SPRITE_WIDTH,SPRITE_HEIGHT);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    bombLeft.frame=CGRectMake(bombLeft.frame.origin.x - 2,bombLeft.frame.origin.y,SPRITE_WIDTH,SPRITE_HEIGHT);
    [UIView commitAnimations];
    
    if(bombLeft.frame.origin.x < 0) 
    {
        int ran = arc4random()%5;
        bombLeft.frame=CGRectMake(SCREEN_WIDTH+GRID_SIZE,ran*GRID_SIZE,SPRITE_WIDTH,SPRITE_HEIGHT);
    }
}

- (void) moveRight 
{
    bombRight.hidden = false;

    bombRight.frame=CGRectMake(bombRight.frame.origin.x,bombRight.frame.origin.y,SPRITE_WIDTH,SPRITE_HEIGHT);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    bombRight.frame=CGRectMake(bombRight.frame.origin.x + 2,bombRight.frame.origin.y,SPRITE_WIDTH,SPRITE_HEIGHT);
    [UIView commitAnimations];
    if(bombRight.frame.origin.x > SCREEN_WIDTH) 
    {
        int ran = arc4random()%5;
        bombRight.frame=CGRectMake(-GRID_SIZE,ran*GRID_SIZE,SPRITE_WIDTH,SPRITE_HEIGHT);
    }
}

- (void)animate
{
    if (animationCount < 5)
    {
        [UIView animateWithDuration:0.5 
                         animations:^{
                             sprite.alpha = 1.0;
                         }];
    }
}

- (void)animateBack
{
    [UIView animateWithDuration:0.5 
                     animations:^{
                         sprite.alpha = 0.3;
                     }];
    [hurtSound stop];
    [scoringSound stop];
    [winningSound stop];
    [losingSound stop];

}


// checks for collision and takes necessary action when a collision is detected
- (void) checkCollision{
    
    if(CGRectIntersectsRect(sprite.frame, bombLeft.frame))
    {
        bombLeft.hidden = true;
        int ran = arc4random()%5;
        bombLeft.frame=CGRectMake(SCREEN_WIDTH+GRID_SIZE,ran*GRID_SIZE,SPRITE_WIDTH,SPRITE_HEIGHT); 
        health = health - 0.1;
        healthBar.progress = health;
        [self animateBack];
        [hurtSound play];
    }
    if(CGRectIntersectsRect(sprite.frame, bombRight.frame))
    {
        bombRight.hidden = true;
        int ran = arc4random()%5;
        bombRight.frame=CGRectMake(-GRID_SIZE,ran*GRID_SIZE,SPRITE_WIDTH,SPRITE_HEIGHT);  
        health = health - 0.1;
        healthBar.progress = health;
        [self animateBack];
        [hurtSound play];
    }
    if(CGRectIntersectsRect(sprite.frame, coin.frame))
    {
        coin.hidden = true;
        int ran = arc4random()%8;
        coin.frame=CGRectMake(ran*GRID_SIZE,-GRID_SIZE,SPRITE_WIDTH,SPRITE_HEIGHT);
        score = score + 0.1;
        scoreBar.progress = score;
        [scoringSound play];
    }
    if (health <= 0.0) 
    {
        // stop playing the theme song
        [themePlayer stop];
        [losingSound play];
        // flip the view
        FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
        controller.delegate = self;controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;[self presentModalViewController:controller animated:YES];
        [controller release];
        [self animateBack];
    }
    if(score >= 1.0)
    {
        // stop playing the theme song
        [themePlayer stop];
        [winningSound play];
   
        // flip the view
        FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
        controller.delegate = self;controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;[self presentModalViewController:controller animated:YES];
        [controller release];
        [self animateBack];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations.
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
