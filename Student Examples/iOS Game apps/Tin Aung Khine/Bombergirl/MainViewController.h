//
//  MainViewController.h
//  animategame
//
//  Created by Tin Aung Khine on 10/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define SPRITE_WIDTH 56 
#define SPRITE_HEIGHT 56
#define GRID_SIZE 60
#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 300

#import "FlipsideViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> {
    
    float health;       // for keeping track of sprite's health
    float score;          // for counting number of coins gained
    int animationCount; // for flashing/blinking when sprite gets hit
    
    NSMutableArray *spriteLeftAnim;
    NSMutableArray *spriteRightAnim;
    NSMutableArray *spriteTopAnim;
    NSMutableArray *spriteBottomAnim;
    NSMutableArray *bombLeftAnim;
    NSMutableArray *bombRightAnim;
    NSMutableArray *coinAnim;
    
    IBOutlet UIImageView *sprite;
    IBOutlet UIImageView *bombLeft;
    IBOutlet UIImageView *bombRight;
    IBOutlet UIImageView *coin;
    IBOutlet UIProgressView *healthBar;
    IBOutlet UIProgressView *scoreBar;

    NSTimer *collisionTimer;
    NSTimer *topTimer;
    NSTimer *bottomTimer;
    NSTimer *leftTimer;
    NSTimer *rightTimer;
    
    AVAudioPlayer *themePlayer;
    AVAudioPlayer *hurtSound;
    AVAudioPlayer *dyingSound;
    AVAudioPlayer *scoringSound;
    AVAudioPlayer *winningSound;
    AVAudioPlayer *losingSound;

}

// custom methods
- (void) moveSpriteTop: (UISwipeGestureRecognizer *)recognizer;
- (void) moveSpriteBottom: (UISwipeGestureRecognizer *)recognizer;
- (void) moveSpriteLeft: (UISwipeGestureRecognizer *)recognizer;
- (void) moveSpriteRight: (UISwipeGestureRecognizer *)recognizer;
- (void) moveTop;
- (void) moveBottom;
- (void) moveLeft;
- (void) moveRight;
- (void) startSound;
- (void) startTimers;
- (void) setupArrays;
- (void) animate;
- (void) animateBack;
- (IBAction)showInfo:(id)sender;

@end
