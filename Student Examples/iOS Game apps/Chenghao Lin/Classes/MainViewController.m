//
//  MainViewController.m
//  AirCraft
//
//  Created by chenghao lin on 10/19/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController
@synthesize count,label,aircraft1,aircraft2,aircraft3,missile,collisionTimer,enemyTimer1,enemyTimer2,missileTimer;



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	[self startTimmers];
}



- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
}


- (IBAction)showInfo:(id)sender {    
	
	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
- (void)startTimmers{
	enemyTimer1=[[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(moveEnemy) userInfo:nil repeats:YES] retain];
	enemyTimer2=[[NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(moveEnemy2) userInfo:nil repeats:YES] retain];
	collisionTimer=[[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkCollision) userInfo:nil repeats:YES]retain];
}
					 
- (void)checkCollision{

	if (CGRectIntersectsRect(aircraft1.frame, missile.frame) ){
		aircraft1.frame=CGRectMake(-200, 0, 76, 89);
		count=count+1;
		self.label.text=[[NSString alloc]initWithFormat:@"Points:%d",count];
	}else {
			
	}

	
	
	
	if (CGRectIntersectsRect(aircraft2.frame, missile.frame) ){
		aircraft2.frame=CGRectMake(-200, 80, 76, 89);
		count=count+1;
		self.label.text=[[NSString alloc]initWithFormat:@"Points:%d",count];
	}else {

	}


}

- (void)moveSelf{}
- (void)moveEnemy{
	aircraft1.frame=CGRectMake(aircraft1.frame.origin.x, 0, 76, 89);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	aircraft1.frame=CGRectMake(aircraft1.frame.origin.x+1.6,0,76,89);
	[UIView commitAnimations];
	if(aircraft1.frame.origin.x >396){
		aircraft1.frame=CGRectMake(-76,0,76,89);
	}
}
- (void)moveEnemy2{
	
	aircraft2.frame=CGRectMake(aircraft2.frame.origin.x,80,76, 89);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	aircraft2.frame=CGRectMake(aircraft2.frame.origin.x+2,80,76,89);
	[UIView commitAnimations];
	
	if(aircraft2.frame.origin.x >396){
		aircraft2.frame=CGRectMake(-76,80,76,89);
	}
}
- (void)EnemyFire{}
- (IBAction)fire{
	if(missileTimer==nil){
	missile.frame=CGRectMake(aircraft3.center.x-8, aircraft3.center.y-20, 17, 56);
	missileTimer=[[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(fireMissile) userInfo:nil repeats:YES]retain];

	}
}
- (void)fireMissile{
	missile.frame= CGRectMake(missile.frame.origin.x, missile.frame.origin.y, 17, 56);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	missile.frame= CGRectMake(missile.frame.origin.x, missile.frame.origin.y-2, 17, 56);
	[UIView commitAnimations];
	if (missile.frame.origin.y<-56) {
		[missileTimer invalidate];
		self.missileTimer=nil;
		NSLog(@"timerDie");
	}
}

- (void)dealloc {
    [super dealloc];
}


@end
