//
//  MainViewController.h
//  AirCraft
//
//  Created by chenghao lin on 10/19/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> {
	IBOutlet UIImageView *aircraft1;
	IBOutlet UIImageView *aircraft2;
	IBOutlet UIImageView *aircraft3;
	IBOutlet UIImageView *missile;
	NSTimer *collisionTimer;
	NSTimer *enemyTimer1;
	NSTimer *enemyTimer2;
	NSTimer *missileTimer;
	IBOutlet UILabel *label;
	int *count;
}
@property (retain)UIImageView *aircraft1;
@property (retain)UIImageView *aircraft2;
@property (retain)UIImageView *aircraft3;
@property (retain)UIImageView *missile;
@property (retain)NSTimer *collisionTimer;
@property (retain)NSTimer *enemyTimer1;
@property (retain)NSTimer *enemyTimer2;
@property (retain)NSTimer *missileTimer;
@property (retain)UILabel *label;
@property (assign)int *count;

- (void)fireMissile;
- (void)checkCollision;
- (void)startTimmers;
- (void)moveSelf;
- (void)moveEnemy;
- (void)moveEnemy2;
- (void)EnemyFire;
- (IBAction)fire;
- (IBAction)showInfo:(id)sender;

@end
