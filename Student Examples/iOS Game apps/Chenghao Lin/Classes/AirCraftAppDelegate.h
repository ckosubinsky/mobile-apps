//
//  AirCraftAppDelegate.h
//  AirCraft
//
//  Created by chenghao lin on 10/19/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface AirCraftAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    MainViewController *mainViewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet MainViewController *mainViewController;

@end

