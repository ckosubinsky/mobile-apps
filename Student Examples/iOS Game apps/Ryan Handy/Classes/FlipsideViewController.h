//
//  FlipsideViewController.h
//  R_Handy_iPhone_1
//
//  Created by Ryan Handy on 10/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<AVFoundation/AVAudioPlayer.h>

@protocol FlipsideViewControllerDelegate;


@interface FlipsideViewController : UIViewController {
	id <FlipsideViewControllerDelegate> delegate;
	IBOutlet UIImageView *player1;
	IBOutlet UIImageView *enemy;
	IBOutlet UINavigationBar *navBar;
	NSMutableArray *attacking;
	NSMutableArray *enemyAttacking;
	NSTimer *attackTimer;
	NSInteger count;
	NSInteger playerHealth;
	NSInteger playerMax;
	NSInteger enemyMax;
	NSInteger enemyHealth;
	NSInteger barWidth;
	IBOutlet UIProgressView *playerHealthBar;
	IBOutlet UIProgressView *enemyHealthBar;
	IBOutlet UILabel *playerLabel;
	IBOutlet UILabel *enemyLabel;
	IBOutlet UILabel *playerHealthLabel;
	AVAudioPlayer *battleMusic;
	AVAudioPlayer *winMusic;
    AVAudioPlayer *loseMusic;
    NSTimer *endGameTimer;
}

@property (nonatomic, retain) UIImageView *player1;
@property (nonatomic, retain) UIImageView *enemy;
@property (nonatomic, retain) UINavigationBar *navBar;
@property (nonatomic, retain) NSMutableArray *attacking;
@property (nonatomic, retain) NSMutableArray *enemyAttacking;
@property (nonatomic, assign) id <FlipsideViewControllerDelegate> delegate;
@property (nonatomic, retain) UIProgressView *playerHealthBar;
@property (nonatomic, retain) UIProgressView *enemyHealthBar;
@property (nonatomic, retain) NSTimer *endGameTimer;
- (IBAction) attack;
- (void) displayInfo;
- (IBAction)done:(id)sender;
- (void)end;
- (void)setHealth:(NSInteger)health;
@end


@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;



@end

