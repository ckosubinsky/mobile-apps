//
//  MainViewController.h
//  R_Handy_iPhone_1
//
//  Created by Ryan Handy on 10/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"
#import<AVFoundation/AVAudioPlayer.h>


@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> {
	IBOutlet UIImageView *player;
	IBOutlet UIImageView *enemy;
	NSInteger enemyDirection;
    NSInteger direction;
    CGPoint endPoint;
	NSMutableArray *walkUp;
	NSMutableArray *walkDown;
	NSMutableArray *walkLeft;
	NSMutableArray *walkRight;
	NSMutableArray *sephUp;
	NSMutableArray *sephDown;
	NSMutableArray *sephLeft;
	NSMutableArray *sephRight;
	NSTimer *walkTimer;
	NSTimer *enemyTimer;
	NSInteger count;
	NSInteger playerHealth;
	
	AVAudioPlayer *battleMusic;
	AVAudioPlayer *bkgdMusic;
}

@property (nonatomic, retain) UIImageView *player;
@property (nonatomic, retain) UIImageView *enemy;
@property (nonatomic, retain) UILabel *positionLabel;
@property (nonatomic, retain) NSMutableArray *walkUp;
@property (nonatomic, retain) NSMutableArray *walkDown;
@property (nonatomic, retain) NSMutableArray *walkLeft;
@property (nonatomic, retain) NSMutableArray *walkRight;
@property (nonatomic, retain) NSMutableArray *sephUp;
@property (nonatomic, retain) NSMutableArray *sephDown;
@property (nonatomic, retain) NSMutableArray *sephLeft;
@property (nonatomic, retain) NSMutableArray *sephRight;

- (IBAction)showInfo:(id)sender;
- (void) increaseHealth;
- (void) walk;
- (void) moveEnemy;

@end
