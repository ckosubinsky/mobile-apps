//
//  FlipsideViewController.m
//  R_Handy_iPhone_1
//
//  Created by Ryan Handy on 10/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"


@implementation FlipsideViewController

@synthesize delegate;
@synthesize player1;
@synthesize navBar;
@synthesize attacking;
@synthesize enemy;
@synthesize enemyAttacking;
@synthesize playerHealthBar;
@synthesize enemyHealthBar;
@synthesize endGameTimer;


- (void)viewDidLoad {
    [super viewDidLoad];
    //if the endGameTimer was running previously (not sure why it would be) invalidate it
	[endGameTimer invalidate];
    
    //set player and enemy healths (to be overwritten in setHealth)
	playerHealth = 100;
	enemyHealth = 100;
    
    //load battle music, win music, and lose music (win and lose music loaded now to avoid any
    //lag when the player wins or loses
	NSString *path = [[NSBundle mainBundle] pathForResource:@"battle" ofType:@"m4a"];    
	battleMusic=[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];        
	//battleMusic.delegate=self;
	path = [[NSBundle mainBundle] pathForResource:@"win" ofType:@"m4a"];
	winMusic=[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
	//winMusic.delegate=self;
    path = [[NSBundle mainBundle] pathForResource:@"lose" ofType:@"m4a"];
    loseMusic=[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];
    //loseMusic.delegate=self;
    
    //play the battle music
	[battleMusic play];
	
    //Count is used to keep track of who is attacking when the player taps on the main character
	count = 0;
    
    //Set the player and enemy healthbars to full health
	[playerHealthBar setProgress:1.0];
	[enemyHealthBar setProgress:1.0];
    
    //Use a timer to make the healthbars and labels for player health and both characters' names appear
    //when the player and enemy have moved to their final location on the screen
	[NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(displayInfo) userInfo:nil repeats:YES];
    
    //make all labels and healthbars invisible until the timer is called a second time
	[playerLabel setAlpha:0.0];
	[enemyLabel setAlpha:0.0];
	[playerHealthBar setAlpha:0.0];
	[enemyHealthBar setAlpha:0.0];
	[playerHealthLabel setAlpha:0.0];
    
    //Inform the player via UINavigationBar that they are in a battle and that it's a big deal
    //(hence the "OMG")
	navBar.topItem.title = @"OMG A Battle!";
	
    //Move the player and enemy from their offscreen start locations to the bottom left and top
    //right corners (respectively)
    player1.frame = CGRectMake(320, 340, 120, 120);
	enemy.frame = CGRectMake(-180, 44, 180, 180);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:2];
	[UIView setAnimationDelegate:self];
	player1.frame = CGRectMake(0, 340, 120, 120);
	enemy.frame = CGRectMake(140, 44, 180, 180);
	[UIView commitAnimations];
	
    //Set attacking images for both the player and the enemy (not documented due to how
    //often it's been covered in class)
	attacking = [[NSMutableArray alloc]init];
	enemyAttacking = [[NSMutableArray alloc]init];
	for(int i=1; i<5; i++)
	{
		NSString *pic=[NSString stringWithFormat:@"attack%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[attacking addObject:img];
		
		pic = [NSString stringWithFormat:@"seph_attack%d.png", i];
		img = [UIImage imageNamed:pic];
		if (img)[enemyAttacking addObject:img];
		
	}
	
	
	
}

/*
 displayInfo makes all healthbars and labels visible (for when the player and enemy
 are in their final locations)
*/
- (void) displayInfo {
	[playerLabel setAlpha:1.0];
	[enemyLabel setAlpha:1.0];
	[playerHealthBar setAlpha:1.0];
	[enemyHealthBar setAlpha:1.0];
	[playerHealthLabel setAlpha:1.0];
    
    //Specifically tell the player how to attack once the
    //player and enemy are in their final locations
	if([navBar.topItem.title isEqualToString:@"OMG A Battle!"])
	{
		navBar.topItem.title = @"Tap Ash to Attack!";
	}
}

/*
 attack is called when the player's image is touched (via custom button)
*/
- (IBAction) attack {
    
    //Make sure the player is not already attacking
    //If the player has not attacked yet, the count would be 0
    //otherwise, the player and enemy are finished attacking when
    //the count is above 2
	if((count > 2 || count == 0)&&[playerHealthBar alpha]>0.0)
	{
        //You also can't attack if you or the enemy have already lost
        if(playerHealth > 0 && enemyHealth > 0)
        {
            //restart the counter
            count = 0;
            //restart the attack timer
            [attackTimer invalidate];
            
            //set the player's animation to attack
            [player1 setAnimationImages:attacking];
            [player1 setAnimationDuration:.5];
            
            //AttackTimer is used to decrease health when the player or enemy are attacked and
            //to change animations between player and enemy attacking
            attackTimer = [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(stopAttacking) userInfo:nil repeats:YES];
            
            //Inform the user they are attacking
			navBar.topItem.title = @"You Attack!";
            
            //animate the player's attack because the first loop from the attackTimer
            //would have stopped that animation
            [player1 startAnimating];
        }
	}

}

- (void) stopAttacking {
	[player1 stopAnimating];
	count ++;
    //when the player is done animating count is 1
	if (count == 1) {
        //find out how much you've hurt the enemy
		NSInteger damage = arc4random()%15;
        
        //remove that from the enemy's health
		enemyHealth -= damage;
        
        //if you didn't hurt the enemy it's because you missed
		if(damage == 0)
			navBar.topItem.title = @"You Missed!";
		else
			navBar.topItem.title = [NSString stringWithFormat:@"You did %d damage!",damage];
        
        //if the enemy has no more health you win
		if(enemyHealth <= 0)
		{
            //tell the player they won
			navBar.topItem.title = @"You Win!";
            
            //set the enemy's health to 0 (for later when increasing the player's health
			enemyHealth=0;
            
            //stop the battle music and play the win music
			[battleMusic stop];
			[winMusic play];
            
            //set count to 0 for the endGameTimer
			count = 0;
            
            //stop using the attackTimer (since no one can attack)
            [attackTimer invalidate];
            
            //let the endGameTimer end call end (which will switch back to MainView when
            //the win music has ended
			endGameTimer = [NSTimer scheduledTimerWithTimeInterval:3.5 target:self selector:@selector(end) userInfo:nil repeats:YES];
		}
        //If the enemy is not dead, use a float to avoid integer division when setting the
        //enemy's health bar to reflect his current health
		float tempHealth = enemyHealth;
		[enemyHealthBar setProgress:tempHealth/enemyMax];
		if(enemyHealth == 0)count = 4;
	}
	else if(count == 2)//The enemy starts attacking when count is 2
	{
        //set the enemy's animation to attack
		[enemy setAnimationImages:enemyAttacking];
		[enemy setAnimationDuration:.5];
		[enemy startAnimating];
        
        //use the UINavigationBar to tell the player the enemy is attacking
		navBar.topItem.title = @"Sephiroth Attacks!";
	}
	else if(count == 3)//The enemy has finished attacking when count is 3
	{
        //Find out how much damage has been done
		NSInteger damage = arc4random()%15;
        
        //set the player's health to reflect that change
		playerHealth -= damage;
        
        //if the enemy did no damage it is because he missed
		if(damage == 0)
			navBar.topItem.title = @"Sephiroth Missed!";
		else
			navBar.topItem.title = [NSString stringWithFormat:@"Sephiroth did %d damage!",damage];
        
        //If the player has no health left the player loses
		if(playerHealth <= 0)
		{
            //Inform the player that the battle has ended in a loss
			navBar.topItem.title = @"You Lose!";
            
            //set the player's health to 0 (in case it was below 0)
			playerHealth = 0;
            
            //Stop the battle music and start the lose music
			[battleMusic stop];
            [loseMusic play];
            
            //We no longer need the attack timer
            [attackTimer invalidate];
            
            //count becomes 0 for the endGameTimer
            count = 0;
            
            //endGameTimer is used to switch back to MainView when the loseMusic ends
            endGameTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(end) userInfo:nil repeats:YES];
		}
        //If the player is still alive, use a float to avoid integer division when setting the
        //healthbar to reflect the player's health
		float tempHealth = playerHealth;
		[playerHealthBar setProgress:tempHealth/playerMax];
        
        //Let the player see exactly how much health they have left
		[playerHealthLabel setText:[NSString stringWithFormat:@"HP: %d/%d",playerHealth,playerMax]];
        
        //The enemy is done attacking, so we stop animating the attack
		[enemy stopAnimating];
	}
	
}


/*
 end is used to switch back to MainView when the player wins/loses
*/
- (void) end {
    //increment count to end on the second run
    count++;
    
    //when count is greater than one and one of the players is dead
    //we can switch back to MainView
    if(count > 1 && (playerHealth == 0 || enemyHealth == 0))
    {
        [self done:self];
    }
}

/*
 done is used to switch back to MainView
*/
- (IBAction)done:(id)sender {
	count ++;//not sure if that's necessary anymore, but afraid to delete it...
    
    //again, not really sure what I was doing with that but I think it was for fleeing
	if(playerHealth == 100 || count > 1)
	{
        //when we leave, the battle music must stop
		[battleMusic stop];
        
        //endGameTimer (if used) must stop
        [endGameTimer invalidate];
        
        //If the enemy lost, increase the player's health by 1 (the value is stored
        //in MainView and passed back to FlipsideView in MainView's showInfo method
		if(enemyHealth == 0)
			[self.delegate increaseHealth];
        
        //flipsideViewControllerDidFinish is used to switch back to MainView
		[self.delegate flipsideViewControllerDidFinish:self];
	}
}

/*
 setHealth is called from MainView to set the player's max health
*/
- (void)setHealth:(NSInteger)health {
    //set the player's maxhealth and currenthealth to the passed value
	playerHealth = health;
	playerMax = health;
    
    //set the player's health label to reflect the player's current health
	[playerHealthLabel setText:[NSString stringWithFormat:@"HP: %d/%d",playerHealth,playerMax]];
    
    //set the enemys health somewhere from 100 to the player's health
	enemyMax = (arc4random()%(playerMax - 99))+100;
	enemyHealth = enemyMax;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
    [super dealloc];
}


@end
