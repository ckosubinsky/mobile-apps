//
//  MainViewController.m
//  R_Handy_iPhone_1
//
//  Created by Ryan Handy on 10/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController

@synthesize player;
@synthesize enemy;
@synthesize positionLabel;
@synthesize walkUp;
@synthesize walkDown;
@synthesize walkLeft;
@synthesize walkRight;
@synthesize sephUp;
@synthesize sephDown;
@synthesize sephLeft;
@synthesize sephRight;

/*
 In the touchesBegan method, the destination point for the player to walkt towards is set 
 to the point touched on the screen
 */
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
    //Obtain touch
    UITouch *myTouch = [[event allTouches] anyObject];
    
    //Turn the touch into a location (CGPoint)
    CGPoint touchPoint = [myTouch locationInView:self.view];
    //touchPoint.x -= 25;
    //touchPoint.y -= 25;
    
    //Set the player's destination to the point that has been touched
    //(This can be changed at any point as the player is walking
    endPoint = touchPoint;
}

/*
 The walk method is used to move the player a small distance during each loop of the walkTimer object
 The player will always walk vertically, then horizontally
 */
- (void) walk{
    //Check if the player needs to move downwards
	if (endPoint.y > player.center.y)
    {
        //Move the player's UIImageView on the screen downwards by 2 pixels
        player.frame = CGRectMake(player.frame.origin.x, player.frame.origin.y, 50, 50);
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:.2];
		[UIView setAnimationDelegate:self];
		player.frame = CGRectMake(player.frame.origin.x, player.frame.origin.y+2, 50, 50);
		[UIView commitAnimations];
        //If we are below the destination y location, move the player to the destination's y location
        //so that the player can begin moving horizontally
        if(player.center.y > endPoint.y)
        {
            endPoint.y = player.center.y;
        }
        //If the player's character on screen was not walking down in the previous call to walk
        //set the player's animation to make the player walk down. Inform the system that the player
        //is now walking down by setting his direction to 0 (which is the down direction indicator)
        if(direction != 0)
        {
            [player setAnimationImages:walkDown];
            [player setAnimationDuration:.75];
            [player startAnimating];
            direction = 0;
        }
    }
    //Check if the player needs to move upwards
    else if(endPoint.y < player.center.y)
    {
        //Move the player's UIImageView on the screen upwards by 2 pixels
        player.frame = CGRectMake(player.frame.origin.x, player.frame.origin.y, 50, 50);
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:.2];
		[UIView setAnimationDelegate:self];
		player.frame = CGRectMake(player.frame.origin.x, player.frame.origin.y-2, 50, 50);
		[UIView commitAnimations];
        //If we are above the destination y location, move the player to the destination's y location
        //so that the player can begin moving horizontally
        if(player.center.y < endPoint.y)
        {
            endPoint.y = player.center.y;
        }
        //If the player's character on screen was not walking up in the previous call to walk
        //set the player's animation to make the player walk up. Inform the system that the player
        //is now walking up by setting his direction to 1 (which is the up direction indicator)
        if(direction != 1)
        {
            [player setAnimationImages:walkUp];
            [player setAnimationDuration:.75];
            [player startAnimating];
            direction = 1;
        }
    }
    //Check if the player needs to move right
    else if(endPoint.x > player.center.x)
    {
        //Move the player's UIImageView on the screen right by 2 pixels
        player.frame = CGRectMake(player.frame.origin.x, player.frame.origin.y, 50, 50);
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:.2];
		[UIView setAnimationDelegate:self];
		player.frame = CGRectMake(player.frame.origin.x+2, player.frame.origin.y, 50, 50);
		[UIView commitAnimations];
        //If we are to the right of the destination x location, move the player to the destination's
        //x location so that the player can finish moving
        if(player.center.x > endPoint.x)
        {
            endPoint.x = player.center.x;
        }
        //If the player's character on the screen was not walking right in the previous call to walk
        //set the player's animation to make the player walk right. Inform the system that the player
        //is now walking right by setting his direction to 2 (which is the right direction indicator)
        if(direction != 2)
        {
            [player setAnimationImages:walkRight];
            [player setAnimationDuration:.75];
            [player startAnimating];
            direction = 2;
        }
    }
    //Check if the player needs to move left
    else if(endPoint.x < player.center.x)
    {
        //Move the player's UIImageView on the screen left by 2 pixels
        player.frame = CGRectMake(player.frame.origin.x, player.frame.origin.y, 50, 50);
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:.2];
		[UIView setAnimationDelegate:self];
		player.frame = CGRectMake(player.frame.origin.x-2, player.frame.origin.y, 50, 50);
		[UIView commitAnimations];
        //If we are to the left of the destination x location, move the player to the destination's
        //x location so that the player can finish moving
        if(player.center.x < endPoint.x)
        {
            endPoint.x = player.center.x;
        }
        //If the player's character on the screen was not walking left in the previous call to walk
        //set the player's animation to make the player walk left. Inform the system that the player
        //is now walking left by setting his direction to 3 (which is the left direction indicator)
        if(direction != 3)
        {
            [player setAnimationImages:walkLeft];
            [player setAnimationDuration:.75];
            [player startAnimating];
            direction = 3;
        }
    }
    //If the player is at the destination location
    else
    {
        //Stop animating
        [player stopAnimating];
        //Set the direction to 6 to indicate that the player is not animating
        //so the player can begin animating no matter what direction the player
        //is to move in when the screen is touched
        direction = 6;
    }
	
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	count = 0;//set count to 0 so the enemy can begin walking as soon as the enemy's timer is instantiated
	playerHealth = 100;//playerHealth begins at 100 and is incremented every time the player wins
    direction = 6;//Set the player's direction to 6 so the player can begin a walk animation when 
                  //the screen is touched
    endPoint = player.center;//Set the player's destination location to where the player is standing so the
                             //player is not walking anywhere at the start of the game
    
    //Set up player and enemy animations (we've covered this a lot in class, so I'm not documenting it further
	walkUp = [[NSMutableArray alloc]init];
	walkDown = [[NSMutableArray alloc]init];
	walkLeft = [[NSMutableArray alloc]init];
	walkRight = [[NSMutableArray alloc]init];
	sephUp = [[NSMutableArray alloc]init];
	sephDown = [[NSMutableArray alloc]init];
	sephLeft = [[NSMutableArray alloc]init];
	sephRight = [[NSMutableArray alloc]init];
	for(int i=1; i<5; i++)
	{
		NSString *pic=[NSString stringWithFormat:@"walkUp%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[walkUp addObject:img];
		
		pic = [NSString stringWithFormat:@"walkDown%d.png", i];
		img = [UIImage imageNamed:pic];
		if (img)[walkDown addObject:img];
		
		pic = [NSString stringWithFormat:@"walkLeft%d.png", i];
		img = [UIImage imageNamed:pic];
		if (img)[walkLeft addObject:img];
		
		pic = [NSString stringWithFormat:@"walkRight%d.png", i];
		img = [UIImage imageNamed:pic];
		if (img)[walkRight addObject:img];
		
		pic = [NSString stringWithFormat:@"sephUp%d.png", i];
		img = [UIImage imageNamed:pic];
		if (img)[sephUp addObject:img];
		
		pic = [NSString stringWithFormat:@"sephDown%d.png", i];
		img = [UIImage imageNamed:pic];
		if (img)[sephDown addObject:img];
		
		pic = [NSString stringWithFormat:@"sephRight%d.png", i];
		img = [UIImage imageNamed:pic];
		if (img)[sephRight addObject:img];
		
		pic = [NSString stringWithFormat:@"sephLeft%d.png", i];
		img = [UIImage imageNamed:pic];
		if (img)[sephLeft addObject:img];
	}
    //Load the background music and play it
    NSString *path = [[NSBundle mainBundle] pathForResource:@"background" ofType:@"m4a"];    
	bkgdMusic=[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL]; 
	[bkgdMusic play];
    
    //Set the player's walk timer and the enemy's walk timer. Make the enemy a little slower than the player
    walkTimer = [NSTimer scheduledTimerWithTimeInterval:.03 target:self selector:@selector(walk) userInfo:nil repeats:YES];
	enemyTimer = [NSTimer scheduledTimerWithTimeInterval:.05 target:self selector:@selector(moveEnemy) userInfo:nil repeats:YES];
}

/*
 moveEnemy is used to move the enemy in a random direction every 2 seconds
*/
- (void) moveEnemy {
    //increment count (so you can keep track of steps taken
	count ++;
    //If the enemy is not moving for any reason or the counter is reset
	if(count == 1 || enemyDirection == 5)
	{
        //give the enemy a random direction and set his walk animation to
        //correspond to his direction
		enemyDirection = arc4random()%4;
		if(enemyDirection == 0)//0 = up direction
		{
			[enemy setAnimationImages:sephUp];
            [enemy setAnimationDuration:.75];
            [enemy startAnimating];
		}
		else if(enemyDirection == 1)//1 = down direction
		{
			[enemy setAnimationImages:sephDown];
            [enemy setAnimationDuration:.75];
            [enemy startAnimating];
		}
		else if(enemyDirection == 2)//2 = left direction
		{
			[enemy setAnimationImages:sephLeft];
            [enemy setAnimationDuration:.75];
            [enemy startAnimating];
		}
		else if(enemyDirection == 3)//3 = right direction
		{
			[enemy setAnimationImages:sephRight];
            [enemy setAnimationDuration:.75];
            [enemy startAnimating];
		}
	}
    //if the counter has not recently been restarted
	else if(count > 1)
	{
        //If the enemy has walked into a wall or 2 seconds have passed reset count
		if((enemy.frame.origin.x <= 0 && enemyDirection == 2)||
		   (enemy.frame.origin.x >= 320-50 && enemyDirection == 3)||
		   (enemy.frame.origin.y <= 0 && enemyDirection == 0)||
		   (enemy.frame.origin.y >= 480-75 && enemyDirection == 1)||
		   count == 40)
		{
			count = 0;
		}
        //move the enemy 2 pixels in the corresponding direction
		else if(enemyDirection == 0)
		{
			enemy.frame = CGRectMake(enemy.frame.origin.x, enemy.frame.origin.y, 50, 50);
			[UIView beginAnimations:nil context:nil];
			[UIView setAnimationDuration:.2];
			[UIView setAnimationDelegate:self];
			enemy.frame = CGRectMake(enemy.frame.origin.x, enemy.frame.origin.y-2, 50, 50);
			[UIView commitAnimations];
		}
		else if(enemyDirection == 1)
		{
			enemy.frame = CGRectMake(enemy.frame.origin.x, enemy.frame.origin.y, 50, 50);
			[UIView beginAnimations:nil context:nil];
			[UIView setAnimationDuration:.2];
			[UIView setAnimationDelegate:self];
			enemy.frame = CGRectMake(enemy.frame.origin.x, enemy.frame.origin.y+2, 50, 50);
			[UIView commitAnimations];
		}
		else if(enemyDirection == 2)
		{
			enemy.frame = CGRectMake(enemy.frame.origin.x, enemy.frame.origin.y, 50, 50);
			[UIView beginAnimations:nil context:nil];
			[UIView setAnimationDuration:.2];
			[UIView setAnimationDelegate:self];
			enemy.frame = CGRectMake(enemy.frame.origin.x-2, enemy.frame.origin.y, 50, 50);
			[UIView commitAnimations];
		}
		else if(enemyDirection == 3)
		{
			enemy.frame = CGRectMake(enemy.frame.origin.x, enemy.frame.origin.y, 50, 50);
			[UIView beginAnimations:nil context:nil];
			[UIView setAnimationDuration:.2];
			[UIView setAnimationDelegate:self];
			enemy.frame = CGRectMake(enemy.frame.origin.x+2, enemy.frame.origin.y, 50, 50);
			[UIView commitAnimations];
		}
	}
	
    //If the player has walked into the enemy
	if(CGRectIntersectsRect(enemy.frame, player.frame))
	{
        //stop moving the enemy and the player
		endPoint = player.center;
        enemyDirection = 4;
        [enemy stopAnimating];
        //show info is used to change to the battle screen
		[self showInfo:self];
	}
	
}

/*
 flipsideViewControllerDidFinish is used when a battle has ended (either by win, loss, or fleeing)
*/
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
    //If the player is low on the screen move the enemy to the top (to avoid immediately starting
    //another battle
	if(player.center.y > 300)
	{
		enemy.frame = CGRectMake(enemy.frame.origin.x, enemy.frame.origin.y, 50, 50);
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:.001];
		[UIView setAnimationDelegate:self];
		enemy.frame = CGRectMake(enemy.frame.origin.x, 0, 50, 50);
	}
    //If the player is high on the screen move the enemy to the bottom
	else 
	{
		enemy.frame = CGRectMake(enemy.frame.origin.x, enemy.frame.origin.y, 50, 50);
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:.001];
		[UIView setAnimationDelegate:self];
		enemy.frame = CGRectMake(enemy.frame.origin.x, 410, 50, 50);
	}
	[UIView commitAnimations];
    
    //Set the enemy direction to allow movement in a random direction
    enemyDirection = 5;
    
    //resume playing the background music
    [bkgdMusic play];
}

/*
 showInfo is used to swith to the battle screen (FlipsideView)
*/
- (IBAction)showInfo:(id)sender {    
	
    //Stop background music so the flipside view can play the battle music
    [bkgdMusic stop];
    
    //Apple does all of the switching code for me
	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
	[self presentModalViewController:controller animated:YES];
    
    //Set the player's health (in case it is above the base setting
	[controller setHealth:playerHealth];
	[controller release];
}

/*
 increaseHealth is called from the battle view if the player wins a battle
*/
- (void) increaseHealth {
    //Increase the player's health by 1
	playerHealth += 1;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
    [super dealloc];
}


@end
