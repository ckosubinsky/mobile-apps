//
//  SupermanViewController.m
//  Superman
//
//  Created by chenghao lin on 9/27/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "SupermanViewController.h"

@implementation SupermanViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	srand([[NSDate date] timeIntervalSince1970]);
	[NSTimer scheduledTimerWithTimeInterval:3.3 target:self selector:@selector(move) userInfo:nil repeats:YES];
	[self move];
	
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}
-(void) move{
	
	NSInteger ran=rand()%4;
	NSInteger left;
	NSInteger top;
	if(ran==0)
	{
		left=-160;
		top=-160;
		[self changeToSuperman];
	}
	if(ran==1)
	{
		left=320;
		top=-160;
		[self changeToSuperman];
	}
	if(ran==2)
	{
		left=320;
		top=480;
		[self changeLeftSuperman];
	}
	if(ran==3)
	{
		left=-160;
		top=480;
		[self changeRightSuperman];
	}
	
	imageView.frame=CGRectMake(left,top,100,100);
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1.5];
	
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(moveOff)];
	
	imageView.frame=CGRectMake(80,160,100,100);
	
	[UIView commitAnimations];	
	
}
-(IBAction)changeLeftSuperman{
	imageView.image=[UIImage imageNamed:@"LeftSuperman.png"];
}
-(IBAction)changeRightSuperman{
	imageView.image=[UIImage imageNamed:@"RightSuperman.png"];
}

-(IBAction)changeToSuperman{
	imageView.image=[UIImage imageNamed:@"image.png"];
}

-(void) moveOff{
	
	NSInteger ran=rand()%4;
	NSInteger left;
	NSInteger top;
	NSInteger w=100;
	NSInteger h=100;
	if(ran==0)
	{
		left=10;
		top=120;
		[self changeLeftSuperman];
		w=0;
		h=0;
	}
	if(ran==1)
	{
		left=280;
		top=120;
		[self changeRightSuperman];
		w=0;
		h=0;
	}
	
	if(ran==2)
	{
		left=320;
		top=520;
		[self changeToSuperman];
	}
	if(ran==3)
	{
		left=-160;
		top=520;
		[self changeToSuperman];
	}
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1.5];
	
	imageView.frame=CGRectMake(left,top,w,h);
	
	[UIView commitAnimations];
	
	

}

@end
