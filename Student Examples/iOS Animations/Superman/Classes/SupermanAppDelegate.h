//
//  SupermanAppDelegate.h
//  Superman
//
//  Created by chenghao lin on 9/27/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SupermanViewController;

@interface SupermanAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    SupermanViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet SupermanViewController *viewController;

@end

