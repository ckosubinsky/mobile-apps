 //
//  SupermanViewController.h
//  Superman
//
//  Created by chenghao lin on 9/27/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupermanViewController : UIViewController {
	IBOutlet UIImageView *imageView;
	IBOutlet UIImageView *background;
	
}
-(void)move;
-(void)moveOff;
-(IBAction)changeLeftSuperman;
-(IBAction)changeRightSuperman;
-(IBAction)changeToSuperman;
@end

