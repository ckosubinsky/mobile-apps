//
//  main.m
//  Superman
//
//  Created by chenghao lin on 9/27/10.
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
