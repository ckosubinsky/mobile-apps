//
//  boneyardViewController.h
//  boneyard
//
//  Created by teabones on 10/3/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface boneyardViewController : UIViewController {
	
	// These 2 methods create the UIImage object and the array of images that will display inside of it
	IBOutlet UIImageView *walk;
	NSMutableArray *anim;

}

//Properties for the 2 methods... 
@property (nonatomic, retain)UIImageView *walk;
@property (nonatomic, retain)NSMutableArray *anim;

// A method to move the animated object...

- (void) move;

@end

