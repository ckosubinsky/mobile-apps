//
//  boneyardViewController.m
//  boneyard
//
//  Created by teabones on 10/3/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "boneyardViewController.h"

@implementation boneyardViewController

// Synthesize the 2 properties

@synthesize walk;
@synthesize anim;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// This event handler was uncommented to populate the array and animate between the image frames.



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	

	[NSTimer scheduledTimerWithTimeInterval:14 target:self selector:@selector(move) userInfo:nil repeats:YES];
	[self move];
	
	anim=[[NSMutableArray alloc] init];
	
	// A for loop is used instead of a list of each picture in the array.
	
	for(int i=0;i<11;i++){
		
		NSString *pic=[NSString stringWithFormat:@"%d.png", i];
		UIImage *img=[UIImage imageNamed:pic];
		if (img)[anim addObject:img];
		
		}
		
		[walk setAnimationImages:anim];
		[walk setAnimationDuration:.75];
		[walk startAnimating];

    [super viewDidLoad];
}


// This code was uncommented to force the app into landscape mode.

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if ((interfaceOrientation==UIInterfaceOrientationPortrait)||(interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown))
	
	// NO locks out the portrait mode of the iPhone. The initial orientation also has to be set in the info plist.
	
	{
		return NO;
	}
	if ((interfaceOrientation==UIInterfaceOrientationLandscapeLeft)||(interfaceOrientation==UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


- (void) move
{
	walk.frame=CGRectMake(-250,80,240,180);
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:13.5];
	[UIView setAnimationDelegate:self];
	walk.frame=CGRectMake(500,80,240,180);
	[UIView commitAnimations];
}


@end
