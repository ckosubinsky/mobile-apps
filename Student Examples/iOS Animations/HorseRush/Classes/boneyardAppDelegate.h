//
//  boneyardAppDelegate.h
//  boneyard
//
//  Created by teabones on 10/3/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class boneyardViewController;

@interface boneyardAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    boneyardViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet boneyardViewController *viewController;

@end

