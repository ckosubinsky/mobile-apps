//
//  AddCoursesViewController.h
//  Schedule
//
//  Created by Raman Nanda on 11/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AddCoursesViewController : UIViewController <UIActionSheetDelegate, UIPickerViewDataSource> {
	IBOutlet UIScrollView *scrollView;
	
	IBOutlet UITextField *textFieldCrsTitle;
	IBOutlet UITextField *textFieldDept;
	IBOutlet UITextField *textFieldCrsNum;
	IBOutlet UITextField *textFieldProf;
	IBOutlet UITextField *textFieldLocation;
	
	IBOutlet UITextField *textFieldStartTime;
	IBOutlet UITextField *textFieldEndTime;
	
	IBOutlet UISwitch *mondayCtrl;
	IBOutlet UISwitch *tuesdayCtrl;
	IBOutlet UISwitch *wednesdayCtrl;
	IBOutlet UISwitch *thursdayCtrl;
	IBOutlet UISwitch *fridayCtrl;
	
	IBOutlet UIActionSheet *actionSheet;
}

- (IBAction) backToScheduleView:(id)sender;
- (IBAction) addCourseToSchedule:(id)sender;

- (IBAction) textFieldReturn:(id)sender;
- (IBAction) backgroundTouched:(id)sender;

@property (nonatomic, retain) UITextField *textFieldCrsTitle;
@property (nonatomic, retain) UITextField *textFieldDept;
@property (nonatomic, retain) UITextField *textFieldCrsNum;
@property (nonatomic, retain) UITextField *textFieldProf;
@property (nonatomic, retain) UITextField *textFieldLocation;
@property (nonatomic, retain) UITextField *textFieldStartTime;
@property (nonatomic, retain) UITextField *textFieldEndTime;

@property (nonatomic, retain, readonly) UISwitch *mondayCtrl;
@property (nonatomic, retain, readonly) UISwitch *tuesdayCtrl;
@property (nonatomic, retain, readonly) UISwitch *wednesdayCtrl;
@property (nonatomic, retain, readonly) UISwitch *thursdayCtrl;
@property (nonatomic, retain, readonly) UISwitch *fridayCtrl;

@end