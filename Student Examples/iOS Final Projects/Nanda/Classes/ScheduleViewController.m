//
//  FirstViewController.m
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ScheduleViewController.h"
#import "ScheduleAppDelegate.h"
#import "Course.h"
#import "I7CustomizableTableViewCell.h"
#import "ScheduleTableViewCell.h"
#import "AddCoursesViewController.h"

@implementation ScheduleViewController

AddCoursesViewController *addCourseView;

@synthesize backgroundImage;


// Returns the number of sections in the table. Here we will only have one section... just for the courses
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// This method was overridden. It sets the height of a given cell.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 60;
}
 
// Returns the number of the cells in the table. In my case the number of cells corresponds to the courses array
// so i will just return the size of that array.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	return appDelegate.courses.count;
}


// This is where things get a little complicated. This method is called for every cell in the table. It populates the data in the cell
// and sets the style.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {	
	static NSString *CellIdentifier = @"cell";
	
	// Creates a Cell - It resuses a cell if it not being use. This is for memory saving.
	ScheduleTableViewCell *cell = (ScheduleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		// Creates a custom cell defined in my ScheduleTableViewCell class
		cell = [[[ScheduleTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		
	}
	
	// The position will be overriden here. This is just the default
	cell.position = I7CustimizableTableViewCellPositionSingle;
	
	// Gets a handle to the specific course we are looking at based on the cell we're looking at.
	// The Array in the application delegate has a 1 to 1 correpondence to the layout and data of the courses.
	// So the position of the course in the arraylist is the position of the cell in the table. 
    ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	Course *thisCourse = (Course *)[appDelegate.courses objectAtIndex:indexPath.row];
	
	if(self.tableView.editing)
		cell.position = ScheduleTableViewCellSingle;
	else {
		// We have three different types of cells. The images of the cells are different based on the layout.
		// The Cell top has rounded edges on the top, Cell middle doesnt have any rounded edges. and Cell Button has
		// rounded edges on the bottom
		if(appDelegate.courses.count > 1) {
			if(indexPath.row == 0) {
				cell.position = ScheduleTableViewCellTop;
			}
			else if(indexPath.row < appDelegate.courses.count - 1) {
				cell.position = ScheduleTableViewCellMiddle;
			}
			else {
				cell.position = ScheduleTableViewCellBottom;
			}
		}
	}
	
	//cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	// Sets the data of the Cell
	cell.titleLabel.text = thisCourse.crsTitle;
	cell.instructorLabel.text = thisCourse.professor;
	cell.locationLabel.text = thisCourse.loc;
	
	cell.crsNumberLabel.text = [thisCourse.dept stringByAppendingString:@" "];
	cell.crsNumberLabel.text = [cell.crsNumberLabel.text stringByAppendingString:[NSString stringWithFormat:@"%d", thisCourse.crsNum]];
	
	if([thisCourse.startTime isEqualToString: @"None"] && [thisCourse.endTime isEqualToString:@"None"])
	{
		cell.timeLabel.text = nil;
		cell.mondayLabel.text = nil;
		cell.tuesdayLabel.text = nil;
		cell.wednesdayLabel.text = nil;
		cell.thursdayLabel.text = nil;
		cell.fridayLabel.text = nil;
	}
	else 
	{
		// Date formatter - This will read the Text in the start Time and end time of the course and format them
		// so 14:30  = 2:30 PM
		NSLocale* usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
	
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setLocale:usLocale];
		[dateFormatter setDateFormat:@"HH:mm"];
		NSDate *startDate = [dateFormatter dateFromString:thisCourse.startTime];
		NSDate *endDate = [dateFormatter dateFromString:thisCourse.endTime];
	
		NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
		[outputDateFormatter setLocale:usLocale];
		[outputDateFormatter setDateFormat:@"h:mm a"];
		NSString *outputStart = [outputDateFormatter stringFromDate:startDate];
		NSString *outputEnd = [outputDateFormatter stringFromDate:endDate];
	
		cell.timeLabel.text = [outputStart stringByAppendingString:@" - "];
		cell.timeLabel.text = [cell.timeLabel.text stringByAppendingString:outputEnd];
	
		// Sets the font of the course day to bold if course occurs on that day.
		
		if(thisCourse.isMonday == (int *)1)
			cell.mondayLabel.font = [UIFont boldSystemFontOfSize:10];
		else
			cell.mondayLabel.font = [UIFont	systemFontOfSize:10];
	
		if(thisCourse.isTuesday == (int *)1)
			cell.tuesdayLabel.font = [UIFont boldSystemFontOfSize:10];
		else
			cell.tuesdayLabel.font = [UIFont systemFontOfSize:10];
	
		if(thisCourse.isWednesday == (int *)1)
			cell.wednesdayLabel.font = [UIFont boldSystemFontOfSize:10];
		else
			cell.wednesdayLabel.font = [UIFont systemFontOfSize:10];
	
		if(thisCourse.isThursday == (int *)1)
			cell.thursdayLabel.font = [UIFont boldSystemFontOfSize:10];
		else
			cell.thursdayLabel.font = [UIFont systemFontOfSize:10];
	
		if(thisCourse.isFriday == (int *)1)
			cell.fridayLabel.font = [UIFont boldSystemFontOfSize:10];
		else
			cell.fridayLabel.font = [UIFont	systemFontOfSize:10];
		
	}
	
	return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		// Removes the course from the table and database when in the editing mode.
		Course *item = [[appDelegate.courses objectAtIndex:indexPath.row] retain];
		[appDelegate removeCourse:(NSString *)item.crsTitle];
		[appDelegate.courses removeObject:item];
		[self.tableView beginUpdates];
		[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
		[self.tableView endUpdates];
	}
	if (editingStyle == UITableViewCellEditingStyleInsert) {
		NSLog(@"HERE WE AREE");
	}
}



- (void)addNew {
	// Pops up the Add Course Form
	addCourseView = [[AddCoursesViewController alloc] initWithNibName:@"AddCourse" bundle:nil];
	[self presentModalViewController:addCourseView animated:YES];
}

// This is the helper method for editing a particular cell. 
// I disabled adding a new course when editing courses.
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	[super setEditing:editing animated:animated];
	if(editing) {
		self.navigationItem.rightBarButtonItem.enabled = NO;
	} else {
		self.navigationItem.rightBarButtonItem.enabled = YES;
	}
	[self.tableView reloadData];
}

// Helper method for allowing the table to be reordered.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

// This method reorders the cell. Since the array and the table are 1 to 1, When i move the cell to a specific position.
// I would move the object of the course to that position in the array.
- (void)tableView:(UITableView *)tableView  moveRowAtIndexPath:(NSIndexPath *)fromIndexPath
	  toIndexPath:(NSIndexPath *)toIndexPath {
	ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSString *item = [[appDelegate.courses objectAtIndex:fromIndexPath.row] retain];
	[appDelegate.courses removeObject:item];
	[appDelegate.courses insertObject:item atIndex:toIndexPath.row];
	[item release];
}

- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	// Create the Edit button in the navigation
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	
	// Create the + button in the navigation
	UIBarButtonItem *tempButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNew)];
	self.navigationItem.rightBarButtonItem = tempButton;
	[tempButton release];
	
	/* create the backdrop */
	UIView *bgview = [[[UIImageView alloc] initWithImage:backgroundImage]autorelease];
	
	/* clear the tableviews color */
	self.tableView.backgroundColor = [UIColor clearColor];
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	
	/* set the backdrop */
	[self.view.superview insertSubview:bgview atIndex:0];	
	
	[self.tableView reloadData];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"My Schedule";
	self.backgroundImage = [UIImage imageNamed:@"backgroundimage.png"];
	tableHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,20)];
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if(section == 0) {
		return 40; 
		//tableHeader.frame.size.height;
	}
	return 0;
}

- (UIView *) tableView:(UITableView *)aTableView viewForHeaderInSection:(NSInteger)section {
	return tableHeader;
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[backgroundImage release];
	[addCourseView release];
    [super dealloc];
}

@end
