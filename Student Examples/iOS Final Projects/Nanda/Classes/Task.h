//
//  Task.h
//  Schedule
//
//  Created by Raman Nanda on 11/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Task : NSObject {
	NSString	*taskTitle;
	NSString	*details;
	int			*isCompleted;
	int			*isVisible;
}

@property (nonatomic, retain) NSString	*taskTitle;
@property (nonatomic, retain) NSString	*details;

- (int) isCompleted;
- (int) isVisible;

- (void) setIsCompleted:(int)input;
- (void) setIsVisible:(int)input;

// Creates an Task object given the parameters

- (id) init:(NSString *)taskT details:(NSString *)det isCompleted:(int)isComp isVisible:(int)isVis;

- (void) dealloc;

@end
