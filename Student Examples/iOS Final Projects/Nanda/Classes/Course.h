//
//  Class.h
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Course : NSObject {
	NSString	*dept;						// Course Dept Variable
	int			*crsNum;					// Course Number
	NSString	*crsTitle;
	NSString	*professor;
	NSString	*loc;
	NSString	*startTime;
	NSString	*endTime;
	int			*isMonday;					// Int value for the days of the week
	int			*isTuesday;						// Will be '1' if the course occurs on that day
	int			*isWednesday;					// '0' Otherwise
	int			*isThursday;
	int			*isFriday;
}

@property (nonatomic, retain) NSString	*dept;
@property (nonatomic, retain) NSString	*crsTitle;
@property (nonatomic, retain) NSString	*professor;
@property (nonatomic, retain) NSString	*loc;
@property (nonatomic, retain) NSString	*startTime;
@property (nonatomic, retain) NSString	*endTime;

- (int) crsNum;
- (int) isMonday;
- (int) isTuesday;
- (int) isWednesday;
- (int) isThursday;
- (int) isFriday;

- (void) setCrsNum:(int)input;
- (void) setIsMonday:(int)input;
- (void) setIsTuesday:(int)input;
- (void) setIsWednesday:(int)input;
- (void) setIsThursday:(int)input;
- (void) setIsFriday:(int)input;

- (NSString *) getDept;
- (NSString *) getCrsTitle;
- (NSString *) getLocation;
- (NSString *) getTime;

// This is the method that will create the instance of the Course Object, given all of its parameters

- (id) init:(NSString *)dep crsNum:(int)crsN crsTitle:(NSString *)crsT professor:(NSString *)prof loc:(NSString *)locate 
	startTime:(NSString *)startT endTime:(NSString *)endT isMonday:(int)mon isTuesday:(int)tues
	isWednesday:(int)wed isThursday:(int)thurs isFriday:(int)fri;

- (void) dealloc;

@end
