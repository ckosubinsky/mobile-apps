//
//  ScheduleTableViewCell.h
//  Schedule
//
//  Created by Raman Nanda on 11/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "I7CustomizableTableViewCell.h"

@interface ScheduleTableViewCell : I7CustomizableTableViewCell {
	UILabel *titleLabel;
	UILabel *crsNumberLabel;
	UILabel *instructorLabel;
	UILabel	*locationLabel;
	UILabel *timeLabel;
	UILabel *mondayLabel;
	UILabel *tuesdayLabel;
	UILabel *wednesdayLabel;
	UILabel *thursdayLabel;
	UILabel *fridayLabel;
}

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *crsNumberLabel;
@property (nonatomic, retain) UILabel *instructorLabel;
@property (nonatomic, retain) UILabel *locationLabel;
@property (nonatomic, retain) UILabel *timeLabel;
@property (nonatomic, retain) UILabel *mondayLabel;
@property (nonatomic, retain) UILabel *tuesdayLabel;
@property (nonatomic, retain) UILabel *wednesdayLabel;
@property (nonatomic, retain) UILabel *thursdayLabel;
@property (nonatomic, retain) UILabel *fridayLabel;

@end
