//
//  FirstViewController.m
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CurrentViewController.h"
#import "ScheduleAppDelegate.h"

#define BASEURL [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]

@implementation CurrentViewController
@synthesize backgroundImage, webView;

/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */


 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
 [super viewDidLoad];
	 self.view.backgroundColor = [UIColor blackColor];
	
	 // Moves the UI View to the point where the clock is
	 // Rotates the Clock
	 CGPoint center = (CGPoint)[webView center]; 
	 center.x += -490; 
	 center.y += -400; 
	 [webView setCenter:center];
	 //self.webView = CGAffineTransformTranslate(self.webView, 50.0, 100.0);
	 
	 [self.view setTransform:CGAffineTransformMakeRotation(M_PI / 2.0)];
	 
	 
	 // Sets the link of the clock
	 NSString *urlAddress = @"http://www2.mm.cs.sunysb.edu/~106171352/clock.html";
	 
	 //Create a URL object.
	 NSURL *url = [NSURL URLWithString:urlAddress];
	 
	 //URL Requst Object
	 NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
	 
	 //Load the request in the UIWebView.
	 [webView loadRequest:requestObj];
	 
	 //[self.view setBackgroundColor:[UIColor blackColor]];
 }
 


 // Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
