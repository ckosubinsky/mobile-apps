//
//  FirstViewController.m
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TasksViewController.h"
#import "ScheduleAppDelegate.h"
#import "Task.h"
#import "I7CustomizableTableViewCell.h"
#import "ACustomTableViewCell.h"

@implementation TasksViewController

@synthesize backgroundImage;
@synthesize taskField;
@synthesize clearCompleted;


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	return appDelegate.tasks.count;
}

// This method is the same as the one in the Courses Class
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {	
	static NSString *CellIdentifier = @"cell";
	
	ACustomTableViewCell *cell = (ACustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[[ACustomTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		
	}
	
	cell.position = I7CustimizableTableViewCellPositionSingle;
	
    ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	Task *thisTask = (Task *)[appDelegate.tasks objectAtIndex:indexPath.row];
	
	if(self.tableView.editing)
		cell.position = I7CustimizableTableViewCellPositionSingle;
	else {
		
		if(appDelegate.tasks.count > 1) {
			if(indexPath.row == 0) {
				cell.position = I7CustimizableTableViewCellPositionTop;
			}
			else if(indexPath.row < appDelegate.tasks.count - 1) {
				cell.position = I7CustimizableTableViewCellPositionMiddle;
			}
			else {
				cell.position = I7CustimizableTableViewCellPositionBottom;
			}
		}
	}
	
	if (thisTask.isCompleted == (int *)0)
	{
		[cell setAccessoryType:UITableViewCellAccessoryNone];
	}
	else
	{
		[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
	}
	
	cell.textLabel.text = thisTask.taskTitle;
	
	return cell;
}

- (void)tableView:(UITableView *)tblView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	Task *thisTask = (Task *)[appDelegate.tasks objectAtIndex:indexPath.row];
	
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if (thisTask.isCompleted == (int *)1)
	{
		thisTask.isCompleted = 0;
		[[self.tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
	}
	else
	{
		thisTask.isCompleted = 1;
		[[self.tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
	}
	[appDelegate toggleTaskProperties:thisTask.taskTitle isCompleted:(int)thisTask.isCompleted];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		Task *item = [[appDelegate.tasks objectAtIndex:indexPath.row] retain];
		[appDelegate deleteTask:(NSString *)item.taskTitle];
		[appDelegate.tasks removeObject:item];
		[self.tableView beginUpdates];
		[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
		[self.tableView endUpdates];
	}
	if (editingStyle == UITableViewCellEditingStyleInsert) {
		NSLog(@"HERE WE AREE");
	}
}


// Pushes out a Dialog box with a textfield to enter the name of a new task.
- (void)addNewDialog {
	UIAlertView *taskAlert = [[UIAlertView alloc] initWithTitle:@"Tasks" message:@"\n\n\n"
														   delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) otherButtonTitles:NSLocalizedString(@"OK",nil), nil];
	
	UILabel *taskLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,40,260,25)];
	taskLabel.font = [UIFont systemFontOfSize:16];
	taskLabel.textColor = [UIColor whiteColor];
	taskLabel.backgroundColor = [UIColor clearColor];
	taskLabel.shadowColor = [UIColor blackColor];
	taskLabel.shadowOffset = CGSizeMake(0,-1);
	taskLabel.textAlignment = UITextAlignmentCenter;
	taskLabel.text = @"New Task";
	[taskAlert addSubview:taskLabel];
	
	UIImageView *taskImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"passwordfield" ofType:@"png"]]];
	taskImage.frame = CGRectMake(11,79,262,31);
	[taskAlert addSubview:taskImage];
	
	taskField = [[UITextField alloc] initWithFrame:CGRectMake(16,83,252,25)];
	taskField.font = [UIFont systemFontOfSize:18];
	taskField.backgroundColor = [UIColor whiteColor];
	taskField.autocapitalizationType = UITextAutocapitalizationTypeWords;
	taskField.secureTextEntry = NO;
	taskField.keyboardAppearance = UIKeyboardAppearanceAlert;
				
	[taskField becomeFirstResponder];
	[taskAlert addSubview:taskField];
	
	[taskAlert show];
	[taskAlert release];
	[taskField release];
	[taskImage release];
	[taskLabel release];
	
}

// Checks to see what button was pressed on the Alert. If Okay was selected. Push the task into the DB.
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex == 1) {
		ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
		if([taskField.text isEqualToString:@""])
		{
			
		}
		else
		{
			[appDelegate addTaskToDatabase:taskField.text];
		}
	} 
	[self.tableView reloadData];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	[super setEditing:editing animated:animated];
	if(editing) {
		self.navigationItem.rightBarButtonItem.enabled = NO;
	} else {
		self.navigationItem.rightBarButtonItem.enabled = YES;
	}
	[self.tableView reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

- (void)tableView:(UITableView *)tableView  moveRowAtIndexPath:(NSIndexPath *)fromIndexPath
	  toIndexPath:(NSIndexPath *)toIndexPath {
	ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	NSString *item = [[appDelegate.tasks objectAtIndex:fromIndexPath.row] retain];
	[appDelegate.tasks removeObject:item];
	[appDelegate.tasks insertObject:item atIndex:toIndexPath.row];
	[item release];
}

- (void)viewWillAppear:(BOOL)animated {
	
	[super viewWillAppear:animated];
	
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	
	UIBarButtonItem *tempButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewDialog)];
	self.navigationItem.rightBarButtonItem = tempButton;
	[tempButton release];
	
	/* create the backdrop */
	UIView *bgview = [[[UIImageView alloc] initWithImage:backgroundImage]autorelease];
	
	/* clear the tableviews color */
	self.tableView.backgroundColor = [UIColor clearColor];
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	
	/* set the backdrop */
	[self.view.superview insertSubview:bgview atIndex:0];	
	
	[self.tableView reloadData];
}


/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"My Tasks";
	self.backgroundImage = [UIImage imageNamed:@"backgroundimage.png"];
	tableHeader = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,20)];
}


// Go through all the Task and see if they have been marked as completed.
// If the task has been completed. Call the method to delete the task from the database
- (IBAction)clearCompletedTasks:(id)sender {
	ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
	NSMutableArray *rowsToBeDeleted = [[NSMutableArray alloc] init];
	
	for (int i = 0; i < [appDelegate.tasks count]; i++) {
		Task* temp = [appDelegate.tasks objectAtIndex:i];
		if(temp.isCompleted == (int *)1)
		{
			[rowsToBeDeleted addObject:[appDelegate.tasks objectAtIndex:i]];
			NSUInteger pathSource[2] = {0, i};
			NSIndexPath *path = [NSIndexPath indexPathWithIndexes:pathSource length:2];
			[indexPaths addObject:path];
		}
	}
	[appDelegate hideCompletedTasks];
	for (id value in rowsToBeDeleted)
	{
		[appDelegate.tasks removeObject:value];
	}
	[self.tableView beginUpdates];
	[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationRight];
	[self.tableView endUpdates];
	[self.tableView reloadData];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if(section == 0) {
		return tableHeader.frame.size.height;
	}
	return 0;
}

- (UIView *) tableView:(UITableView *)aTableView viewForHeaderInSection:(NSInteger)section {
	return tableHeader;
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[backgroundImage release];
    [super dealloc];
}

@end
