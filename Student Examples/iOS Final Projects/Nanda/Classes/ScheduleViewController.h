//
//  FirstViewController.h
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "I7CustomizableTableViewCell.h"


@interface ScheduleViewController : UITableViewController {
	UIImage *backgroundImage;
	UIImage *cellImage;
	UIView *tableHeader;
	
}
@property (nonatomic, retain) UIImage *backgroundImage;

@end
