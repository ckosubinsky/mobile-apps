//
//  ScheduleTableViewCell.m
//  Schedule
//
//  Created by Raman Nanda on 11/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ScheduleTableViewCell.h"


// This is the custom cell for the Schedule View

@implementation ScheduleTableViewCell

@synthesize titleLabel, crsNumberLabel, instructorLabel, locationLabel, timeLabel, 
mondayLabel, tuesdayLabel, wednesdayLabel, thursdayLabel, fridayLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self != nil) {

		// Initialization code for all labels
		
		titleLabel = [[UILabel alloc]init];
		crsNumberLabel = [[UILabel alloc] init];
		instructorLabel = [[UILabel alloc]init];
		locationLabel = [[UILabel alloc]init];
		timeLabel = [[UILabel alloc]init];
		mondayLabel = [[UILabel alloc]init];
		tuesdayLabel = [[UILabel alloc]init];
		wednesdayLabel = [[UILabel alloc]init];
		thursdayLabel = [[UILabel alloc]init];
		fridayLabel = [[UILabel alloc]init];
		
		self.titleLabel.shadowOffset = CGSizeMake(1,1);
		self.crsNumberLabel.shadowOffset = CGSizeMake(1, 1);
		self.instructorLabel.shadowOffset = CGSizeMake(1,1);
		self.locationLabel.shadowOffset = CGSizeMake(1,1);
		self.timeLabel.shadowOffset = CGSizeMake(1,1);
		self.mondayLabel.shadowOffset = CGSizeMake(1,1);
		self.tuesdayLabel.shadowOffset = CGSizeMake(1,1);
		self.wednesdayLabel.shadowOffset = CGSizeMake(1,1);
		self.thursdayLabel.shadowOffset = CGSizeMake(1,1);
		self.fridayLabel.shadowOffset = CGSizeMake(1,1);
		
		titleLabel.textAlignment = UITextAlignmentLeft;
		crsNumberLabel.textAlignment = UITextAlignmentCenter;
		instructorLabel.textAlignment = UITextAlignmentLeft;
		locationLabel.textAlignment = UITextAlignmentLeft;
		timeLabel.textAlignment = UITextAlignmentRight;
		mondayLabel.textAlignment = UITextAlignmentLeft;
		tuesdayLabel.textAlignment = UITextAlignmentLeft;
		wednesdayLabel.textAlignment = UITextAlignmentLeft;
		thursdayLabel.textAlignment = UITextAlignmentLeft;
		fridayLabel.textAlignment = UITextAlignmentLeft;
		
		self.titleLabel.backgroundColor = [UIColor clearColor];
		self.crsNumberLabel.backgroundColor = [UIColor clearColor];
		self.instructorLabel.backgroundColor = [UIColor clearColor];
		self.locationLabel.backgroundColor = [UIColor clearColor];
		self.timeLabel.backgroundColor = [UIColor clearColor];
		self.mondayLabel.backgroundColor = [UIColor clearColor];
		self.tuesdayLabel.backgroundColor = [UIColor clearColor];
		self.wednesdayLabel.backgroundColor = [UIColor clearColor];
		self.thursdayLabel.backgroundColor = [UIColor clearColor];
		self.fridayLabel.backgroundColor = [UIColor clearColor];
		
		titleLabel.font = [UIFont boldSystemFontOfSize:14];
		crsNumberLabel.font = [UIFont boldSystemFontOfSize:10];
		instructorLabel.font = [UIFont systemFontOfSize:10];
		locationLabel.font = [UIFont systemFontOfSize:10];
		timeLabel.font = [UIFont systemFontOfSize:10];
		mondayLabel.font = [UIFont systemFontOfSize:10];
		tuesdayLabel.font = [UIFont systemFontOfSize:10];
		wednesdayLabel.font = [UIFont systemFontOfSize:10];
		thursdayLabel.font = [UIFont systemFontOfSize:10];
		fridayLabel.font = [UIFont systemFontOfSize:10];
		
		mondayLabel.text = @"M";
		tuesdayLabel.text = @"Tu";
		wednesdayLabel.text = @"W";
		thursdayLabel.text = @"Th";
		fridayLabel.text = @"F";
		
		[self.contentView addSubview:titleLabel];
		[self.contentView addSubview:crsNumberLabel];
		[self.contentView addSubview:instructorLabel];
		[self.contentView addSubview:locationLabel];
		[self.contentView addSubview:timeLabel];
		[self.contentView addSubview:mondayLabel];
		[self.contentView addSubview:tuesdayLabel];
		[self.contentView addSubview:wednesdayLabel];
		[self.contentView addSubview:thursdayLabel];
		[self.contentView addSubview:fridayLabel];
		[self.contentView bringSubviewToFront:titleLabel];
		
		[titleLabel release];
		[crsNumberLabel release];
		[instructorLabel release];
		[locationLabel release];
		[timeLabel release];
		[mondayLabel release];
		[tuesdayLabel release];
		[wednesdayLabel release];
		[thursdayLabel release];
		[fridayLabel release];
 		
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
}

- (void)layoutSubviews {
	[super layoutSubviews];
	CGRect contentRect = self.contentView.bounds;
	CGFloat boundsX = contentRect.origin.x;
	CGFloat titleBar = 25;
	CGRect frame;
	
	// Special Layout for when editing.
	
	if(self.editing)
	{
		frame= CGRectMake(15, 15, 250, 25);
		titleLabel.frame = frame;
		frame= CGRectMake(30, 18, 50, 50);
		crsNumberLabel.frame = frame;
		crsNumberLabel.transform = CGAffineTransformMakeRotation(0);
		crsNumberLabel.font = [UIFont systemFontOfSize:10];
		
		[instructorLabel setHidden:YES];
		[locationLabel setHidden:YES];
		[timeLabel setHidden:YES];
		[mondayLabel setHidden:YES];
		[tuesdayLabel setHidden:YES];
		[wednesdayLabel setHidden:YES];
		[thursdayLabel setHidden:YES];
		[fridayLabel setHidden:YES];
	}
	else
	{
		// Coordinate System for the Labels
		
		[instructorLabel setHidden:NO];
		[locationLabel setHidden:NO];
		[timeLabel setHidden:NO];
		[mondayLabel setHidden:NO];
		[tuesdayLabel setHidden:NO];
		[wednesdayLabel setHidden:NO];
		[thursdayLabel setHidden:NO];
		[fridayLabel setHidden:NO];
		
		frame= CGRectMake(boundsX+titleBar+10, 5, 275, 25);
		titleLabel.frame = frame;
	
		frame= CGRectMake(boundsX+titleBar+25 ,22, 250, 25);
		instructorLabel.frame = frame;
	
		frame= CGRectMake(boundsX+titleBar+35 ,35, 240, 25);
		locationLabel.frame = frame;
	
		frame= CGRectMake(-10, 5, 50, 50);
		crsNumberLabel.font = [UIFont boldSystemFontOfSize:10];
		crsNumberLabel.frame = frame;
		crsNumberLabel.transform = CGAffineTransformMakeRotation(-M_PI/2 );
	
		frame= CGRectMake(193, 22, 100, 20);
		timeLabel.frame = frame;
	
		frame = CGRectMake(230, 35, 20, 20);
		mondayLabel.frame = frame;
		frame = CGRectMake(242, 35, 20, 20);
		tuesdayLabel.frame = frame;
		frame = CGRectMake(257, 35, 20, 20);
		wednesdayLabel.frame = frame;
		frame = CGRectMake(270, 35, 20, 20);
		thursdayLabel.frame = frame;
		frame = CGRectMake(285, 35, 20, 20);
		fridayLabel.frame = frame;
	}
	
}

- (void)dealloc {
	// make sure you free the memory
	[titleLabel release];
	[instructorLabel release];
	[locationLabel release];
	[timeLabel release];
	[mondayLabel release];
	[tuesdayLabel release];
	[wednesdayLabel release];
	[thursdayLabel release];
	[fridayLabel release];
	[super dealloc];
}

@end
