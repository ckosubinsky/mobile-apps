//
//  FirstViewController.m
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MapViewController.h"
#import "Course.h"
#import "ScheduleAppDelegate.h"
#import "CourseAnnotation.h"

@implementation MapViewController


/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */


 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
	 [super viewDidLoad];
	 mapView=[[MKMapView alloc] initWithFrame:self.view.bounds];
	 mapView.showsUserLocation=TRUE;
	 
	 // Type of Google Map
	 mapView.mapType=MKMapTypeStandard;
	 
	 /*Region and Zoom*/
	 MKCoordinateRegion region;
	 MKCoordinateSpan span;
	 
	 // The Zoom on the Map - the Smaller the value the more detailed the View
	 span.latitudeDelta=0.0059;
	 span.longitudeDelta=0.00695;
	 
	 CLLocationCoordinate2D location=mapView.userLocation.coordinate;
	 
	 // Coordinates of the Campus (SBU)
	 location.latitude=40.91385;
	 location.longitude=-73.12280;
	 region.span=span;
	 region.center=location;
	 
	 
	 
	 [mapView setRegion:region animated:TRUE];
	 [mapView regionThatFits:region];
	 [self.view addSubview:mapView];
	 [self loadMarkers];
	 
 }
 
// This method creates and adds all the markers for the Courses.
- (void) loadMarkers {
	ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
	for(int i = 0; i < [appDelegate.courses count]; i++)
	{
		Course *item = [appDelegate.courses objectAtIndex:i];
		NSLocale* usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
		
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setLocale:usLocale];
		[dateFormatter setDateFormat:@"HH:mm"];
		NSDate *startDate = [dateFormatter dateFromString:item.startTime];
		NSDate *endDate = [dateFormatter dateFromString:item.endTime];
		
		NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
		[outputDateFormatter setLocale:usLocale];
		[outputDateFormatter setDateFormat:@"h:mm a"];
		NSString *outputStart = [outputDateFormatter stringFromDate:startDate];
		NSString *outputEnd = [outputDateFormatter stringFromDate:endDate];
		
		NSString *time = [outputStart stringByAppendingString:@" - "];
		time = [time stringByAppendingString:outputEnd];
		
		CourseAnnotation *addAnnotation = [[CourseAnnotation alloc] initWithCourse:[item.dept stringByAppendingString:[@" " stringByAppendingString:[NSString stringWithFormat:@"%d", item.crsNum]]] location:item.loc time:time offset:i];
		[mapView addAnnotation:addAnnotation];
		[addAnnotation release];
	}
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
