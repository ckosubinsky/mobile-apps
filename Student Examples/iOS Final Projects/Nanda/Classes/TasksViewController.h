//
//  FirstViewController.h
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TasksViewController : UITableViewController {
	UIImage *backgroundImage;
	UIImage *cellImage;
	UIView *tableHeader;
	
}

- (IBAction) clearCompletedTasks:(id)sender;

@property (nonatomic, retain) UIImage *backgroundImage;
@property (nonatomic, retain) UITextField *taskField;
@property (nonatomic, retain) UIBarButtonItem *clearCompleted;

@end