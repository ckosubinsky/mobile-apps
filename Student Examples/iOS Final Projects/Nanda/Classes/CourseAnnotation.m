//
//  CourseAnnotation.m
//  Schedule
//
//  Created by Raman Nanda on 11/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CourseAnnotation.h"
#import "Course.h"


@implementation CourseAnnotation
@synthesize coordinate, courseName, classTime;


- (NSString *)subtitle{
	return classTime;
}

- (NSString *)title{
	return courseName;
}

// This will create an annotation for the Map given the time and location of the scheduled course
// It will look for keywords in the location string and map them to actual coordinates to building on stony brook's campus

-(id)initWithCourse:(NSString *)crsName location:(NSString *)loc time:(NSString *)timeString offset:(int)value{
	self.courseName = crsName;
	self.classTime = timeString;
	
	NSRange aRange = [[loc lowercaseString] rangeOfString:@"union"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.91692, longitude: (-73.12225 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"sac"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.91436, longitude: (-73.12365 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"phy"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.91607, longitude: (-73.12573 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"chem"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.916164, longitude: (-73.12349 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"old chem"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.915539, longitude: (-73.12402 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"earth"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.91476, longitude: (-73.12556 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"ess"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.91476, longitude: (-73.12556 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"hum"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.91359, longitude: (-73.12097 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"comp"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.91197, longitude: -73.12212 + value/100};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"lib"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.915329, longitude: (-73.12258 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"staller"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.915969, longitude: (-73.12152 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"math"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.915669, longitude: (-73.12634 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"sport"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.917210, longitude: (-73.12494 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"life"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.911761, longitude: (-73.12008 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"harr"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.915499, longitude: (-73.12508 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"wang"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.915750, longitude: (-73.12024 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"light engi"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.913618, longitude: (-73.12555 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"heavy engi"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.912548, longitude: (-73.12567 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"engi"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.912896, longitude: (-73.12442 + value/1000)};
		coordinate = temp;
	}
	
	aRange = [[loc lowercaseString] rangeOfString:@"javi"];
	if (aRange.location!=NSNotFound) {
		CLLocationCoordinate2D temp = {latitude: 40.912929, longitude: (-73.12207 + value/1000)};
		coordinate = temp;
	}
	
	return self;
}

@end
