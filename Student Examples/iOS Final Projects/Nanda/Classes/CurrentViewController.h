//
//  FirstViewController.h
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
	
@interface CurrentViewController : UIViewController {
	UIImage *backgroundImage;
	IBOutlet UIWebView *webView;
}

@property (nonatomic, retain) UIImage *backgroundImage;
@property (nonatomic, retain) UIWebView *webView;

@end