//
//  ScheduleAppDelegate.h
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "Course.h"

@interface ScheduleAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
    UIWindow *window;
    UITabBarController *tabBarController;
	
	NSString *databaseName;
	NSString *databasePath;
	
	NSMutableArray *courses;
	NSMutableArray *tasks;

}

@property (nonatomic, retain) NSString *databasePath;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, retain) NSMutableArray *courses;
@property (nonatomic, retain) NSMutableArray *tasks;

- (void) checkAndCreateDatabase;
- (void) readCoursesFromDatabase;
- (void) readTasksFromDatabase;
- (void) hideCompletedTasks;
- (void) addTaskToDatabase:(NSString *)newTask;

- (void) addCourseToDatabase:(Course *)newCourse;
- (void) removeCourse:(NSString *)courseName;

- (void) toggleTaskProperties:(NSString *)taskName isCompleted:(int)isComp;
- (void) deleteTask:(NSString *)taskName;

@end
