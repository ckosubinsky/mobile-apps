//
//  DrugsViewController.h
//  FP390
//
//  Created by Stéphane Kahn on 25/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Drug.h"


@interface DrugsViewController : UIViewController <UIPickerViewDelegate> {
	IBOutlet UILabel *expireLabel;
	IBOutlet UILabel *quantityLabel;
	IBOutlet UILabel *nameLabel;
	IBOutlet UILabel *titleNameLabel;
	
	UITextField *quantityField;
	UITextField *nameField;
	IBOutlet UIDatePicker *datePicker;

	Drug *drug;
}

@property (nonatomic, retain) IBOutlet UILabel *expireLabel;
@property (nonatomic, retain) IBOutlet UILabel *quantityLabel;
@property (nonatomic, retain) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) IBOutlet UILabel *titleNameLabel;

@property (nonatomic, retain) Drug *drug;

-(void) update;
-(void) changeDate;

@end
