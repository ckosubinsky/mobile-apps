//
//  addMedic.h
//  FP390
//
//  Created by Stéphane Kahn on 22/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FP390AppDelegate.h"


@interface AddViewController : UIViewController {
	FP390AppDelegate *appDelegate;
	
	IBOutlet UITextField *product;
	IBOutlet UITextField *quantity;
	
	IBOutlet UILabel *barcode;
	IBOutlet UIDatePicker *date;
	
	IBOutlet UIBarButtonItem *save;
	
	NSString *strBarcode;
	
	NSArray *locarray;
}

@property (nonatomic, retain) IBOutlet UITextField *product;
@property (nonatomic, retain) IBOutlet UITextField *quantity;
@property (nonatomic, retain) IBOutlet UILabel *barcode;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *save;
@property (nonatomic, retain) IBOutlet UIDatePicker *date;

@property (nonatomic, retain) NSArray *locarray;
@property (nonatomic, retain) NSString *strBarcode;

- (IBAction) dismissView:(id) sender;
- (IBAction) dismissKeyboard:(id) sender;
- (IBAction) saveDrug:(id) sender;

@end
