//
//  addMedic.m
//  FP390
//
//  Created by Stéphane Kahn on 22/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import "AddViewController.h"
#import "YAJL/YAJL.h"
#import <QuartzCore/QuartzCore.h>
#import "Drug.h"


@implementation AddViewController

@synthesize product, quantity,
barcode, save, date;
@synthesize locarray;
@synthesize strBarcode;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

}*/

-(void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	
	appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	if([strBarcode compare:NSLocalizedString(@"Manually", @"")] != NSOrderedSame){
		
		NSString *prefLang1;
		NSString *prefLang2;
	
		if([(NSString *)[[NSLocale preferredLanguages] objectAtIndex:0] compare:@"fr"] == NSOrderedSame){
			prefLang1 = @"fr";
			prefLang2 = @"en";
		} else {
			prefLang1 = @"en";
			prefLang2 = @"fr";
		}

		NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat: @"http://fr.getbarco.de/json.php?bc=%@", strBarcode]]];
		
		NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
		
		NSDictionary *array;
		array = [response yajl_JSON];

		if ([(NSString *)[array valueForKey:@"name"] compare: @""] == NSOrderedSame) {
			product.placeholder = NSLocalizedString(@"ProductNotFound", @"");
		} else {
			product.text = [array valueForKey:@"name"];
		}
		
//		NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
		
//		NSLog(@"%@", json_string);		
	}
	
	
	barcode.textColor = [UIColor colorWithRed:0.6 green:0.6 blue:1 alpha:1];
	barcode.font = [UIFont italicSystemFontOfSize:19.0];
	barcode.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
	barcode.shadowOffset = CGSizeMake(0, -1.0);
	barcode.text = strBarcode;
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void) viewDidLoad{
	date.date = [NSDate date];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction) dismissView:(id) sender{
	[self dismissModalViewControllerAnimated:YES];
}

- (IBAction) saveDrug:(id) sender{
	if(([[product text] compare:@""] == NSOrderedSame) || ([[quantity text] compare:@""] == NSOrderedSame)){
		UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WrongInput", @"") message:NSLocalizedString(@"MsgErrorInput", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Continue", @"") otherButtonTitles:nil] autorelease];
		[alert show];
	} else {
		Drug *drug = [[Drug alloc] initWithName:[[product text] capitalizedString] quantity:[quantity.text integerValue] expiration:[date date]];

		[Drug addDrugToDatabase:drug];
		
		[drug release];

		[self dismissModalViewControllerAnimated:YES];
	}
}

- (IBAction) dismissKeyboard:(id) sender{
	UITextField *input = (UITextField *) sender;
	[input resignFirstResponder];
}



- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {

	UITouch *myTouch = [[event allTouches] anyObject];
	
	if([product isEditing] && ([myTouch view] != product)){
		[product resignFirstResponder];
	} else if([quantity isEditing] && ([myTouch view] != quantity)){
		[quantity resignFirstResponder];
	}
}

@end
