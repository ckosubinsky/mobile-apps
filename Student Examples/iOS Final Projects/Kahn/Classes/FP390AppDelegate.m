//
//  FP390AppDelegate.m
//  FP390
//
//  Created by Stéphane Kahn on 22/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import "FP390AppDelegate.h"
#import "Drug.h"
#import "enumSort.h"
#import "Reachability.h"
#import "DeviceDetection.h"

@implementation FP390AppDelegate

@synthesize window;
@synthesize navigationController;
@synthesize inputFormatter, outputFormatter;
@synthesize drugsDictionary;
@synthesize sort, databasePath;
@synthesize systemName;
@synthesize reachable, scannable, alertDisplayed;
@synthesize sectionArray, titleSectionArray;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
	alertDisplayed = NO;
	
	UIDevice *device = [UIDevice currentDevice];
	systemName = [device model];

	
	// We check if the host is reachable. If getbarco.de is unreachable,
	// we won't be able to scan barcode since we won't fetch name from the Internet
	Reachability *hostReach = [Reachability reachabilityWithHostName:@"getbarco.de"];
	NetworkStatus status = [hostReach currentReachabilityStatus];
	
	switch (status) {
		case NotReachable:
			NSLog(@"Not Reachable");
			reachable = FALSE;
			break;
		case ReachableViaWiFi:
			NSLog(@"Reachable via Wifi");
			reachable = TRUE;
			break;
		case ReachableViaWWAN:
			NSLog(@"Reachable via WWAN");
			reachable = TRUE;
			break;
		default:
			break;
	}
	
	if([DeviceDetection detectDevice] == MODEL_IPHONE_4 || [DeviceDetection detectDevice] == MODEL_IPHONE_3GS) {
	// Does not handle if the device is a 3GS/4 or not yet.
		scannable = TRUE;
	} else {
		scannable = FALSE;
	}

	
	//On définit le nom de la base de données
	databaseName = @"medicDB.sql";	
	sort = SortByDate;
	
	// Initialisation des DateFormatter
	inputFormatter = [[NSDateFormatter alloc] init];
	[inputFormatter setDateFormat:@"yyyy-MM-dd"];
	outputFormatter = [[NSDateFormatter alloc] init];
	[outputFormatter setDateFormat:@"MM/yyyy"];
	
	
	// On récupère le chemin
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	
	// Execute the "checkAndCreateDatabase" function
	[self checkAndCreateDatabase];
	
	[window addSubview:[navigationController view]];

    [window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
	[drugsDictionary release];
	[inputFormatter release];
	[outputFormatter release];
	[navigationController release];
    [window release];
    [super dealloc];
}


- (void) checkAndCreateDatabase{
	// On vérifie si la BDD a déjà été sauvegardée dans l'iPhone de l'utilisateur
	BOOL success;

	// Crée un objet FileManagerCreate qui va servir à vérifer le status
	// de la base de données et de la copier si nécessaire
	NSFileManager *fileManager = [NSFileManager defaultManager];

	// Vérifie si la BDD a déjà été créée  dans les fichiers system de l'utilisateur
	success = [fileManager fileExistsAtPath:databasePath];
	
	// Si la BDD existe déjà "return" sans faire la suite
	if(success) return;
	
	// Si ce n'est pas le cas alors on copie la BDD de l'application vers les fichiers système de l'utilisateur
	
	// On récupère le chemin vers la BDD dans l'application
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
	
	// On copie la BDD de l'application vers le fichier systeme de l'application
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
	
	[fileManager release];
}

@end
