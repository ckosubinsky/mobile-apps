//
//  iphonetestViewController.m
//  iphonetest
//
//  Created by ColinWinnicki on 11/14/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//





#import "iphonetestViewController.h"


@implementation iphonetestViewController
@synthesize name;
@synthesize gametime;
@synthesize history;
@synthesize playeranswer;
@synthesize correctanswer;
@synthesize checkanswer;
@synthesize checktime;






/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];	
	mycounter = 0;
	ttltime = 0;
	name.text = @"Match the pattern as quickly as possible";
	history.text = @"Press new game to begin";
	
}


//initalizes the uiviews with uiimage depending on the value stored in the correct answer array
-(void) creategrid
{
	
	
	G0.image = [UIImage imageNamed:[[correctanswer objectAtIndex:0] stringByAppendingString:@".png"]];
	G1.image = [UIImage imageNamed:[[correctanswer objectAtIndex:1] stringByAppendingString:@".png"]];
	G2.image = [UIImage imageNamed:[[correctanswer objectAtIndex:2] stringByAppendingString:@".png"]];
	G3.image = [UIImage imageNamed:[[correctanswer objectAtIndex:3] stringByAppendingString:@".png"]];
	G4.image = [UIImage imageNamed:[[correctanswer objectAtIndex:4] stringByAppendingString:@".png"]];
	G5.image = [UIImage imageNamed:[[correctanswer objectAtIndex:5] stringByAppendingString:@".png"]];
	G6.image = [UIImage imageNamed:[[correctanswer objectAtIndex:6] stringByAppendingString:@".png"]];
	G7.image = [UIImage imageNamed:[[correctanswer objectAtIndex:7] stringByAppendingString:@".png"]];
	G8.image = [UIImage imageNamed:[[correctanswer objectAtIndex:8] stringByAppendingString:@".png"]];
	G9.image = [UIImage imageNamed:[[correctanswer objectAtIndex:9] stringByAppendingString:@".png"]];
	G10.image = [UIImage imageNamed:[[correctanswer objectAtIndex:10] stringByAppendingString:@".png"]];
	G11.image = [UIImage imageNamed:[[correctanswer objectAtIndex:11] stringByAppendingString:@".png"]];
	G12.image = [UIImage imageNamed:[[correctanswer objectAtIndex:12] stringByAppendingString:@".png"]];
	G13.image = [UIImage imageNamed:[[correctanswer objectAtIndex:13] stringByAppendingString:@".png"]];
	G14.image = [UIImage imageNamed:[[correctanswer objectAtIndex:14] stringByAppendingString:@".png"]];
	G15.image = [UIImage imageNamed:[[correctanswer objectAtIndex:15] stringByAppendingString:@".png"]];
	G16.image = [UIImage imageNamed:[[correctanswer objectAtIndex:16] stringByAppendingString:@".png"]];
	G17.image = [UIImage imageNamed:[[correctanswer objectAtIndex:17] stringByAppendingString:@".png"]];
	G18.image = [UIImage imageNamed:[[correctanswer objectAtIndex:18] stringByAppendingString:@".png"]];
	G19.image = [UIImage imageNamed:[[correctanswer objectAtIndex:19] stringByAppendingString:@".png"]];
	G20.image = [UIImage imageNamed:[[correctanswer objectAtIndex:20] stringByAppendingString:@".png"]];
	G21.image = [UIImage imageNamed:[[correctanswer objectAtIndex:21] stringByAppendingString:@".png"]];
	G22.image = [UIImage imageNamed:[[correctanswer objectAtIndex:22] stringByAppendingString:@".png"]];
	G23.image = [UIImage imageNamed:[[correctanswer objectAtIndex:23] stringByAppendingString:@".png"]];
	G24.image = [UIImage imageNamed:[[correctanswer objectAtIndex:24] stringByAppendingString:@".png"]];
	
}

//initalizes images on all buttons to ensure they are blank at the begining of each game
//sets their state to enabled
-(void) initializebuttons
{
	[ btn0 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn1 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn2 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn3 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn4 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn5 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn6 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn7 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn8 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn9 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn10 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn11 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn12 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn13 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn14 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn15 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn16 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn17 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn18 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn19 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn20 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn21 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn22 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn23 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	[ btn24 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	btn0.enabled = TRUE;
	btn1.enabled = TRUE;
	btn2.enabled = TRUE;
	btn3.enabled = TRUE;
	btn4.enabled = TRUE;
	btn5.enabled = TRUE;
	btn6.enabled = TRUE;
	btn7.enabled = TRUE;
	btn8.enabled = TRUE;
	btn9.enabled = TRUE;
	btn10.enabled = TRUE;
	btn11.enabled = TRUE;
	btn12.enabled = TRUE;
	btn13.enabled = TRUE;
	btn14.enabled = TRUE;
	btn15.enabled = TRUE;
	btn16.enabled = TRUE;
	btn17.enabled = TRUE;
	btn18.enabled = TRUE;
	btn19.enabled = TRUE;
	btn20.enabled = TRUE;
	btn21.enabled = TRUE;
	btn22.enabled = TRUE;
	btn23.enabled = TRUE;
	btn24.enabled = TRUE;
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if ((interfaceOrientation==UIInterfaceOrientationPortrait)||(interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown))
		
		// NO locks out the portrait mode of the iPhone. The initial orientation also has to be set in the info plist.
		
	{
		return NO;
	}
	if ((interfaceOrientation==UIInterfaceOrientationLandscapeLeft)||(interfaceOrientation==UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;

}


- (void)dealloc {
    [super dealloc];
}

/*
 NOTE
 Since all buttons are the same only btn0 is commented
*/



-(IBAction) btn0
{	
	int i = 0;
	// sets the value of temp to whatever is stored in player answer array at object index i
	NSString *temp = [playeranswer objectAtIndex:i];
	// checks if the value in temp is equal to 0
	if (temp == @"0")
	{
		//replaces the object at index i with a string 1
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		//sets button image to 1.png which shows a star
		[ btn0 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	// checks if the value in temp is equal to 1
	if (temp == @"1")
	{
		// replaces object at index i with a string 0
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		//clears the image on the button by sending a null string for the image name
		[ btn0 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn1
{
	int i = 1;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn1 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn1 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
}
-(IBAction) btn2
{
	int i = 2;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn2 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn2 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}

}
-(IBAction) btn3
{
	int i = 3;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn3 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn3 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
}
-(IBAction) btn4
{
	int i = 4;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn4 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn4 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
}

-(IBAction) btn5
{	
	int i = 5;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn5 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn5 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}

-(IBAction) btn6
{	
	int i = 6;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn6 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn6 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn7
{	
	int i = 7;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn7 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn7 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}

-(IBAction) btn8
{	
	int i = 8;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn8 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn8 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}

-(IBAction) btn9
{	
	int i = 9;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn9 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn9 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn10
{	
	int i = 10;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn10 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn10 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn11
{	
	int i = 11;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn11 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn11 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn12
{	
	int i = 12;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn12 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn12 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn13
{	
	int i = 13;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn13 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn13 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn14
{	
	int i = 14;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn14 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn14 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}

-(IBAction) btn15
{	
	int i = 15;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn15 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn15 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn16
{	
	int i = 16;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn16 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn16 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn17
{	
	int i = 17;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn17 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn17 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn18
{	
	int i = 18;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn18 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn18 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn19
{	
	int i = 19;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn19 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn19 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn20
{	
	int i = 20;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn20 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn20 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn21
{	
	int i = 21;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn21 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn21 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn22
{	
	int i = 22;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn22 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn22 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn23
{	
	int i = 23;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn23 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn23 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}
-(IBAction) btn24
{	
	int i = 24;
	NSString *temp = [playeranswer objectAtIndex:i];
	if (temp == @"0")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"1"];	
		[ btn24 setImage: [UIImage imageNamed: @"1.png"] forState: UIControlStateNormal];	
	}
	if (temp == @"1")
	{
		[playeranswer replaceObjectAtIndex:i withObject:@"0"];	
		[ btn24 setImage: [UIImage imageNamed: @""] forState: UIControlStateNormal];	
	}
	
}




//new game method run when new game button is clicked

-(IBAction) newgame
{
	//these were used for testing
	//NSString *test = [NSString stringWithFormat:"@%i",arc4random() % 1];
	//NSString *myNewString = [NSString stringWithFormat:@"%i",arc4random()%1];
	//NSLog(@"Random:%@",[NSString stringWithFormat:@"%i",arc4random()%1]);
	
	//disables the new game button
	newgame.enabled = FALSE;
	//sets time begin to the current time and retains it
	timebegin = [[NSDate date]retain];
	//NSLog(@"Start Time:%@",timebegin);
	//initalizes the check answer timer
	checkanswer=[NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(submitanswer) userInfo:nil repeats:YES];
	//initalizes the checktime timer
	checktime = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(gettime) userInfo:nil repeats:YES];
	name.text = @"Match the pattern as quickly as possible";
	history.text = @"Incorrect";
	//fills correct answer array with random numbers between 0 and 1
	correctanswer = [[NSMutableArray alloc] initWithObjects:[NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					 [NSString stringWithFormat:@"%i",arc4random()%2],
					  nil];
	//NSLog(@"CorrectAnswer:%@",correctanswer);
	
	//fills player answer array with all zeros
	playeranswer = [[NSMutableArray alloc] initWithObjects:@"0", @"0", @"0", @"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
	
	//NSLog(@"CorrectAnswer:%@",playeranswer);
	
	//calls creategrid
	[self creategrid];
	//calls initalize buttons
	[self initializebuttons];
	
	//used for testing
	//NSLog(@"timebegin retain count:%i",[timebegin retainCount]);
	//NSLog(@"checkanswer retain count:%i",[checkanswer retainCount]);
	//NSLog(@"checktime retain count:%i",[checktime retainCount]);
	//NSLog(@"correctanswer retain count:%i",[correctanswer retainCount]);
	//NSLog(@"playeranswer retain count:%i",[playeranswer retainCount]);
	

}

//submit answer method checks if the answer is correct
-(void) submitanswer
{
	// checks if the entire correctanswer array is equal to the entire playeranswer array
	if([correctanswer isEqualToArray:playeranswer])
	{
		
		history.text = @"Correct";
		//invalidates checkanswer timer
		[checkanswer invalidate];
		//invalidates checktime timer
		[checktime invalidate];
		//used for testing
		//NSLog(@"timebegin retain count:%i",[timebegin retainCount]);
		//NSLog(@"checkanswer retain count:%i",[checkanswer retainCount]);
		//NSLog(@"checktime retain count:%i",[checktime retainCount]);
		//NSLog(@"correctanswer retain count:%i",[correctanswer retainCount]);
		//NSLog(@"playeranswer retain count:%i",[playeranswer retainCount]);
		
		//sets all buttons to be disabled
		
		btn0.enabled = FALSE;
		btn1.enabled = FALSE;
		btn2.enabled = FALSE;
		btn3.enabled = FALSE;
		btn4.enabled = FALSE;
		btn5.enabled = FALSE;
		btn6.enabled = FALSE;
		btn7.enabled = FALSE;
		btn8.enabled = FALSE;
		btn9.enabled = FALSE;
		btn10.enabled = FALSE;
		btn11.enabled = FALSE;
		btn12.enabled = FALSE;
		btn13.enabled = FALSE;
		btn14.enabled = FALSE;
		btn15.enabled = FALSE;
		btn16.enabled = FALSE;
		btn17.enabled = FALSE;
		btn18.enabled = FALSE;
		btn19.enabled = FALSE;
		btn20.enabled = FALSE;
		btn21.enabled = FALSE;
		btn22.enabled = FALSE;
		btn23.enabled = FALSE;
		btn24.enabled = FALSE;
		
		//re-enables newgame button		
		newgame.enabled=TRUE;
		
		//increments mycounter inorder to take player average
		mycounter = mycounter + 1;
		//adds the currentcompletion time to ttltime which is the average time
		ttltime = ttltime + fabs([timebegin timeIntervalSinceNow]);
		//presents a popup which displays the players average time
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Average" message:[NSString stringWithFormat:@"%.2f",ttltime/mycounter]  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
		[alert show];
		[alert release];
		
		
		//NSLog(@"Counter:%i",mycounter);
		//NSLog(@"timeholder:%f",ttltime);
		//NSLog(@"Current Time:%f",fabs([timebegin timeIntervalSinceNow]));
		//NSLog(@"Average Time:%.2f",ttltime/mycounter);
	}
}

//method used to display running time
-(void) gettime
{
	//stores the current time in gametime.txt
	//had to take absolute value because the timeinterval was returning negative time
	gametime.text = [NSString stringWithFormat:@"%.1f",fabs([timebegin timeIntervalSinceNow])];
}



@end
