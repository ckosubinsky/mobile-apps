//
//  iphonetestAppDelegate.h
//  iphonetest
//
//  Created by ColinWinnicki on 11/14/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class iphonetestViewController;

@interface iphonetestAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    iphonetestViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet iphonetestViewController *viewController;

@end

