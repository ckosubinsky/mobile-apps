//
//  EightGameAppDelegate.h
//  EightGame
//
//  Created by AntonKanevsky on 11/17/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EightGameViewController;

@interface EightGameAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    EightGameViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet EightGameViewController *viewController;

@end

