//
//  ProjectViewController.h
//  Project
//
//  Created by MingtungWong on 10/20/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectViewController : UIViewController {
	
	IBOutlet UIImageView *prev1;
	IBOutlet UIImageView *prev2;
	IBOutlet UIImageView *top;
	IBOutlet UIImageView *bottom;
	IBOutlet UIImageView *paddle;
	IBOutlet UIImageView *paddle2;	
	IBOutlet UIImageView *ball;
	IBOutlet UILabel *score;
	IBOutlet UILabel *score2;	
	

}

//@property (nonatomic, retain) UIImageView *prev1;
//@property (nonatomic, retain) UIImageView *prev2;
@property (nonatomic, retain) UIImageView *paddle;
@property (nonatomic, retain) UIImageView *paddle2;
@property (nonatomic, retain) UILabel *score;
@property (nonatomic, retain) UILabel *score2;

- (void) checkCollision;

- (void) move;



@end

