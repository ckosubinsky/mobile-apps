//
//  DisplayMap.h
//  CookingMadeEasy
//
//  Created by SammyLi on 12/7/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>


@interface DisplayMap : NSObject <MKAnnotation>{
	//Our own object of annotation allows us to create custom points
	CLLocationCoordinate2D coordinate; 
	NSString *title; 
	NSString *subtitle;
}


@property (nonatomic, assign) CLLocationCoordinate2D coordinate; 
@property (nonatomic, copy) NSString *title; 
@property (nonatomic, copy) NSString *subtitle;

@end
