//
//  RecipesDetailViewController.h
//  CookingMadeEasy
//
//  Created by SammyLi on 12/1/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RecipesDetailViewController : UIViewController {
	UILabel *recipeName;
	UILabel *cooktime;
	IBOutlet UIImageView *foodImage;
	
	IBOutlet UITextView *ingredients;
	IBOutlet UITextView *steps;
	
	IBOutlet UIScrollView *scrollView;
}


@property (nonatomic, retain) IBOutlet UILabel *recipeName;
@property (nonatomic, retain) IBOutlet UILabel *cooktime;
@property (nonatomic, retain) IBOutlet UITextView *steps;
@property (nonatomic, retain) IBOutlet UITextView *ingredients;
@property (nonatomic, retain) IBOutlet UIImageView *foodImage;
@end
