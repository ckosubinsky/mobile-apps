//
//  Task.m
//  Schedule
//
//  Created by Raman Nanda on 11/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Task.h"


@implementation Task
@synthesize taskTitle, details;

- (int) isCompleted	{	return (int)isCompleted;	}
- (int) isVisible	{	return (int)isVisible;		}

- (void) setIsCompleted:(int)input	{	isCompleted = (int *)input;		}
- (void) setIsVisible:(int)input	{	isVisible = (int *)input;		}

- (id) init:(NSString *)taskT details:(NSString *)det isCompleted:(int)isComp isVisible:(int)isVis
{
	self.taskTitle = taskT;
	self.details = det;
	self.isCompleted = isComp;
	self.isVisible = isVis;
	return self;
}

- (void) dealloc
{
	[taskTitle release];
	[details release];
	[super dealloc];
}
@end
