//
//  Class.m
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Course.h"


@implementation Course
@synthesize dept, crsTitle, professor, loc, startTime, endTime;

- (int) crsNum		{	return (int)crsNum;			}
- (int) isMonday	{	return (int)isMonday;		}
- (int) isTuesday	{	return (int)isTuesday;		}
- (int) isWednesday {	return (int)isWednesday;	}
- (int) isThursday	{	return (int)isThursday;		}
- (int) isFriday	{	return (int)isFriday;		}

- (NSString *) getDept		{	return dept;		}
- (NSString *) getCrsTitle	{	return crsTitle;	}
- (NSString *) getLocation	{	return loc;			}
- (NSString *) getTime		{	return startTime;	}

- (void) setCrsNum:(int)input		{	crsNum = (int *)input;			}
- (void) setIsMonday:(int)input		{	isMonday = (int *)input;		}
- (void) setIsTuesday:(int)input	{	isTuesday = (int *)input;		}
- (void) setIsWednesday:(int)input	{	isWednesday = (int *)input;		}
- (void) setIsThursday:(int)input	{	isThursday = (int *)input;		}
- (void) setIsFriday:(int)input		{	isFriday = (int *)input;		}

- (id) init:(NSString *)dep crsNum:(int)crsN crsTitle:(NSString *)crsT professor:(NSString *)prof loc:(NSString *)locate startTime:(NSString *)startT endTime:(NSString *)endT isMonday:(int)mon isTuesday:(int)tues isWednesday:(int)wed isThursday:(int)thurs isFriday:(int)fri
{
	// This method contructs a Course Object given its parameters
	self.dept = dep;
	self.crsNum = crsN;
	self.crsTitle = crsT;
	self.loc = locate;
	self.professor = prof;
	self.startTime = startT;
	self.endTime = endT;
	self.isMonday = mon;
	self.isTuesday = tues;
	self.isWednesday = wed;
	self.isThursday = thurs;
	self.isFriday = fri;
	return	self;
}



- (void) dealloc
{
	[dept release];
	[crsTitle release];
	[professor release];
	[loc release];
	[startTime release];
	[endTime release];
	[super dealloc];
}

@end
