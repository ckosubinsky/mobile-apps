//
//  CourseAnnotation.h
//  Schedule
//
//  Created by Raman Nanda on 11/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

// Helper method for the Map view

@interface CourseAnnotation : NSObject<MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	NSString *courseName;
	NSString *classTime;
}

//- (CLLocationCoordinate2D)getCoordinateFromLocation:(NSString *) location;
- (id)initWithCourse:(NSString *)crsName location:(NSString *)loc time:(NSString *)timeString offset:(int)value;

@property (nonatomic, retain) NSString	*courseName;
@property (nonatomic, retain) NSString	*classTime;


@end