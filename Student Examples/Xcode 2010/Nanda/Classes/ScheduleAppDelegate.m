//
//  ScheduleAppDelegate.m
//  Schedule
//
//  Created by Raman Nanda on 11/16/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ScheduleAppDelegate.h"
#import "Course.h"
#import "Task.h"

@implementation ScheduleAppDelegate
@synthesize databasePath;
@synthesize window;
@synthesize tabBarController;
@synthesize	courses;
@synthesize tasks;

#pragma mark -
#pragma mark Application lifecycle

// Checks to See if the Database exists in the filepath of the iPhone App

- (void) checkAndCreateDatabase {
	BOOL success;
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	success = [fileManager fileExistsAtPath:databasePath];
	
	if (success) {
		return;
	}
	
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
	[fileManager release];
}

// Takes in a parameter of the course and adds that course to the database

- (void) addCourseToDatabase:(Course *)newCourse {
	sqlite3 *database;
	
	databaseName = @"Schedule.sqlite";
	
	NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentsPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	
	NSInteger dbOpen;
	dbOpen = sqlite3_open([databasePath UTF8String], &database);
	
	
	if (dbOpen == SQLITE_OK) {
		const char *sqlStatement = "INSERT INTO Courses(dept, crsNum, crsTitle, location, startTime, endTime, professor, isMonday, isTuesday, isWednesday, isThursday, isFriday) Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
		{
			sqlite3_bind_text(compiledStatement, 1, [newCourse.dept UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_int(compiledStatement, 2, (int)newCourse.crsNum);
			sqlite3_bind_text(compiledStatement, 3, [newCourse.crsTitle UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(compiledStatement, 4, [newCourse.loc UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(compiledStatement, 5, [newCourse.startTime UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(compiledStatement, 6, [newCourse.endTime UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(compiledStatement, 7, [newCourse.professor UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_int(compiledStatement, 8, (int)newCourse.isMonday);
			sqlite3_bind_int(compiledStatement, 9, (int)newCourse.isTuesday);
			sqlite3_bind_int(compiledStatement, 10, (int)newCourse.isWednesday);
			sqlite3_bind_int(compiledStatement, 11, (int)newCourse.isThursday);
			sqlite3_bind_int(compiledStatement, 12, (int)newCourse.isFriday);
		}
		if(sqlite3_step(compiledStatement) != SQLITE_DONE ) {
			NSLog( @"Error: %s", sqlite3_errmsg(database) );
		} else {
			NSLog( @"Insert into row id = %d", sqlite3_last_insert_rowid(database));
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);	
	[self readCoursesFromDatabase];
}

// Reads all the courses from the database and adds them to the Courses array in this class.
// From this point on all view controllers will have a handle to this class and will access this array

- (void) readCoursesFromDatabase {
	sqlite3 *database;
	
	courses = [[NSMutableArray alloc] init];
	
	if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT dept, crsNum, crsTitle, professor, location, startTime, endTime, isMonday, isTuesday, isWednesday, isThursday, isFriday from Courses";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
				while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
					NSString *dbDept = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
					int *dbCrsNum = (int *)sqlite3_column_int(compiledStatement, 1);
					NSString *dbCrsTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
					NSString *dbProfessor = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
					NSString *dbLocation = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
					NSString *dbStartTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
					NSString *dbEndTime = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
					int *dbIsMonday = (int *)sqlite3_column_int(compiledStatement, 7);
					int *dbIsTuesday = (int *)sqlite3_column_int(compiledStatement, 8);
					int *dbIsWednesday = (int *)sqlite3_column_int(compiledStatement, 9);
					int *dbIsThursday = (int *)sqlite3_column_int(compiledStatement, 10);
					int *dbIsFriday = (int *)sqlite3_column_int(compiledStatement, 11);
										   
					Course *newCourse = [[Course alloc] init:dbDept crsNum:(int)dbCrsNum crsTitle:dbCrsTitle professor:dbProfessor
						loc:dbLocation startTime:dbStartTime endTime:dbEndTime isMonday:(int)dbIsMonday isTuesday:(int)dbIsTuesday 
												 isWednesday:(int)dbIsWednesday isThursday:(int)dbIsThursday isFriday:(int)dbIsFriday];
					
					[courses addObject:newCourse];
					[newCourse release];
				}
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}

// Reads the tasks from the Database and enters them in the tasks Arraylist. Like Courses, all other view controllers will have
// access to the schedule delegate and its task array list.

- (void) readTasksFromDatabase {
	sqlite3 *database;
	
	tasks = [[NSMutableArray alloc] init];
	
	if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
		const char *sqlStatement = "SELECT taskTitle, taskDetails, isCompleted, isVisible from Tasks WHERE isVisible = '1'";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			while (sqlite3_step(compiledStatement) == SQLITE_ROW) {
				NSString *dbTaskTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
				NSString *dbDetails = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
				int *dbIsCompleted = (int *)sqlite3_column_int(compiledStatement, 2);
				int *dbIsVisible = (int *)sqlite3_column_int(compiledStatement, 3);
				Task *newTask = [[Task alloc] init:dbTaskTitle 
					details:dbDetails isCompleted:(int)dbIsCompleted isVisible:(int)dbIsVisible];
				
				[tasks addObject:newTask];
				[newTask release];
			}
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}

// This method will go through all of the tasks in the database and if they have been marked as complete then they will be
// deleted from the table

- (void) hideCompletedTasks {
	sqlite3 *database;
	
	databaseName = @"Schedule.sqlite";
	
	NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentsPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	
	NSInteger dbOpen;
	dbOpen = sqlite3_open([databasePath UTF8String], &database);
	
	
	if (dbOpen == SQLITE_OK) {
		const char *sqlStatement = "DELETE FROM Tasks WHERE isCompleted = '1'";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
		{}
		if(sqlite3_step(compiledStatement) != SQLITE_DONE ) {
			NSLog( @"Error: %s", sqlite3_errmsg(database) );
		} else {
			NSLog( @"Update into row id = %d", sqlite3_last_insert_rowid(database));
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);	
	[self readTasksFromDatabase];
}

// This method takes in a task object and adds it to the database

- (void) addTaskToDatabase:(NSString *)newTask {
	sqlite3 *database;
	
	databaseName = @"Schedule.sqlite";
	
	NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentsPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	
	NSInteger dbOpen;
	dbOpen = sqlite3_open([databasePath UTF8String], &database);
	
	
	if (dbOpen == SQLITE_OK) {
		const char *sqlStatement = "INSERT INTO Tasks(taskTitle, taskDetails, isCompleted, isVisible) Values(?, ?, ?, ?)";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
		{
			sqlite3_bind_text(compiledStatement, 1, [newTask UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_text(compiledStatement, 2, [@"None" UTF8String], -1, SQLITE_TRANSIENT);
			sqlite3_bind_int(compiledStatement, 3, 0);
			sqlite3_bind_int(compiledStatement, 4, 1);
		}
		if(sqlite3_step(compiledStatement) != SQLITE_DONE ) {
			NSLog( @"Error: %s", sqlite3_errmsg(database) );
		} else {
			NSLog( @"Insert into row id = %d", sqlite3_last_insert_rowid(database));
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);	
	[self readTasksFromDatabase];
}

// This method will set the is Complete variable of Task given a particular task. 

- (void) toggleTaskProperties:(NSString *)taskName isCompleted:(int)isComp;
{
	sqlite3 *database;
	
	databaseName = @"Schedule.sqlite";
	
	NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentsPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	
	NSInteger dbOpen;
	dbOpen = sqlite3_open([databasePath UTF8String], &database);
	
	if (dbOpen == SQLITE_OK) {
		const char *sqlStatement = "UPDATE Tasks set isCompleted = ? WHERE taskTitle = ? AND taskDetails = 'None'";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
		{
			sqlite3_bind_int(compiledStatement, 1, isComp);
			sqlite3_bind_text(compiledStatement, 2, [taskName UTF8String], -1, SQLITE_TRANSIENT);
		}
		if(sqlite3_step(compiledStatement) != SQLITE_DONE ) {
			NSLog( @"Error: %s", sqlite3_errmsg(database) );
		} else {
			NSLog( @"Updated into row id = %d", sqlite3_last_insert_rowid(database));
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);	
	[self readTasksFromDatabase];
}

// Given a Task name this method will delete that instance from the Table

- (void) deleteTask:(NSString *)taskName;
{
	sqlite3 *database;
	
	databaseName = @"Schedule.sqlite";
	
	NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentsPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	
	NSInteger dbOpen;
	dbOpen = sqlite3_open([databasePath UTF8String], &database);
	
	if (dbOpen == SQLITE_OK) {
		const char *sqlStatement = "DELETE FROM Tasks WHERE taskTitle = ? AND taskDetails = 'None'";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
		{
			sqlite3_bind_text(compiledStatement, 1, [taskName UTF8String], -1, SQLITE_TRANSIENT);
		}
		if(sqlite3_step(compiledStatement) != SQLITE_DONE ) {
			NSLog( @"Error: %s", sqlite3_errmsg(database) );
		} else {
			NSLog( @"Updated into row id = %d", sqlite3_last_insert_rowid(database));
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);	
	[self readTasksFromDatabase];
}

// This method will remove a course given a course name from the table.


- (void) removeCourse:(NSString *)courseName;
{
	sqlite3 *database;
	
	databaseName = @"Schedule.sqlite";
	
	NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentsPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	
	NSInteger dbOpen;
	dbOpen = sqlite3_open([databasePath UTF8String], &database);
	
	if (dbOpen == SQLITE_OK) {
		const char *sqlStatement = "DELETE FROM Courses WHERE crsTitle = ?";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
		{
			sqlite3_bind_text(compiledStatement, 1, [courseName UTF8String], -1, SQLITE_TRANSIENT);
		}
		if(sqlite3_step(compiledStatement) != SQLITE_DONE ) {
			NSLog( @"Error: %s", sqlite3_errmsg(database) );
		} else {
			NSLog( @"Updated into row id = %d", sqlite3_last_insert_rowid(database));
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);	
	[self readTasksFromDatabase];
}

// When the application launches we want to check to see if the DB File exist, and populate both arrays of tasks and 
// courses based on the tables. 

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    databaseName = @"Schedule.sqlite";
	
	NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentsPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
	
	[self checkAndCreateDatabase];
	
	[self readCoursesFromDatabase];
	
	[self readTasksFromDatabase];

    // Add the tab bar controller's view to the window and display.
    [window addSubview:tabBarController.view];
    [window makeKeyAndVisible];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

// Updates the Badge Number with the number of tasks
- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
	int numberOfIncompleteTasks = 0;
	for(int i = 0; i < [tasks count]; i++)
	{
		Task *item = [tasks objectAtIndex:i];
		if(item.isCompleted == (int *)0)
			numberOfIncompleteTasks++;
	}
	
	[[UIApplication sharedApplication] setApplicationIconBadgeNumber:numberOfIncompleteTasks];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark UITabBarControllerDelegate methods

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
}
*/


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
	[courses release];
    [tabBarController release];
    [window release];
    [super dealloc];
}

@end

