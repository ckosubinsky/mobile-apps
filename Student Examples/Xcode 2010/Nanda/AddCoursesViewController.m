//
//  AddCoursesViewController.m
//  Schedule
//
//  Created by Raman Nanda on 11/17/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AddCoursesViewController.h"
#import "Course.h"
#import "ScheduleAppDelegate.h"


@implementation AddCoursesViewController
@synthesize textFieldCrsTitle, textFieldDept, textFieldCrsNum, textFieldProf, textFieldLocation, textFieldStartTime, textFieldEndTime, 
mondayCtrl, tuesdayCtrl, wednesdayCtrl, thursdayCtrl, fridayCtrl;


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	[scrollView setScrollEnabled:YES];
	[scrollView setContentSize:CGSizeMake(320, 1500)];
	
	mondayCtrl.on = NO;
	tuesdayCtrl.on = NO;
	wednesdayCtrl.on = NO;
	thursdayCtrl.on = NO;
	fridayCtrl.on = NO;
	
}

- (BOOL) canBecomeFirstResponder {
	return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[textFieldCrsTitle  resignFirstResponder];
	[textFieldCrsNum  resignFirstResponder];
	[textFieldDept  resignFirstResponder];
	[textFieldProf  resignFirstResponder];
	[textFieldLocation  resignFirstResponder];
	[textFieldStartTime  resignFirstResponder];
	[textFieldEndTime  resignFirstResponder];
}

// Frees the Keyboard when the return key is pressed
- (IBAction) textFieldReturn:(id)sender
{
	[sender  resignFirstResponder];
}

- (IBAction) backToScheduleView:(id)sender
{
	[self.parentViewController dismissModalViewControllerAnimated:YES];
}

// Adds the course to the array and the database
- (IBAction) addCourseToSchedule:(id)sender
{
	if([textFieldCrsTitle.text length] == 0 || [textFieldCrsNum.text length] == 0 || [textFieldDept.text length] == 0 || [textFieldProf.text length] == 0 || [textFieldLocation.text length] == 0)
	{
		UIAlertView *alertFields = [[UIAlertView alloc] initWithTitle:@"Incomplete Fields" message:@"Please Fill In all Fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertFields show];
		[alertFields release];
	}
	else {
		int isMonday = 0;
		int isTuesday = 0;
		int isWednesday = 0;
		int isThursday = 0;
		int isFriday = 0;
		
		int crsNum = [textFieldCrsNum.text intValue];
		
		if(mondayCtrl.on)		{	isMonday = 1;		}
		if(tuesdayCtrl.on)		{	isTuesday = 1;		}
		if(wednesdayCtrl.on)	{	isWednesday = 1;	}
		if(thursdayCtrl.on)		{	isThursday = 1;		}
		if(fridayCtrl.on)		{	isFriday = 1;		}

		Course *newCourse = [[Course alloc] init:textFieldDept.text crsNum:(int)crsNum crsTitle:textFieldCrsTitle.text professor:textFieldProf.text
											 loc:textFieldLocation.text startTime:textFieldStartTime.text endTime:textFieldEndTime.text isMonday:(int)isMonday isTuesday:(int)isTuesday 
									 isWednesday:(int)isWednesday isThursday:(int)isThursday isFriday:(int)isFriday];
		
		ScheduleAppDelegate *appDelegate = (ScheduleAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate addCourseToDatabase:newCourse];
		
		[self.parentViewController dismissModalViewControllerAnimated:YES];
		
	}
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
