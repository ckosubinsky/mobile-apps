//
//  QB_StatsAppDelegate.h
//  QB Stats
//
//  Created by DayneHoffman on 11/8/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QB_StatsViewController;

@interface QB_StatsAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    QB_StatsViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet QB_StatsViewController *viewController;

@end

