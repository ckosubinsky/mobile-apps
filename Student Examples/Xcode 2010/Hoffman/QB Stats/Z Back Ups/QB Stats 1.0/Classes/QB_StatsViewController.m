//
//  QB_StatsViewController.m
//  QB Stats
//
//  Created by DayneHoffman on 11/8/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "QB_StatsViewController.h"

@implementation QB_StatsViewController


//Synthesize Text Fields
@synthesize CompletionText;
@synthesize AttemptText;

@synthesize CompletionPercentageText;


//Synthesize Timer
@synthesize GameStatTimer;


//Universal Variables
float Completion;
float Attempt;

float CompletionPercentage;



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	
	//Start Timer for the continual updates of stats
	[self StartTimer];
}


//Start Timer for the continual updates of stats
- (void) StartTimer
{
	GameStatTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(GameStats) userInfo:nil repeats:YES] retain];
}


//Stat Calculations
- (void) GameStats
{
	//Completion Percentage
	CompletionPercentage = ((Completion/Attempt)*100);
	
	//Display Value in Output Field
	[CompletionPercentageText setText:[NSString stringWithFormat:@"%1.1f",CompletionPercentage]];
		
	[CompletionText setText:[NSString stringWithFormat:@"%1.0f",Completion]];
	
	[AttemptText setText:[NSString stringWithFormat:@"%1.0f",Attempt]];
	
}


//Buttons
- (IBAction) AddCompletion
{
	Completion = (Completion + 1);
	Attempt = (Attempt + 1);
}


- (IBAction) AddAttempt
{
	Attempt = (Attempt + 1);
}


- (IBAction) SubtractCompletion
{
	Completion = (Completion - 1);
}


- (IBAction) SubtractAttempt
{
	Attempt = (Attempt - 1);
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
