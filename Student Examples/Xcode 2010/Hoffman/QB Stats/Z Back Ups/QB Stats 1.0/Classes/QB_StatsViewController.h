//
//  QB_StatsViewController.h
//  QB Stats
//
//  Created by DayneHoffman on 11/8/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QB_StatsViewController : UIViewController 
{
	//Stat Input Text Fields
	IBOutlet UITextField *CompletionText;									//Completions
	IBOutlet UITextField *AttemptText;										//Attempts
	
	//Stat Output Text Fields
	IBOutlet UITextField *CompletionPercentageText;							//Completion Percentage
}


//Property Input Stats
@property (nonatomic, retain) UITextField *CompletionText;					//Completions
@property (nonatomic, retain) UITextField *AttemptText;						//Attempts


//Property Output Stats
@property (nonatomic, retain) UITextField *CompletionPercentageText;		 //Completion Percentage


//Timer Property
@property (nonatomic, retain) NSTimer *GameStatTimer;


//Button Actions
- (IBAction) DismissKeyboard;


//Addition Buttons
- (IBAction) AddCompletion;
- (IBAction) AddAttempt;


//Subtraction Buttons
- (IBAction) SubtractCompletion;
- (IBAction) SubtractAttempt;


- (void) GameStats;


@end

