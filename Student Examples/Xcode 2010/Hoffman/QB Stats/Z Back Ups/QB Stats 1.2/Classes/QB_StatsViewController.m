//
//  QB_StatsViewController.m
//  QB Stats
//
//  Created by DayneHoffman on 11/8/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "QB_StatsViewController.h"

@implementation QB_StatsViewController


//Synthesize Text Fields
@synthesize CompletionText;
@synthesize AttemptText;
@synthesize YardText;
@synthesize YardTextEnter;
@synthesize TouchDownText;
@synthesize InterceptionText;

@synthesize CompletionPercentageText;
@synthesize CollegiateRatingText;
@synthesize ProRatingText;
@synthesize RatingText;


//Synthesize Timer
@synthesize GameStatTimer;


//Universal Variables
float Completion;
float Attempt;
float Yard;
float PlayYard;
float TouchDown;
float Interception;

float CompletionPercentage;
float CollegiateRating;
float ProRating;
float Rating;
float X;



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	
	//Start Timer for the continual updates of stats
	[self StartTimer];
}


//Start Timer for the continual updates of stats
- (void) StartTimer
{
	GameStatTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(GameStats) userInfo:nil repeats:YES] retain];
}


//Stat Calculations
- (void) GameStats
{
	PlayYard = ([YardTextEnter.text floatValue]);
	
	
	//Completion Percentage
	CompletionPercentage = ((Completion/Attempt)*100);
	
	
	//QB Rating
	//Collegiate Rating
	if (X = 0)
	{
		Rating = (((8.4*Yard)+(330*TouchDown)+(100*Completion)-(200*Interception))/Attempt);
	}
	
	//Pro Rating 
	if (X = 1)
	{
		//Pro Rating Initial
		float alpha = (((Completion/Attempt)-.3)*5);
		float beta = (((Yard/Attempt)-3)*.25);
		float kappa = ((TouchDown/Attempt)*20);
		float delta = (2.375-((Interception/Attempt)*25));
		
		Rating = (((alpha+beta+kappa+delta)/6)*100);
	}
				  
		
	//Display Value in Output Field
	[CompletionPercentageText setText:[NSString stringWithFormat:@"%1.1f",CompletionPercentage]];
		
	[CompletionText setText:[NSString stringWithFormat:@"%1.0f",Completion]];
	
	[AttemptText setText:[NSString stringWithFormat:@"%1.0f",Attempt]];
	
	[YardText setText:[NSString stringWithFormat:@"%1.0f",Yard]];
	
	[TouchDownText setText:[NSString stringWithFormat:@"%1.0f",TouchDown]];
	
	[InterceptionText setText:[NSString stringWithFormat:@"%1.0f",Interception]];
	
	[RatingText setText:[NSString stringWithFormat:@"%1.1f",Rating]];
}


//Buttons
- (IBAction) AddCompletion
{
	Completion = (Completion + 1);
	Attempt = (Attempt + 1);
}

- (IBAction) SubtractCompletion
{
	Completion = (Completion - 1);
}


- (IBAction) AddAttempt
{
	Attempt = (Attempt + 1);
}

- (IBAction) SubtractAttempt
{
	Attempt = (Attempt - 1);
}


- (IBAction) AddYard
{
	Yard = (Yard + 1);
}

- (IBAction) SubtractYard
{
	Yard = (Yard - 1);
}


- (IBAction) AddYardTextEnter
{
	Yard = (Yard + PlayYard);
	[YardTextEnter setText:nil];
}

- (IBAction) SubtractYardTextEnter
{
	Yard = (Yard - PlayYard);
	[YardTextEnter setText:nil];
}


- (IBAction) AddTouchDown
{
	TouchDown = (TouchDown + 1);
}

- (IBAction) SubtractTouchDown
{
	TouchDown = (TouchDown - 1);
}


- (IBAction) AddInterception
{
	Interception = (Interception + 1);
}

- (IBAction) SubtractInterception
{
	Interception = (Interception - 1);
}



//Number Generator for Rating
- (IBAction) MakeCollegiateRating
{
	X = 0;
}

- (IBAction) MakeProRating
{
	X = 1;
}




- (IBAction) DismissKeyboard
{
	[YardTextEnter resignFirstResponder];
}




/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
