//
//  QB_StatsViewController.h
//  QB Stats
//
//  Created by DayneHoffman on 11/8/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QB_StatsViewController : UIViewController 
{
	//Stat Input Text Fields
	IBOutlet UITextField *CompletionText;									//Completions
	IBOutlet UITextField *AttemptText;										//Attempts
	IBOutlet UITextField *YardText;											//Yards
	IBOutlet UITextField *YardTextEnter;									//Yard Text Enter
	
	//Stat Output Text Fields
	IBOutlet UITextField *CompletionPercentageText;							//Completion Percentage
}


//Property Input Stats
@property (nonatomic, retain) UITextField *CompletionText;					//Completions
@property (nonatomic, retain) UITextField *AttemptText;						//Attempts
@property (nonatomic, retain) UITextField *YardText;						//Yards
@property (nonatomic, retain) UITextField *YardTextEnter;					//Yard Text Enter


//Property Output Stats
@property (nonatomic, retain) UITextField *CompletionPercentageText;		 //Completion Percentage


//Timer Property
@property (nonatomic, retain) NSTimer *GameStatTimer;


//Button Actions
- (IBAction) DismissKeyboard;


//Addition Buttons
- (IBAction) AddCompletion;
- (IBAction) AddAttempt;
- (IBAction) AddYard;
- (IBAction) AddYardTextEnter;


//Subtraction Buttons
- (IBAction) SubtractCompletion;
- (IBAction) SubtractAttempt;
- (IBAction) SubtractYard;
- (IBAction) SubtractYardTextEnter;


- (void) GameStats;


@end

