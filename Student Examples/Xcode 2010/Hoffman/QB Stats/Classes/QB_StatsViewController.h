//
//  QB_StatsViewController.h
//  QB Stats
//
//  Created by DayneHoffman on 11/8/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QB_StatsViewController : UIViewController 
{
	//Stat Input Text Fields
	IBOutlet UITextField *CompletionText;									//Completions
	IBOutlet UITextField *AttemptText;										//Attempts
	IBOutlet UITextField *YardText;											//Yards
	IBOutlet UITextField *YardTextEnter;									//Yard Text Enter
	IBOutlet UITextField *TouchDownText;									//Touch Downs
	IBOutlet UITextField *InterceptionText;									//Interceptions
		
	//Stat Output Text Fields
	IBOutlet UITextField *CompletionPercentageText;							//Completion Percentage
	IBOutlet UITextField *CollegiateRatingText;								//College Rating
	IBOutlet UITextField *ProRatingText;									//Pro Rating
	IBOutlet UITextField *RatingText;											//Rating dependent on variable
}


//Property Input Stats
@property (nonatomic, retain) UITextField *CompletionText;					//Completions
@property (nonatomic, retain) UITextField *AttemptText;						//Attempts
@property (nonatomic, retain) UITextField *YardText;						//Yards
@property (nonatomic, retain) UITextField *YardTextEnter;					//Yard Text Enter
@property (nonatomic, retain) UITextField *TouchDownText;					//Touch Downs
@property (nonatomic, retain) UITextField *InterceptionText;				//Interceptions


//Property Output Stats
@property (nonatomic, retain) UITextField *CompletionPercentageText;		//Completion Percentage
@property (nonatomic, retain) UITextField *CollegiateRatingText;			//College Rating
@property (nonatomic, retain) UITextField *ProRatingText;					//Pro Rating
@property (nonatomic, retain) UITextField *RatingText;							//Rating dependent on variable


//Timer Property
@property (nonatomic, retain) NSTimer *GameStatTimer;


//Button Actions
- (IBAction) DismissKeyboard;


//Addition Buttons
- (IBAction) AddCompletion;
- (IBAction) AddAttempt;
- (IBAction) AddYard;
- (IBAction) AddYardTextEnter;
- (IBAction) AddTouchDown;
- (IBAction) AddInterception; 


//Subtraction Buttons
- (IBAction) SubtractCompletion;
- (IBAction) SubtractAttempt;
- (IBAction) SubtractYard;
- (IBAction) SubtractYardTextEnter;
- (IBAction) SubtractTouchDown;
- (IBAction) SubtractInterception; 


//Control Buttons
- (IBAction) MakeCollegiateRating;
- (IBAction) MakeProRating;


- (void) GameStats;


@end

