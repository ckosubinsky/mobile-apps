//
//  Quarterback_RatingAppDelegate.h
//  Quarterback Rating
//
//  Created by DayneHoffman on 10/13/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Quarterback_RatingViewController;

@interface Quarterback_RatingAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    Quarterback_RatingViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Quarterback_RatingViewController *viewController;

@end

