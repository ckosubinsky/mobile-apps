//
//  Quarterback_RatingViewController.h
//  Quarterback Rating
//
//  Created by DayneHoffman on 10/13/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

#import<AVFoundation/AVAudioPlayer.h>

@interface Quarterback_RatingViewController : UIViewController 
{
	IBOutlet UITextField *comp;
	IBOutlet UITextField *att;
	IBOutlet UITextField *yards;
	IBOutlet UITextField *td;
	IBOutlet UITextField *intc;
	IBOutlet UITextField *qbrate;
	IBOutlet UIButton *calculate;
}

- (IBAction) createrate;

@property (nonatomic, retain) UITextField *comp;
@property (nonatomic, retain) UITextField *att;
@property (nonatomic, retain) UITextField *yards;
@property (nonatomic, retain) UITextField *td;
@property (nonatomic, retain) UITextField *intc;
@property (nonatomic, retain) UITextField *qbrate;

- (IBAction) playSound:(id)sender;

@end

