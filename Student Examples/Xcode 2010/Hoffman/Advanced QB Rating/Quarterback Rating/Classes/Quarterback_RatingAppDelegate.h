//
//  Quarterback_RatingAppDelegate.h
//  Quarterback Rating
//
//  Copyright Franchise Athletic LLC 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Quarterback_RatingViewController;

@interface Quarterback_RatingAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    Quarterback_RatingViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Quarterback_RatingViewController *viewController;

@end

