//
//  Batting_StatisticsViewController.m
//  Batting Statistics
//
//  Created by DayneHoffman on 11/1/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "Batting_StatisticsViewController.h"

@implementation Batting_StatisticsViewController

//Stats that must be input by user
@synthesize BG;		//Games (Batting)
@synthesize BGS;	//Games Started (Batting)
@synthesize AB;		//At Bats
@synthesize R;		//Runs
@synthesize OneB;	//Single
@synthesize TwoB;	//Double
@synthesize ThreeB;	//Triple
@synthesize HR;		//Home Run
@synthesize RBI;	//Runs Batted In
@synthesize BB;		//Base on Balls
@synthesize SO;		//Strike Outs
@synthesize BSB;	//Stolen Bases (Batting)
@synthesize BCS;	//Caught Stealing (Batting)
@synthesize SF;		//Sac Flies
@synthesize HBP;	//Hit by Pitch

//Stats that are calculated after inputs
@synthesize H;		//Hits
@synthesize XBH;	//Extra Base Hits
@synthesize BA;		//Batting Average
@synthesize OBP;	//On-Base Percentage
@synthesize SLG;	//Slugging Percentage
@synthesize OPS;	//On-Base Plus Slugging
@synthesize BSBP;	//Stolen Base Percentage (Batting)



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

//StatCalcTimer so that stats are continously updated
- (void) StatCalc
{
	StatCalcTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(GameStats) userInfo:nil repeats:YES] retain];

	//Accept Inputs Stats
	float BG=([BG.text floatValue]);			//Games (Batting)
	float BGS=([BGS.text floatValue]);			//Games Started (Batting)
	float AB=([AB.text floatValue]);			//At Bats
	float R=([R.text floatValue]);				//Runs
	float OneB=([OneB.text floatValue]);		//Single
	float TwoB=([TwoB.text floatValue]);		//Double
	float ThreeB=([ThreeB.text floatValue]);	//Triple
	float HR=([HR.text floatValue]);			//Home Runs
	float RBI=([RBI.text floatValue]);			//Runs Batted In
	float BB=([BB.text floatValue]);			//Base on Balls
	float SO=([SO.text floatValue]);			//Strike Outs 
	float BSB=([BSB.text floatValue]);			//Stolen Bases (Batting)
	float BCS=([BCS.text floatValue]);			//Caught Stealing (Batting)
	float SF=([SF.text floatValue]);			//Sac Flies
	float HBP=([HBP.text floatValue]);			//Hit by Pitch
	
	//Calculate Output Stats
	float H=(OneB+TwoB+ThreeB+HR);						//Hits
	float XBH=(TwoB+ThreeB+HR);							//Extra Base Hits
	float BA=(H/AB);										//Batting Average
	float OBP=((H+BB+HBP)/(AB+BB+HBP+SF));				//On-Base Percentage
	float SLG=((OneB)+(2*TwoB)+(3*ThreeB)+(4*HR)/AB);		//Slugging Percentage
	float OPS=(OBP+SLG);									//On-Base Plus Slugging
	float BSBP=(BSB/(BSB+BCS));							//Stolen Base Percentage (Batting)

	//Display Values in Output Fields
	[H setText:[NSString stringWithFormat:@"%1.0f",H]];
	[XBH setText:[NSString stringWithFormat:@"%1.0f",XBH]];
	[BA setText:[NSString stringWithFormat:@"%1.0f",BA]];
	[OBP setText:[NSString stringWithFormat:@"%1.0f",OBP]];
	[SLG setText:[NSString stringWithFormat:@"%1.0f",SLG]];
	[OPS setText:[NSString stringWithFormat:@"%1.0f",OPS]];
	[BSBP setText:[NSString stringWithFormat:@"%1.0f",BSBP]];
}

	

@end
