//
//  Batting_StatisticsViewController.h
//  Batting Statistics
//
//  Created by DayneHoffman on 11/1/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Batting_StatisticsViewController : UIViewController 
{
	//Input Stats
	IBOutlet UITextField *BG;		//Games (Batting)
	IBOutlet UITextField *BGS;		//Games Started (Batting)
	IBOutlet UITextField *AB;		//At Bats
	IBOutlet UITextField *R;		//Runs
	IBOutlet UITextField *OneB;		//Single
	IBOutlet UITextField *TwoB;		//Double
	IBOutlet UITextField *ThreeB;	//Triple
	IBOutlet UITextField *HR;		//Home Run
	IBOutlet UITextField *RBI;		//Runs Batted In
	IBOutlet UITextField *BB;		//Base on Balls
	IBOutlet UITextField *SO;		//Strike Outs
	IBOutlet UITextField *BSB;		//Stolen Bases (Batting)
	IBOutlet UITextField *BCS;		//Caught Stealing (Batting)
	IBOutlet UITextField *SF;		//Sac Flies
	IBOutlet UITextField *HBP;		//Hit by Pitch
	
	//Output Stats
	IBOutlet UITextField *H;		//Hits
	IBOutlet UITextField *XBH;		//Extra Base Hits
	IBOutlet UITextField *BA;		//Batting Average
	IBOutlet UITextField *OBP;		//On-Base Percentage
	IBOutlet UITextField *SLG;		//Slugging Percentage
	IBOutlet UITextField *OPS;		//On-Base Plus Slugging
	IBOutlet UITextField *BSBP;		//Stolen Base Percentage (Batting)
}

- (IBAction) GameStats;

//Input Stats
@property (nonatomic, retain) UITextField *BG;		//Games (Batting)
@property (nonatomic, retain) UITextField *BGS;		//Games Started (Batting)
@property (nonatomic, retain) UITextField *AB;		//At Bats
@property (nonatomic, retain) UITextField *R;		//Runs
@property (nonatomic, retain) UITextField *OneB;	//Single
@property (nonatomic, retain) UITextField *TwoB;	//Double
@property (nonatomic, retain) UITextField *ThreeB;	//Triple
@property (nonatomic, retain) UITextField *HR;		//Home Run
@property (nonatomic, retain) UITextField *RBI;		//Runs Batted In
@property (nonatomic, retain) UITextField *BB;		//Base on Balls
@property (nonatomic, retain) UITextField *SO;		//Strike Outs
@property (nonatomic, retain) UITextField *BSB;		//Stolen Bases (Batting)
@property (nonatomic, retain) UITextField *BCS;		//Caught Stealing (Batting)
@property (nonatomic, retain) UITextField *SF;		//Sac Flies
@property (nonatomic, retain) UITextField *HBP;		//Hit by Pitch

//Output Stats
@property (nonatomic, retain) UITextField *H;		//Hits
@property (nonatomic, retain) UITextField *XBH;		//Extra Base Hits
@property (nonatomic, retain) UITextField *BA;		//Batting Average
@property (nonatomic, retain) UITextField *OBP;		//On-Base Percentage
@property (nonatomic, retain) UITextField *SLG;		//Slugging Percentage
@property (nonatomic, retain) UITextField *OPS;		//On-Base Plus Slugging
@property (nonatomic, retain) UITextField *BSBP;	//Stolen Base Percentage (Batting)

//StatCalc timer
@property (nonatomic, retain) NSTimer*StatCalcTimer;

- (void) StatCalc;

@end

