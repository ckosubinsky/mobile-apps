//
//  Batting_StatisticsViewController.m
//  Batting Statistics
//
//  Created by DayneHoffman on 11/1/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "Batting_StatisticsViewController.h"

@implementation Batting_StatisticsViewController

//Stats that must be input by user
@synthesize BG;		//Games (Batting)
@synthesize BGS;	//Games Started (Batting)
@synthesize AB;		//At Bats
@synthesize R;		//Runs
@synthesize OneB;	//Single
@synthesize TwoB;	//Double
@synthesize ThreeB;	//Triple
@synthesize HR;		//Home Run
@synthesize RBI;	//Runs Batted In
@synthesize BB;		//Base on Balls
@synthesize SO;		//Strike Outs
@synthesize BSB;	//Stolen Bases (Batting)
@synthesize BCS;	//Caught Stealing (Batting)
@synthesize SF;		//Sac Flies
@synthesize HBP;	//Hit by Pitch

//Stats that are calculated after inputs
@synthesize H;		//Hits
@synthesize XBH;	//Extra Base Hits
@synthesize BA;		//Batting Average
@synthesize OBP;	//On-Base Percentage
@synthesize SLG;	//Slugging Percentage
@synthesize OPS;	//On-Base Plus Slugging
@synthesize BSBP;	//Stolen Base Percentage (Batting)



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

//StatCalcTimer so that stats are continously updated
//- (void) StatCalcTimer
//{
//	StatCalcTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(GameStats) userInfo:nil repeats:YES] retain];
//}

- (IBAction) GameStats
{
	//Accept Inputs Stats
	float Games=([BG.text floatValue]);			//Games (Batting)
	float GamesStarted=([BGS.text floatValue]);			//Games Started (Batting)
	float AtBats=([AB.text floatValue]);			//At Bats
	float Runs=([R.text floatValue]);				//Runs
	float Single=([OneB.text floatValue]);		//Single
	float Double=([TwoB.text floatValue]);		//Double
	float Triple=([ThreeB.text floatValue]);	//Triple
	float HomeRun=([HR.text floatValue]);			//Home Runs
	float RunsBattedIn=([RBI.text floatValue]);			//Runs Batted In
	float BaseOnBalls=([BB.text floatValue]);			//Base on Balls
	float StrikeOuts=([SO.text floatValue]);			//Strike Outs 
	float StolenBases=([BSB.text floatValue]);			//Stolen Bases (Batting)
	float CaughtStealing=([BCS.text floatValue]);			//Caught Stealing (Batting)
	float SacFlies=([SF.text floatValue]);			//Sac Flies
	float HitByPitch=([HBP.text floatValue]);			//Hit by Pitch
	
	//Calculate Output Stats
	float Hits=(Single+Double+Triple+HomeRun);						//Hits
	float ExtraBaseHits=(Double+Triple+HomeRun);							//Extra Base Hits
	float BattingAverage=((Single+Double+Triple+HomeRun)/(AtBats));										//Batting Average
	float OnBasePercentage=((Hits+BaseOnBalls+HitByPitch)/(AtBats+BaseOnBalls+HitByPitch+SacFlies));				//On-Base Percentage
	float Slugging=((Single)+(2*Double)+(3*Triple)+(4*HomeRun)/AtBats);		//Slugging Percentage
	float OnBasePlusSlugging=(OnBasePercentage+Slugging);									//On-Base Plus Slugging
	float StolenBasePercentage=(StolenBases/(StolenBases+CaughtStealing));							//Stolen Base Percentage (Batting)

	//Display Values in Output Fields
	[H setText:[NSString stringWithFormat:@"%1.0f",Hits]];
	[XBH setText:[NSString stringWithFormat:@"%1.0f",ExtraBaseHits]];
	[BA setText:[NSString stringWithFormat:@"%1.3f",BattingAverage]];
	[OBP setText:[NSString stringWithFormat:@"%1.3f",OnBasePercentage]];
	[SLG setText:[NSString stringWithFormat:@"%1.3f",Slugging]];
	[OPS setText:[NSString stringWithFormat:@"%1.3f",OnBasePlusSlugging]];
	[BSBP setText:[NSString stringWithFormat:@"%1.3f",StolenBases]];
}

- (IBAction) DismissKeyboard
{
	[BG resignFirstResponder];
	[BGS resignFirstResponder];
	[AB resignFirstResponder];
	[R resignFirstResponder];
	[OneB resignFirstResponder];
	[TwoB resignFirstResponder];
	[ThreeB resignFirstResponder];
	[HR resignFirstResponder];
	[RBI resignFirstResponder];
	[BB resignFirstResponder];
	[SO resignFirstResponder];
	[BSB resignFirstResponder];
	[BCS resignFirstResponder];
	[SF resignFirstResponder];
	[HBP resignFirstResponder];
}


	

@end
