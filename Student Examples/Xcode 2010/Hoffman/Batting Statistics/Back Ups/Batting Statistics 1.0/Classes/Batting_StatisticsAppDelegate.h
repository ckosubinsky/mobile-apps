//
//  Batting_StatisticsAppDelegate.h
//  Batting Statistics
//
//  Created by DayneHoffman on 11/1/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Batting_StatisticsViewController;

@interface Batting_StatisticsAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    Batting_StatisticsViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Batting_StatisticsViewController *viewController;

@end

