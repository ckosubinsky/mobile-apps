//
//  FlipsideViewController.m
//  Franchise Game Day Stat Tracker
//
//  Copyright Franchise Athletic LLC 2010. All rights reserved.
//

#import "FlipsideViewController.h"

@implementation FlipsideViewController

@synthesize delegate;


/* -- Functional -- */
//Synthesize Scroller
@synthesize InfoScroll;
/* -- Functional -- */


/* -- Functional -- */
//InfoScroll Varibles
//Frame
float XInfoScrollFrame = 0;
float YInfoScrollFrame = 44;
float InfoScrollFrameWidth = 320;
float InfoScrollFrameHeight = 416;

//Content Size
float ISWidth = 320;
float ISHeight = 795;
/* -- Functional -- */


- (void)viewDidLoad 
{
    //Set the viewable frame of the scroll view
    InfoScroll.frame = CGRectMake(XInfoScrollFrame, YInfoScrollFrame, InfoScrollFrameWidth, InfoScrollFrameHeight);

	//Set the content size of the scroll view
    [InfoScroll setContentSize:CGSizeMake(ISWidth, ISHeight)]; 
	
	[super viewDidLoad];
    self.view.backgroundColor = [UIColor viewFlipsideBackgroundColor];      
}


- (IBAction)done:(id)sender {
	[self.delegate flipsideViewControllerDidFinish:self];	
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
    [super dealloc];
}


@end
