//
//  MainViewController.h
//  Franchise Game Day Stat Tracker
//
//  Copyright Franchise Athletic LLC 2010. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> 
{
	/* -- Passing -- */
	//Stat Input Text Fields
	IBOutlet UITextField *CompletionText;									//Completions
	IBOutlet UITextField *AttemptText;										//Attempts
	IBOutlet UITextField *YardText;											//Yards
	IBOutlet UITextField *YardTextEnter;									//Yard Text Enter
	IBOutlet UITextField *TouchDownText;									//Touch Downs
	IBOutlet UITextField *InterceptionText;									//Interceptions
	
	//Stat Output Text Fields
	IBOutlet UITextField *CompletionPercentageText;							//Completion Percentage
	IBOutlet UITextField *CollegiateRatingText;								//College Rating dependent on variable
	IBOutlet UITextField *ProRatingText;									//Pro Rating dependent on variable
	IBOutlet UITextField *YardPerCompletionText;							//Yard Per Completion
	IBOutlet UITextField *YardPerAttemptText;								//Yard Per Attempt
	IBOutlet UITextField *LongText;											//Long Completion
	
	/*
	//Only Variable Rate
	IBOutlet UITextField *RatingText;										//Rating Dependent on Variable for pro or college
	*/
	/* -- Passing -- */
	
	
	/* -- Rushing -- */
	//Stat Input Text Fields
	IBOutlet UITextField *RushingAttemptText;								//Rushing Attempts
	IBOutlet UITextField *RushingYardText;									//Rushing Yards
	IBOutlet UITextField *RushingYardTextEnter;								//Rushing Yards Enter
	IBOutlet UITextField *RushingTouchDownText;								//Rushing Touch Down
	IBOutlet UITextField *RushingFirstDownText;								//Rushing First Down
	IBOutlet UITextField *RushingBrokenTackleText;							//Rushing Broken Tackles
	IBOutlet UITextField *RushingFumbleText;								//Rushing Fumbles
	IBOutlet UITextField *RushingFumbleLostText;							//Rushing Fumbles Lost
	
	//Stat Output Text Fields
	IBOutlet UITextField *RushingAverageText;								//Rushing Average
	IBOutlet UITextField *RushingLongText;									//Long Rush
	/* -- Rushing -- */
	
	
	/* -- Receiving -- */
	//Stat Input Text Fields
	IBOutlet UITextField *ReceivingAttemptText;								//Receiving Attempts
	IBOutlet UITextField *ReceivingThrownToText;							//Thrown To
	IBOutlet UITextField *ReceivingYardText;								//Receiving Yards
	IBOutlet UITextField *ReceivingYardTextEnter;							//Receiving Yards Enter
	IBOutlet UITextField *ReceivingTouchDownText;							//Receiving Touch Down
	IBOutlet UITextField *ReceivingFirstDownText;							//Receiving First Down
	IBOutlet UITextField *ReceivingBrokenTackleText;						//Receiving Broken Tackles
	IBOutlet UITextField *ReceivingFumbleText;								//Receiving Fumbles
	IBOutlet UITextField *ReceivingFumbleLostText;							//Receiving Fumbles Lost
	IBOutlet UITextField *ReceivingDropText;								//Receiving Drops
	
	//Stat Output Text Fields
	IBOutlet UITextField *ReceivingAverageText;								//Receiving Average
	IBOutlet UITextField *ReceivingLongText;								//Long Receiving
	/* -- Receiving -- */
	
	
	/* -- Total -- */
	//Stat Output Text Fields
	IBOutlet UITextField *TotalTouchText;										//Touches
	IBOutlet UITextField *TotalYardText;									//Total Yards
	IBOutlet UITextField *TotalYardPerText;									//Total Yards per touch
	IBOutlet UITextField *TotalTouchDownText;								//Total Touch Downs
	IBOutlet UITextField *TotalFirstDownText;								//Total First Downs
	IBOutlet UITextField *TotalBrokenTackleText;							//Total Broken Tackle
	IBOutlet UITextField *TotalTurnoverText;								//Total Turnovers
	/* -- Total -- */
	
	
	/* -- Functional -- */
	//Scroll View
	IBOutlet UIScrollView *StatScroll;
	
	//Keypad adjust
	BOOL KeyboardVisible;
	CGPoint Offset;
	/* -- Functional -- */
}


/* -- Passing -- */
//Property Input Stats
@property (nonatomic, retain) UITextField *CompletionText;					//Completions
@property (nonatomic, retain) UITextField *AttemptText;						//Attempts
@property (nonatomic, retain) UITextField *YardText;						//Yards
@property (nonatomic, retain) UITextField *YardTextEnter;					//Yard Text Enter
@property (nonatomic, retain) UITextField *TouchDownText;					//Touch Downs
@property (nonatomic, retain) UITextField *InterceptionText;				//Interceptions

//Property Output Stats
@property (nonatomic, retain) UITextField *CompletionPercentageText;		//Completion Percentage
@property (nonatomic, retain) UITextField *CollegiateRatingText;			//College Rating
@property (nonatomic, retain) UITextField *ProRatingText;					//Pro Rating
@property (nonatomic, retain) UITextField *YardPerCompletionText;			//Yards per Completions
@property (nonatomic, retain) UITextField *YardPerAttemptText;				//Yards per Attempts
@property (nonatomic, retain) UITextField *LongText;						//Long Completion

/*
//Requird for button to make either rating as opposed to having both
@property (nonatomic, retain) UITextField *RatingText;						//Rating dependent on variable
*/
/* -- Passing -- */


/* -- Rushing -- */
//Property Input Stats
@property (nonatomic, retain) UITextField *RushingAttemptText;	
@property (nonatomic, retain) UITextField *RushingYardText;	
@property (nonatomic, retain) UITextField *RushingYardTextEnter;	
@property (nonatomic, retain) UITextField *RushingTouchDownText;
@property (nonatomic, retain) UITextField *RushingFirstDownText;
@property (nonatomic, retain) UITextField *RushingBrokenTackleText;
@property (nonatomic, retain) UITextField *RushingFumbleText;
@property (nonatomic, retain) UITextField *RushingFumbleLostText;

//Property Output Stats
@property (nonatomic, retain) UITextField *RushingAverageText;
@property (nonatomic, retain) UITextField *RushingLongText;
/* -- Rushing -- */


/* -- Receiving -- */
//Property Input Stats
@property (nonatomic, retain) UITextField *ReceivingAttemptText;	
@property (nonatomic, retain) UITextField *ReceivingThrownToText;
@property (nonatomic, retain) UITextField *ReceivingYardText;	
@property (nonatomic, retain) UITextField *ReceivingYardTextEnter;	
@property (nonatomic, retain) UITextField *ReceivingTouchDownText;
@property (nonatomic, retain) UITextField *ReceivingFirstDownText;
@property (nonatomic, retain) UITextField *ReceivingBrokenTackleText;
@property (nonatomic, retain) UITextField *ReceivingFumbleText;
@property (nonatomic, retain) UITextField *ReceivingFumbleLostText;
@property (nonatomic, retain) UITextField *ReceivingDropText;

//Property Output Stats
@property (nonatomic, retain) UITextField *ReceivingAverageText;
@property (nonatomic, retain) UITextField *ReceivingLongText;
/* -- Receiving -- */


/* -- Total -- */
@property (nonatomic, retain) UITextField *TotalTouchText;
@property (nonatomic, retain) UITextField *TotalYardText;
@property (nonatomic, retain) UITextField *TotalYardPerText;
@property (nonatomic, retain) UITextField *TotalTouchDownText;
@property (nonatomic, retain) UITextField *TotalFirstDownText;
@property (nonatomic, retain) UITextField *TotalBrokenTackleText;
@property (nonatomic, retain) UITextField *TotalTurnoverText;
/* -- Total -- */


/* -- Functional -- */
//Timer Property
@property (nonatomic, retain) NSTimer *GameStatTimer;

//Stat Scroller
@property (nonatomic, retain) UIScrollView *StatScroll;
/* -- Functional -- */


/* -- Functional -- */
//Button Actions
- (IBAction) DismissKeyboard;
/* -- Functional -- */


/* -- Passing -- */
//Addition Buttons
- (IBAction) AddCompletion;
- (IBAction) AddAttempt;
- (IBAction) AddYard;
- (IBAction) AddYardTextEnter;
- (IBAction) AddTouchDown;
- (IBAction) AddInterception; 

//Subtraction Buttons
- (IBAction) SubtractCompletion;
- (IBAction) SubtractAttempt;
- (IBAction) SubtractYard;
- (IBAction) SubtractYardTextEnter;
- (IBAction) SubtractTouchDown;
- (IBAction) SubtractInterception; 

/*
//Control Buttons
//Only when using variable
- (IBAction) MakeCollegiateRating;
- (IBAction) MakeProRating;
*/
/* -- Passing -- */


/* -- Rushing -- */
//Addition Buttons
- (IBAction) AddRushingAttempt;
- (IBAction) AddRushingYard;
- (IBAction) AddRushingYardTextEnter;
- (IBAction) AddRushingTouchDown;
- (IBAction) AddRushingFirstDown;
- (IBAction) AddRushingBrokenTackle;
- (IBAction) AddRushingFumble;
- (IBAction) AddRushingFumbleLost;

//Subtraction Buttons
- (IBAction) SubtractRushingAttempt;
- (IBAction) SubtractRushingYard;
- (IBAction) SubtractRushingYardTextEnter;
- (IBAction) SubtractRushingTouchDown;
- (IBAction) SubtractRushingFirstDown;
- (IBAction) SubtractRushingBrokenTackle;
- (IBAction) SubtractRushingFumble;
- (IBAction) SubtractRushingFumbleLost;
/* -- Rushing -- */


/* -- Receiving -- */
//Addition Buttons
- (IBAction) AddReceivingAttempt;
- (IBAction) AddReceivingThrownTo;
- (IBAction) AddReceivingYard;
- (IBAction) AddReceivingYardTextEnter;
- (IBAction) AddReceivingTouchDown;
- (IBAction) AddReceivingFirstDown;
- (IBAction) AddReceivingBrokenTackle;
- (IBAction) AddReceivingFumble;
- (IBAction) AddReceivingFumbleLost;
- (IBAction) AddReceivingDrop;

//Subtraction Buttons
- (IBAction) SubtractReceivingAttempt;
- (IBAction) SubtractReceivingThrownTo;
- (IBAction) SubtractReceivingYard;
- (IBAction) SubtractReceivingYardTextEnter;
- (IBAction) SubtractReceivingTouchDown;
- (IBAction) SubtractReceivingFirstDown;
- (IBAction) SubtractReceivingBrokenTackle;
- (IBAction) SubtractReceivingFumble;
- (IBAction) SubtractReceivingFumbleLost;
- (IBAction) SubtractReceivingDrop;
/* -- Receiving -- */


/* -- Functional -- */
- (IBAction) ClearAll;
/* -- Functional -- */


//Game Stats Timer
- (void) GameStats;


- (IBAction)showInfo:(id)sender;

/*
//Keyboard Notification
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidShow:) name: UIKeyboardDidShowNotification object:nil];

[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name: UIKeyboardDidHideNotification object:nil];
*/

@end
