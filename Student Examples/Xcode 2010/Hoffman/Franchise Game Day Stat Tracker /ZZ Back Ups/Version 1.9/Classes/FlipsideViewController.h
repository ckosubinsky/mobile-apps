//
//  FlipsideViewController.h
//  Franchise Game Day Stat Tracker
//
//  Copyright Franchise Athletic LLC 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FlipsideViewControllerDelegate;

@interface FlipsideViewController : UIViewController 
{
	id <FlipsideViewControllerDelegate> delegate;
	
	
	/* -- Functional -- */
	//Scroll View
	IBOutlet UIScrollView *InfoScroll;
	/* -- Functional -- */
}


/* -- Functional -- */
//Info Scroller
@property (nonatomic, retain) UIScrollView *InfoScroll;
/* -- Functional -- */


@property (nonatomic, assign) id <FlipsideViewControllerDelegate> delegate;
- (IBAction)done:(id)sender;
@end


@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end

