//
//  MainViewController.m
//  Franchise Game Day Stat Tracker
//
//  Copyright Franchise Athletic LLC 2010. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController


//Synthesize Text Fields
@synthesize CompletionText;
@synthesize AttemptText;
@synthesize YardText;
@synthesize YardTextEnter;
@synthesize TouchDownText;
@synthesize InterceptionText;

@synthesize CompletionPercentageText;
@synthesize CollegiateRatingText;
@synthesize ProRatingText;
@synthesize YardPerCompletionText;
@synthesize YardPerAttemptText;
@synthesize LongText;

//Only for Varibale rate
@synthesize RatingText;


//Synthesize Timer
@synthesize GameStatTimer;


//Synthesize Scroller
@synthesize StatScroll;


//Universal Variables
float Completion;
float Attempt;
float Yard;
float PlayYard;
float TouchDown;
float Interception;

float CompletionPercentage;
float CollegiateRating;
float ProRating;
float YardPerCompletion;
float YardPerAttempt;
float Long;

//Only Varibale Rate
float Rating;
NSInteger X;



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	//Start Timer for the continual updates of stats
	[self StartTimer];
	
	//Set the viewable frame of the scroll view
    StatScroll.frame = CGRectMake(0, 100, 320, 314);
	
    //Set the content size of the scroll view
    [StatScroll setContentSize:CGSizeMake(320, 550)];  
	
	[super viewDidLoad];
}


//Start Timer for the continual updates of stats
- (void) StartTimer
{
	GameStatTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(GameStats) userInfo:nil repeats:YES] retain];
}


//Stat Calculations
- (void) GameStats
{
	//Keyboard Notification
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidShow:) name: UIKeyboardDidShowNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name: UIKeyboardDidHideNotification object:nil]; 
	
	//Play Yard Entry
	PlayYard = ([YardTextEnter.text floatValue]);
	
	//Yards per Completion
	YardPerCompletion = (Yard/Completion);
	
	//Yards per Attempt
	YardPerAttempt = (Yard/Attempt);
	
	//Completion Percentage
	CompletionPercentage = ((Completion/Attempt)*100);
	
	//QB Rating
	//Collegiate Rating
	{
		CollegiateRating = (((8.4*Yard)+(330*TouchDown)+(100*Completion)-(200*Interception))/Attempt);
	}
	
	//Pro Rating 
	{
		//Pro Rating Initial
		float alpha = (((Completion/Attempt)-.3)*5);
		if (alpha < 0) 
		{
			alpha = 0;
		}
		if (alpha > 2.375) 
		{
			alpha = 2.375;
		}
		
		float beta = (((Yard/Attempt)-3)*.25);
		if (beta < 0) 
		{
			beta = 0;
		}
		if (beta > 2.375) 
		{
			beta = 2.375;
		}
		float kappa = ((TouchDown/Attempt)*20);
		if (kappa < 0) 
		{
			kappa = 0;
		}
		if (kappa > 2.375) 
		{
			kappa = 2.375;
		}
		float delta = (2.375-((Interception/Attempt)*25));
		if (delta < 0) 
		{
			delta = 0;
		}
		if (delta > 2.375) 
		{
			delta = 2.375;
		}
		
		ProRating = (((alpha+beta+kappa+delta)/6)*100);
	}
	
	
	//Display Value in Output Field
	[CompletionPercentageText setText:[NSString stringWithFormat:@"%1.1f",CompletionPercentage]];
	
	[CompletionText setText:[NSString stringWithFormat:@"%1.0f",Completion]];
	
	[AttemptText setText:[NSString stringWithFormat:@"%1.0f",Attempt]];
	
	[YardText setText:[NSString stringWithFormat:@"%1.0f",Yard]];
	
	[TouchDownText setText:[NSString stringWithFormat:@"%1.0f",TouchDown]];
	
	[InterceptionText setText:[NSString stringWithFormat:@"%1.0f",Interception]];
	
	[CollegiateRatingText setText:[NSString stringWithFormat:@"%1.1f",CollegiateRating]];
	
	[ProRatingText setText:[NSString stringWithFormat:@"%1.1f",ProRating]];
	
	[YardPerCompletionText setText:[NSString stringWithFormat:@"%1.1f",YardPerCompletion]];
	
	[YardPerAttemptText setText:[NSString stringWithFormat:@"%1.1f",YardPerAttempt]];
	
	[LongText setText:[NSString stringWithFormat:@"%1.0f",Long]];
}


//Buttons
- (IBAction) AddCompletion
{
	Completion = (Completion + 1);
	Attempt = (Attempt + 1);
}

- (IBAction) SubtractCompletion
{
	Completion = (Completion - 1);
}


- (IBAction) AddAttempt
{
	Attempt = (Attempt + 1);
}

- (IBAction) SubtractAttempt
{
	Attempt = (Attempt - 1);
}


- (IBAction) AddYard
{
	Yard = (Yard + 1);
}

- (IBAction) SubtractYard
{
	Yard = (Yard - 1);
}


- (IBAction) AddYardTextEnter
{
	Yard = (Yard + PlayYard);
	if (PlayYard > Long) 
	{
		Long = PlayYard;
	}
	[YardTextEnter setText:nil];
}

- (IBAction) SubtractYardTextEnter
{
	Yard = (Yard - PlayYard);
	[YardTextEnter setText:nil];
}


- (IBAction) AddTouchDown
{
	TouchDown = (TouchDown + 1);
}

- (IBAction) SubtractTouchDown
{
	TouchDown = (TouchDown - 1);
}


- (IBAction) AddInterception
{
	Interception = (Interception + 1);
}

- (IBAction) SubtractInterception
{
	Interception = (Interception - 1);
}


/*
//Number Generator for Rating
- (IBAction) MakeCollegiateRating
{
	X == 0;
}

- (IBAction) MakeProRating
{
	X == 1;
}
*/


- (IBAction) DismissKeyboard
{
	[YardTextEnter resignFirstResponder];
}
- (IBAction) DismissKeyboard2
{
	[YardTextEnter resignFirstResponder];
}



- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
}


- (IBAction)showInfo:(id)sender {    
	
	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
    [super dealloc];
}




- (void) keyboardDidShow: (NSNotification *)notif
{
	//If Keyboard is visible, return
	if (KeyboardVisible) 
	{
		NSLog(@"Keyboard is already visible. Ignoring notification.");
		return;
	}
	
	//Get the size of the keyboard
	NSDictionary* info = [notif userInfo];
	NSValue* aValue = [info objectForKey:UIKeyboardBoundsUserInfoKey];
	CGSize KeyboardSize = [aValue CGRectValue].size;
	
	//Save the current location to restore when keyboard is dismissed
	Offset = StatScroll.contentOffset;
	
	//Resize the scroll view to make room for the keyboard
	CGRect viewFrame = StatScroll.frame;
	viewFrame.size.height -= KeyboardSize.height-66;
	StatScroll.frame = viewFrame;
	
	//Keyboard is now visible
	KeyboardVisible = YES;
}


- (void) keyboardDidHide: (NSNotification *)notif
{
	//Is the keyboard already shown
	if (!KeyboardVisible) 
	{
		NSLog(@"Keyboard is already hidden. Ignoring notification.");
		return;
	}
	
	//Reset the height of the scroll view to its original value
	StatScroll.frame = CGRectMake(0, 100, 320, 314);
	
	//Reset the scrollview to previous location
	StatScroll.contentOffset = Offset;
	
	//Keyboard is no longer visible
	KeyboardVisible = NO;
}


@end
