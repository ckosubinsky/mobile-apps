//
//  Franchise_Game_Day_Stat_TrackerAppDelegate.h
//  Franchise Game Day Stat Tracker
//
//  Created by DayneHoffman on 11/10/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface Franchise_Game_Day_Stat_TrackerAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    MainViewController *mainViewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet MainViewController *mainViewController;

@end

