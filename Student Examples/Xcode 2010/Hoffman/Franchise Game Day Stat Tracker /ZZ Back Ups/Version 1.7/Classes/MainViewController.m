//
//  MainViewController.m
//  Franchise Game Day Stat Tracker
//
//  Copyright Franchise Athletic LLC 2010. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController


//Synthesize Text Fields
/* -- Passing -- */
//Input
@synthesize CompletionText;
@synthesize AttemptText;
@synthesize YardText;
@synthesize YardTextEnter;
@synthesize TouchDownText;
@synthesize InterceptionText;

//Output
@synthesize CompletionPercentageText;
@synthesize CollegiateRatingText;
@synthesize ProRatingText;
@synthesize YardPerCompletionText;
@synthesize YardPerAttemptText;
@synthesize LongText;

/*
//Only for Varibale rate
@synthesize RatingText;
*/
/* -- Passing -- */


/* -- Rushing -- */
//Input
@synthesize RushingAttemptText;
@synthesize RushingYardText;
@synthesize RushingYardTextEnter;
@synthesize RushingTouchDownText;
@synthesize RushingFirstDownText;
@synthesize RushingBrokenTackleText;
@synthesize RushingFumbleText;
@synthesize RushingFumbleLostText;

//Output
@synthesize RushingAverageText;
/* -- Rushing -- */


/* -- Receiving -- */
//Input
@synthesize ReceivingAttemptText;
@synthesize ReceivingThrownToText;
@synthesize ReceivingYardText;
@synthesize ReceivingYardTextEnter;
@synthesize ReceivingTouchDownText;
@synthesize ReceivingFirstDownText;
@synthesize ReceivingBrokenTackleText;
@synthesize ReceivingFumbleText;
@synthesize ReceivingFumbleLostText;
@synthesize ReceivingDropText;

//Output
@synthesize ReceivingAverageText;
/* -- Receiving -- */


/* -- Total -- */
//Output
@synthesize TotalTouchText;
@synthesize TotalYardText;
@synthesize TotalYardPerText;
@synthesize TotalTouchDownText;
@synthesize TotalFirstDownText;
@synthesize TotalBrokenTackleText;
@synthesize TotalTurnoverText;
/* -- Total -- */


/* -- Functional -- */
//Synthesize Timer
@synthesize GameStatTimer;

//Synthesize Scroller
@synthesize StatScroll;
/* -- Functional -- */


//Universal Variables
/* -- Passing -- */
float Completion;
float Attempt;
float Yard;
float PlayYard;
float TouchDown;
float Interception;

float CompletionPercentage;
float CollegiateRating;
float ProRating;
float YardPerCompletion;
float YardPerAttempt;
float Long;

/*
//Only Varibale Rate
float Rating;
NSInteger X;
*/
/* -- Passing -- */


/* -- Rushing -- */
float RushingAttempt;
float RushingYard;
float RushingPlayYard;
float RushingTouchDown;
float RushingFirstDown;
float RushingBrokenTackle;
float RushingFumble;
float RushingFumbleLost;

float RushingAverage;
float RushingLong;
/* -- Rushing -- */


/* -- Receiving -- */
float ReceivingAttempt;
float ReceivingThrownTo;
float ReceivingYard;
float ReceivingPlayYard;
float ReceivingTouchDown;
float ReceivingFirstDown;
float ReceivingBrokenTackle;
float ReceivingFumble;
float ReceivingFumbleLost;
float ReceivingDrop;

float ReceivingAverage;
float ReceivingLong;
/* -- Receiving -- */


/* -- Total -- */
float TotalTouch;
float TotalYard;
float TotalYardPer;
float TotalTouchDown;
float TotalFirstDown;
float TotalBrokenTackle;
float TotalTurnover;
/* -- Total -- */


/* -- Functional -- */
//StatScroll Varibles
//Frame
float XScrollFrame = 0;
float YScrollFrame = 100;
float ScrollFrameWidth = 320;
float ScrollFrameHeight = 314;

//Content Size
float SSWidth = 320;
float SSHeight = 1841;
/* -- Functional -- */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	//Start Timer for the continual updates of stats
	[self StartTimer];
	
	//Set the viewable frame of the scroll view
    StatScroll.frame = CGRectMake(XScrollFrame, YScrollFrame, ScrollFrameWidth, ScrollFrameHeight);
	
    //Set the content size of the scroll view
    [StatScroll setContentSize:CGSizeMake(SSWidth, SSHeight)];  
	
	//Keyboard Notification
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidShow:) name: UIKeyboardDidShowNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (keyboardDidHide:) name: UIKeyboardDidHideNotification object:nil]; 
	 
	[super viewDidLoad];
}


//Start Timer for the continual updates of stats
- (void) StartTimer
{
	GameStatTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(GameStats) userInfo:nil repeats:YES] retain];	
}


//Stat Calculations and Outputs
- (void) GameStats
{
	/* -- Passing --*/	
	//Play Yard Entry
	PlayYard = ([YardTextEnter.text floatValue]);
	
	//Yards per Completion
	YardPerCompletion = (Yard/Completion);
	
	//Yards per Attempt
	YardPerAttempt = (Yard/Attempt);
	
	//Completion Percentage
	CompletionPercentage = ((Completion/Attempt)*100);
	
	//QB Rating
	//Collegiate Rating
	{
		CollegiateRating = (((8.4*Yard)+(330*TouchDown)+(100*Completion)-(200*Interception))/Attempt);
	}
	
	//Pro Rating 
	{
		//Pro Rating Initial
		float alpha = (((Completion/Attempt)-.3)*5);
		if (alpha < 0) 
		{
			alpha = 0;
		}
		if (alpha > 2.375) 
		{
			alpha = 2.375;
		}
		
		float beta = (((Yard/Attempt)-3)*.25);
		if (beta < 0) 
		{
			beta = 0;
		}
		if (beta > 2.375) 
		{
			beta = 2.375;
		}
		float kappa = ((TouchDown/Attempt)*20);
		if (kappa < 0) 
		{
			kappa = 0;
		}
		if (kappa > 2.375) 
		{
			kappa = 2.375;
		}
		float delta = (2.375-((Interception/Attempt)*25));
		if (delta < 0) 
		{
			delta = 0;
		}
		if (delta > 2.375) 
		{
			delta = 2.375;
		}
		
		ProRating = (((alpha+beta+kappa+delta)/6)*100);
	}
	
	
	//Display Value in Output Field
	[CompletionPercentageText setText:[NSString stringWithFormat:@"%1.1f",CompletionPercentage]];
	
	[CompletionText setText:[NSString stringWithFormat:@"%1.0f",Completion]];
	
	[AttemptText setText:[NSString stringWithFormat:@"%1.0f",Attempt]];
	
	[YardText setText:[NSString stringWithFormat:@"%1.0f",Yard]];
	
	[TouchDownText setText:[NSString stringWithFormat:@"%1.0f",TouchDown]];
	
	[InterceptionText setText:[NSString stringWithFormat:@"%1.0f",Interception]];
	
	[CollegiateRatingText setText:[NSString stringWithFormat:@"%1.1f",CollegiateRating]];
	
	[ProRatingText setText:[NSString stringWithFormat:@"%1.1f",ProRating]];
	
	[YardPerCompletionText setText:[NSString stringWithFormat:@"%1.1f",YardPerCompletion]];
	
	[YardPerAttemptText setText:[NSString stringWithFormat:@"%1.1f",YardPerAttempt]];
	
	[LongText setText:[NSString stringWithFormat:@"%1.0f",Long]];
	/* -- Passing --*/
	
	
	/* -- Rushing --*/
	//Play Yard Entry
	RushingPlayYard = ([RushingYardTextEnter.text floatValue]);
	
	//Rushing Average
	RushingAverage = (RushingYard/RushingAttempt);
	
	//Display Output Fields
	[RushingAttemptText setText: [NSString stringWithFormat:@"%1.0f",RushingAttempt]];
	
	[RushingYardText setText: [NSString stringWithFormat:@"%1.0f",RushingYard]];
	
	[RushingTouchDownText setText: [NSString stringWithFormat:@"%1.0f",RushingTouchDown]];
	
	[RushingFirstDownText setText: [NSString stringWithFormat:@"%1.0f",RushingFirstDown]];
	
	[RushingBrokenTackleText setText: [NSString stringWithFormat:@"%1.0f",RushingBrokenTackle]];
	
	[RushingFumbleText setText: [NSString stringWithFormat:@"%1.0f",RushingFumble]];
	
	[RushingFumbleLostText setText: [NSString stringWithFormat:@"%1.0f",RushingFumbleLost]];
	
	[RushingAverageText setText: [NSString stringWithFormat:@"%1.1f",RushingAverage]];
	
	[RushingLongText setText: [NSString stringWithFormat:@"%1.0f",RushingLong]];
	/* -- Rushing --*/
	
	
	/* -- Receiving --*/
	//Play Yard Entry
	ReceivingPlayYard = ([ReceivingYardTextEnter.text floatValue]);
	
	//Rushing Average
	ReceivingAverage = (ReceivingYard/ReceivingAttempt);
	
	//Display Output Fields
	[ReceivingAttemptText setText: [NSString stringWithFormat:@"%1.0f",ReceivingAttempt]];
	
	[ReceivingThrownToText setText: [NSString stringWithFormat:@"%1.0f",ReceivingThrownTo]];
	
	[ReceivingYardText setText: [NSString stringWithFormat:@"%1.0f",ReceivingYard]];
	
	[ReceivingTouchDownText setText: [NSString stringWithFormat:@"%1.0f",ReceivingTouchDown]];
	
	[ReceivingFirstDownText setText: [NSString stringWithFormat:@"%1.0f",ReceivingFirstDown]];
	
	[ReceivingBrokenTackleText setText: [NSString stringWithFormat:@"%1.0f",ReceivingBrokenTackle]];
	
	[ReceivingFumbleText setText: [NSString stringWithFormat:@"%1.0f",ReceivingFumble]];
	
	[ReceivingFumbleLostText setText: [NSString stringWithFormat:@"%1.0f",ReceivingFumbleLost]];
	
	[ReceivingAverageText setText: [NSString stringWithFormat:@"%1.1f",ReceivingAverage]];
	
	[ReceivingLongText setText: [NSString stringWithFormat:@"%1.0f",ReceivingLong]];
	
	[ReceivingDropText setText: [NSString stringWithFormat:@"%1.0f",ReceivingDrop]];
	/* -- Receiving --*/
	
	
	/* -- Total -- */
	//Total Touches
	TotalTouch = (Attempt+RushingAttempt+ReceivingAttempt+ReceivingDrop);
	
	//Total Yards
	TotalYard = (Yard+RushingYard+ReceivingYard);
	
	//Total Yards Per Touch
	TotalYardPer = (TotalYard)/(TotalTouch);
	
	//Total Touch Downs
	TotalTouchDown = (TouchDown+RushingTouchDown+ReceivingTouchDown);
	
	//Total First Down
	TotalFirstDown = (RushingFirstDown+ReceivingFirstDown);
	
	//Total Broken Tackles
	TotalBrokenTackle = (RushingBrokenTackle+ReceivingBrokenTackle);
	
	//Total Turnovers
	TotalTurnover = (Interception+RushingFumbleLost+ReceivingFumbleLost);
	
	[TotalTouchText setText: [NSString stringWithFormat:@"%1.0f",TotalTouch]];
	
	[TotalYardText setText: [NSString stringWithFormat:@"%1.0f",TotalYard]];
	
	[TotalYardPerText setText: [NSString stringWithFormat:@"%1.1f",TotalYardPer]];
	
	[TotalTouchDownText setText: [NSString stringWithFormat:@"%1.0f",TotalTouchDown]];
	
	[TotalFirstDownText setText: [NSString stringWithFormat:@"%1.0f",TotalFirstDown]];
	
	[TotalBrokenTackleText setText: [NSString stringWithFormat:@"%1.0f",TotalBrokenTackle]];
	
	[TotalTurnoverText setText: [NSString stringWithFormat:@"%1.0f",TotalTurnover]];
	/* -- Total -- */
}

/* -- Passing -- */
//Buttons
- (IBAction) AddCompletion
{
	Completion = (Completion + 1);
	Attempt = (Attempt + 1);
}

- (IBAction) SubtractCompletion
{
	Completion = (Completion - 1);
}


- (IBAction) AddAttempt
{
	Attempt = (Attempt + 1);
}

- (IBAction) SubtractAttempt
{
	Attempt = (Attempt - 1);
}


- (IBAction) AddYard
{
	Yard = (Yard + 1);
}

- (IBAction) SubtractYard
{
	Yard = (Yard - 1);
}


- (IBAction) AddYardTextEnter
{
	Yard = (Yard + PlayYard);
	
	if (PlayYard > Long) 
	{
		Long = PlayYard;
	}
	
	
	/* 
	Entery based on Value
	//With PlayYard = 0 then Completion and Attempt go up by one
	if (PlayYard == 0) 
	{
		Completion = Completion + 1;
		Attempt = Attempt + 1;
	}
	
	//With PlayYard > 0 then Completion and Attempt go up
	if (PlayYard > 0) 
	{
		Completion = Completion + 1;
		Attempt = Attempt + 1;
	}
	*/
	
	Completion = Completion + 1;
	Attempt = Attempt + 1;
	
	[YardTextEnter setText:nil];
}

- (IBAction) SubtractYardTextEnter
{
	Yard = (Yard - PlayYard);
	[YardTextEnter setText:nil];
}


- (IBAction) AddTouchDown
{
	TouchDown = (TouchDown + 1);
}

- (IBAction) SubtractTouchDown
{
	TouchDown = (TouchDown - 1);
}


- (IBAction) AddInterception
{
	Interception = (Interception + 1);
}

- (IBAction) SubtractInterception
{
	Interception = (Interception - 1);
}


/*
//Number Generator for Rating
- (IBAction) MakeCollegiateRating
{
	X == 0;
}

- (IBAction) MakeProRating
{
	X == 1;
}
*/
/* -- Passing -- */


/* -- Rushing -- */
- (IBAction) AddRushingAttempt
{
	RushingAttempt = (RushingAttempt + 1);
}

- (IBAction) SubtractRushingAttempt
{
	RushingAttempt = (RushingAttempt - 1);
}


- (IBAction) AddRushingYard
{
	RushingYard = (RushingYard + 1);
}

- (IBAction) SubtractRushingYard
{
	RushingYard = (RushingYard - 1);
}


- (IBAction) AddRushingTouchDown
{
	RushingTouchDown = (RushingTouchDown + 1);
}

- (IBAction) SubtractRushingTouchDown
{
	RushingTouchDown = (RushingTouchDown - 1);
}


- (IBAction) AddRushingFirstDown
{
	RushingFirstDown = (RushingFirstDown + 1);
}

- (IBAction) SubtractRushingFirstDown
{
	RushingFirstDown = (RushingFirstDown - 1);
}


- (IBAction) AddRushingBrokenTackle
{
	RushingBrokenTackle = (RushingBrokenTackle + 1);
}

- (IBAction) SubtractRushingBrokenTackle
{
	RushingBrokenTackle = (RushingBrokenTackle - 1);
}


- (IBAction) AddRushingFumble
{
	RushingFumble = (RushingFumble + 1);
}

- (IBAction) SubtractRushingFumble
{
	RushingFumble = (RushingFumble - 1);
}


- (IBAction) AddRushingFumbleLost
{
	RushingFumbleLost = (RushingFumbleLost + 1);
	
	RushingFumble = (RushingFumble + 1);
}

- (IBAction) SubtractRushingFumbleLost
{
	RushingFumbleLost = (RushingFumbleLost - 1);
}


- (IBAction) AddRushingYardTextEnter
{
	RushingYard = (RushingYard + RushingPlayYard);
	
	if (RushingPlayYard > RushingLong) 
	{
		RushingLong = RushingPlayYard;
	}
	
	//Add Rushing Attempt When Value is Entered
	RushingAttempt = RushingAttempt + 1;
	
	[RushingYardTextEnter setText:nil];
}

- (IBAction) SubtractRushingYardTextEnter
{
	RushingYard = (RushingYard - RushingPlayYard);
	
	//Add Rushing Attempt When Value is Entered
	RushingAttempt = RushingAttempt + 1;
	
	[RushingYardTextEnter setText:nil];
}
/* -- Rushing -- */


/* -- Receiving -- */
- (IBAction) AddReceivingAttempt
{
	ReceivingAttempt = (ReceivingAttempt + 1);
	
	ReceivingThrownTo = (ReceivingThrownTo + 1);
}

- (IBAction) SubtractReceivingAttempt
{
	ReceivingAttempt = (ReceivingAttempt - 1);
}


- (IBAction) AddReceivingThrownTo
{
	ReceivingThrownTo = (ReceivingThrownTo + 1);
}

- (IBAction) SubtractReceivingThrownTo
{
	ReceivingThrownTo = (ReceivingThrownTo - 1);
}


- (IBAction) AddReceivingYard
{
	ReceivingYard = (ReceivingYard + 1);
}

- (IBAction) SubtractReceivingYard
{
	ReceivingYard = (ReceivingYard - 1);
}


- (IBAction) AddReceivingTouchDown
{
	ReceivingTouchDown = (ReceivingTouchDown + 1);
}

- (IBAction) SubtractReceivingTouchDown
{
	ReceivingTouchDown = (ReceivingTouchDown - 1);
}


- (IBAction) AddReceivingFirstDown
{
	ReceivingFirstDown = (ReceivingFirstDown + 1);
}

- (IBAction) SubtractReceivingFirstDown
{
	ReceivingFirstDown = (ReceivingFirstDown - 1);
}


- (IBAction) AddReceivingBrokenTackle
{
	ReceivingBrokenTackle = (ReceivingBrokenTackle + 1);
}

- (IBAction) SubtractReceivingBrokenTackle
{
	ReceivingBrokenTackle = (ReceivingBrokenTackle - 1);
}


- (IBAction) AddReceivingFumble
{
	ReceivingFumble = (ReceivingFumble + 1);
}

- (IBAction) SubtractReceivingFumble
{
	ReceivingFumble = (ReceivingFumble - 1);
}


- (IBAction) AddReceivingFumbleLost
{
	ReceivingFumbleLost = (ReceivingFumbleLost + 1);
	
	ReceivingFumble = (ReceivingFumble + 1);
}

- (IBAction) SubtractReceivingFumbleLost
{
	ReceivingFumbleLost = (ReceivingFumbleLost - 1);
}


- (IBAction) AddReceivingDrop
{
	ReceivingDrop = (ReceivingDrop + 1);
	
	ReceivingThrownTo = (ReceivingThrownTo + 1);
}

- (IBAction) SubtractReceivingDrop
{
	ReceivingDrop = (ReceivingDrop - 1);
}


- (IBAction) AddReceivingYardTextEnter
{
	ReceivingYard = (ReceivingYard + ReceivingPlayYard);
	
	if (ReceivingPlayYard > ReceivingLong) 
	{
		ReceivingLong = ReceivingPlayYard;
	}
	
	//Add Rushing Attempt When Value is Entered
	ReceivingAttempt = ReceivingAttempt + 1;
	
	ReceivingThrownTo = ReceivingThrownTo + 1;
	
	[ReceivingYardTextEnter setText:nil];
}

- (IBAction) SubtractReceivingYardTextEnter
{
	ReceivingYard = (ReceivingYard - ReceivingPlayYard);
	
	//Add Rushing Attempt When Value is Entered
	ReceivingAttempt = ReceivingAttempt + 1;
	
	ReceivingThrownTo = ReceivingThrownTo + 1;
	
	[ReceivingYardTextEnter setText:nil];
}
/* -- Receiving -- */


/* -- Functional -- */
- (IBAction) DismissKeyboard
{
	[YardTextEnter resignFirstResponder];
	[RushingYardTextEnter resignFirstResponder];
	[ReceivingYardTextEnter resignFirstResponder];
}
/* -- Functional -- */


- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
}


- (IBAction)showInfo:(id)sender {    
	
	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
    [super dealloc];
}




- (void) keyboardDidShow: (NSNotification *)notif
{
	//If Keyboard is visible, return
	if (KeyboardVisible) 
	{
		NSLog(@"Keyboard is already visible. Ignoring notification.");
		return;
	}
	
	//Get the size of the keyboard
	NSDictionary* info = [notif userInfo];
	NSValue* aValue = [info objectForKey:UIKeyboardBoundsUserInfoKey];
	CGSize KeyboardSize = [aValue CGRectValue].size;
	
	//Save the current location to restore when keyboard is dismissed
	Offset = StatScroll.contentOffset;
	
	//Resize the scroll view to make room for the keyboard
	CGRect viewFrame = StatScroll.frame;
	viewFrame.size.height -= KeyboardSize.height-66;
	StatScroll.frame = viewFrame;
	
	//Keyboard is now visible
	KeyboardVisible = YES;
}


- (void) keyboardDidHide: (NSNotification *)notif
{
	//Is the keyboard already shown
	if (!KeyboardVisible) 
	{
		NSLog(@"Keyboard is already hidden. Ignoring notification.");
		return;
	}
	
	//Reset the height of the scroll view to its original value
	StatScroll.frame = CGRectMake(0, 100, 320, 314);
	
	//Reset the scrollview to previous location
	StatScroll.contentOffset = Offset;
	
	//Keyboard is no longer visible
	KeyboardVisible = NO;
}


@end
