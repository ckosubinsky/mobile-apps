//
//  MainViewController.h
//  Puck Catch
//
//  Created by DayneHoffman on 10/6/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> 
{
	// Create puck and cup images
	IBOutlet UIImageView *puck;
	IBOutlet UIImageView *cup;
}

//Properties for method
@property (nonatomic, retain)UIImageView *puck;
@property (nonatomic, retain)UIImageView *cup;

//Method to move puck
- (IBAction)showInfo:(id)sender;

- (void) checkCollision;

- (void) move;

@end
