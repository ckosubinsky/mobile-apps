//
//  MainViewController.m
//  Puck Catch
//
//  Created by DayneHoffman on 10/6/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController

@synthesize puck;
@synthesize cup;

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *myTouch = [[event allTouches] anyObject];
	
	puck.center = [myTouch locationInView:self.view];
	
	[self checkCollision];	
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	srand([[NSDate date] timeIntervalSince1970]);
	
	[NSTimer scheduledTimerWithTimeInterval:1.6 target:self selector:@selector(move) userInfo:nil repeats:YES];

	NSInteger ran=rand()%6;
	
	[self move];

	NSInteger left;
	NSInteger top;
	NSInteger gotoX;
	NSInteger gotoY;
	
	if(ran==0)
	{
		left=25;
		top=-50;
		gotoX=25;
		gotoY=450;
	}
	if(ran==1)
	{
		left=50;
		top=-50;
		gotoX=50;
		gotoY=450;
	}
	if(ran==2)
	{
		left=75;
		top=-50;
		gotoX=75;
		gotoY=450;
	}
	if(ran==3)
	{
		left=100;
		top=-50;
		gotoX=100;
		gotoY=450;
	}
	if(ran==4)
	{
		left=125;
		top=-50;
		gotoX=125;
		gotoY=450;
	}
	if(ran==5)
	{
		left=150;
		top=-50;
		gotoX=150;
		gotoY=450;
	}
	
	puck.frame=CGRectMake(left,top,50,50);
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:.5];
	puck.frame=CGRectMake(gotoX,gotoY,50,50);
	[UIView commitAnimations];
}

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
}
*/


- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
}


- (IBAction)showInfo:(id)sender {    
	
	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
    [super dealloc];
}

- (void) move
{
	
}


@end
