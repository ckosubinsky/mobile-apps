//
//  MainViewController.m
//  Drop the Puck
//
//  Created by DayneHoffman on 10/11/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController

@synthesize puck;
@synthesize cup;
@synthesize checkCollisionTimer;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
	
	srand([[NSDate date] timeIntervalSince1970]);
	
	[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(move) userInfo:nil repeats:YES];
	
	[self move];
	
	[self startTimer];
}


//Start timer for collision
- (void) startTimer
{
	checkCollisionTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(checkCollision) userInfo:nil repeats:YES] retain];
}


//Collision 
- (void) checkCollision 
{
	
	if(CGRectIntersectsRect(puck.frame, cup.frame)) 
		
	{
		
		FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];controller.delegate = self;
		
		controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;[self presentModalViewController:controller animated:YES];
		
		[controller release];
		
		cup.frame=CGRectMake(75,250,150,172);
				
	}
	
}


//Movement on touch of cup image
- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{
	
	UITouch *myTouch = [[event allTouches] anyObject];
	
	cup.center = [myTouch locationInView:self.view];
		
	[self checkCollision];
	
}


- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller 
{
	[self dismissModalViewControllerAnimated:YES];
}


- (IBAction)showInfo:(id)sender {    
	
	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc 
{
    [super dealloc];
}


//Animation of falling pucks, random number generated for starting location of puck, speed variable for the speed of the falling pucks
- (void) move
{
	NSInteger ran=rand()%10;
	NSInteger left;
	NSInteger top;
	NSInteger speed = 3;
	if(ran==0)
	{
		left=25;
		top=-50;
	}
	if(ran==1)
	{
		left=50;
		top=-50;
	}
	if(ran==2)
	{
		left=75;
		top=-50;
	}
	if(ran==3)
	{
		left=100;
		top=-50;
	}
	if(ran==4)
	{
		left=125;
		top=-50;
	}
	if(ran==5)
	{
		left=150;
		top=-50;
	}
	if(ran==6)
	{
		left=175;
		top=-50;
	}
	if(ran==7)
	{
		left=200;
		top=-50;
	}
	if(ran==8)
	{
		left=225;
		top=-50;
	}
	if(ran==9)
	{
		left=250;
		top=-50;
	}

	puck.frame=CGRectMake(puck.frame.origin.x,puck.frame.origin.y,50,50);
	[UIView beginAnimations:nil context:nil];
	
	[UIView setAnimationDelegate:self];
	puck.frame=CGRectMake(puck.frame.origin.x,puck.frame.origin.y + speed,50,50);
	[UIView commitAnimations];
	
	if(puck.frame.origin.y > 530)
	{
		puck.frame=CGRectMake(left,top,50,50);
	}
	
	[UIView commitAnimations];
			
}


@end
