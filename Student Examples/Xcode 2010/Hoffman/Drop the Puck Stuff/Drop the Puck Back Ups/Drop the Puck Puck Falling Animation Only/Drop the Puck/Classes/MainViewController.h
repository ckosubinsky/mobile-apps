//
//  MainViewController.h
//  Drop the Puck
//
//  Created by DayneHoffman on 10/11/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> 
{
	IBOutlet UIImageView *imageView;
}

- (void) move;

- (IBAction)showInfo:(id)sender;

@end
