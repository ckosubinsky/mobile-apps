//
//  MainViewController.m
//  Drop the Puck
//
//  Created by DayneHoffman on 10/11/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "MainViewController.h"


@implementation MainViewController



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
	
	srand([[NSDate date] timeIntervalSince1970]);
	
	[NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(move) userInfo:nil repeats:YES];
	
	[self move];	
}



- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
    
	[self dismissModalViewControllerAnimated:YES];
}


- (IBAction)showInfo:(id)sender {    
	
	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
	
	[controller release];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
    [super dealloc];
}

- (void) move
{
	NSInteger ran=rand()%10;
	NSInteger left;
	NSInteger top;
	NSInteger gotoX;
	NSInteger gotoY;
	if(ran==0)
	{
		left=25;
		top=-50;
		gotoX=25;
		gotoY=600;
	}
	if(ran==1)
	{
		left=50;
		top=-50;
		gotoX=50;
		gotoY=600;
	}
	if(ran==2)
	{
		left=75;
		top=-50;
		gotoX=75;
		gotoY=600;
	}
	if(ran==3)
	{
		left=100;
		top=-50;
		gotoX=100;
		gotoY=600;
	}
	if(ran==4)
	{
		left=125;
		top=-50;
		gotoX=125;
		gotoY=600;
	}
	if(ran==5)
	{
		left=150;
		top=-50;
		gotoX=150;
		gotoY=600;
	}
	if(ran==6)
	{
		left=175;
		top=-50;
		gotoX=175;
		gotoY=600;
	}
	if(ran==7)
	{
		left=200;
		top=-50;
		gotoX=200;
		gotoY=600;
	}
	if(ran==8)
	{
		left=225;
		top=-50;
		gotoX=225;
		gotoY=600;
	}
	if(ran==9)
	{
		left=250;
		top=-50;
		gotoX=250;
		gotoY=600;
	}

	imageView.frame=CGRectMake(left,top,50,50);
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:.75];
	
	imageView.frame=CGRectMake(gotoX,gotoY,50,50);
	
	[UIView commitAnimations];
	
}


@end
