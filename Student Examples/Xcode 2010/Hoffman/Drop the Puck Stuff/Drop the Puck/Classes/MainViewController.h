//
//  MainViewController.h
//  Drop the Puck
//
//  Created by DayneHoffman on 10/11/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "FlipsideViewController.h"

@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> 
{
	IBOutlet UIImageView *puck;
	IBOutlet UIImageView *cup;
}

@property (nonatomic, retain) UIImageView *puck;
@property (nonatomic, retain) UIImageView *cup;
@property (nonatomic, retain) NSTimer*checkCollisionTimer;

- (IBAction)showInfo:(id)sender;

//Sounds
- (IBAction) Skate1:(id)sender;

- (void) checkCollision;
- (void) move;

@end
