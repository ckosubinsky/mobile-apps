//
//  Dayne_s_Image_ViewerViewController.m
//  Dayne's Image Viewer
//
//  Created by DayneHoffman on 9/22/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "Dayne_s_Image_ViewerViewController.h"

@implementation Dayne_s_Image_ViewerViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (IBAction) showimg1
{
	imageView.image=[UIImage imageNamed:@"image1.jpg"];
}

- (IBAction) showimg2
{
	imageView.image=[UIImage imageNamed:@"image2.jpg"];
}

- (IBAction) showimg3
{
	imageView.image=[UIImage imageNamed:@"image3.jpg"];
}

- (IBAction) showimg4
{
	imageView.image=[UIImage imageNamed:@"image4.jpg"];
}

- (IBAction) showimg5
{
	imageView.image=[UIImage imageNamed:@"image5.jpg"];
}

- (IBAction) showimg6
{
	imageView.image=[UIImage imageNamed:@"image6.jpg"];
}

- (IBAction) showimg7
{
	imageView.image=[UIImage imageNamed:@"image7.jpg"];
}

- (IBAction) showimg8
{
	imageView.image=[UIImage imageNamed:@"image8.jpg"];
}

- (IBAction) showimg9
{
	imageView.image=[UIImage imageNamed:@"image9.jpg"];
}

-(IBAction) playSound:(id)sender
{
	
	NSString *path = [[NSBundle mainBundle] pathForResource:@"sound2" ofType:@"mp3"];    
	AVAudioPlayer* theAudio=[[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:NULL];        
	
	theAudio.delegate=self;   
	[theAudio play];
}


@end
