//
//  Dayne_s_Image_ViewerViewController.h
//  Dayne's Image Viewer
//
//  Created by DayneHoffman on 9/22/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

#import<AVFoundation/AVAudioPlayer.h>

@interface Dayne_s_Image_ViewerViewController : UIViewController 
{
	IBOutlet UIImageView *imageView;
}	

- (IBAction) showimg1;
- (IBAction) showimg2;
- (IBAction) showimg3;
- (IBAction) showimg4;
- (IBAction) showimg5;
- (IBAction) showimg6;
- (IBAction) showimg7;
- (IBAction) showimg8;
- (IBAction) showimg9;

- (IBAction) playSound:(id)sender;

@end

