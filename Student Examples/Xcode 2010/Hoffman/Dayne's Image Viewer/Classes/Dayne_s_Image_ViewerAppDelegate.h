//
//  Dayne_s_Image_ViewerAppDelegate.h
//  Dayne's Image Viewer
//
//  Created by DayneHoffman on 9/22/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Dayne_s_Image_ViewerViewController;

@interface Dayne_s_Image_ViewerAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    Dayne_s_Image_ViewerViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Dayne_s_Image_ViewerViewController *viewController;

@end

