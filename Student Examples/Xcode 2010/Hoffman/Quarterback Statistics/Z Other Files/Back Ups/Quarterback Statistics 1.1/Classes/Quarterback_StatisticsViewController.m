//
//  Quarterback_StatisticsViewController.m
//  Quarterback Statistics
//
//  Created by DayneHoffman on 11/3/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "Quarterback_StatisticsViewController.h"

@implementation Quarterback_StatisticsViewController


//Stats that must be input by the user
@synthesize COMP;							//Completion
@synthesize ATT;							//Attempts


//Stats that are caculated from Inputs
@synthesize PER;							//Completion Percentage


//Timer
@synthesize GameStatTimer;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



//Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
	
	//Start Timer for the continual updates of stats
	[self StartTimer];
}


//Start Timer for the continual updates of stats
- (void) StartTimer
{
	GameStatTimer = [[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(GameStats) userInfo:nil repeats:YES] retain];
}


//Stat calculations
- (void) GameStats
{
	//Accept Inputs Stats
	float Completions = ([COMP.text floatValue]);						//Completions input
	float Attempts = ([ATT.text floatValue]);							//Attempts input
	
	
	//Calculate Output
	float CompletionPercentage = ((Completions/Attempts)*100);			//Completion Percentage
	
	
	//Display Values in Output Fields
	[PER setText:[NSString stringWithFormat:@"%1.1f",CompletionPercentage]];
}


//Button Actions

//Dismiss Keyboard
- (IBAction) DismissKeyboard
{
	[COMP resignFirstResponder];
	[ATT resignFirstResponder];
}

//Addition Buttons
- (IBAction) AddCOMP
{
	[COMP + 1];
}

- (IBAction) AddATT
{
	[ATT + 1];
}

 
 
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}






@end
