//
//  Quarterback_StatisticsAppDelegate.h
//  Quarterback Statistics
//
//  Created by DayneHoffman on 11/3/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Quarterback_StatisticsViewController;

@interface Quarterback_StatisticsAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    Quarterback_StatisticsViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Quarterback_StatisticsViewController *viewController;

@end

