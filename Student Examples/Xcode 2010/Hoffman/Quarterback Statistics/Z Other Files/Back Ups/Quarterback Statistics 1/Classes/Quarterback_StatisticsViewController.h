//
//  Quarterback_StatisticsViewController.h
//  Quarterback Statistics
//
//  Created by DayneHoffman on 11/3/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Quarterback_StatisticsViewController : UIViewController 
{
	//Stat Input Text Fields
	IBOutlet UITextField *COMP;				//Completetions
	IBOutlet UITextField *ATT;				//Attempts
	IBOutlet UITextField *PER;				//Completion Percentage
}

@end

