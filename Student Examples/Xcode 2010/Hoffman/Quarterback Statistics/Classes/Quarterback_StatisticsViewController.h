//
//  Quarterback_StatisticsViewController.h
//  Quarterback Statistics
//
//  Created by DayneHoffman on 11/3/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Quarterback_StatisticsViewController : UIViewController 
{
	//Stat Input Text Fields
	IBOutlet UITextField *COMP;						//Completions
	IBOutlet UITextField *ATT;						//Attempts
	
	//Stat Output Text Fields
	IBOutlet UITextField *PER;						//Completion Percentage
}


//Input Stats
@property (nonatomic, retain) UITextField *COMP;		//Completions
@property (nonatomic, retain) UITextField *ATT;			//Attempts


//Output Stats
@property (nonatomic, retain) UITextField *PER;			//Completion Percentage


//Timer Property
@property (nonatomic, retain) NSTimer *GameStatTimer; 


//Button Actions
- (IBAction) DismissKeyboard;

//Addition Buttons
- (IBAction) AddCOMP;
- (IBAction) AddATT;

//Subtraction Buttons
- (IBAction) SubCOMP;
- (IBAction) SubATT;


- (void) GameStats;


@end

