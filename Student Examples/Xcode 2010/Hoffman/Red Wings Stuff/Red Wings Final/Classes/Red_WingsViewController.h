//
//  Red_WingsViewController.h
//  Red Wings
//
//  Created by DayneHoffman on 9/27/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Red_WingsViewController : UIViewController 
{
	IBOutlet UIImageView *imageView;
	IBOutlet UISlider *myslider;
}

@property (nonatomic, retain) IBOutlet UISlider *myslider;

- (IBAction) sliderValueChanged:(id)sender;

- (void) move;

@end

