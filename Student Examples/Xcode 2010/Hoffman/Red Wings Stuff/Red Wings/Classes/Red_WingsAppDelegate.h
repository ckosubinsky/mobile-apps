//
//  Red_WingsAppDelegate.h
//  Red Wings
//
//  Created by DayneHoffman on 9/27/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Red_WingsViewController;

@interface Red_WingsAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    Red_WingsViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet Red_WingsViewController *viewController;

@end

