//
//  Red_WingsViewController.h
//  Red Wings
//
//  Created by Dayne Hoffman on 9/27/10.
//  Copyright Frachise Athletic LLC 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Red_WingsViewController : UIViewController {
	IBOutlet UIImageView *imageView;
}

- (void) move;

@end

