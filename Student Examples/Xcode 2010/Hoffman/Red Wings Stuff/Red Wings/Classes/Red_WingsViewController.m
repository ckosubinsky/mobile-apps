//
//  Red_WingsViewController.m
//  Red Wings
//
//  Created by DayneHoffman on 9/27/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "Red_WingsViewController.h"

@implementation Red_WingsViewController



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

- (void)viewDidLoad {
    [super viewDidLoad];
 
 self.view.backgroundColor=[UIColor whiteColor];
 
 srand([[NSDate date] timeIntervalSince1970]);
 
 [NSTimer scheduledTimerWithTimeInterval:.6 target:self selector:@selector(move) userInfo:nil repeats:YES];

 
 NSInteger ran=rand()%8; 
	
 NSInteger slide=UISlider;
	
 [self move];
	
	NSInteger left;
	NSInteger top;
	NSInteger gotoX;
	NSInteger gotoY;
	
	if(ran==0)
	{
		left=-160;
		top=-160;
	}
	if(ran==1)
	{
		left=320;
		top=-160;
	}
	if(ran==2)
	{
		left=320;
		top=480;
	}
	if(ran==3)
	{
		left=-160;
		top=480;
	}
	if(ran==4)
	{
		left=80;
		top=-160;
	}
	if(ran==5)
	{
		left=80;
		top=480;
	}
	if(ran==6)
	{
		left=-160;
		top=160;
	}
	if(ran==7)
	{
		left=480;
		top=160;
	}

	imageView.frame=CGRectMake(left,top,160,160);

	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:slide];
	imageView.frame=CGRectMake(gotoX,gotoY,160,160);
	[UIView commitAnimations];
	
	
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


- (void) move
{
	NSInteger slide=UISlider;
	NSInteger ran=rand()%8;
	NSInteger left;
	NSInteger top;
	if(ran==0)
	{
		left=-160;
		top=-160;
	}
	if(ran==1)
	{
		left=320;
		top=-160;
	}
	if(ran==2)
	{
		left=320;
		top=480;
	}
	if(ran==3)
	{
		left=-160;
		top=480;
	}
	if(ran==4)
	{
		left=80;
		top=-160;
	}
	if(ran==5)
	{
		left=80;
		top=480;
	}
	if(ran==6)
	{
		left=-160;
		top=160;
	}
	if(ran==7)
	{
		left=480;
		top=160;
	}
	
	imageView.frame=CGRectMake(left,top,160,160);
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:slide];
	
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(moveOff)];
	
	imageView.frame=CGRectMake(80,160,160,160);
	
	[UIView commitAnimations];
}

- (void) moveOff
{
	NSInteger slide=UISlider;
	NSInteger ran=rand()%8;
	NSInteger left;
	NSInteger top;
	if(ran==0)
	{
		left=-160;
		top=-160;
	}
	if(ran==1)
	{
		left=320;
		top=-160;
	}
	if(ran==2)
	{
		left=320;
		top=480;
	}
	if(ran==3)
	{
		left=-160;
		top=480;
	}
	if(ran==4)
	{
		left=80;
		top=-160;
	}
	if(ran==5)
	{
		left=80;
		top=480;
	}
	if(ran==6)
	{
		left=-160;
		top=160;
	}
	if(ran==7)
	{
		left=480;
		top=160;
	}
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:slide];
	
	imageView.frame=CGRectMake(left,top,160,160);
	
	[UIView commitAnimations];
}


@end
