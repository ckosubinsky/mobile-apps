/*
 *  enumSort.h
 *  FP390
 *
 *  Created by Stéphane Kahn on 26/11/10.
 *  Copyright 2010 EFREI. All rights reserved.
 *
 */


typedef enum {
	SortByDate,
	SortByName
} SortCells;