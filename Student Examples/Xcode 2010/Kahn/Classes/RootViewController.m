//
//  navigationViewController.m
//  FP390
//
//  Created by Stéphane Kahn on 22/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import "RootViewController.h"
#import "FP390AppDelegate.h"
#import "Drug.h"
#import <QuartzCore/QuartzCore.h>
#import "AddViewController.h"


@implementation RootViewController

@synthesize drugView;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];

	self.title = NSLocalizedString(@"TitleKey", @"");

	appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];

	[Drug readDrugsFromDatabase];
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];

	[Drug readDrugsFromDatabase];
	[self.tableView reloadData];
	
	NSArray *stringItems = [NSArray arrayWithObjects:NSLocalizedString(@"DateKey", @""), NSLocalizedString(@"NameKey", @""), nil];
	UISegmentedControl *sortSegmented = [[UISegmentedControl alloc] initWithItems:stringItems];
	sortSegmented.segmentedControlStyle = UISegmentedControlStyleBar;
	sortSegmented.tintColor = [UIColor redColor];
	if(appDelegate.sort == SortByDate)
		sortSegmented.selectedSegmentIndex = 0;
	else
		sortSegmented.selectedSegmentIndex = 1;
	
	[sortSegmented addTarget:self action:@selector(action:) forControlEvents:UIControlEventValueChanged];
	UIBarButtonItem *sortButton = [[[UIBarButtonItem alloc] initWithCustomView:sortSegmented] autorelease];
	UIBarButtonItem *flexible = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
	UIBarButtonItem *plus = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addDrugSheet)] autorelease];
	sortButton.style = UIBarButtonItemStyleBordered;
	
	//Add the toolbar as a subview to the navigation controller.
	
	[self setToolbarItems:[NSArray arrayWithObjects:flexible, sortButton, flexible, plus, nil] animated:YES];
	[self.navigationController setToolbarHidden:NO animated:YES];
	self.navigationController.navigationBar.tintColor = [UIColor redColor];
	self.navigationController.toolbar.tintColor = [UIColor redColor];
	
	[sortSegmented release];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	
	// If the hostname is not reachable, or if the device does not permit to scan
	if(!appDelegate.scannable && self.drugView == nil){
		UIAlertView *alertReachable = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NoBarcodeTitle", @"") message:NSLocalizedString(@"NoBarcodeMessage", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Continue", @"") otherButtonTitles:nil] autorelease];
		[alertReachable show];
	}
	if(!appDelegate.reachable && self.drugView == nil){
		UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NoConnectionTitle", @"") message:NSLocalizedString(@"NoConnectionMessage", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Continue", @"") otherButtonTitles:nil] autorelease];
		[alert show];
	}
}

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
	self.navigationController.toolbar.hidden = TRUE;// removeFromSuperview];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.

	return [appDelegate.titleSectionArray count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	
	return [[appDelegate.sectionArray objectAtIndex:section] count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...

	// We retrieve the drug at the indicated path
	Drug *drug = (Drug *) [[appDelegate.sectionArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	
	cell.textLabel.text = drug.name;
	cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", drug.quantity ];
	
	if(drug.quantity < 10)
		cell.detailTextLabel.textColor = [UIColor redColor];
	else {
		cell.detailTextLabel.textColor = [UIColor blackColor]; 
	}

	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	
	return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger) section {
	//Return the title of a section
	
	return (NSString *) [appDelegate.titleSectionArray objectAtIndex:section];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
	// Determine the display of a section
	
	UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 22)] autorelease];	
	
	// Creation of the gradient
	CAGradientLayer *gradient = [CAGradientLayer layer];
	gradient.frame = headerView.bounds;
	gradient.startPoint = CGPointMake(0.0, 0.5);
	gradient.endPoint = CGPointMake(1.0, 0.5);
	UIColor *startColor = [UIColor blackColor];
	UIColor *endColor;
	UIColor *textColor;	
	
	UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)] autorelease];
	label.text = [self tableView:tableView titleForHeaderInSection:section];
	label.textColor = [UIColor redColor];
	label.backgroundColor = [UIColor clearColor];
	
	if(appDelegate.sort == SortByName){
		textColor = [UIColor whiteColor];
		endColor = [UIColor colorWithRed:0.8 green:0.8 blue:1 alpha:1];
	} else {
		NSDate *today = [NSDate date];
		NSDate *sectionDate = [appDelegate.outputFormatter dateFromString:[self tableView:tableView titleForHeaderInSection:section]];
		if ([sectionDate compare:today] != NSOrderedDescending) {
			textColor = [UIColor redColor];
			endColor = [UIColor colorWithRed:1 green:0.4 blue:0.4 alpha:1];
		} else {
			textColor = [UIColor whiteColor];
			endColor = [UIColor colorWithRed:0.7 green:1 blue:0.7 alpha:1];
		}
	}
	label.textColor = textColor;
	
	gradient.colors = [NSArray arrayWithObjects:(id)[startColor CGColor], (id)[endColor CGColor], nil];
	
	[headerView.layer insertSublayer:gradient atIndex:0];
	[headerView addSubview:label];
	
	return headerView;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.

	Drug *drug = (Drug *) [[appDelegate.sectionArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	
	if(self.drugView == nil) {
		DrugsViewController *viewController = [[DrugsViewController alloc] initWithNibName:@"DrugsViewController" bundle:nil];
		self.drugView = viewController;
		[viewController release];
	}

	self.drugView.drug = drug;
	
	// Setup the animation
	[self.navigationController pushViewController:self.drugView animated:YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

#pragma mark -
#pragma mark Order

- (void) action:(id)sender {
	UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
	
	if([segmentedControl selectedSegmentIndex] == 0){
		appDelegate.sort = SortByDate;
	} else {
		appDelegate.sort = SortByName;
	}

	// We reload the data depending on the new sort order
	[Drug readDrugsFromDatabase];
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Barcode

- (void) scanDrug
{
    // ADD: present a barcode reader that scans from the camera feed
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
	
    ZBarImageScanner *scanner = reader.scanner;
	
    // disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
				   config: ZBAR_CFG_ENABLE
					   to: 0];
	
    // present and release the controller
    [self presentModalViewController: reader animated: YES];
    [reader release];
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // get the decode results
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
	
    ZBarSymbol *symbol = nil;
	
    for(symbol in results)
        // just grab the first barcode
        break;
	
	//Creation of the add view controller
	AddViewController *viewController = [[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
	
    // Dismiss the controller
    [reader dismissModalViewControllerAnimated: NO];

	if(symbol != nil)
		viewController.strBarcode = symbol.data;

	[self presentModalViewController:viewController animated:NO];
	
	[viewController release];
}


-(void) addDrugSheet{
	UIActionSheet *addDrugSheet;

	addDrugSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Add", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Scan", @""), NSLocalizedString(@"Enter", @""), NSLocalizedString(@"Manual", @""), nil  ] ;

	// If either the host is not reachable or the scanner unavailable (i.e. wrong device)
	if(!(appDelegate.reachable && appDelegate.scannable)){
		
        // We browse the buttons from the UIActionSheet
		for (UIView* view in [addDrugSheet subviews])
		{
			// We check the class
			if ([[[view class] description] isEqualToString:@"UIThreePartButton"])
			{
				
				if ([view respondsToSelector:@selector(title)])
				{

					NSString* title = [view performSelector:@selector(title)];
					
					// If the host is not reachable or the scanner not available, we cannot scan.
					if ([title isEqualToString:NSLocalizedString(@"Scan", @"")] && [view respondsToSelector:@selector(setEnabled:)] && (!appDelegate.reachable || !appDelegate.scannable))
					{
						[view performSelector:@selector(setEnabled:) withObject:NO];
					}
					
					// If the host is not reachable, we cannot manually enter a barcode
					if ([title isEqualToString:NSLocalizedString(@"Enter", @"")] && [view respondsToSelector:@selector(setEnabled:)] && !appDelegate.reachable)
					{
						[view performSelector:@selector(setEnabled:) withObject:NO];
					}
				}
			}
		}
	}
	
	[addDrugSheet showFromToolbar:self.navigationController.toolbar];
}

- (void)actionSheet:(UIActionSheet *)drugSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if ([[drugSheet buttonTitleAtIndex:buttonIndex] compare:NSLocalizedString(@"Scan", @"")] == NSOrderedSame){
		// If the Scan button is touched, we launch the scanner
		[self scanDrug];
	} else if([[drugSheet buttonTitleAtIndex:buttonIndex] compare:NSLocalizedString(@"Enter", @"")] == NSOrderedSame){
		// If the Enter button is touched, we ask the user to enter a barcode
		UIAlertView *barcodeAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Enter", @"") message:NSLocalizedString(@"EAN13Please", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:NSLocalizedString(@"Continue", @""), nil];
		
		// Function not documented, but it works.
		[barcodeAlertView addTextFieldWithValue:@"" label:NSLocalizedString(@"Barcode", @"")];
		[[barcodeAlertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
		
		[barcodeAlertView show];
	} else if([[drugSheet buttonTitleAtIndex:buttonIndex] compare:NSLocalizedString(@"Manual", @"")] == NSOrderedSame) {
		// If the user decides to manual enter the medicaments, we only create a new AddViewController
		
		AddViewController *viewController = [[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
		
		viewController.strBarcode = NSLocalizedString(@"Manually", @"");
		
		[self presentModalViewController:viewController animated:NO];
		
		[viewController release];		
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if([[alertView buttonTitleAtIndex:buttonIndex] compare:NSLocalizedString(@"Continue", @"")] == NSOrderedSame){
		// If the user clicked the Continue button, we check if the value is a valid barcode and we create a new view
		UITextField *tf = [alertView textFieldAtIndex:0];
		if([RootViewController isBarCodeValid:tf.text]){
			AddViewController *viewController = [[AddViewController alloc] initWithNibName:@"AddViewController" bundle:nil];
			
			viewController.strBarcode = tf.text;

			[self presentModalViewController:viewController animated:YES];
			
			[viewController release];
			
		} else {
			UIAlertView *incorrectBCAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"IncorrectBCTitle", @"") message:NSLocalizedString(@"IncorrectBCMessage", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:nil];
			[incorrectBCAlertView show];
		}
	}
}

+(BOOL) isBarCodeValid:(NSString *)code{		
	// We only deal with EAN13 barcode
	if([code length] == 13){
		int i = 0;
		
		NSInteger sum = 0;
		
		for(i=0; i < [code length]-1; i++){

			// Depending on its position, the figure is multiplied by 3 or not;
			if(i%2 == 0){
				sum = sum + [[code substringWithRange:NSMakeRange(i, 1)] integerValue];
			} else {
				sum = sum +[[code substringWithRange:NSMakeRange(i, 1)] integerValue]*3;
			}
		}
		
		// We retrieve the last digit
		NSString *strSum = [[NSString alloc] init];
		strSum = [NSString stringWithFormat:@"%d", sum];
		NSInteger sumLastDigit = [[strSum substringWithRange:NSMakeRange([strSum length]-1, 1)] integerValue] ;

		// If the key (10-lastDigit) matches with the last digit, the barcode is valid
		if(10-sumLastDigit == [[code substringWithRange:NSMakeRange([code length]-1, 1) ] integerValue]) {
			return TRUE;
		}
	}
	return FALSE;
}


@end

