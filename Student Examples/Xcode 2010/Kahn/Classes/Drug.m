//
//  Drugs.m
//  FP390
//
//  Created by Stéphane Kahn on 25/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import "Drug.h"
#import <sqlite3.h>
#import "FP390AppDelegate.h"


@implementation Drug
@synthesize name, quantity, primaryKey, expire;

-(id)initWithName:(NSString *)n quantity:(NSInteger )q expiration:(NSDate *) e {
	FP390AppDelegate *appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	self.name = n;
	quantity = q;
	self.expire = e;
	
	NSLog(@"New drug : %@ - %@ - %d", name, [appDelegate.outputFormatter stringFromDate: expire], quantity);
	
	return self;
}

-(id)initWithId:(NSInteger)i name:(NSString *)n quantity:(NSInteger )q expiration:(NSDate *) e {
	FP390AppDelegate *appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	self.primaryKey = i;
	self.name = n;
	quantity = q;
	self.expire = e;
	
	NSLog(@"New drug : %@ - %@ - %d", name, [appDelegate.outputFormatter stringFromDate: expire], quantity);
	
	return self;
}


+ (void) readDrugsFromDatabase {	
	FP390AppDelegate *appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	// Déclaration de l'objet database
	sqlite3 *database;
	
	if (appDelegate.drugsDictionary == nil){
		appDelegate.drugsDictionary = [[NSMutableDictionary alloc] init];
	} else {
		[appDelegate.drugsDictionary removeAllObjects];
	}
	if(appDelegate.sectionArray == nil){
		appDelegate.sectionArray = [[NSMutableArray alloc] init];
		appDelegate.titleSectionArray = [[NSMutableArray alloc] init];
	} else {
		[appDelegate.sectionArray removeAllObjects];
		[appDelegate.titleSectionArray removeAllObjects];
	}

	// On ouvre la BDD à partir des fichiers système
	if(sqlite3_open([[[appDelegate databasePath]retain] UTF8String], &database) == SQLITE_OK) {
		
		// Préparation de la requête SQL qui va permettre de récupérer les objets score de la BDD
		//en triant les scores dans l'ordre décroissant
		char *comboquery;
		
		if(appDelegate.sort == SortByDate){
			comboquery = "select * from medics ORDER BY expire ASC";
		} else {
			comboquery = "select *, substr(name, 1, 1) as first from medics order by name asc";
		}
		
		const char *comboStatement = comboquery;
		
		//création d'un objet permettant de connaître le status de l'exécution de la requête

		//---------------------------------------------------------------------------------------------------
		sqlite3_stmt *compiledStatementCombo;
		if(sqlite3_prepare_v2(database, comboStatement, -1, &compiledStatementCombo, NULL) == SQLITE_OK) {
			NSString *section = nil;
			NSString *tmpSection = nil;

			NSMutableArray *tmpDrugs = nil;
						
			while(sqlite3_step(compiledStatementCombo) == SQLITE_ROW) {

				// On récupère le nom du médicament dans la deuxième colonne
				NSString *aName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementCombo, 1)];
				NSLog(@"Traitement de %@", aName);
				// On récupère la date dans la troisième colonne, puis on la transforme à l'aide du NSDateFormatter
				NSString *date = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementCombo, 2)];
				NSDate *expire = [appDelegate.inputFormatter dateFromString:date];
				
				// On récupère la quantité dans la quatrième colonne
				NSInteger quantity = sqlite3_column_int(compiledStatementCombo, 3);
				
				NSInteger primary_key = sqlite3_column_int(compiledStatementCombo, 0);
				
				// On récupère la section de l'objet courant selon le tri voulu
				if(appDelegate.sort == SortByName)
					tmpSection = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementCombo, 4)];
				else if(appDelegate.sort == SortByDate)
					tmpSection = [appDelegate.outputFormatter stringFromDate:expire];
				
				
				// On crée le médicament avant de l'ajouter dans le tableau
				Drug *drug = [[Drug alloc] initWithId:primary_key name:aName quantity:quantity expiration:expire];

				
				if (section == nil) {
					NSLog(@"1)section: %@ - tmpSection: %@", section, tmpSection );
					section = [NSString stringWithString:tmpSection];
					tmpDrugs = [[NSMutableArray alloc] init];
					NSLog(@"2)section: %@ - tmpSection: %@", section, tmpSection );
				}
				if ([section compare:tmpSection] == NSOrderedSame){
					NSLog(@"NSOrderedSame add: %@", drug.name);
					[tmpDrugs addObject:drug];
					NSLog(@"%d objects in tmpDrugs (NSOrderedSame)", [tmpDrugs count]);
				} else {
					NSArray *array = [NSArray arrayWithArray:tmpDrugs];
					[tmpDrugs removeAllObjects];
					[appDelegate.drugsDictionary setObject:array forKey:section];
					[appDelegate.titleSectionArray addObject:section];
					[appDelegate.sectionArray addObject:array];
					NSLog(@"Ajout de %d objets pour la clef %@", [array  count], section);
					[tmpDrugs addObject:drug];
					section = [NSString stringWithString:tmpSection];
					//[array release];
				}
				
				[drug release];
			}

			NSArray *array = [NSArray arrayWithArray:tmpDrugs];
			[tmpDrugs release];
			[appDelegate.drugsDictionary setObject:array forKey:section];
			[appDelegate.titleSectionArray addObject:section];
			[appDelegate.sectionArray addObject:array];
			NSLog(@"Ajout de %d objets pour la clef %@", [array count], section);
			//[array release];

		}
		sqlite3_finalize(compiledStatementCombo);


	}
	//On ferme la BDD
	sqlite3_close(database);
	
}

+ (void) addDrugToDatabase:(Drug *)drug {
	
	FP390AppDelegate *appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];

	sqlite3 *database;
	
	
	
	if(sqlite3_open([[[appDelegate databasePath]retain] UTF8String], &database) == SQLITE_OK) {

		NSString *query = [NSString stringWithFormat:@"INSERT INTO medics ('name', 'expire', 'quantity') VALUES ('%@', '%@', '%d')",
						   [drug name], [appDelegate.inputFormatter stringFromDate:[drug expire]], [drug quantity]];
		NSLog(@"%@", query);

		sqlite3_stmt *compiledStatement;
		const char * statement = [query UTF8String];

		if(sqlite3_prepare_v2(database, statement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			sqlite3_step(compiledStatement);
		}
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}

+(void) updateDrug:(Drug *) drug {
	
	FP390AppDelegate *appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	sqlite3 *database;
	
	if(sqlite3_open([[[appDelegate databasePath]retain] UTF8String], &database) == SQLITE_OK) {

		const char *sql = "UPDATE medics SET name = ?, expire = ?, quantity = ? WHERE id = ?";
		
		sqlite3_stmt *updateStmt = nil;

		
		if(sqlite3_prepare_v2(database, sql, -1, &updateStmt, NULL) != SQLITE_OK)
			NSLog(@"Error while creating update statement. %s", sqlite3_errmsg(database));
		
		sqlite3_bind_text(updateStmt, 1, [drug.name UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(updateStmt, 2, [[appDelegate.inputFormatter stringFromDate: [drug expire]] UTF8String], -1, SQLITE_TRANSIENT );
		sqlite3_bind_int(updateStmt, 3, drug.quantity);
		sqlite3_bind_int(updateStmt, 4, drug.primaryKey);
		
		if(SQLITE_DONE != sqlite3_step(updateStmt))
			NSLog(@"Error while updating. %s", sqlite3_errmsg(database));
				
		sqlite3_reset(updateStmt);
		
		
		sqlite3_finalize(updateStmt);
	}
	sqlite3_close(database);
	 
}

@end
