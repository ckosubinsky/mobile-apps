//
//  FP390AppDelegate.h
//  FP390
//
//  Created by Stéphane Kahn on 22/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>    // Import du framework sqlite
#import "enumSort.h"

@interface FP390AppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	UINavigationController *navigationController;

	// Variables de la Base de Données
	NSString *databaseName;
	NSString *databasePath;
	
	NSMutableDictionary *drugsDictionary;
	
	NSMutableArray *sectionArray;
	NSMutableArray *titleSectionArray;
	
	NSDateFormatter *inputFormatter;
	NSDateFormatter *outputFormatter;
	// Tri des médicaments
	SortCells sort;
	
	BOOL reachable;
	BOOL scannable;
	BOOL alertDisplayed;
	
	NSString *systemName;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic, retain) NSMutableDictionary *drugsDictionary;
@property (nonatomic, retain) NSMutableArray *sectionArray;
@property (nonatomic, retain) NSMutableArray *titleSectionArray;
@property (nonatomic, retain) NSDateFormatter *inputFormatter;
@property (nonatomic, retain) NSDateFormatter *outputFormatter;
@property SortCells sort;
@property (nonatomic, retain) NSString *databasePath;
@property (nonatomic, retain) NSString *systemName;
@property (nonatomic, readonly) BOOL reachable;
@property (nonatomic, readonly) BOOL scannable; 
@property (nonatomic) BOOL alertDisplayed; 

- (void) checkAndCreateDatabase;

@end

