//
//  Drugs.h
//  FP390
//
//  Created by Stéphane Kahn on 25/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Drug : NSObject {
	NSInteger primaryKey;
	NSString *name;
	NSDate *expire;
	NSInteger quantity;
}

@property (assign, nonatomic) NSInteger primaryKey;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSDate *expire;
@property (assign, nonatomic) NSInteger quantity;

-(id) initWithName:(NSString *)n quantity:(NSInteger )q expiration:(NSDate *) e; 
-(id) initWithId:(NSInteger)i name:(NSString *)n quantity:(NSInteger )q expiration:(NSDate *) e; 
+(void) readDrugsFromDatabase;
+(void) addDrugToDatabase:(Drug *)drug;
+(void) updateDrug:(Drug *)drug;

@end
