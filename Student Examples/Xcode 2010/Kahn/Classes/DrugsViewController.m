//
//  DrugsViewController.m
//  FP390
//
//  Created by Stéphane Kahn on 25/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import "DrugsViewController.h"
#import "FP390AppDelegate.h"
#import "Drug.h"


@implementation DrugsViewController

@synthesize expireLabel, quantityLabel, nameLabel, titleNameLabel;
@synthesize drug;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.navigationItem.rightBarButtonItem = [self editButtonItem];
	
	[[self editButtonItem] setAction:@selector(update)];

	// Creation of the quantity field
	quantityField = [[UITextField alloc] initWithFrame:CGRectMake(quantityLabel.frame.origin.x, quantityLabel.frame.origin.y, quantityLabel.bounds.size.width, quantityLabel.bounds.size.height)];
	quantityField.backgroundColor = [UIColor greenColor];
	quantityField.borderStyle = UITextBorderStyleRoundedRect;
	quantityField.textColor = [UIColor blackColor]; //text color
	quantityField.font = [UIFont systemFontOfSize:17.0];  //font size
	quantityField.backgroundColor = [UIColor whiteColor]; //background color
	quantityField.autocorrectionType = UITextAutocorrectionTypeNo;	// no auto correction support
	quantityField.keyboardType = UIKeyboardTypeNumberPad;  // type of the keyboard
	quantityField.returnKeyType = UIReturnKeyDone;  // type of the return key
	quantityField.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
	quantityField.text = [NSString stringWithFormat:@"%d", drug.
						  quantity];
	
	[self.view addSubview:quantityField];
	
	
	// Creation of the name field
	nameField = [[UITextField alloc] initWithFrame:CGRectMake(nameLabel.frame.origin.x, nameLabel.frame.origin.y, nameLabel.frame.size.width, nameLabel.frame.size.height)];
	nameField.backgroundColor = [UIColor greenColor];
	nameField.borderStyle = UITextBorderStyleRoundedRect;
	nameField.textColor = [UIColor blackColor]; //text color
	nameField.font = [UIFont systemFontOfSize:17.0];  //font size
	nameField.backgroundColor = [UIColor whiteColor]; //background color
	nameField.autocorrectionType = UITextAutocorrectionTypeNo;	// no auto correction support
	nameField.keyboardType = UIKeyboardTypeDefault;
	nameField.returnKeyType = UIReturnKeyDone;  // type of the return key
	nameField.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
	nameField.text = drug.name;

	[self.view addSubview:nameField];
	
	
	datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 158, 320, 250)];
	datePicker.datePickerMode = UIDatePickerModeDate;
	datePicker.hidden = NO;
	datePicker.date = [NSDate date];
	[datePicker addTarget:self action:@selector(changeDate) forControlEvents:UIControlEventValueChanged];
	[self.view addSubview:datePicker];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[quantityField release];
	[nameField release];
	[datePicker release];

    [super dealloc];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	
	quantityLabel.text = [NSString stringWithFormat:@"%d", drug.quantity];
	
	FP390AppDelegate *appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];
	expireLabel.text = [appDelegate.outputFormatter stringFromDate:drug.expire];
	
	[self setEditing:FALSE animated:animated];
}

-(void) update{

	if(([nameField.text compare:@""] == NSOrderedSame) || ([quantityField.text compare:@""] == NSOrderedSame)){
		UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"WrongInput", @"") message:NSLocalizedString(@"FieldNotNull", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"Continue", @"") otherButtonTitles:nil] autorelease];
		[alert show];
	} else {
		if(self.editing){
			self.title = drug.name;

			drug.expire = datePicker.date;
			drug.quantity = [quantityField.text integerValue];
			drug.name = nameField.text;
			[Drug updateDrug:drug];
		}
		[self setEditing:!self.editing animated:TRUE];
	}
}


-(void) setEditing:(BOOL)editing animated:(BOOL)animated {
	[super setEditing:editing animated:animated];
	
	[self.navigationItem setHidesBackButton:editing animated:animated];

	
	
	if(editing == TRUE){
		NSLog(@"editing");
		
		// We hide the "Name:" label
		[titleNameLabel setHidden:FALSE];
		
		// We hide the quantity label
		[quantityLabel setHidden:TRUE];
		
		[quantityField setHidden:FALSE];
		[nameField setHidden:FALSE];
		[datePicker setHidden:FALSE];
		[UIView transitionFromView:nameLabel toView:nameField duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
	} else {		
		[titleNameLabel setHidden:TRUE];
		[nameField setHidden:TRUE];
		[quantityLabel setHidden:FALSE];
		[quantityField setHidden:TRUE];
		[datePicker setHidden:TRUE];
		[UIView transitionFromView:nameField toView:nameLabel duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight completion:nil];
		NSLog(@"notediting");
	}
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *myTouch = [[event allTouches] anyObject];
	
	if([nameField isEditing] && ([myTouch view] != nameField)){
		[nameField resignFirstResponder];
	} else if([quantityField isEditing] && ([myTouch view] != quantityField)){
		[quantityField resignFirstResponder];
	}
}


-(void)changeDate{
	FP390AppDelegate *appDelegate = (FP390AppDelegate *)[[UIApplication sharedApplication] delegate];
	expireLabel.text = [appDelegate.outputFormatter stringFromDate:[datePicker date]];
}

@end
