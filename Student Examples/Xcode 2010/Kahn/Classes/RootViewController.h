//
//  navigationViewController.h
//  FP390
//
//  Created by Stéphane Kahn on 22/11/10.
//  Copyright 2010 EFREI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrugsViewController.h"
#import "ZBarSDK.h"
#import "FP390AppDelegate.h"


@interface RootViewController : UITableViewController < ZBarReaderDelegate, UIActionSheetDelegate, UIAlertViewDelegate> {
	DrugsViewController *drugView;
	FP390AppDelegate *appDelegate;
}

@property (nonatomic, retain) DrugsViewController *drugView;


- (void) action:(id) sender;
- (void) scanDrug;
- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info;
- (void) addDrugSheet;

+(BOOL) isBarCodeValid:(NSString *)code;

@end
