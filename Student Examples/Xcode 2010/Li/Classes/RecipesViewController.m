//
//  RecipesViewController.m
//  CookingMadeEasy
//
//  Created by SammyLi on 12/1/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "RecipesViewController.h"
#import "RecipesDetailViewController.h";
#import "CookingMadeEasyAppDelegate.h";
#import "Project.h"

@implementation RecipesViewController
@synthesize recipeDetailView;


#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	
	// change color of bar
	UINavigationBar *bar = [self.navigationController navigationBar];
	[bar setTintColor:[UIColor colorWithRed:139.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:1.0]];
	
	// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    CookingMadeEasyAppDelegate *appDelegate = (CookingMadeEasyAppDelegate *)[[UIApplication sharedApplication] delegate];
	return appDelegate.projects.count;

}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }
    
	// Configure the cell.
	
	CookingMadeEasyAppDelegate *appDelegate = (CookingMadeEasyAppDelegate *)[[UIApplication sharedApplication] delegate];
	Project *project = (Project *)[appDelegate.projects objectAtIndex:indexPath.row];
	
	[cell setText:project.name];
	
    return cell;
	
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	CookingMadeEasyAppDelegate *appDelegate = (CookingMadeEasyAppDelegate *)[[UIApplication sharedApplication] delegate];
	Project *project = (Project *)[appDelegate.projects objectAtIndex:indexPath.row];
	
	//check of theres a view already
	if(self.recipeDetailView == nil) {
		RecipesDetailViewController *recipes = [[RecipesDetailViewController alloc] initWithNibName:@"RecipesDetailViewController" bundle:nil];
		self.recipeDetailView = recipes;
		[recipes release];
	}
	
	[self.navigationController pushViewController:self.recipeDetailView animated:YES];
	
	self.recipeDetailView.title = [project name];
	
	// set the view for all variables
	self.recipeDetailView.recipeName.text = [project name];
	
	self.recipeDetailView.cooktime.text = [project description];
	
	[self.recipeDetailView.ingredients setText:[project ingredients]];
	
	[self.recipeDetailView.steps setText:[project steps]];
	
	self.recipeDetailView.foodImage.image = [UIImage imageNamed:[project imageURL]];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

