//
//  AlertTask.m
//  CookingMadeEasy
//
//  Created by SammyLi on 12/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AlertTask.h"

@implementation AlertTask
@synthesize textField;
@synthesize enteredText;

//Create the alert object
- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle okButtonTitle:(NSString *)okayButtonTitle
{
	
    if (self = [super initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:okayButtonTitle, nil])
    {
        UITextField *theTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 40.0, 260.0, 25.0)]; 
        [theTextField setBackgroundColor:[UIColor whiteColor]]; 
        [self addSubview:theTextField];
        self.textField = theTextField;
        [theTextField release];
    }
    return self;
}

//Display the text field
- (void)show
{
    [textField becomeFirstResponder];
    [super show];
}

//Getter for entered text
- (NSString *)enteredText
{
    return textField.text;
}

- (void)dealloc
{
    [textField release];
    [super dealloc];
}
@end
