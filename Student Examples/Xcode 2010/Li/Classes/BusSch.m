//
//  BusSch.m
//  CookingMadeEasy
//
//  Created by SammyLi on 12/6/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BusSch.h"


@implementation BusSch

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[scrollView setScrollEnabled:YES];
	[scrollView setContentSize:CGSizeMake(320, 1500)];
	UINavigationBar *bar = [self.navigationController navigationBar];
	[bar setTintColor:[UIColor colorWithRed:139.0/255.0 green:1.0/255.0 blue:1.0/255.0 alpha:1.0]];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
