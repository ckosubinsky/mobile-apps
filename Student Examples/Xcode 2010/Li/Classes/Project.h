//
//  Project.h
//  MMlab
//
//  Created by teabones on 11/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//Code taken from lecture slides and changed accordingly
@interface Project : NSObject {
	NSString *name;
	NSString *description;
	NSString *imageURL;
	NSString *ingredients;
	NSString *steps;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *imageURL;
@property (nonatomic, retain) NSString *ingredients;
@property (nonatomic, retain) NSString *steps;

-(id)initWithName:(NSString *)n description:(NSString *)d url:(NSString *)u ingred:(NSString *)i steps:(NSString *)s;

@end
