//
//  KitchenAllyViewController.h
//  KitchenAlly
//
//  Created by SammyLi on 10/15/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KitchenAllyViewController : UIViewController <UITextFieldDelegate>{
	IBOutlet UITextField *input;
	
	IBOutlet UITextField *output;
	
	IBOutlet UILabel *help;
	
	IBOutlet UIButton *tbl_tsp;
	IBOutlet UIButton *tsp_tbl;
	IBOutlet UIButton *tbl_mL;
	IBOutlet UIButton *mL_tbl;
	IBOutlet UIButton *cup_tbl;
	IBOutlet UIButton *tbl_cup;
	IBOutlet UIButton *cup_floz;
	IBOutlet UIButton *floz_cup;
	IBOutlet UIButton *floz_tbl;
	IBOutlet UIButton *tbl_floz;
	IBOutlet UIButton *floz_mL;
	IBOutlet UIButton *mL_floz;
	IBOutlet UIButton *g_floz;
	IBOutlet UIButton *floz_g;
	IBOutlet UIButton *g_lb;
	IBOutlet UIButton *lb_g;
	
}
- (IBAction) tbl_tsp;
- (IBAction) tsp_tbl;
- (IBAction) tbl_mL;
- (IBAction) mL_tbl;
- (IBAction) cup_tbl;
- (IBAction) tbl_cup;
- (IBAction) cup_floz;
- (IBAction) floz_cup;
- (IBAction) floz_tbl;
- (IBAction) tbl_floz;
- (IBAction) floz_mL;
- (IBAction) mL_floz;
- (IBAction) g_floz;
- (IBAction) floz_g;
- (IBAction) g_lb;
- (IBAction) lb_g;


@property (nonatomic, retain) UILabel *help;

@end

