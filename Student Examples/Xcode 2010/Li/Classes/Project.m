//
//  Project.m
//  MMlab
//
//  Created by teabones on 11/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Project.h"


@implementation Project
@synthesize name, description, imageURL, ingredients, steps;

-(id)initWithName:(NSString *)n description:(NSString *)d url:(NSString *)u ingred:(NSString *)i steps:(NSString *)s{
	self.name = n;
	self.description = d;
	self.imageURL = u;
	self.ingredients = i;
	self.steps = s;
	return self;
}

@end


