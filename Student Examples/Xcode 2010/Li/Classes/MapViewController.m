//
//  MapViewController.m
//  CookingMadeEasy
//
//  Created by SammyLi on 12/1/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MapViewController.h"
#import "DisplayMap.h"


@implementation MapViewController

@synthesize mapView;

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}

 
 
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
        
	
	// Set up the map to zoom in on Stony Brook
	[mapView setMapType:MKMapTypeStandard];
	[mapView setZoomEnabled:YES];
	[mapView setScrollEnabled:YES];
	
	MKCoordinateRegion region;
	MKCoordinateSpan span;
	span.latitudeDelta=0.060;
	span.longitudeDelta=0.025;
	
	CLLocationCoordinate2D location=mapView.userLocation.coordinate;
	
	
	location.latitude=40.8898;
	location.longitude=-73.1106;
	region.span=span;
	region.center=location;
	
	
	[mapView setRegion:region animated:TRUE];
	[mapView regionThatFits:region];
	[self.view insertSubview:mapView atIndex:0];
	
	
	// add all the annotations we want to the map
	CLLocationCoordinate2D coords = mapView.userLocation.coordinate;
	coords.latitude=40.85764;
	coords.longitude=-73.08672;
	region.span=span;
	region.center=coords;
	
	[mapView setDelegate:self];
        
	DisplayMap *path = [[DisplayMap alloc] init]; 
	path.title = @" PathMark";
	path.subtitle = @"Meats!!"; 
	path.coordinate = region.center; 
	[mapView addAnnotation:path];
	
	coords.latitude=40.86251;
	coords.longitude=-73.08148;
	region.span=span;
	region.center=coords;
	
	DisplayMap *walmart = [[DisplayMap alloc] init]; 
	walmart.title = @" Walmart";
	walmart.subtitle = @"Cheap Eggs!"; 
	walmart.coordinate = region.center; 
	[mapView addAnnotation:walmart];
	
	coords.latitude=40.88399;
	coords.longitude=-73.10191;
	region.span=span;
	region.center=coords;
	
	DisplayMap *target = [[DisplayMap alloc] init]; 
	target.title = @" Target";
	target.subtitle = @"Everything Else!"; 
	target.coordinate = region.center; 
	[mapView addAnnotation:target];
	
	coords.latitude=40.86527;
	coords.longitude=-73.13337;
	region.span=span;
	region.center=coords;
	
	DisplayMap *traders = [[DisplayMap alloc] init]; 
	traders.title = @" Trader's Joe";
	traders.subtitle = @"Organic Food!"; 
	traders.coordinate = region.center; 
	[mapView addAnnotation:traders];
	
	coords.latitude=40.86946;
	coords.longitude=-73.12612;
	region.span=span;
	region.center=coords;
	
	DisplayMap *wholefoods = [[DisplayMap alloc] init]; 
	wholefoods.title = @" Whole Foods";
	wholefoods.subtitle = @"Free Samples"; 
	wholefoods.coordinate = region.center; 
	[mapView addAnnotation:wholefoods];
	
	coords.latitude=40.87319;
	coords.longitude=-73.12577;
	region.span=span;
	region.center=coords;
	
	DisplayMap *wal = [[DisplayMap alloc] init]; 
	wal.title = @" Waldbaums";
	wal.subtitle = @"Good Fruits"; 
	wal.coordinate = region.center; 
	[mapView addAnnotation:wal];
	
	coords.latitude=40.90696;
	coords.longitude=-73.07256;
	region.span=span;
	region.center=coords;
	
	DisplayMap *walb = [[DisplayMap alloc] init]; 
	walb.title = @" Waldbaums";
	walb.subtitle = @"Good Fruits"; 
	walb.coordinate = region.center; 
	[mapView addAnnotation:walb];
	
	coords.latitude=40.8794;
	coords.longitude=-73.1162;
	region.span=span;
	region.center=coords;
	
	DisplayMap *chi = [[DisplayMap alloc] init]; 
	chi.title = @" Oriental Food";
	chi.subtitle = @"Asian Stuff"; 
	chi.coordinate = region.center; 
	[mapView addAnnotation:chi];
	
	coords.latitude=40.8793;
	coords.longitude=-73.1039;
	region.span=span;
	region.center=coords;
	
	DisplayMap *ss = [[DisplayMap alloc] init]; 
	ss.title = @" Stop and Shop";
	ss.subtitle = @"Year Round Sales"; 
	ss.coordinate = region.center; 
	[mapView addAnnotation:ss];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}



// the method that displays the annotations
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:
(id <MKAnnotation>)annotation {
	MKPinAnnotationView *pinView = nil; 
	if(annotation != mapView.userLocation) 
	{
		static NSString *defaultPinID = @"com.invasivecode.pin";
		pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
		if ( pinView == nil ) pinView = [[[MKPinAnnotationView alloc]
										  initWithAnnotation:annotation reuseIdentifier:defaultPinID] autorelease];
		pinView.pinColor = MKPinAnnotationColorRed; 
		pinView.canShowCallout = YES;
		pinView.animatesDrop = YES;
	} 
	else {
		[mapView.userLocation setTitle:@"I am here"];
	}
	return pinView;
}


@end
