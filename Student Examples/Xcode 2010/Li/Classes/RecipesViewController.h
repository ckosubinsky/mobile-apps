//
//  RecipesViewController.h
//  CookingMadeEasy
//
//  Created by SammyLi on 12/1/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RecipesDetailViewController;


@interface RecipesViewController : UITableViewController {
	RecipesDetailViewController *recipeDetailView;
}

@property(nonatomic, retain) NSMutableArray *array;
@property(nonatomic, retain) RecipesDetailViewController *recipeDetailView;
@end
