//
//  CookingMadeEasyAppDelegate.h
//  CookingMadeEasy
//
//  Created by SammyLi on 12/1/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface CookingMadeEasyAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate> {
    UIWindow *window;
    UITabBarController *tabBarController;
	
	// Database variables
	NSString *databaseName;
	NSString *databasePath;
	
	NSMutableArray *projects;
	
}



@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, retain) NSMutableArray *projects;

@end
