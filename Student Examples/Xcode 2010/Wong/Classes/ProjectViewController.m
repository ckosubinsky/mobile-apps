//
//  ProjectViewController.m
//  Project
//
//  Created by MingtungWong on 10/20/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "ProjectViewController.h"

@implementation ProjectViewController

@synthesize score;
@synthesize score2;

NSInteger initXVel = 10;
NSInteger initYVel = 10;
NSInteger velCap = 40;
NSInteger xVel;
NSInteger yVel;
float scoreVal = 0;
float scoreVal2 = 0;
float lastScore = 0;
NSInteger paddleHeight = 150;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	scoreVal = 0;
	scoreVal2 = 0;
	
	[score setText:[NSString stringWithFormat:@"%3.0f", scoreVal]];
	[score2 setText:[NSString stringWithFormat:@"%3.0f", scoreVal2]];
	ball.frame = CGRectMake(-110, -50, 20, 20);
	top.frame = CGRectMake(0, 0, 480, 10);
	bottom.frame = CGRectMake(0, 290, 480, 10);
	paddle.frame = CGRectMake(0, 150 - paddleHeight / 2, 10, paddleHeight);
	paddle2.frame = CGRectMake(470, 150 - paddleHeight / 2, 10, paddleHeight);
	
	srand([[NSDate date] timeIntervalSince1970]);
	[NSTimer scheduledTimerWithTimeInterval:.05 target:self selector:@selector(move) userInfo:nil repeats:YES];
	
	[self move];
	
}




// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {

	if ((interfaceOrientation==UIInterfaceOrientationPortrait)||(interfaceOrientation==UIInterfaceOrientationPortraitUpsideDown))
		
		// NO locks out the portrait mode of the iPhone. The initial orientation also has to be set in the info plist.
	{
		return NO;
	}
	if ((interfaceOrientation==UIInterfaceOrientationLandscapeLeft)||(interfaceOrientation==UIInterfaceOrientationLandscapeRight))
	{
		return YES;
	}
	
	
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (void) move
{
	
	
	
	if(ball.frame.origin.x < 0 || ball.frame.origin.x > 490){
		
		ball.frame = CGRectMake(240, 100, 20, 20);
		
		NSInteger xDir = rand()%2;
		NSInteger yDir = rand()%2;

		xVel = initXVel * (-1 + (2 * xDir));
		yVel = initYVel * (-1 + (2 * yDir));

		
	}
	
	else{
		
		ball.frame = CGRectMake(ball.frame.origin.x + xVel, ball.frame.origin.y + yVel, 20, 20);		
	}
	
	
	[self checkCollision];
	
	
	
}

- (void) checkCollision {
	
	if(CGRectIntersectsRect(ball.frame, paddle.frame)){
		
		xVel = -xVel;
		
		
	}
	   
	if(CGRectIntersectsRect(ball.frame, top.frame)){
		
		yVel = -yVel;
		
	}

	if(CGRectIntersectsRect(ball.frame, paddle2.frame)){
		
		xVel = -xVel;
		
	}
	
	   
	if(CGRectIntersectsRect(ball.frame, bottom.frame)){
		
		yVel = -yVel;
		
	}
	
	if(ball.frame.origin.x < 0){
		
		scoreVal2++;
		
		[score2 setText: nil];
		[score2 setText:[NSString stringWithFormat:@"%3.0f", scoreVal2]];
		
	}
	
	if(ball.frame.origin.x > 490){
		
		scoreVal++;
		
		[score setText: nil];
		[score setText:[NSString stringWithFormat:@"%3.0f", scoreVal]];
		
	}
	
}	
	 
	- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
		
		UITouch *myTouch = [[event allTouches] anyObject];
		
		
		NSInteger p1prevY = paddle.frame.origin.y;
		NSInteger p2prevY = paddle2.frame.origin.y;
		
		paddle.center = [myTouch locationInView:self.view];
		
		NSInteger whereTouched = paddle.frame.origin.x;
		
		if(whereTouched <= 240){
			
			paddle.frame = CGRectMake(0, paddle.frame.origin.y, 10, paddleHeight);
			paddle2.frame = CGRectMake(470, p2prevY, 10, paddleHeight);
			
		}
		
		else{
			
			paddle2.frame = CGRectMake(470, paddle.frame.origin.y, 10, paddleHeight);
			paddle.frame = CGRectMake(0, p1prevY, 10, paddleHeight);

		}
		
		
		
		if(CGRectIntersectsRect(paddle.frame, top.frame)){
			
			paddle.frame = CGRectMake(0, top.frame.size.height, 10, paddleHeight);
			
		}
		   
		if(CGRectIntersectsRect(paddle.frame, bottom.frame)){
			
			paddle.frame = CGRectMake(0, 290 - paddleHeight, 10, paddleHeight);
			
		}
		
		
		
		if(CGRectIntersectsRect(paddle2.frame, top.frame)){
			
			paddle2.frame = CGRectMake(470, 10, 10, paddleHeight);
			
		}
		
		if(CGRectIntersectsRect(paddle2.frame, bottom.frame)){
			
			paddle2.frame = CGRectMake(470, 290 - paddleHeight, 10, paddleHeight);
			
		}
		
		
		
	}

	
	



@end
