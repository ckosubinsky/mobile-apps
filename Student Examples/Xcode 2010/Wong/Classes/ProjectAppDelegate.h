//
//  ProjectAppDelegate.h
//  Project
//
//  Created by MingtungWong on 10/20/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProjectViewController;

@interface ProjectAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    ProjectViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet ProjectViewController *viewController;

@end

