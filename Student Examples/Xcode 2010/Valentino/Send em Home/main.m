//
//  main.m
//  Send em Home
//
//  Created by Tristan Valentino on 11/1/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	int retVal = UIApplicationMain(argc, argv, nil, @"Send_em_HomeAppDelegate");
	[pool release];
	return retVal;
}
