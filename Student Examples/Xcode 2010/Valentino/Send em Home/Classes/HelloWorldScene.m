//
//  HelloWorldLayer.m
//  Send em Home
//
//  Created by Tristan Valentino on 11/1/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

// Import the interfaces
#import "HelloWorldScene.h"


// HelloWorld implementation
@implementation HelloWorld

@synthesize label;

+(id) scene {
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorld *layer = [HelloWorld node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if ((self = [super initWithColor:ccc4(86,126,79,255)])) {
		
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		
		//add header label
		
		label = [CCLabelTTF labelWithString:@"Send em Home" fontName:@"Marker Felt" fontSize:100];
		label.color = ccc3(0,0,0);
		label.position = ccp(winSize.width/2, 700);
		[self addChild:label];
		
		
		//add blue graphics
		CCSprite * blue1 = [CCSprite spriteWithFile:@"blueBob.png"];
		blue1.position = ccp(200,300);
		[self addChild:blue1];
		
		CCSprite * blue2 = [CCSprite spriteWithFile:@"blueBob.png"];
		blue2.position = ccp(700,300);
		[self addChild:blue2];
		
		CCSprite * blue3 = [CCSprite spriteWithFile:@"blueBob.png"];
		blue3.position = ccp(700,500);
		[self addChild:blue3];
		
		
		//add red graphics
		CCSprite * red1 = [CCSprite spriteWithFile:@"redBob.png"];
		red1.position = ccp(300,200);
		[self addChild:red1];
		
		CCSprite * red2 = [CCSprite spriteWithFile:@"redBob.png"];
		red2.position = ccp(400,600);
		[self addChild:red2];
		
		CCSprite * red3 = [CCSprite spriteWithFile:@"redBob.png"];
		red3.position = ccp(900,700);
		[self addChild:red3];
		
		CCSprite * red4 = [CCSprite spriteWithFile:@"redBob.png"];
		red4.position = ccp(800,200);
		[self addChild:red4];
		
		
		//add bomb graphics
		CCSprite * bomb1 = [CCSprite spriteWithFile:@"bomb1.png"];
		bomb1.position = ccp(150,650);
		[self addChild:bomb1];
		
		CCSprite * bomb2 = [CCSprite spriteWithFile:@"bomb2.png"];
		bomb2.position = ccp(550,250);
		[self addChild:bomb2];
		
		//add play button to menu
		CCMenuItem *playMenuItem = [CCMenuItemImage itemFromNormalImage:@"playButton.png" selectedImage:@"playButton.png" target:self selector:@selector(playButtonTapped:)];
		playMenuItem.position = ccp(winSize.width/2, (winSize.height/2+(2*playMenuItem.contentSize.height) + 20));
		
		//add instructions button to menu
		CCMenuItem *instrMenuItem = [CCMenuItemImage itemFromNormalImage:@"instrButton.png" selectedImage:@"instrButton.png" target:self selector:@selector(instrButtonTapped:)];
		instrMenuItem.position = ccp(winSize.width/2, winSize.height/2+playMenuItem.contentSize.height + 10);
		
		//add scores button to menu
		CCMenuItem *scoresMenuItem = [CCMenuItemImage itemFromNormalImage:@"scoresButton.png" selectedImage:@"scoresButton.png" target:self selector:@selector(scoresButtonTapped:)];
		scoresMenuItem.position = ccp(winSize.width/2, winSize.height/2);
		
		//add menu
		CCMenu *startMenu = [CCMenu menuWithItems:playMenuItem, scoresMenuItem, instrMenuItem, nil];
		startMenu.position = CGPointZero;
		[self addChild:startMenu];
		
		self.isTouchEnabled = YES;
		
	}
	return self;
}


- (void)playButtonTapped:(id)sender {
	gameScene *playGame = [gameScene node];
    [[CCDirector sharedDirector] replaceScene:playGame];	
}

- (void)scoresButtonTapped:(id)sender {
	ScoresScene *scoresScene = [ScoresScene node];
	
    [[CCDirector sharedDirector] replaceScene:scoresScene];	
}

- (void)instrButtonTapped:(id)sender {
	InstructionsScene *instrScene = [InstructionsScene node];
    
	[[CCDirector sharedDirector] replaceScene:instrScene];	
}

- (void) dealloc {
	[super dealloc];
}
@end
