//
//  HelloWorldLayer.h
//  Send em Home
//
//  Created by Tristan Valentino on 11/1/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "gameScene.h"
#import "instructions.h"
#import "scores.h"


// HelloWorld Layer
@interface HelloWorld : CCColorLayer {
	CCLabelTTF *_label;
}

@property (nonatomic, retain) CCLabelTTF *label;
// returns a Scene that contains the HelloWorld as the only child
+(id) scene;

@end
//static int tempScore;