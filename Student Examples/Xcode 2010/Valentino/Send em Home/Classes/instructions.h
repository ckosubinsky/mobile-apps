#import "cocos2d.h"

@interface InstructionsLayer : CCColorLayer {
	CCLabelTTF *_header;
}
@property (nonatomic, retain) CCLabelTTF *header;
@end

@interface InstructionsScene : CCScene {
	InstructionsLayer *_layer;
}
@property (nonatomic, retain) InstructionsLayer *layer;
@end