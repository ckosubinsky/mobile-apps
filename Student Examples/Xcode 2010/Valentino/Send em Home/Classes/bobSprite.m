#import "bobSprite.h"
#import "HelloWorldScene.h"
#import "gameScene.h"

@implementation bobSprite

@synthesize walkAction;

-(id) init{
	if((self=[super init])){
		
		CGSize winSize = [CCDirector sharedDirector].winSize;
		//LOAD SPRITE SHEET
		[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"bobs.plist"];
		CCSpriteSheet * spriteSheet = [CCSpriteSheet spriteSheetWithFile: @"bobs.png"];
		
		int r = ((arc4random() % 2)+1);
		int dir = ((arc4random() % 2)+1);
		
		//CREATE AN ARRAY OF IMAGES TO CYCLE THROUGH TO CREATE
		//THE ANIMATION OF WALKING
		NSMutableArray *walkFrames = [NSMutableArray array];
		switch (r) {
			case 1:
				[walkFrames addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"bob1.png"]];
				[walkFrames addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"bob2.png"]];
				self.tag = 1;
				break;
			case 2:
				[walkFrames addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"bob3.png"]];
				[walkFrames addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"bob4.png"]];
				self.tag = 2;
				break;
		}
		//CREATE THE ANIMATION 
		CCAnimation *walkAnim = [CCAnimation animationWithName:@"walk" delay:0.1f frames:walkFrames];
		
		self.walkAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:walkAnim restoreOriginalFrame:NO]];
		
		self.position = ccp(winSize.width/2, winSize.height/2);
		
		//DETERMINE SPEED
		int minDuration = 3.0;
		int maxDuration = 5.0;
		int rangeDuration = maxDuration - minDuration;
		int actualDuration = (arc4random() % rangeDuration) + minDuration;
		
		int minX = 120 + self.contentSize.width/2;
		int maxX = 890 - self.contentSize.width/2;
		int rangeX = maxX - minX;
		int actualX = (arc4random() % rangeX) + minX;
		
		id actionMove;
		id actionMoveDone;
		
		//START RUNNING IN PLACE
		[self runAction:walkAction];
		
		switch (dir) {
			case 1:
				self.position = ccp(actualX, 0);
				actionMove = [CCMoveTo actionWithDuration:actualDuration position:ccp(actualX, (winSize.height-self.contentSize.height/2))];
				break;
			case 2:
				self.position = ccp(actualX, 768);
				actionMove = [CCMoveTo actionWithDuration:actualDuration position:ccp(actualX, self.contentSize.height/2)];
				break;
		}
		actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(bobMoveEnded)];

		[self runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
		
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
	}
	return self;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	[[CCDirector sharedDirector] resume];
	[[CCDirector sharedDirector] replaceScene:[HelloWorld scene]];
}

-(void)bobMoveEnded {
	if ((self.position.y <= 10) || (self.position.y >= 760)) {
		//PAUSE AND SHOW ALERT
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Game Over" message:@"You Lost :-(\nTry Again!" delegate:self cancelButtonTitle:@"Go To Main Menu" otherButtonTitles:nil];
		[alert show];
		[[CCDirector sharedDirector] pause];
	}
	[self stopAction:walkAction];
}

-(BOOL) isTouchOnSprite:(CGPoint)touch {
	if(CGRectContainsPoint(CGRectMake(self.position.x - ((self.contentSize.width/2)*self.scale)-15, self.position.y - ((self.contentSize.height/2)*self.scale)-15, self.contentSize.width*self.scale+15, self.contentSize.height*self.scale+15), touch)) 
		return YES;
	else return NO;
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint touchPoint = [touch locationInView:[touch view]];
	touchPoint = [[CCDirector sharedDirector] convertToGL:touchPoint];
	
	if([self isTouchOnSprite:touchPoint]){
		[self pauseSchedulerAndActions];
		whereTouch=ccpSub(self.position, touchPoint);
		return YES;
	}
	
	return NO;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint touchPoint = [touch locationInView:[touch view]];
	touchPoint = [[CCDirector sharedDirector] convertToGL:touchPoint];
	
	self.position=ccpAdd(touchPoint,whereTouch);
	
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	int rand = (arc4random() % 2);
	int dir;
	//determine side to run to when let go
	switch(rand){
		case 0:
			dir = 0;
			break;
		case 1:
			dir = 768;
			break;
	}	
	
	//red
	if(self.tag == 2){
		if(self.position.x > 890){
			[self stopAction:walkAction];
			bobsSentHome++;
		}else {
			[self stopAllActions];
			[self runAction:walkAction];
			id actionMove = [CCMoveTo actionWithDuration:2.0 position:ccp(self.position.x, dir)];
			id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(bobMoveEnded)];
			[self runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
		}
	}
	//blue
	if(self.tag == 1) {
		if(self.position.x < 120) {
			[self stopAction:walkAction];
			bobsSentHome++;
		}else {
			[self stopAllActions];
			[self runAction:walkAction];
			id actionMove = [CCMoveTo actionWithDuration:2.0 position:ccp(self.position.x, dir)];
			id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(bobMoveEnded)];
			[self runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
		}
	}
}

@end
