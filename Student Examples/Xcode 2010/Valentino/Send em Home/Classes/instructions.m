#import "instructions.h"
#import "HelloWorldScene.h"

@implementation InstructionsScene
@synthesize layer = _layer;

- (id)init {
	
	if ((self = [super init])) {
		self.layer = [InstructionsLayer node];
		[self addChild:_layer];
	}
	return self;
}

- (void)dealloc {
	[_layer release];
	_layer = nil;
	[super dealloc];
}

@end

@implementation InstructionsLayer
@synthesize header = _header;


-(id) init
{
	if( (self=[super initWithColor:ccc4(86,126,79,255)] )) {
		
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		
		CCSprite * bg = [CCSprite spriteWithFile:@"instructions.png"];
		bg.position = ccp(winSize.width/2, winSize.height/2);
		[self addChild:bg z:0];
		
		self.header = [CCLabelTTF labelWithString:@"Instructions" fontName:@"Marker Felt" fontSize:48];
		_header.color = ccc3(0,0,0);
		_header.position = ccp(winSize.width/2, (winSize.height-125));
		[self addChild:_header];
		
		//add play button to menu
		CCMenuItem *playMenuItem = [CCMenuItemImage itemFromNormalImage:@"playButton.png" selectedImage:@"playButton.png" target:self selector:@selector(playButtonTapped)];
		playMenuItem.position = ccp(winSize.width/2, winSize.height/2);
		
		//add menu button to menu
		CCMenuItem *menuMenuItem = [CCMenuItemImage itemFromNormalImage:@"menuButton.png" selectedImage:@"menuButton.png" target:self selector:@selector(multiDone)];
		menuMenuItem.position = ccp(winSize.width/2, winSize.height/2-75);
		
		//add menu to screen
		CCMenu *startMenu = [CCMenu menuWithItems:playMenuItem, menuMenuItem, nil];
		startMenu.position = CGPointZero;
		[self addChild:startMenu];
		
	}	
	return self;
}

- (void)playButtonTapped {
	gameScene *playGame = [gameScene node];
    [[CCDirector sharedDirector] replaceScene:playGame];	
}

- (void)multiDone {
	[[CCDirector sharedDirector] replaceScene:[HelloWorld scene]];
}

- (void)dealloc {
	[_header release];
	_header = nil;
	[super dealloc];
}

@end