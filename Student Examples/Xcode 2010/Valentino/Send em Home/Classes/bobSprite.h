#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface bobSprite : CCSprite <CCTargetedTouchDelegate> {
	BOOL isDrag;
	CGPoint whereTouch;
	CCAction *walkAction;
	CCAction *moveAction;
}
@property (nonatomic, retain) CCAction *walkAction;
@property (nonatomic, retain) CCAction *moveAction;
-(BOOL) isTouchOnSprite:(CGPoint)touch;
@end
static int bobsSentHome;
