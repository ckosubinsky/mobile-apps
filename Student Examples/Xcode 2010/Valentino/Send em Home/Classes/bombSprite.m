#import "bombSprite.h"
#import "HelloWorldScene.h"
#import "gameScene.h"

@implementation bombSprite

@synthesize walkAction;

-(id) init{
	if((self=[super init])){
		
		CGSize winSize = [CCDirector sharedDirector].winSize;
		//LOAD SPRITE SHEET
		[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"bomb.plist"];
		CCSpriteSheet * spriteSheet = [CCSpriteSheet spriteSheetWithFile: @"bomb.png"];
		
		int dir = ((arc4random() % 2)+1);
		
		//CREATE AN ARRAY OF IMAGES TO CYCLE THROUGH TO CREATE
		//THE ANIMATION OF WALKING
		NSMutableArray *walkFrames = [NSMutableArray array];
		[walkFrames addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"bomb1.png"]];
		[walkFrames addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: @"bomb2.png"]];

		//CREATE THE ANIMATION 
		CCAnimation *walkAnim = [CCAnimation animationWithName:@"walk" delay:0.1f frames:walkFrames];
		
		self.walkAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:walkAnim restoreOriginalFrame:NO]];
		
		self.position = ccp(winSize.width/2, winSize.height/2);
		
		//DETERMINE SPEED
		int minDuration = 3.0;
		int maxDuration = 5.0;
		int rangeDuration = maxDuration - minDuration;
		int actualDuration = (arc4random() % rangeDuration) + minDuration;
		
		int minX = 120 + self.contentSize.width/2;
		int maxX = 890 - self.contentSize.width/2;
		int rangeX = maxX - minX;
		int actualX = (arc4random() % rangeX) + minX;
		
		id actionMove;
		id actionMoveDone;
		
		//START RUNNING IN PLACE
		[self runAction:walkAction];
		
		switch (dir) {
			case 1:
				self.position = ccp(actualX, 0);
				actionMove = [CCMoveTo actionWithDuration:actualDuration position:ccp(actualX, (winSize.height+20))];
				break;
			case 2:
				self.position = ccp(actualX, 768);
				actionMove = [CCMoveTo actionWithDuration:actualDuration position:ccp(actualX, -20)];
				break;
		}
		[self runAction:actionMove];
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
	}
	return self;
}

-(BOOL) isTouchOnSprite:(CGPoint)touch {
	if(CGRectContainsPoint(CGRectMake(self.position.x - ((self.contentSize.width/2)*self.scale)-10, self.position.y - ((self.contentSize.height/2)*self.scale)-10, self.contentSize.width*self.scale+10, self.contentSize.height*self.scale+10), touch))
		return YES;
	else return NO;
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint touchPoint = [touch locationInView:[touch view]];
	touchPoint = [[CCDirector sharedDirector] convertToGL:touchPoint];
	
	if([self isTouchOnSprite:touchPoint]){
		[self pauseSchedulerAndActions];
		whereTouch=ccpSub(self.position, touchPoint);
		
		//PAUSE AND SHOW ALERT
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Game Over" message:@"You Lost :-(\nTry Again!" delegate:self cancelButtonTitle:@"Go To Main Menu" otherButtonTitles:nil];
		[alert show];
		[[CCDirector sharedDirector] pause];
		
		//[[CCDirector sharedDirector] replaceScene:[HelloWorld scene]];
		return YES;
	}
	return NO;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	[[CCDirector sharedDirector] resume];
	[[CCDirector sharedDirector] replaceScene:[HelloWorld scene]];
}

@end