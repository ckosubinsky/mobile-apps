#import "gameScene.h"
#import "HelloWorldScene.h"
#import "bobSprite.h"
#import "bombSprite.h"

@implementation gameScene
@synthesize layer;

- (id)init {
	if ((self = [super init])) {
		self.layer = [gameLayer node];
		[self addChild:layer];
	}
	return self;
}

- (void)dealloc {
	[layer release];
	layer = nil;
	[super dealloc];
}

@end

@implementation gameLayer

@synthesize timeLabel;

-(id) init {
	if( (self=[super init] )) {
		//start the director and load the background
		[[CCDirector sharedDirector] resume];
		secs = 0;
		rate = 3.0;
		CCSprite * level = [CCSprite spriteWithFile:@"level.png"];
		level.anchorPoint = CGPointMake(0,0);
		[self addChild:level];
		
		CGSize winSize = [CCDirector sharedDirector].winSize;
		NSString *tempTimeString = [NSString stringWithFormat:@"Seconds Survived: %i",secs];
		//create the label to display the seconds that have passed
		self.timeLabel = [CCLabelTTF labelWithString:tempTimeString fontName:@"Marker Felt" fontSize:32];
		timeLabel.color = ccc3(0,0,0);
		timeLabel.position = ccp(winSize.width/2, ((winSize.height/2)+ timeLabel.contentSize.height));
		[self addChild:timeLabel];
		//start all the timers
		[self schedule:@selector(gameLogic:) interval: rate];
		[self schedule:@selector(countUp) interval: 1.0];
		[self schedule:@selector(changeSpeed:) interval:10];
		
	}	
	return self;
}

- (void)changeSpeed:(ccTime)dt {
	if (rate>1.5) {
		rate = rate-0.3;
		[self unschedule:@selector(gameLogic)];
		[self schedule:@selector(gameLogic:) interval:rate];
	}
}

-(void)gameLogic:(ccTime)dt {
	[self addTarget];
}

-(void)addTarget {
	int whichSprite = ((arc4random() % 6)+1);
	bombSprite *bomb;
	bobSprite *bob;
	
	//add either a bomb or bob
	if(whichSprite == 1) {
		bomb = [bombSprite spriteWithFile:@"bomb1.png"];
		[self addChild:bomb];
	}
	else {
		bob = [bobSprite spriteWithFile:@"redBob.png"];
		[self addChild:bob];
	}
}

-(void)countUp {
	
	secs++;
	[timeLabel setString:[NSString stringWithFormat:@"Seconds Survived: %i", secs]];
	
	//For setting high score:
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSInteger highScore = [defaults	integerForKey:@"bobScore"];
	if(secs > highScore) {
		[[NSUserDefaults standardUserDefaults] setInteger:secs forKey:@"bobScore"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

- (void)quit {
	[[CCDirector sharedDirector] replaceScene:[HelloWorld scene]];
}

- (void)dealloc {
	[timeLabel release];
	timeLabel = nil;
	[super dealloc];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	return YES;
}
@end