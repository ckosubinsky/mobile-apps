#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface bombSprite : CCSprite <CCTargetedTouchDelegate> {
	CGPoint whereTouch;
	CCAction *walkAction;
	CCAction *moveAction;
}
@property (nonatomic, retain) CCAction *walkAction;
@property (nonatomic, retain) CCAction *moveAction;
-(BOOL) isTouchOnSprite:(CGPoint)touch;
@end
