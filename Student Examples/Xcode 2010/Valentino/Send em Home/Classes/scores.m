#import "scores.h"
#import "HelloWorldScene.h"

@implementation ScoresScene
@synthesize layer = _layer;

- (id)init {
	
	if ((self = [super init])) {
		self.layer = [ScoresLayer node];
		[self addChild:_layer];
	}
	return self;
}

- (void)dealloc {
	[_layer release];
	_layer = nil;
	[super dealloc];
}

@end

@implementation ScoresLayer
@synthesize label = _label;
@synthesize header = _header;


-(id) init
{
	if( (self=[super initWithColor:ccc4(86,126,79,255)] )) {
		
		CGSize winSize = [[CCDirector sharedDirector] winSize];

		self.header = [CCLabelTTF labelWithString:@"Scores & Info" fontName:@"Marker Felt" fontSize:48];
		_header.color = ccc3(0,0,0);
		_header.position = ccp(winSize.width/2, (winSize.height-125));
		[self addChild:_header];

		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSInteger highScore = [defaults	integerForKey:@"bobScore"];
		NSString *temp = [NSString stringWithFormat:@"High Score on %@: %i seconds.", [[UIDevice currentDevice] name],highScore];
		
		self.label = [CCLabelTTF labelWithString:temp fontName:@"Marker Felt" fontSize:32];
		_label.color = ccc3(0,0,0);
		_label.position = ccp(winSize.width/2, winSize.height/2);
		[self addChild:_label];
		
		
		//add blue graphics
		CCSprite * blue1 = [CCSprite spriteWithFile:@"blueBob.png"];
		blue1.position = ccp(200,300);
		[self addChild:blue1];
		
		CCSprite * blue2 = [CCSprite spriteWithFile:@"blueBob.png"];
		blue2.position = ccp(700,300);
		[self addChild:blue2];
		
		CCSprite * blue3 = [CCSprite spriteWithFile:@"blueBob.png"];
		blue3.position = ccp(700,500);
		[self addChild:blue3];
		
		
		//add red graphics
		CCSprite * red1 = [CCSprite spriteWithFile:@"redBob.png"];
		red1.position = ccp(300,200);
		[self addChild:red1];
		
		CCSprite * red2 = [CCSprite spriteWithFile:@"redBob.png"];
		red2.position = ccp(400,500);
		[self addChild:red2];
		
		CCSprite * red3 = [CCSprite spriteWithFile:@"redBob.png"];
		red3.position = ccp(900,700);
		[self addChild:red3];
		
		CCSprite * red4 = [CCSprite spriteWithFile:@"redBob.png"];
		red4.position = ccp(800,200);
		[self addChild:red4];
		
		
		//add bomb graphics
		CCSprite * bomb1 = [CCSprite spriteWithFile:@"bomb1.png"];
		bomb1.position = ccp(150,650);
		[self addChild:bomb1];
		
		CCSprite * bomb2 = [CCSprite spriteWithFile:@"bomb2.png"];
		bomb2.position = ccp(550,50);
		[self addChild:bomb2];
		
		//add play button to menu
		CCMenuItem *playMenuItem = [CCMenuItemImage itemFromNormalImage:@"playButton.png" selectedImage:@"playButton.png" target:self selector:@selector(playButtonTapped)];
		playMenuItem.position = ccp(winSize.width/2, winSize.height/2-125);
		
		//add menu button to menu
		CCMenuItem *menuMenuItem = [CCMenuItemImage itemFromNormalImage:@"menuButton.png" selectedImage:@"menuButton.png" target:self selector:@selector(multiDone)];
		menuMenuItem.position = ccp(winSize.width/2, winSize.height/2-200);
		
		//add menu to screen
		CCMenu *startMenu = [CCMenu menuWithItems:playMenuItem, menuMenuItem, nil];
		startMenu.position = CGPointZero;
		[self addChild:startMenu];
		
	}	
	return self;
}

- (void)playButtonTapped {
	gameScene *playGame = [gameScene node];
    [[CCDirector sharedDirector] replaceScene:playGame];	
}

- (void)multiDone {
	[[CCDirector sharedDirector] replaceScene:[HelloWorld scene]];
}

- (void)dealloc {
	[_label release];
	_label = nil;
	[_header release];
	_header = nil;
	[super dealloc];
}

@end