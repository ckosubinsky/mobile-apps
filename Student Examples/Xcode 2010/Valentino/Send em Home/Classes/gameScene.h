#import "cocos2d.h"

@interface gameLayer : CCColorLayer {
	CCLabelTTF *timeLabel;
	double rate;
}
-(void)incrementScore;

@property (nonatomic, retain) CCLabelTTF *timeLabel;
@end

@interface gameScene : CCScene {
	gameLayer *layer;

}

@property (nonatomic, retain) gameLayer *layer;
@end
int secs;