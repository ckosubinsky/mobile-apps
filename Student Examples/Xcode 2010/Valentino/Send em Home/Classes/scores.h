#import "cocos2d.h"

@interface ScoresLayer : CCColorLayer {
	CCLabelTTF *_label;
	CCLabelTTF *_header;
}
@property (nonatomic, retain) CCLabelTTF *label;
@property (nonatomic, retain) CCLabelTTF *header;
@end

@interface ScoresScene : CCScene {
	ScoresLayer *_layer;
}
@property (nonatomic, retain) ScoresLayer *layer;
@end