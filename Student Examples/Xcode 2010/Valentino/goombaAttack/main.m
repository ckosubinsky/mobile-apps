//
//  main.m
//  goombaAttack
//
//  Created by Tristan Valentino on 10/6/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	int retVal = UIApplicationMain(argc, argv, nil, @"goombaAttackAppDelegate");
	[pool release];
	return retVal;
}
