#import "HelloWorldScene.h"

@implementation HelloWorld

@synthesize enemies = _enemies;
@synthesize mario = _mario;
@synthesize goomba = _goomba;
@synthesize goombaMoveAction = _goombaMoveAction;
@synthesize goombaWalkAction = _goombaWalkAction;
@synthesize marioMoveAction = _marioMoveAction;
@synthesize marioWalkAction = _marioWalkAction;


+(id) scene {
	CCScene *scene = [CCScene node];
	HelloWorld *layer = [HelloWorld node];
	[scene addChild: layer];
	return scene;
}

-(id) init {
    if((self=[super init])) {
		
		//START MUSIC
		[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"smb.mp3"];
		
		//ARRAY TO HOUSE ENEMIES
		_enemies = [[NSMutableArray alloc] init];
		
		rate = 1.1;
		
		//ADD GREEN BACKGROUND
		CCSprite * bg = [CCSprite spriteWithFile:@"greenbg.png"];
		bg.scale = 1.6;
		bg.anchorPoint = CGPointMake(0,0);
		[self addChild:bg];
		
		//ADD LABEL TO DISPLAY "PAUSED"
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		_pauseLabel = [[CCLabel labelWithString:@"" dimensions:CGSizeMake(320, 50) alignment:UITextAlignmentCenter fontName:@"Arial" fontSize:32.0] retain];
		_pauseLabel.color = ccc3(0,0,0);
		_pauseLabel.position = ccp(winSize.width/2, winSize.height-(_pauseLabel.contentSize.height/2));
		[self addChild:_pauseLabel];
		
		//ADD PAUSE BUTTON THAT TOGGLES TO PLAY
		_pauseItem = [[CCMenuItemImage itemFromNormalImage:@"pause.png" selectedImage:@"pause.png" target:nil selector:nil] retain];
		_playItem = [[CCMenuItemImage itemFromNormalImage:@"play.png" selectedImage:@"play.png" target:nil selector:nil] retain];
		CCMenuItemToggle *toggleItem = [CCMenuItemToggle itemWithTarget:self selector:@selector(playPauseTapped:) items:_pauseItem, _playItem, nil];
		CCMenu *toggleMenu = [CCMenu menuWithItems:toggleItem, nil];
		toggleMenu.position = ccp(30, 30);
		[self addChild:toggleMenu];
		
		self.isTouchEnabled = YES;
		[self generateMario];
		//start timers
		[self schedule:@selector(changeSpeed:) interval:10];
		[self schedule:@selector(gameLogic:) interval:1.0];
		[self schedule:@selector(update:)];
	}
    return self;
}

//CHANGE RATE AT WHICH ENEMIES ARE SPAWNED, BUT NOT TOO FAST
- (void)changeSpeed:(ccTime)dt {
	if (rate>0.5) {
		rate = rate-0.1;
		[self unschedule:@selector(gameLogic)];
		[self schedule:@selector(gameLogic:) interval:rate];
	}
}

//IF PAUSED, THEN RESUME... OTHERWISE, PAUSE
- (void)playPauseTapped:(id)sender {
	if ([CCDirector sharedDirector].isPaused) {
		[[CCDirector sharedDirector] resume];
		[_pauseLabel setString:[NSString stringWithFormat: @"Score: %i",_goombasAvoided]];
		[[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
	}else{
		[[CCDirector sharedDirector] pause];
		[_pauseLabel setString:@"-PAUSED-"];
		[[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
	}
}

-(void)generateMario{
	//LOAD SPRITE SHEET
	[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"combo.plist"];
	CCSpriteSheet *spriteSheet = [CCSpriteSheet spriteSheetWithFile:@"combo.png"];
	[self addChild:spriteSheet];
	
	//MARIO WALK FRAMES
	NSMutableArray *walkAnimFrames = [NSMutableArray array];
	for(int i = 1; i <= 3; ++i) {
		[walkAnimFrames addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"%d.png", (i)]]];
	}
	
	//MARIO WALK ANIM
	CCAnimation *marioWalkAnim = [CCAnimation animationWithName:@"walk" delay:0.1f frames:walkAnimFrames];
	CGSize winSize = [CCDirector sharedDirector].winSize;
	self.mario = [CCSprite spriteWithSpriteFrameName:@"1.png"];        
	_mario.position = ccp(winSize.width/2, winSize.height/2);
	self.marioWalkAction = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:marioWalkAnim restoreOriginalFrame:NO]];
	
	[spriteSheet addChild:_mario];
}

-(void)gameLogic:(ccTime)dt {
	//call method to spawn character
	[self addTarget];
}

- (void)update:(ccTime)dt {
	for (CCSprite *enemy in _enemies) {
		//intentionally allow a very small amount of overlapping by subtracting 10px from each rect
		//to make up for each characters' transparancy
		CGRect enemyRect = CGRectMake(enemy.position.x - (enemy.contentSize.width/2), enemy.position.y - (enemy.contentSize.height/2), enemy.contentSize.width-10, enemy.contentSize.height-10);
		CGRect marioRect = CGRectMake(_mario.position.x - (_mario.contentSize.width/2), _mario.position.y - (_mario.contentSize.height/2), _mario.contentSize.width-10, _mario.contentSize.height-10);
		
		if ((CGRectIntersectsRect(enemyRect, marioRect))&& marioRect.origin.x>0) {
			if(enemy.tag == 3) {
				//if its a mushroom, add points and play sound as well as vibrate the phone
				AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
				[[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
				[[SimpleAudioEngine sharedEngine] playEffect:@"powerup.mp3"];
				_goombasAvoided += 50;
				[_pauseLabel setString:[NSString stringWithFormat: @"Score: %i",_goombasAvoided]];
				enemy.tag = 4;
				[[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
				//[_enemies removeObject:enemy];
				[self removeChild:enemy cleanup:YES];
			}
			if(enemy.tag == 2) {
				//if its a mushroom, add points and play sound as well as vibrate the phone
				AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
				[[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
				[[SimpleAudioEngine sharedEngine] playEffect:@"powerup.mp3"];
				_goombasAvoided += 25;
				[_pauseLabel setString:[NSString stringWithFormat: @"Score: %i",_goombasAvoided]];
				enemy.tag = 4;
				[[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
				//[_enemies removeObject:enemy];
				[self removeChild:enemy cleanup:YES];
			}
			if(enemy.tag == 1){
				//if its an ememy then vibrate, and switch to game over scene
				AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
				[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
				GameOverScene *gameOverScene = [GameOverScene node];
				NSString *temp = [NSString stringWithFormat:@"Score: %d", _goombasAvoided];
				[gameOverScene.layer.label setString:temp];
				
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				NSInteger highScore = [defaults	integerForKey:@"score"];
				//if a new high score was set then display an alert
				if (highScore < _goombasAvoided) {
					highScore = _goombasAvoided;
					[[NSUserDefaults standardUserDefaults] setInteger:highScore forKey:@"score"];
					[[NSUserDefaults standardUserDefaults] synchronize];
					[[SimpleAudioEngine sharedEngine] playEffect:@"win.mp3"];
					NSString *alertTemp = [NSString stringWithFormat:@"You Set a New High Score of %d!", _goombasAvoided];
					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congrats!" message:alertTemp delegate:self cancelButtonTitle:@"Yay!" otherButtonTitles:nil];
					[alert show];
					[alert release];
				}else {
					[[SimpleAudioEngine sharedEngine] playEffect:@"lose.mp3"];
				}
				NSString *highScoreContent = [NSString stringWithFormat:@"High Score: %d", highScore];
				[gameOverScene.layer.highScoreLabel setString:highScoreContent];
				[[CCDirector sharedDirector] replaceScene:gameOverScene];	
			}
		}	
	}
}

-(void)spriteMoveFinished:(id)sender {
	CCSprite *sprite = (CCSprite *)sender;
	//check to see if sprite that is to be destroyed is a mushroom, if its not then add a point
	if(sprite.tag == 1) {
		_goombasAvoided++;
		[_pauseLabel setString:[NSString stringWithFormat: @"Score: %i",_goombasAvoided]];
	}
	[_enemies removeObject:sprite];
	[self removeChild:sprite cleanup:YES];
}

-(void)addTarget {
	
	int which = ((arc4random() % 20)+1);
	//spawn mushroom
	if ((which == 2) || (which == 3) || (which == 4)) {
		self.goomba = [CCSprite spriteWithFile:@"m2.gif"];
		_goomba.tag = 2;
	}
	//spawn mushroom
	if (which == 1) {
		self.goomba = [CCSprite spriteWithFile:@"m1.gif"];
		_goomba.tag = 3;
	}
	if(which >=5){
		
		//LOAD SPRITE SHEET
		[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile: @"combo.plist"];
		CCSpriteSheet *spriteSheet = [CCSpriteSheet spriteSheetWithFile:@"combo.png"];
		[self addChild:spriteSheet];
		
		int r = ((arc4random() % 7)+1);
		
		int first;
		int second;
		
		switch (r) {
			//extra cases because i wanted it to be more goombas than anything else
			case 1:
			case 2:
			case 3:
			case 4:
				first=4;
				second=5;
				self.goomba = [CCSprite spriteWithSpriteFrameName: @"4.png"]; 
				break;
			case 5:
				first=6;
				second=7;
				self.goomba = [CCSprite spriteWithSpriteFrameName: @"6.png"]; 
				break;
			case 6:
				first=8;
				second=9;
				self.goomba = [CCSprite spriteWithSpriteFrameName: @"8.png"]; 
				break;
			case 7:
				first=10;
				second=11;
				self.goomba = [CCSprite spriteWithSpriteFrameName: @"10.png"]; 
				break;
		}
		
		//ENEMY WALK FRAMES
		NSMutableArray *goombawalkAnimFrames = [NSMutableArray array];
		for(int i = first; i <= second; ++i) {
			[goombawalkAnimFrames addObject: [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"%d.png", i]]];
		}
		//enemy animation sequence
		CCAnimation *goombaWalkAnim = [CCAnimation animationWithName:@"walk" delay:0.1f frames:goombawalkAnimFrames];
		_goomba.tag = 1;
		
		self.goombaWalkAction = [CCRepeatForever actionWithAction: [CCAnimate actionWithAnimation:goombaWalkAnim restoreOriginalFrame:NO]];
		[_goomba runAction:_goombaWalkAction];
	}
	
	CGSize goombaWinSize = [CCDirector sharedDirector].winSize;
	_goomba.position = ccp(goombaWinSize.width/2, goombaWinSize.height/2);
	int minY = _goomba.contentSize.height/2;
	int maxY = goombaWinSize.height - _goomba.contentSize.height/2;
	int rangeY = maxY - minY;
	int actualY = (arc4random() % rangeY) + minY;
	
	[_enemies addObject:_goomba];
	
	_goomba.position = ccp(goombaWinSize.width + (_goomba.contentSize.width/2), actualY);
	[self addChild:_goomba];
	
	// Determine speed of the goomba
	int minDuration = 2.0;
	int maxDuration = 4.0;
	int rangeDuration = maxDuration - minDuration;
	int actualDuration = (arc4random() % rangeDuration) + minDuration;
	
	// Create the actions
	id actionMove = [CCMoveTo actionWithDuration:actualDuration position:ccp(-_goomba.contentSize.width/2, actualY)];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(spriteMoveFinished:)];
	[_goomba runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}

-(void) registerWithTouchDispatcher {
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
	return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    //make mario move to position on screen which was touched
	//also adjust the direction he's facing to correspond with the direction hes moving
	CGPoint touchLocation = [touch locationInView: [touch view]];		
	touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
	touchLocation = [self convertToNodeSpace:touchLocation];
	float velocity = 480.0/3.0;
	CGPoint moveDifference = ccpSub(touchLocation, _mario.position);
	float distanceToMove = ccpLength(moveDifference);
	float moveDuration = distanceToMove / velocity;
	if (moveDifference.x > 0) {
		_mario.flipX = NO;
	} else {
		_mario.flipX = YES;
	}
	[_mario stopAction:_marioMoveAction];
	
	if (!_moving) {
		[_mario runAction:_marioWalkAction];
	}
	
	self.marioMoveAction = [CCSequence actions: [CCMoveTo actionWithDuration:moveDuration position:touchLocation], [CCCallFunc actionWithTarget:self selector:@selector(marioMoveEnded)], nil];
	
	[_mario runAction:_marioMoveAction];   
	_moving = TRUE;
}

-(void)marioMoveEnded {
	//make sure he doesnt run in place if he's not moving
	[_mario stopAction:_marioWalkAction];
	_moving = FALSE;
}

- (void) dealloc {
	[_pauseItem release];
	_pauseItem = nil;
	[_playItem release];
	_playItem = nil;
	[_pauseLabel release];
	_pauseLabel = nil;
	self.mario = nil;
	self.goomba = nil;
	self.goombaWalkAction = nil;
	self.marioWalkAction = nil;
	[_enemies release];
	_enemies = nil;
	[super dealloc];
}
@end
