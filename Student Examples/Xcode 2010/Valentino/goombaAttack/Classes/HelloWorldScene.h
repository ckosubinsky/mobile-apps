//
//  HelloWorldLayer.h
//  goombaAttack
//
//  Created by Tristan Valentino on 10/6/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "SimpleAudioEngine.h"
#import "GameOverScene.h"

// HelloWorld Layer
@interface HelloWorld : CCColorLayer
{
	CCSprite *_goomba;
	CCSprite *_mario;
	CCAction *_marioWalkAction;
	CCAction *_marioMoveAction;
	CCAction *_goombaWalkAction;
	CCAction *_goombaMoveAction;
	CCLabel *_pauseLabel;
	NSMutableArray *_enemies;
	NSMutableArray *_targetsToDelete;
	BOOL _moving;
	int _goombasAvoided;
	double rate;
	CCMenuItem *_playItem; 
	CCMenuItem *_pauseItem;
}
@property (nonatomic, retain) NSMutableArray *enemies;
@property (nonatomic, retain) NSMutableArray *targetsToDelete;
@property (nonatomic, retain) CCSprite *mario;
@property (nonatomic, retain) CCSprite *goomba;
@property (nonatomic, retain) CCAction *marioWalkAction;
@property (nonatomic, retain) CCAction *marioMoveAction;
@property (nonatomic, retain) CCAction *goombaWalkAction;
@property (nonatomic, retain) CCAction *goombaMoveAction;

// returns a Scene that contains the HelloWorld as the only child
+(id) scene;

@end
