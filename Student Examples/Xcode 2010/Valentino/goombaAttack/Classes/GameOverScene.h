#import "cocos2d.h"

@interface GameOverLayer : CCColorLayer {
	CCLabel *_label;
	CCLabel *_gameOverLabel;
	CCLabel *_highScoreLabel;
}
@property (nonatomic, retain) CCLabel *label;
@property (nonatomic, retain) CCLabel *gameOverLabel;
@property (nonatomic, retain) CCLabel *highScoreLabel;
@end

@interface GameOverScene : CCScene {
	GameOverLayer *_layer;
}
@property (nonatomic, retain) GameOverLayer *layer;
@end