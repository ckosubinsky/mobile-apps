//
//  goombaAttackAppDelegate.h
//  goombaAttack
//
//  Created by Tristan Valentino on 10/6/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface goombaAttackAppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow *window;
}

@property (nonatomic, retain) UIWindow *window;

@end
