#import "GameOverScene.h"
#import "HelloWorldScene.h"

@implementation GameOverScene
@synthesize layer = _layer;

- (id)init {
	
	if ((self = [super init])) {
		self.layer = [GameOverLayer node];
		[self addChild:_layer];
	}
	return self;
}

- (void)dealloc {
	[_layer release];
	_layer = nil;
	[super dealloc];
}

@end

@implementation GameOverLayer
@synthesize label = _label;
@synthesize gameOverLabel = _gameOverLabel;
@synthesize highScoreLabel = _highScoreLabel;


-(id) init
{
	if( (self=[super initWithColor:ccc4(255,255,255,255)] )) {
		//add the background
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		CCSprite * bg = [CCSprite spriteWithFile:@"greenbg.png"];
		bg.scale = 1.6;
		bg.anchorPoint = CGPointMake(0,0);
		[self addChild:bg];
		//label to display current score
		self.label = [CCLabel labelWithString:@"" fontName:@"Arial" fontSize:20];
		_label.color = ccc3(0,0,0);
		_label.position = ccp(winSize.width/2, ((winSize.height/2)-25));
		[self addChild:_label];
		//main label to display game over
		self.gameOverLabel = [CCLabel labelWithString:@"Game Over" fontName:@"Arial" fontSize:32];
		_gameOverLabel.color = ccc3(0,0,0);
		_gameOverLabel.position = ccp(winSize.width/2, winSize.height/2);
		[self addChild:_gameOverLabel];
		//high score label
		self.highScoreLabel = [CCLabel labelWithString:@"" fontName:@"Arial" fontSize:15];
		_highScoreLabel.color = ccc3(0,0,0);
		_highScoreLabel.position = ccp(winSize.width/2, 300);
		[self addChild:_highScoreLabel];
		
		//start timer to switch back to game
		[self runAction:[CCSequence actions:
						 [CCDelayTime actionWithDuration:5],
						 [CCCallFunc actionWithTarget:self selector:@selector(gameOverDone)],
						 nil]];
		
	}	
	return self;
}

- (void)gameOverDone {
	//switch back to game
	[[CCDirector sharedDirector] replaceScene:[HelloWorld scene]];
	
}

- (void)dealloc {
	[_label release];
	_label = nil;
	[_gameOverLabel release];
	_gameOverLabel = nil;
	[_highScoreLabel release];
	_highScoreLabel = nil;
	[super dealloc];
}

@end