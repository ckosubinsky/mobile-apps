//
//  EightGameViewController.m
//  EightGame
//
//  Created by AntonKanevsky on 11/17/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "EightGameViewController.h"

@implementation EightGameViewController

// synthesize property methods
@synthesize moves;
@synthesize boardSetup;
@synthesize rightBoardSetup;
@synthesize imageSet;
@synthesize imageSet1;
@synthesize imageSet2;
@synthesize imageSet3;
@synthesize imageSet4;
@synthesize imageSet5;
@synthesize imageSet6;
@synthesize timeRemaining;
@synthesize gameTimer;
@synthesize isFrozen;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
		
	// set up default value of imageSet
	imageSet = -1;
	
	// set up image sets
	imageSet1 = [[NSArray alloc] initWithObjects: 
				  [NSString stringWithString:@"sc01_01.gif"], [NSString stringWithString:@"sc01_02.gif"], [NSString stringWithString:@"sc01_03.gif"], 
				  [NSString stringWithString:@"sc01_04.gif"], [NSString stringWithString:@"sc01_05.gif"], [NSString stringWithString:@"sc01_06.gif"],
				  [NSString stringWithString:@"sc01_07.gif"], [NSString stringWithString:@"sc01_08.gif"], nil];
	imageSet2 = [[NSArray alloc] initWithObjects: 
				 [NSString stringWithString:@"sc02_01.gif"], [NSString stringWithString:@"sc02_02.gif"], [NSString stringWithString:@"sc02_03.gif"], 
				 [NSString stringWithString:@"sc02_04.gif"], [NSString stringWithString:@"sc02_05.gif"], [NSString stringWithString:@"sc02_06.gif"],
				 [NSString stringWithString:@"sc02_07.gif"], [NSString stringWithString:@"sc02_08.gif"], nil];
	imageSet3 = [[NSArray alloc] initWithObjects: 
				 [NSString stringWithString:@"sc03_01.gif"], [NSString stringWithString:@"sc03_02.gif"], [NSString stringWithString:@"sc03_03.gif"], 
				 [NSString stringWithString:@"sc03_04.gif"], [NSString stringWithString:@"sc03_05.gif"], [NSString stringWithString:@"sc03_06.gif"],
				 [NSString stringWithString:@"sc03_07.gif"], [NSString stringWithString:@"sc03_08.gif"], nil];
	imageSet4 = [[NSArray alloc] initWithObjects: 
				 [NSString stringWithString:@"sc04_01.gif"], [NSString stringWithString:@"sc04_02.gif"], [NSString stringWithString:@"sc04_03.gif"], 
				 [NSString stringWithString:@"sc04_04.gif"], [NSString stringWithString:@"sc04_05.gif"], [NSString stringWithString:@"sc04_06.gif"],
				 [NSString stringWithString:@"sc04_07.gif"], [NSString stringWithString:@"sc04_08.gif"], nil];
	imageSet5 = [[NSArray alloc] initWithObjects: 
				 [NSString stringWithString:@"sc05_01.gif"], [NSString stringWithString:@"sc05_02.gif"], [NSString stringWithString:@"sc05_03.gif"], 
				 [NSString stringWithString:@"sc05_04.gif"], [NSString stringWithString:@"sc05_05.gif"], [NSString stringWithString:@"sc05_06.gif"],
				 [NSString stringWithString:@"sc05_07.gif"], [NSString stringWithString:@"sc05_08.gif"], nil];
	imageSet6 = [[NSArray alloc] initWithObjects: 
				 [NSString stringWithString:@"sc06_01.gif"], [NSString stringWithString:@"sc06_02.gif"], [NSString stringWithString:@"sc06_03.gif"], 
				 [NSString stringWithString:@"sc06_04.gif"], [NSString stringWithString:@"sc06_05.gif"], [NSString stringWithString:@"sc06_06.gif"],
				 [NSString stringWithString:@"sc06_07.gif"], [NSString stringWithString:@"sc06_08.gif"], nil];
		
	// seed generator of random numbers
	srand ([[NSDate date] timeIntervalSince1970]);
	
	// reset game
	[self resetGame];
	
	// set the right board setup
	rightBoardSetup = [[NSMutableArray alloc] initWithObjects: 
				  [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], 
				  [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], 
				  [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], [NSNumber numberWithInt:-1], nil];
	
	// junk code to test for whether winning is detected
	/* boardSetup = [[NSMutableArray alloc] initWithObjects: 
	 [NSNumber numberWithInt:1], [NSNumber numberWithInt:2], [NSNumber numberWithInt:3], 
	 [NSNumber numberWithInt:4], [NSNumber numberWithInt:5], [NSNumber numberWithInt:6], 
	 [NSNumber numberWithInt:7], [NSNumber numberWithInt:8], [NSNumber numberWithInt:-1], nil];
	 [self arrange];
	 [self checkWin]; */
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

// randomizes the button locations and image set
- (void) randomize {
	
	// randomize image set, but make sure it's not the same as the last two times
	int ran = imageSet;
	while (ran == imageSet)
	{
		ran = rand() % 6;
	}
	imageSet = ran;
	
	NSArray *arrayPtr;
	switch (imageSet) {
		case 0:
			arrayPtr = imageSet1;
			break;
		case 1:
			arrayPtr = imageSet2;
			break;
		case 2:
			arrayPtr = imageSet3;
			break;
		case 3:
			arrayPtr = imageSet4;
			break;
		case 4:
			arrayPtr = imageSet5;
			break;
		case 5:
			arrayPtr = imageSet6;
			break;
		default:
			break;
	}
	
	[buttonOne setImage:[UIImage imageNamed:[arrayPtr objectAtIndex:0]] forState:UIControlStateNormal];
	[buttonTwo setImage:[UIImage imageNamed:[arrayPtr objectAtIndex:1]] forState:UIControlStateNormal];
	[buttonThree setImage:[UIImage imageNamed:[arrayPtr objectAtIndex:2]] forState:UIControlStateNormal];
	[buttonFour setImage:[UIImage imageNamed:[arrayPtr objectAtIndex:3]] forState:UIControlStateNormal];
	[buttonFive setImage:[UIImage imageNamed:[arrayPtr objectAtIndex:4]] forState:UIControlStateNormal];
	[buttonSix setImage:[UIImage imageNamed:[arrayPtr objectAtIndex:5]] forState:UIControlStateNormal];
	[buttonSeven setImage:[UIImage imageNamed:[arrayPtr objectAtIndex:6]] forState:UIControlStateNormal];
	[buttonEight setImage:[UIImage imageNamed:[arrayPtr objectAtIndex:7]] forState:UIControlStateNormal];
	
	// randomize button locations
	boardSetup = [[NSMutableArray alloc] initWithObjects: 
				  [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], 
				  [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], 
				  [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], [NSNumber numberWithInt:-1], nil];
	for (int i = 1; i <= 8; i++)
	{
		int ran = rand() % 9;
		if ([[boardSetup objectAtIndex:ran] intValue] == -1)
		{
			[boardSetup replaceObjectAtIndex:ran withObject:[NSNumber numberWithInt:i]];
		}
		else
		{
			i--;
		}
	}
	NSLog(@"The content of boardSetup is%@", boardSetup);
}

// arranges the buttons on the screen according to board setup
- (void) arrange {
	
	NSArray *xValues = [[NSArray alloc] initWithObjects:
						[NSNumber numberWithInt:49], [NSNumber numberWithInt:127], [NSNumber numberWithInt:205],
						[NSNumber numberWithInt:49], [NSNumber numberWithInt:127], [NSNumber numberWithInt:205],
						[NSNumber numberWithInt:49], [NSNumber numberWithInt:127], [NSNumber numberWithInt:205], nil];
	NSArray *yValues = [[NSArray alloc] initWithObjects:
						[NSNumber numberWithInt:51], [NSNumber numberWithInt:51], [NSNumber numberWithInt:51],
						[NSNumber numberWithInt:129], [NSNumber numberWithInt:129], [NSNumber numberWithInt:129],
						[NSNumber numberWithInt:207], [NSNumber numberWithInt:207], [NSNumber numberWithInt:207], nil];	
	
	// place each piece at the right location on the screen
	for (int i = 0; i < 9; i++)
	{
		// select piece at location
		int piece;
		if (!isFrozen)
		{
			piece = [[boardSetup objectAtIndex:i] intValue];
		}
		else
		{
			piece = [[rightBoardSetup objectAtIndex:i] intValue];
		}

		// put it at the appropriate spot
		if (piece == 1)
		{
			buttonOne.frame = CGRectMake([[xValues objectAtIndex:i] intValue], [[yValues objectAtIndex:i] intValue], 70, 70);
		}
		else if (piece == 2)
		{
			buttonTwo.frame = CGRectMake([[xValues objectAtIndex:i] intValue], [[yValues objectAtIndex:i] intValue], 70, 70);
		}
		else if (piece == 3)
		{
			buttonThree.frame = CGRectMake([[xValues objectAtIndex:i] intValue], [[yValues objectAtIndex:i] intValue], 70, 70);
		}
		else if (piece == 4)
		{
			buttonFour.frame = CGRectMake([[xValues objectAtIndex:i] intValue], [[yValues objectAtIndex:i] intValue], 70, 70);
		}
		else if (piece == 5)
		{
			buttonFive.frame = CGRectMake([[xValues objectAtIndex:i] intValue], [[yValues objectAtIndex:i] intValue], 70, 70);
		}
		else if (piece == 6)
		{
			buttonSix.frame = CGRectMake([[xValues objectAtIndex:i] intValue], [[yValues objectAtIndex:i] intValue], 70, 70);
		}
		else if (piece == 7)
		{
			buttonSeven.frame = CGRectMake([[xValues objectAtIndex:i] intValue], [[yValues objectAtIndex:i] intValue], 70, 70);
		}
		else if (piece == 8)
		{
			buttonEight.frame = CGRectMake([[xValues objectAtIndex:i] intValue], [[yValues objectAtIndex:i] intValue], 70, 70);
		}
	}
}

// resets the board
- (IBAction) resetGame
{
	// set isFrozen to false
	isFrozen = false;
	
	// randomize the game and then arrange the pieces
	do
	{
		[self randomize];
		[self arrange];
	}
	while ([self checkWin] == true);
	
	// initialize the moves variable and the status label
	moves = 0;
	[statusLabel setText:[NSString stringWithFormat:@"Moves: %d", moves]];
	
	// schedule the timer
	timeRemaining = 120;
	if (gameTimer != NULL)
	{
		[gameTimer invalidate];
	}
	gameTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countTime) userInfo:nil repeats:YES] retain];
	[self countTime];
}

// stub methods for the buttons
- (IBAction) pressOne { [self pressButton:1]; }
- (IBAction) pressTwo { [self pressButton:2]; }
- (IBAction) pressThree { [self pressButton:3]; }
- (IBAction) pressFour { [self pressButton:4]; }
- (IBAction) pressFive { [self pressButton:5]; }
- (IBAction) pressSix { [self pressButton:6]; }
- (IBAction) pressSeven { [self pressButton:7]; }
- (IBAction) pressEight { [self pressButton:8]; }

// handles a button click
- (void) pressButton:(int)buttonNumber
{
	// only allow moves if the game is in play mode, there is time remaining, the timer is ticking, and the user hasn't won
	if (isFrozen || timeRemaining == 0 || ![gameTimer isValid] || [self checkWin])
	{
		return;
	}
	
	// determine the current location of the piece
	int buttonLocation = 0;
	for (int i = 0; i < 9; i++)
	{
		if ([[boardSetup objectAtIndex:i] intValue] == buttonNumber)
		{
			buttonLocation = i;
		}		
	}
	
	// determine where to move the piece next
	int moveTo = -1;
	if (buttonLocation == 0)
	{
		if ([[boardSetup objectAtIndex:1] intValue] == -1)
		{
			moveTo = 1;
		}
		else if ([[boardSetup objectAtIndex:3] intValue] == -1)
		{
			moveTo = 3;
		}
	}
	else if (buttonLocation == 1)
	{
		if ([[boardSetup objectAtIndex:0] intValue] == -1)
		{
			moveTo = 0;
		}
		else if ([[boardSetup objectAtIndex:2] intValue] == -1)
		{
			moveTo = 2;
		}
		else if ([[boardSetup objectAtIndex:4] intValue] == -1)
		{
			moveTo = 4;
		}
	}
	else if (buttonLocation == 2)
	{
		if ([[boardSetup objectAtIndex:1] intValue] == -1)
		{
			moveTo = 1;
		}
		else if ([[boardSetup objectAtIndex:5] intValue] == -1)
		{
			moveTo = 5;
		}
	}
	else if (buttonLocation == 3)
	{
		if ([[boardSetup objectAtIndex:0] intValue] == -1)
		{
			moveTo = 0;
		}
		else if ([[boardSetup objectAtIndex:4] intValue] == -1)
		{
			moveTo = 4;
		}
		else if ([[boardSetup objectAtIndex:6] intValue] == -1)
		{
			moveTo = 6;
		}
	}
	else if (buttonLocation == 4)
	{
		if ([[boardSetup objectAtIndex:1] intValue] == -1)
		{
			moveTo = 1;
		}
		else if ([[boardSetup objectAtIndex:3] intValue] == -1)
		{
			moveTo = 3;
		}
		else if ([[boardSetup objectAtIndex:5] intValue] == -1)
		{
			moveTo = 5;
		}
		else if ([[boardSetup objectAtIndex:7] intValue] == -1)
		{
			moveTo = 7;
		}
	}
	else if (buttonLocation == 5)
	{
		if ([[boardSetup objectAtIndex:2] intValue] == -1)
		{
			moveTo = 2;
		}
		else if ([[boardSetup objectAtIndex:4] intValue] == -1)
		{
			moveTo = 4;
		}
		else if ([[boardSetup objectAtIndex:8] intValue] == -1)
		{
			moveTo = 8;
		}
	}
	else if (buttonLocation == 6)
	{
		if ([[boardSetup objectAtIndex:3] intValue] == -1)
		{
			moveTo = 3;
		}
		else if ([[boardSetup objectAtIndex:7] intValue] == -1)
		{
			moveTo = 7;
		}
	}
	else if (buttonLocation == 7)
	{
		{
			if ([[boardSetup objectAtIndex:6] intValue] == -1)
			{
				moveTo = 6;
			}
			else if ([[boardSetup objectAtIndex:4] intValue] == -1)
			{
				moveTo = 4;
			}
			else if ([[boardSetup objectAtIndex:8] intValue] == -1)
			{
				moveTo = 8;
			}
		}
	}
	else if (buttonLocation == 8)
	{
		{
			if ([[boardSetup objectAtIndex:5] intValue] == -1)
			{
				moveTo = 5;
			}
			else if ([[boardSetup objectAtIndex:7] intValue] == -1)
			{
				moveTo = 7;
			}
		}
	}
	
	// move the piece
	if (moveTo != -1)
	{
		[boardSetup replaceObjectAtIndex:buttonLocation withObject:[NSNumber numberWithInt:-1]];
		[boardSetup replaceObjectAtIndex:moveTo withObject:[NSNumber numberWithInt:buttonNumber]];
		
		// increment moves variable and update the status label
		moves++;
		[statusLabel setText:[NSString stringWithFormat:@"Moves: %d", moves]];
	}
		 
	// arrange the pieces
	[self arrange];
	
	// check winning situtation
	[self checkWin];
}

// checks for a winning situation
- (bool) checkWin
{
	// check winning situation
	bool fail = false;
	for (int i = 0; i < 8; i++)
	{
		if ([[boardSetup objectAtIndex:i] intValue] != i + 1)
		{
			fail = true;
		}		
	}
	
	// set status label
	if (!fail)
	{
		[gameTimer invalidate];
		[statusLabel setText:[NSString stringWithFormat:@"You won in %d moves!", moves]];
	}
	
	// return the winning status
	return !fail;
}

// counts the time and sets the label
- (void) countTime
{
	if (!isFrozen)
	{
		if (timeRemaining > 0)
		{
			timeRemaining--;
			[timeLabel setText:[NSString stringWithFormat:@"Time: %d", timeRemaining]];
		}
		else
		{
			[gameTimer invalidate];
			[timeLabel setText:[NSString stringWithString:@"Time Is Up!"]];
		}	
	}
}

// switches preview mode, but only if the game is still running
- (IBAction) switchPreviewMode
{
	if ([gameTimer isValid])
	{
		isFrozen = !isFrozen;
		[self arrange];
		if (isFrozen)
		{
			[previewButton setTitle:@"Play" forState:UIControlStateNormal];
		}
		else
		{
			[previewButton setTitle:@"Preview" forState:UIControlStateNormal];
		}
	}
}

@end
