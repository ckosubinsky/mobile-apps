//
//  EightGameViewController.h
//  EightGame
//
//  Created by AntonKanevsky on 11/17/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EightGameViewController : UIViewController {

	IBOutlet UIButton *buttonOne;
	IBOutlet UIButton *buttonTwo;
	IBOutlet UIButton *buttonThree;
	IBOutlet UIButton *buttonFour;
	IBOutlet UIButton *buttonFive;
	IBOutlet UIButton *buttonSix;
	IBOutlet UIButton *buttonSeven;
	IBOutlet UIButton *buttonEight;
	IBOutlet UILabel *timeLabel;
	IBOutlet UILabel *statusLabel;
	IBOutlet UIButton *previewButton;
	int moves;
	NSMutableArray *boardSetup;
	NSMutableArray *rightBoardSetup;
	int imageSet;
	NSArray *imageSet1;
	NSArray *imageSet2;
	NSArray *imageSet3;
	NSArray *imageSet4;
	NSArray *imageSet5;
	NSArray *imageSet6;
	int timeRemaining;
	NSTimer *gameTimer;
	bool isFrozen;
}

// property declarations
@property int moves;
@property(nonatomic, retain) NSMutableArray *boardSetup;
@property(nonatomic, retain) NSMutableArray *rightBoardSetup;
@property int imageSet;
@property(nonatomic, retain) NSArray *imageSet1;
@property(nonatomic, retain) NSArray *imageSet2;
@property(nonatomic, retain) NSArray *imageSet3;
@property(nonatomic, retain) NSArray *imageSet4;
@property(nonatomic, retain) NSArray *imageSet5;
@property(nonatomic, retain) NSArray *imageSet6;
@property int timeRemaining;
@property (nonatomic, retain) NSTimer *gameTimer;
@property bool isFrozen;

// method declarations
- (void) randomize;
- (void) arrange;
- (IBAction) resetGame;
- (IBAction) pressOne;
- (IBAction) pressTwo;
- (IBAction) pressThree;
- (IBAction) pressFour;
- (IBAction) pressFive;
- (IBAction) pressSix;
- (IBAction) pressSeven;
- (IBAction) pressEight;
- (void) pressButton:(int)buttonNumber;
- (bool) checkWin;
- (void) countTime;
- (IBAction) switchPreviewMode;

@end

