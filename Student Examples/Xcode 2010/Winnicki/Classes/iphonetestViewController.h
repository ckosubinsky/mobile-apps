//
//  iphonetestViewController.h
//  iphonetest
//
//  Created by ColinWinnicki on 11/14/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface iphonetestViewController : UIViewController {
	IBOutlet UILabel *name;
	IBOutlet UILabel *gametime;
	IBOutlet UILabel *history;
	IBOutlet UIButton *submit;
	// button definitions
	IBOutlet UIButton *btn0;
	IBOutlet UIButton *btn1;
	IBOutlet UIButton *btn2;
	IBOutlet UIButton *btn3;
	IBOutlet UIButton *btn4;
	IBOutlet UIButton *btn5;
	IBOutlet UIButton *btn6;
	IBOutlet UIButton *btn7;
	IBOutlet UIButton *btn8;
	IBOutlet UIButton *btn9;
	IBOutlet UIButton *btn10;
	IBOutlet UIButton *btn11;
	IBOutlet UIButton *btn12;
	IBOutlet UIButton *btn13;
	IBOutlet UIButton *btn14;
	IBOutlet UIButton *btn15;
	IBOutlet UIButton *btn16;
	IBOutlet UIButton *btn17;
	IBOutlet UIButton *btn18;
	IBOutlet UIButton *btn19;
	IBOutlet UIButton *btn20;
	IBOutlet UIButton *btn21;
	IBOutlet UIButton *btn22;
	IBOutlet UIButton *btn23;
	IBOutlet UIButton *btn24;
	IBOutlet UIButton *newgame;
	
	//Grid Image Definitions
	IBOutlet UIImageView *G0;
	IBOutlet UIImageView *G1;
	IBOutlet UIImageView *G2;
	IBOutlet UIImageView *G3;
	IBOutlet UIImageView *G4;
	IBOutlet UIImageView *G5;
	IBOutlet UIImageView *G6;
	IBOutlet UIImageView *G7;
	IBOutlet UIImageView *G8;
	IBOutlet UIImageView *G9;
	IBOutlet UIImageView *G10;
	IBOutlet UIImageView *G11;
	IBOutlet UIImageView *G12;
	IBOutlet UIImageView *G13;
	IBOutlet UIImageView *G14;
	IBOutlet UIImageView *G15;
	IBOutlet UIImageView *G16;
	IBOutlet UIImageView *G17;
	IBOutlet UIImageView *G18;
	IBOutlet UIImageView *G19;
	IBOutlet UIImageView *G20;
	IBOutlet UIImageView *G21;
	IBOutlet UIImageView *G22;
	IBOutlet UIImageView *G23;
	IBOutlet UIImageView *G24;
	
	NSTimer *checkanswer;
	NSTimer *checktime;
	NSMutableArray *playeranswer;
	NSMutableArray *correctanswer;
	NSMutableArray *holder;
	NSString *finalcorrect;
	NSString *finalplayer;
	NSDate *timebegin;
	float ttltime;
	int mycounter;

}

//Button Methods
-(IBAction) btn0;
-(IBAction) btn1;
-(IBAction) btn2;
-(IBAction) btn3;
-(IBAction) btn4;
-(IBAction) btn5;
-(IBAction) btn6;
-(IBAction) btn7;
-(IBAction) btn8;
-(IBAction) btn9;
-(IBAction) btn10;
-(IBAction) btn11;
-(IBAction) btn12;
-(IBAction) btn13;
-(IBAction) btn14;
-(IBAction) btn15;
-(IBAction) btn16;
-(IBAction) btn17;
-(IBAction) btn18;
-(IBAction) btn19;
-(IBAction) btn20;
-(IBAction) btn21;
-(IBAction) btn22;
-(IBAction) btn23;
-(IBAction) btn24;

-(IBAction) submit;
-(IBAction) newgame;
-(void) creategrid;
-(void) initializebuttons;
-(void) submitanswer;
-(void) gettime;





@property (nonatomic, retain) UILabel *name;
@property (nonatomic, retain) UILabel *gametime;
@property (nonatomic, retain) UILabel *history;
@property (nonatomic, retain)NSMutableArray *playeranswer;
@property (nonatomic, retain)NSMutableArray *correctanswer;
@property (nonatomic, retain)NSTimer *checkanswer;
@property (nonatomic, retain)NSTimer *checktime;
//@property (nonatomic, retain)NSDate *timebegin;
@end

