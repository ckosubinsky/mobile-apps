//
//  HighScoresScene.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 7..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "HighScores.h"

@interface HighScoresLayer : CCLayer {
	CCSprite *_main_bkgrnd;
    CCSpriteSheet *_spriteSheet;
	HighScores *_highscore;
	NSMutableArray *_scoreLabels;
}

@property (nonatomic, assign) CCSprite *main_bkgrnd;
@property (nonatomic, assign) CCSpriteSheet *spriteSheet;
@property (nonatomic, retain) HighScores *highscore;
@property (nonatomic, assign) NSMutableArray *scoreLabels;

@end


@interface HighScoresScene : CCScene {
	HighScoresLayer *_layer;
}

@property (nonatomic, retain) HighScoresLayer *layer;
@end
