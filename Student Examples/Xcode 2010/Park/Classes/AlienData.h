//
//  AlienData.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 8..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AlienData : NSObject {
	int _alienType;
	float _spawnTime;
}

@property (nonatomic, assign) int alienType;
@property (nonatomic, assign) float spawnTime;

@end
