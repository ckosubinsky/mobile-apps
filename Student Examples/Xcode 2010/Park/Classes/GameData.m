//
//  GameData.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "GameData.h"
#import "Ladder.h"
#import "LadderSibling.h"
#import "Flier.h"
#import "Defenser.h"
#import "AlienData.h"
#import "SBJsonParser.h"

@implementation GameData
static GameData *sharedData = nil;
@synthesize currentLevel = _currentLevel;
@synthesize ladders = _ladders;
@synthesize ladderSiblings = _ladderSiblings;
@synthesize aliens = _aliens;
@synthesize fliers = _fliers;
@synthesize defensers = _defensers;
@synthesize numberOfLegs = _numberOfLegs;
@synthesize numberOfLadders = _numberOfLadders;
@synthesize maxHP = _maxHP;
@synthesize currentHP = _currentHP;
@synthesize damaging = _damaging;
@synthesize activeLegs = _activeLegs;
@synthesize inactiveLegs = _inactiveLegs;
@synthesize highscore = _highscore;
@synthesize gameState = _gameState;
@synthesize aliensInLevel = _aliensInLevel;
@synthesize currentTimeSpend = _currentTimeSpend;
@synthesize endingTime = _endingTime;

+(GameData*) sharedData {
	@synchronized(self)     {
		if (!sharedData)
			sharedData = [[GameData alloc] init];
		return sharedData;
	}
	return nil;	
}

- (id)init {
	if ((self = [super init])) {
        
		parser = [SBJsonParser new];
		
		self.ladders = [[NSMutableArray alloc] initWithCapacity:maxNumberOfLadders];
		self.ladderSiblings = [[NSMutableArray alloc] initWithCapacity:maxNumberOfLegs];
		self.fliers = [[NSMutableArray alloc] initWithCapacity:maxNumberOfLadders];
		self.defensers = [[NSMutableArray alloc] initWithCapacity:maxNumberOfLadders];
		self.inactiveLegs = [[NSMutableArray alloc] initWithCapacity:maxNumberOfLegs];
		self.activeLegs = [[NSMutableArray alloc] initWithCapacity:maxNumberOfLegs];
		self.aliensInLevel = [[NSMutableArray alloc] init];
		self.highscore = [[HighScores alloc] init];
		
		[_highscore retrieveScores];
		
		self.currentLevel = 0;
		
		CGSize winSize = [CCDirector sharedDirector].winSize;
		static int WINDOW_MARGIN = 50; 
		int marginX = (winSize.width - WINDOW_MARGIN) / maxNumberOfLadders;
		
		
		//add ufos
		for (int i = 0; i < maxNumberOfLadders; i++) {
			[_fliers addObject:[Flier getFlier]];
		}
		//add defenders
		for (int i = 0; i < maxNumberOfLadders; i++) {
			[_defensers addObject:[Defenser getDefenser]];
		}
		//add ladders
		for (int i = 0; i < maxNumberOfLadders; i++) {
			
			[_ladders addObject:[Ladder ladderWithPos:i * marginX + ((marginX + WINDOW_MARGIN)/ 2)
											  length:256]];
			Defenser *defenser = [_defensers objectAtIndex:i];
			Ladder *ladder = [_ladders objectAtIndex:i];
			ladder.defenser = defenser;
			ladder.flier = [_fliers objectAtIndex:i];
			
		}
		//add ladder legs
		for (int i = 0; i < maxNumberOfLegs; i++) {
			[_ladderSiblings addObject:[LadderSibling ladderSiblingWithPos:0]];
		}
		
	}
	
	return self;
}

- (void)loadLevel {
	//clear all initial data
	self.damaging = false;
	self.gameState = OPENING_STATE;
	[_activeLegs removeAllObjects];
	[_inactiveLegs removeAllObjects];
	for(LadderSibling *leg in _ladderSiblings) {
		[self cutConnection:leg];
	}
	_currentTimeSpend = 0.0f;
	
	//Parse the level data from the text file
	NSBundle *bundle1 = [NSBundle mainBundle]; 
    NSString *levelPath = [bundle1 pathForResource:[NSString stringWithFormat:@"level%d" , _currentLevel + 1] ofType:@"json"]; 
	
	NSString *levelText = [NSString stringWithContentsOfFile:levelPath
												   encoding:NSUTF8StringEncoding
													  error:nil];
	NSDictionary *properties = [parser objectWithString:levelText];
	self.numberOfLadders = [[properties objectForKey:@"numberOfLadders"] intValue];
	NSArray *defenders = [properties objectForKey:@"defender"];
	int defenderIndex = 0;
	for(NSNumber *available in defenders) {
		Defenser *defenser = [_defensers objectAtIndex:defenderIndex++];
		defenser.enabled = [available boolValue];
	}
	
	NSArray *spawnAliens = [properties objectForKey:@"aliens"];
	for(NSDictionary *prop in spawnAliens) {
		AlienData *aData = [[[AlienData alloc] init] autorelease];	
		aData.alienType = [[prop objectForKey:@"type"] intValue];
		aData.spawnTime = [[prop objectForKey:@"spawn"] floatValue];
		_endingTime = [[prop objectForKey:@"spawn"] floatValue];
		[_aliensInLevel addObject:aData];
	}

	self.numberOfLegs = 10;
	self.maxHP = 100;
	self.currentHP = self.maxHP;
	
	//positioning ladders
	CGSize winSize = [CCDirector sharedDirector].winSize;
	static int WINDOW_LEFT_MARGIN = 70;
	static int WINDOW_RIGHT_MARGIN = 95;
	
	int marginX = (winSize.width - WINDOW_LEFT_MARGIN - WINDOW_RIGHT_MARGIN) / (_numberOfLadders - 1);
	for (int i = 0; i < maxNumberOfLadders; i++) {
		Ladder *ladder = [_ladders objectAtIndex:i];
		
		int posX = (i >= _numberOfLadders) ? -(ladder.position.x): (i * marginX + WINDOW_LEFT_MARGIN + ((ladder.contentSize.width)/ 2));
		ladder.position = ccp(posX, ladder.position.y);
		ladder.posX = posX;
		ladder.active = (i >= _numberOfLadders) ? NO : YES;
	}
	
	for (int i = 0; i < _numberOfLegs + 5;i++) {
		[_inactiveLegs addObject:[_ladderSiblings objectAtIndex:i]];
	}
}

- (LadderSibling*)selectedLeg:(CGPoint)point {
	
	for(LadderSibling *ls in _activeLegs) {
		static int LEG_MARGIN = 8;
		if (CGRectContainsPoint(CGRectMake(ls.position.x - ( ls.contentSize.width * ls.scaleX / 2),
										   ls.position.y - ( ls.contentSize.height / 2) - LEG_MARGIN,
										   ls.contentSize.width * ls.scaleX,
										   ls.contentSize.height + (LEG_MARGIN * 2)), point)) {
			return ls;
		}
	}
	return nil;
}

- (NSMutableArray*)getAliens:(float)dt {
	NSMutableArray *alienSet = [[[NSMutableArray alloc] init] autorelease];
	AlienData *alienData = ([_aliensInLevel count] == 0) ? nil : [_aliensInLevel objectAtIndex:0];
	_currentTimeSpend += dt;
	while (alienData != nil) {
		if(alienData.spawnTime < _currentTimeSpend) {
			[alienSet addObject:alienData];
			[_aliensInLevel removeObjectAtIndex:0];
			alienData = ([_aliensInLevel count] == 0) ? nil : [_aliensInLevel objectAtIndex:0];
		} else {
			break;
		}
	} 
	
	return alienSet;
}

-(void)cutConnection:(LadderSibling *)leg {
	[leg.leftLadder.siblings removeObject:leg];
	[leg.rightLadder.siblings removeObject:leg];
	leg.connected = false;
	[leg setOpacity:0];
	[_activeLegs removeObject:leg];
	[_inactiveLegs addObject:leg];
}

- (bool)isLegAvailable {
	return _numberOfLegs > [_activeLegs count];
}

- (void)reset {
    self.currentLevel = 0;
}

- (void)nextLevel {
	self.currentTimeSpend = 0;
	self.currentLevel++;    
}

- (bool)levelComplete {
	if (_currentTimeSpend < _endingTime) 
		return false;
	
	for(Ladder *ladder in _ladders) {
		if([ladder.aliens count] > 0)
			return false;
		if([ladder.aliensToRemove count] > 0)
			return false;
	}
		
	return true;
}

- (void)dealloc {
    [super dealloc];
}

@end
