//
//  HighScores.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 7..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "HighScores.h"


@implementation HighScores
@synthesize scores = _scores;

-(id)init {

	if(self = [super init]) {
		_scores = [[NSMutableArray alloc] initWithCapacity:5];
	}
	return self;
}

-(void) addScore:(Score*) score {
	[_scores addObject:score];
}

-(void)storeScores {
	//NSCoding protocol (encodeWithCoder: and initWithCoder:)
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:_scores] forKey:@"highScores"]; 
	[[NSUserDefaults standardUserDefaults] synchronize];
		
}

-(void)retrieveScores {
	NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
	NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:@"highScores"];
	if (dataRepresentingSavedArray != nil)
	{
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        if (oldSavedArray != nil)
			_scores = [[NSMutableArray alloc] initWithArray:oldSavedArray];
        else
			_scores = [[NSMutableArray alloc] init];
	}
}

@end
