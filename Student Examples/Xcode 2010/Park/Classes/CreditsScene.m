//
//  CreditsScene.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 8..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "CreditsScene.h"
#import "Doodle_RaiderAppDelegate.h"

@implementation CreditsScene
@synthesize layer = _layer;

- (id)init {
    if ((self = [super init])) {
        self.layer = [[[CreditsLayer alloc] init] autorelease];
        [self addChild:_layer];
	}
    return self;
}
@end


@implementation CreditsLayer
@synthesize main_bkgrnd = _main_bkgrnd;
@synthesize spriteSheet = _spriteSheet;

- (id) init {
	if( (self=[super init] )) {
		
		CGSize winSize = [CCDirector sharedDirector].winSize;
        
		CCColorLayer *bgLayer = [[CCColorLayer alloc] initWithColor:ccc4(255, 255, 255,255) width:winSize.width height:winSize.height];
		[self addChild:bgLayer z:-2];
		
		self.spriteSheet = [CCSpriteSheet spriteSheetWithTexture:[[CCTextureCache sharedTextureCache] addImage:@"sprite.png"]];
		[self addChild:_spriteSheet z:-1];
		
		//set background
		self.main_bkgrnd = [CCSprite spriteWithSpriteFrameName:@"credits.png"];
        _main_bkgrnd.position = ccp(winSize.width/2, (winSize.height + _main_bkgrnd.contentSize.height)/2 - 30);
        [_spriteSheet addChild:_main_bkgrnd];
		
		//set menus
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:25];
		
		CCMenuItem *mainMenu = [CCMenuItemFont itemFromString:@"<< Back"
													   target:self selector:@selector(GoToMainMenu:)];
		[mainMenu setColor:ccc3(0,0,0)];
		CCMenu *menu = [CCMenu menuWithItems: mainMenu, nil];
		
		menu.position = ccp(50, winSize.height - 20);
		[menu alignItemsVerticallyWithPadding:4];
		[self addChild:menu];
		
		CCLabel *creditLabel = [CCLabel labelWithString:@"Hojoon Park" fontName:@"Marker Felt" fontSize:40];
		creditLabel.position = ccp(creditLabel.contentSize.width / 2 + 50, 110);
		[creditLabel setColor:ccc3(0,0,0)];
		[self addChild:creditLabel];
		
		CCLabel *emailLabel = [CCLabel labelWithString:@"onlytoyou@gmail.com" fontName:@"Marker Felt" fontSize:30];
		emailLabel.position = ccp(emailLabel.contentSize.width / 2 + 50, 75);
		[emailLabel setColor:ccc3(0,0,0)];
		[self addChild:emailLabel];
		
	}
	
	return self;
}		

-(void) GoToMainMenu: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate *) [UIApplication sharedApplication].delegate;
	[delegate launchMainMenu];
}
@end

