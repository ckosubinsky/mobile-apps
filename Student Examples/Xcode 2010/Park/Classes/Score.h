//
//  Score.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 7..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Score : NSObject <NSCoding> {
	int _record;
	int _level;
	NSString *_recordDate;
}
@property (nonatomic, assign) int record;
@property (nonatomic, assign) int level;
@property (nonatomic, assign) NSString *recordDate;

- (void)encodeWithCoder:(NSCoder *)coder;
- (id)initWithCoder:(NSCoder *)coder;

@end
