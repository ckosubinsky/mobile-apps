//
//  Alien.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


typedef enum {
    AlienTypeSlowAndMove,
    AlienTypeFastAndMove
} AlienType;


enum {
	AlienAnimateDown = 0,
	AlienAnimateLeft = 1,
	AlienAnimateRight = 2,
	AlienAnimateAttack = 3,
	AlienAnimateDead = 4,
	AlienDestroy = -1
};


@interface Alien : CCSprite {
	AlienType _alienType;
	int _currentAction;
	int _HP;
	int _maxHP;
	int _AP;
	float _speedX;
	float _speedY;
	bool _crossing;
	bool _spawning;
	NSMutableArray *_actions;
}

@property (nonatomic, assign) float speedX;
@property (nonatomic, assign) float speedY;
@property (nonatomic, assign) int HP;
@property (nonatomic, assign) int maxHP;
@property (nonatomic, assign) int AP;
@property (nonatomic, assign) bool crossing;
@property (nonatomic, assign) bool spawning;
@property (nonatomic, assign) NSMutableArray *actions;
@property (nonatomic, assign) int currentAction;

+ (Alien*)alienWithType:(AlienType)alienType;

-(void) moveDown;
-(void) moveRight;
-(void) moveLeft;
-(void) moveUp;
-(CGRect) getCollisionRect;
-(void)switchAction:(int)targetAction;
@end
