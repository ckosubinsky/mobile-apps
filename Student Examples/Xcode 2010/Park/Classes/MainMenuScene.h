//
//  MainMenuScene.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class Flier;

@interface MainMenuLayer : CCLayer {
	CCSprite *_main_bkgrnd;
	Flier *_ufo;
    CCSpriteSheet *_spriteSheet;
}

@property (nonatomic, assign) CCSprite *main_bkgrnd;
@property (nonatomic, assign) Flier *ufo;
@property (nonatomic, assign) CCSpriteSheet *spriteSheet;
@end

@interface MainMenuScene : CCScene {
	MainMenuLayer *_layer;
}
@property (nonatomic, retain) MainMenuLayer *layer;

@end
