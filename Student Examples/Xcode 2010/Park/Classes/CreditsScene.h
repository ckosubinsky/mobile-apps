//
//  CreditsScene.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 8..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CreditsLayer : CCLayer {
	CCSprite *_main_bkgrnd;
    CCSpriteSheet *_spriteSheet;
	
}

@property (nonatomic, assign) CCSprite *main_bkgrnd;
@property (nonatomic, assign) CCSpriteSheet *spriteSheet;
@end


@interface CreditsScene : CCScene {
	CreditsLayer *_layer;
}

@property (nonatomic, retain) CreditsLayer *layer;
@end
