//
//  Flier.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 3..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "Flier.h"


@implementation Flier
@synthesize velocityX = _velocityX;
@synthesize velocityY = _velocityY;
@synthesize acceleration = _acceleration;
@synthesize targetScale = _targetScale;


- (void)onEnter {
    [super onEnter];	
    
	
	NSMutableArray *flyingAnimFrames = [NSMutableArray array]; 
	[flyingAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"ufo01.png"]];
	[flyingAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"ufo02.png"]];
										
	CCAnimation *flyingAnimation = [CCAnimation animationWithName:@"flying" delay:0.2f frames:flyingAnimFrames];
										
	[self runAction:[CCRepeatForever actionWithAction:
					 [CCSequence actions:
					  [CCAnimate actionWithAnimation:flyingAnimation restoreOriginalFrame:NO],
					  nil]]];	
}

- (void)magnify:(id)sender {
	[self setScale:_targetScale];
}

+(Flier*)getFlier {
	Flier *flier = [Flier spriteWithSpriteFrameName:@"ufo01.png"]; //[[Flier alloc] initWithSpriteFrameName:@"ufo01.png"];
	
	/*
	ccTexParams params = {GL_LINEAR,GL_LINEAR,GL_REPEAT, GL_REPEAT};
	[flier.texture setTexParameters:&params];
	*/	
	flier.velocityX = 0.0f;
	flier.velocityY = 0.0f;
	flier.acceleration = 0.0f;
	return flier;
}
@end
