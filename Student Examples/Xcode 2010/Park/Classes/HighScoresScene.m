//
//  HighScoresScene.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 7..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "HighScoresScene.h"
#import "Doodle_RaiderAppDelegate.h"
#import "Score.h"
#import "GameData.h"

@implementation HighScoresScene
@synthesize layer = _layer;

- (id)init {
    if ((self = [super init])) {
        self.layer = [[[HighScoresLayer alloc] init] autorelease];
        [self addChild:_layer];
		
    }
    return self;
}
@end


@implementation HighScoresLayer
@synthesize main_bkgrnd = _main_bkgrnd;
@synthesize spriteSheet = _spriteSheet;
@synthesize scoreLabels = _scoreLabels;
@synthesize highscore = _highscore;
- (id) init {
	if( (self=[super init] )) {
		
		self.spriteSheet = [CCSpriteSheet spriteSheetWithTexture:[[CCTextureCache sharedTextureCache] addImage:@"sprite.png"]];
		[self addChild:_spriteSheet z:-1];
		
		//set background
		CGSize winSize = [CCDirector sharedDirector].winSize;
        self.main_bkgrnd = [CCSprite spriteWithSpriteFrameName:@"blueprint_background.png"];
        _main_bkgrnd.position = ccp(winSize.width/2, winSize.height/2);
        [_spriteSheet addChild:_main_bkgrnd];
		
		
		//set title
		static int MAIN_TITLE_TOP_MARGIN = 20;
		static int MAIN_TITLE_LEFT_MARGIN = 20;
		CCSprite *_main_title = [CCSprite spriteWithSpriteFrameName:@"title.png"];
		_main_title.position = ccp(_main_title.contentSize.width / 2 + MAIN_TITLE_LEFT_MARGIN, winSize.height - _main_title.contentSize.height/2 - MAIN_TITLE_TOP_MARGIN);
		_main_title.opacity = 100;
		[_spriteSheet addChild:_main_title];
		
		//set menus
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:25];
		
		
		CCMenuItem *mainMenu = [CCMenuItemFont itemFromString:@"<< Back"
													target:self selector:@selector(GoToMainMenu:)];
		
		CCMenu *menu = [CCMenu menuWithItems: mainMenu, nil];
		
		menu.position = ccp(50, winSize.height - 20);
		[menu alignItemsVerticallyWithPadding:4];
		[self addChild:menu];
		
		
		_highscore = [GameData sharedData].highscore;
		
		_scoreLabels = [[NSMutableArray alloc] initWithCapacity:[_highscore.scores count]];
		static int HIGHSCORE_LINE_HEIGHT = 40;
		static int HIGHSCORE_TOP_MARGIN = 100;
		 
		
		for(int i = 0;i < [_highscore.scores count];i++) {			
			CCLabel *label = [CCLabel labelWithString:@"" fontName:@"Marker Felt" fontSize:40];
			label.position = ccp(winSize.width / 2, winSize.height - (HIGHSCORE_LINE_HEIGHT * i++) - HIGHSCORE_TOP_MARGIN);
			[_scoreLabels addObject:label];
			[self addChild:label];
		}
	}
	return self;
}

-(void) onEnter {
	[super onEnter];
	
	int i = 0;
	
	for(CCLabel *label in _scoreLabels) {
		Score *score = [_highscore.scores objectAtIndex:i++];
		if(score != nil) {
		[label setString:[NSString stringWithFormat:@"%d. Level%d: %d",
						  i,
						  score.level,
						  score.record ]];
		} else {
			[label setString:@" . -----------------------"];
		}
	}
	
	
}

-(void) resume: (id) sender {
	
	[[CCDirector sharedDirector] popScene];
}

-(void) GoToMainMenu: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate *) [UIApplication sharedApplication].delegate;
	[delegate launchMainMenu];
}

- (void) dealloc {
    [super dealloc];
}
