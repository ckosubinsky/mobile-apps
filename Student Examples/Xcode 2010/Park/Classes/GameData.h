//
//  GameData.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HighScores.h"
enum {
	//! Default tag
	maxNumberOfLadders = 5,
	maxNumberOfLegs = 20,
	PAUSE_STATE = 0,
	PLAYING_STATE = 1,
	OPENING_STATE = 2
};

@class LadderSibling;
@class SBJsonParser;

@interface GameData : NSObject {
	int _currentLevel;
	float _currentTimeSpend;
	float _endingTime;
	int _maxHP;
	int _currentHP;
	int _numberOfLadders;
	int _numberOfLegs;
	int _gameState;
	bool _damaging;
	HighScores *_highscore;
	SBJsonParser *parser;
	
	NSMutableArray *_ladders;
	NSMutableArray *_ladderSiblings;
	NSMutableArray *_aliens;
	NSMutableArray *_fliers;
	NSMutableArray *_defensers;
	NSMutableArray *_activeLegs;
	NSMutableArray *_inactiveLegs;
	NSMutableArray *_aliensInLevel;
}

@property (nonatomic, assign) int currentLevel;
@property (nonatomic, assign) float currentTimeSpend;
@property (nonatomic, assign) float endingTime;
@property (nonatomic, assign) int maxHP;
@property (nonatomic, assign) int currentHP;
@property (nonatomic, assign) int numberOfLadders;
@property (nonatomic, assign) int numberOfLegs;
@property (nonatomic, assign) int gameState;
@property (nonatomic, assign) bool damaging;
@property (nonatomic, assign) HighScores *highscore;
@property (nonatomic, assign) NSMutableArray *ladders;
@property (nonatomic, assign) NSMutableArray *activeLegs;
@property (nonatomic, assign) NSMutableArray *inactiveLegs;
@property (nonatomic, assign) NSMutableArray *ladderSiblings;
@property (nonatomic, assign) NSMutableArray *aliens;
@property (nonatomic, assign) NSMutableArray *fliers;
@property (nonatomic, assign) NSMutableArray *defensers;
@property (nonatomic, assign) NSMutableArray *aliensInLevel;
- (void)reset;
- (void)nextLevel;
- (void)loadLevel;
- (bool)isLegAvailable;
- (bool)levelComplete;
- (void)cutConnection:(LadderSibling*)leg;
- (LadderSibling*)selectedLeg:(CGPoint)point;
- (NSMutableArray*)getAliens:(float)dt;
+ (GameData*) sharedData;
@end
