//
//  Alien.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "Alien.h"
#import "GameData.h"

@implementation Alien
@synthesize speedX = _speedX;
@synthesize speedY = _speedY;
@synthesize crossing = _crossing;
@synthesize actions = _actions;
@synthesize currentAction = _currentAction;
@synthesize spawning = _spawning;
@synthesize HP = _HP;
@synthesize AP = _AP;
@synthesize maxHP = _maxHP;


-(void)moveDown {
	[self switchAction:AlienAnimateDown];
	[self setPosition:ccp(self.position.x , self.position.y - _speedY)];
}
-(void)moveLeft {
	[self switchAction:AlienAnimateLeft];
	[self setPosition:ccp(self.position.x - _speedX, self.position.y)];
}

-(void)moveRight {
	[self switchAction:AlienAnimateRight];
	[self setPosition:ccp(self.position.x + _speedX, self.position.y)];
}

-(void)switchAction:(int)targetAction {
	if(_currentAction != targetAction && _currentAction > AlienDestroy) {
		[self stopAction:[_actions objectAtIndex:_currentAction]];
		_currentAction = targetAction;
		[self runAction:[_actions objectAtIndex:_currentAction]];
	}
}

-(void)attack:(id)sender {
	[GameData sharedData].damaging = true;
	[GameData sharedData].currentHP -= _AP;
}

-(void)destroyAlien:(id)sender {
	Alien *alien = (Alien *)sender;
	alien.currentAction = AlienDestroy;
}


-(CGRect) getCollisionRect {
	return CGRectMake(self.position.x, self.position.y, self.contentSize.width, self.contentSize.height);
}

+(Alien*)alienWithType:(AlienType)alienType {
	Alien *alien = [[[Alien alloc] initWithSpriteFrameName:@"enemy02_01.png"] autorelease];
	alien.actions = [[NSMutableArray alloc] init];
	
	NSMutableArray *walkAnimFrames = [NSMutableArray array];    
	[walkAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_01.png"]];
	[walkAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_02.png"]];
	CCAnimation *walkAnimation = [CCAnimation animationWithName:@"down" delay:0.2f frames:walkAnimFrames];
	id walkAction = [CCRepeatForever actionWithAction:
					   [CCAnimate actionWithAnimation:walkAnimation restoreOriginalFrame:NO]];
	[alien runAction:walkAction];
	alien.currentAction = AlienAnimateDown;
	[alien.actions addObject:walkAction];
	
	NSMutableArray *walkLeftAnimFrames = [NSMutableArray array];    
	[walkLeftAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_03.png"]];
	[walkLeftAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_04.png"]];
	CCAnimation *walkLeftAnimation = [CCAnimation animationWithName:@"left" delay:0.2f frames:walkLeftAnimFrames];
	id walkLeftAction = [CCRepeatForever actionWithAction:
					 [CCAnimate actionWithAnimation:walkLeftAnimation restoreOriginalFrame:NO]];
	
	[alien.actions addObject:walkLeftAction];
	
	NSMutableArray *walkRightAnimFrames = [NSMutableArray array];    
	[walkRightAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_05.png"]];
	[walkRightAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_06.png"]];
	CCAnimation *walkRightAnimation = [CCAnimation animationWithName:@"right" delay:0.2f frames:walkRightAnimFrames];
	id walkRightAction = [CCRepeatForever actionWithAction:
						 [CCAnimate actionWithAnimation:walkRightAnimation restoreOriginalFrame:NO]];
	[alien.actions addObject:walkRightAction];
	
	NSMutableArray *attackAnimFrames = [NSMutableArray array];    
	[attackAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_07.png"]];
	[attackAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_08.png"]];
	CCAnimation *attackAnimation = [CCAnimation animationWithName:@"attack" delay:0.2f frames:attackAnimFrames];
	id attackAction = [CCSequence actions:[CCAnimate actionWithAnimation:attackAnimation restoreOriginalFrame:NO],
					   [CCCallFuncN actionWithTarget:alien selector:@selector(attack:)],
					   [CCCallFuncN actionWithTarget:alien selector:@selector(destroyAlien:)],
					  nil];
	[alien.actions addObject:attackAction];
	
	NSMutableArray *deadAnimFrames = [NSMutableArray array];    
	[deadAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_09.png"]];
	[deadAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_10.png"]];
	[deadAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"enemy02_11.png"]];
	CCAnimation *deadAnimation = [CCAnimation animationWithName:@"dead" delay:0.2f frames:deadAnimFrames];
	id deadAction = [CCSequence actions:[CCAnimate actionWithAnimation:deadAnimation restoreOriginalFrame:NO],
					 [CCCallFuncN actionWithTarget:alien selector:@selector(destroyAlien:)],
					 nil];
	
	[alien.actions addObject:deadAction];
	
	
	if(alienType == AlienTypeSlowAndMove) {
		alien.speedX = 2.2f;
		alien.speedY = 0.2f;
		alien.AP = 10;
	} else if (alienType == AlienTypeFastAndMove) {
		alien.speedX = 2.2f;
		alien.speedY = 1.0f;
		alien.AP = 5;
		[alien setColor:ccc3(255, 0, 0)];
	}
	
	return alien;
}

@end
