//
//  HighScores.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 7..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Score;

@interface HighScores : NSObject {
	NSMutableArray *_scores;
}

@property (nonatomic, assign) NSMutableArray *scores;

-(void)addScore:(Score*)score;
-(void)storeScores;
-(void)retrieveScores;

@end
