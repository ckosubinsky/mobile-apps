//
//  LoadingScene.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "LoadingScene.h"
#import "Doodle_RaiderAppDelegate.h"


@implementation LoadingScene

- (id)init {
	
    if ((self = [super init])) {
        LoadingLayer *layer = [[LoadingLayer alloc] init];
        [self addChild:layer];
        [layer release];
    }
    return self;
    
}
@end


@implementation LoadingLayer

@synthesize defaultImage = _defaultImage;
@synthesize spriteSheet = _spriteSheet;
@synthesize main_bkgrnd = _main_bkgrnd;
@synthesize main_title = _main_title;
@synthesize ufo = _ufo;
@synthesize tapToCont = _tapToCont;
@synthesize loading = _loading;
@synthesize isLoading = _isLoading;
@synthesize imagesLoaded = _imagesLoaded;
@synthesize scenesLoaded = _scenesLoaded;
@synthesize label = _label;

- (id) init {
    
    if ((self = [super init])) {
        
        // Initialize variables
        self.isLoading = YES;
        self.imagesLoaded = NO;
        self.scenesLoaded = NO;
        
        // Set touch enabled
        self.isTouchEnabled = YES;
		
        // Continue to show Default.png until we're fully loaded...
        CGSize winSize = [CCDirector sharedDirector].winSize;
        self.defaultImage = [CCSprite spriteWithFile:@"DoodleLoading.png"];
        _defaultImage.position = ccp(winSize.width/2, winSize.height/2);
        [self addChild:_defaultImage];        
        
		
        // Load our sprites in the background
        [[CCTextureCache sharedTextureCache] addImageAsync:@"sprite.png" target:self selector:@selector(spritesLoaded:)];
        
        // Load up our sound effects in the background
		/*
		 if ([CDAudioManager sharedManagerState] != kAMStateInitialised) {
		 //The audio manager is not initialised yet so kick off the sound loading as an NSOperation that will wait for
		 //the audio manager
		 NSInvocationOperation* bufferLoadOp = [[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadSoundBuffers:) object:nil] autorelease];
		 NSOperationQueue *opQ = [[[NSOperationQueue alloc] init] autorelease]; 
		 [opQ addOperation:bufferLoadOp];
		 } else {
		 [self loadSoundBuffers:nil];
		 }	
		 */
		
		
		
        // Schedule a periodic method to check status
        [self schedule: @selector(tick:)];
        
    }
    
    return self;
    
}

-(void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
    if (!_isLoading) {
        Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate *) [UIApplication sharedApplication].delegate;
        [delegate launchMainMenu];
    }
    
}

-(void) tick: (ccTime) dt {
    
    if (_imagesLoaded && _scenesLoaded && _isLoading) {
        
        // Set flag that we're no longer loading
        self.isLoading = NO;
        
        // Remove loading sprite
        [_spriteSheet removeChild:_loading cleanup:YES];
        self.loading = nil;
        
		
        // Add "tap to continue" sprite...
		[_label setString:@"Tap to continue..."];
		[_label runAction:[CCRepeatForever actionWithAction:
						   [CCSequence actions:
							[CCFadeOut actionWithDuration:1.0f],
							[CCFadeIn actionWithDuration:1.0f],
							nil]]];
		
        
    }
    
}

-(void) loadScenes:(NSObject*) data {
	
    NSAutoreleasePool *autoreleasepool = [[NSAutoreleasePool alloc] init];
    
    Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate *) [UIApplication sharedApplication].delegate;
    [delegate loadScenes];
    self.scenesLoaded = YES;
    
    [autoreleasepool release];
	
}

-(void) spritesLoaded: (CCTexture2D*) tex {
	
	// Remove existing image
    [self removeChild:_defaultImage cleanup:YES];
    self.defaultImage = nil;
    
    // Store sprite texture in cache and initialize sprite frames
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sprite.plist"];
    
    // Add a sprite sheet based on the loaded texture and add it to the scene
    self.spriteSheet = [CCSpriteSheet spriteSheetWithTexture:tex];
    [self addChild:_spriteSheet];
    
    // Add main background to scene
    CGSize winSize = [CCDirector sharedDirector].winSize;
    self.main_bkgrnd = [CCSprite spriteWithSpriteFrameName:@"blueprint_background.png"];
	
	_main_bkgrnd.position = ccp(winSize.width/2, winSize.height/2);
    [_spriteSheet addChild:_main_bkgrnd];
    
	
	// Add title to scene
	static int MAIN_TITLE_TOP_MARGIN = 20;
	static int MAIN_TITLE_LEFT_MARGIN = 20;
	self.main_title = [CCSprite spriteWithSpriteFrameName:@"title.png"];
	_main_title.position = ccp(self.main_title.contentSize.width / 2 + MAIN_TITLE_LEFT_MARGIN, winSize.height - _main_title.contentSize.height/2 - MAIN_TITLE_TOP_MARGIN);
	[_spriteSheet addChild:_main_title];
	
	// Add Tom to scene
    static int TOM_BOTTOM_MARGIN = 30;
    static int TOM_LEFT_MARGIN = 20;
    self.ufo = [CCSprite spriteWithSpriteFrameName:@"ufo_01.png"];
    _ufo.position = ccp(_ufo.contentSize.width/2 + winSize.width + TOM_LEFT_MARGIN
						, _ufo.contentSize.height/2 + winSize.height + TOM_BOTTOM_MARGIN);
    [_spriteSheet addChild:_ufo];
    	
    // Make tom blink, for fun
    NSMutableArray *blinkAnimFrames = [NSMutableArray array];    
    [blinkAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"ufo_02.png"]];
    [blinkAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"ufo_01.png"]];
    CCAnimation *blinkAnimation = [CCAnimation animationWithName:@"blink" delay:0.2f frames:blinkAnimFrames];
    [_ufo runAction:[CCRepeatForever actionWithAction:
                     [CCSequence actions:
                      [CCAnimate actionWithAnimation:blinkAnimation restoreOriginalFrame:NO],
                      nil]]];
	
	[_ufo runAction:[CCMoveTo actionWithDuration:1.2f position:ccp(380,240)]];
	
	// Add "Loading..." to scene
	static int LABEL_MARGIN = 20;
	static int LABEL_MARGIN_BOTTOM = 100;
	self.label = [CCLabel labelWithString:@"Loading..." dimensions:CGSizeMake(winSize.width - LABEL_MARGIN*2, winSize.height - LABEL_MARGIN-LABEL_MARGIN_BOTTOM) alignment:UITextAlignmentCenter fontName:@"Marker Felt" fontSize:34];
	_label.position = ccp(winSize.width/2, 0);
	_label.color = ccc3(255, 255, 255);
	[self addChild:_label];
	
	
	
    self.imagesLoaded = YES;
	
    // Kick off an operation to load the scenes now that our textures are loaded
    NSInvocationOperation* sceneLoadOp = [[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadScenes:) object:nil] autorelease];
    NSOperationQueue *opQ = [[[NSOperationQueue alloc] init] autorelease]; 
    [opQ addOperation:sceneLoadOp];
    
}

- (void) dealloc {
    [super dealloc];
}


@end
