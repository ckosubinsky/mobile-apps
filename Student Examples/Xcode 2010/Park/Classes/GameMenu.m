//
//  GameMenu.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 7..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "GameMenu.h"
#import "Doodle_RaiderAppDelegate.h"

@implementation GameMenu
-(id)init {

	if(self = [super init]) {
		
		CGSize winSize = [CCDirector sharedDirector].winSize;
        
		//set menus
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:35];
		
		CCMenuItem *resume = [CCMenuItemFont itemFromString:@"Resume"
												   target:self selector:@selector(resume:)];
		
		[CCMenuItemFont setFontSize:20];
		
		CCMenuItem *restartMenu = [CCMenuItemFont itemFromString:@"Restart"
													   target:self selector:@selector(restartMenu:)];
		[CCMenuItemFont setFontSize:20];
		
		CCMenuItem *mainMenu = [CCMenuItemFont itemFromString:@"Main Menu"
													target:self selector:@selector(GoToMainMenu:)];
		
		CCMenu *menu = [CCMenu menuWithItems: resume, restartMenu, mainMenu, nil];
		
		menu.position = ccp(winSize.width / 2, winSize.height / 2);
		[menu alignItemsVerticallyWithPadding:20];
		[self addChild:menu];		
	}
	return self;
}

-(void) resume: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate*) [UIApplication sharedApplication].delegate;
	[delegate launchIngameMenu:NO];
}

-(void) restartMenu: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate*) [UIApplication sharedApplication].delegate;
	[delegate launchRestartGame];
}

-(void) GoToMainMenu: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate*) [UIApplication sharedApplication].delegate;
	[delegate launchMainMenu];
	
}
@end
