//
//  LadderSibling.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "LadderSibling.h"
#import "Ladder.h"
#import "GameData.h"

@implementation LadderSibling
@synthesize posX = _posX;
@synthesize posY = _posY;
@synthesize length = _length;
@synthesize leftLadder = _leftLadder;
@synthesize rightLadder = _rightLadder;
@synthesize connected = _connected;

+ (LadderSibling *)ladderSiblingWithPos:(int)pos {
	LadderSibling *ladderSibling = [[LadderSibling alloc] initWithFile:@"bar.png" rect:CGRectMake(0,0,55,4)];
	ccTexParams params = {GL_LINEAR,GL_LINEAR,GL_LINEAR, GL_LINEAR};
	[ladderSibling.texture setTexParameters:&params];
	[ladderSibling setPosition:ccp(0,0)];
	return ladderSibling;
}

- (void) setWidth:(float)width {
	float adjX = (self.contentSize.width - width) / 2;
	adjX = (width < 0) ? adjX - (self.contentSize.width / 2) : adjX - (self.contentSize.width / 2);
	width = abs(width); // + (self.contentSize.width / 2);
	
	width = (width < 8) ? 8 : width;
	
	float newScale = (width) / self.contentSize.width;
	self.scaleX = newScale;
	[self setPosition:ccp(_posX + adjX, self.position.y)];
	
}

- (void) connectLeftLadder:(Ladder*)lLadder rightLadder:(Ladder*)rLadder {
	self.leftLadder = lLadder;
	self.rightLadder = rLadder;
	
	float width = (float)(_rightLadder.position.x) - (float)(_leftLadder.position.x) - 20; 
	float median = ((float)(_rightLadder.position.x) + (float)(_leftLadder.position.x)) / 2; 
	float newScale = width / self.contentSize.width;
	self.scaleX = newScale;
	[self setPosition:ccp(median, self.position.y)];
}

- (Ladder*) nextLadder:(Ladder*)currentLadder {
	return (currentLadder == _leftLadder) ? _rightLadder : _leftLadder;
}

@end
