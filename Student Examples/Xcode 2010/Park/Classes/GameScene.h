//
//  GameScene.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@class GameData;
@class Alien;
@class Ladder;
@class LadderSibling;
@class GameMenu;

@interface GameLayer : CCLayer {
	CCSprite *_main_bkgrnd;
	CCSpriteSheet *_spriteSheet;
	CCLabel *_label;
	CCLabel *_labelMessage;
	LadderSibling *_selectedLeg;
	int _currentSibling;
	int DEFENSER_TOUCH_AREA_HEIGHT;
}

@property (nonatomic, retain) CCSprite *main_bkgrnd;
@property (nonatomic, assign) CCSpriteSheet *spriteSheet;
@property (nonatomic, assign) CCLabel *label;

- (void)addAlien:(Alien*)alien;
- (void)addBeam:(Ladder*)ladder;
- (void)spawn:(int)alienType;
@end

@interface GameScene : CCScene {
	GameLayer *_layer;
	GameMenu *_menu;
}


@property (nonatomic, retain) GameLayer *layer;
@property (nonatomic, retain) GameMenu *menu;

- (void) popMenu;
- (void) closeMenu;
- (void) restartMenu;
@end
