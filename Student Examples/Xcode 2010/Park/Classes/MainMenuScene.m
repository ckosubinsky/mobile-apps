//
//  MainMenuScene.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "MainMenuScene.h"
#import "GameData.h"
#import "Flier.h"
#import "Doodle_RaiderAppDelegate.h"

@implementation MainMenuScene
@synthesize layer = _layer;

- (id)init {
    if ((self = [super init])) {
        self.layer = [[[MainMenuLayer alloc] init] autorelease];
        [self addChild:_layer];
		
    }
    return self;
}

@end



@implementation MainMenuLayer

@synthesize main_bkgrnd = _main_bkgrnd;
@synthesize ufo = _ufo;
@synthesize spriteSheet = _spriteSheet;

- (id) init {
	if( (self=[super init] )) {
		
		
		self.spriteSheet = [CCSpriteSheet spriteSheetWithTexture:[[CCTextureCache sharedTextureCache] addImage:@"sprite.png"]];
		[self addChild:_spriteSheet z:-1];
		
		//set background
		CGSize winSize = [CCDirector sharedDirector].winSize;
        self.main_bkgrnd = [CCSprite spriteWithSpriteFrameName:@"blueprint_background.png"];
        _main_bkgrnd.position = ccp(winSize.width/2, winSize.height/2);
        [_spriteSheet addChild:_main_bkgrnd];
		
		
		//set title
		static int MAIN_TITLE_TOP_MARGIN = 20;
		static int MAIN_TITLE_LEFT_MARGIN = 20;
		CCSprite *_main_title = [CCSprite spriteWithSpriteFrameName:@"title.png"];
		_main_title.position = ccp(_main_title.contentSize.width / 2 + MAIN_TITLE_LEFT_MARGIN, winSize.height - _main_title.contentSize.height/2 - MAIN_TITLE_TOP_MARGIN);
		[_spriteSheet addChild:_main_title];
		
		//set background objects, specially ufos
		_ufo = [Flier getFlier];
		[_spriteSheet addChild:_ufo];
		
		//set menus
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:35];
		
		CCMenuItem *Play = [CCMenuItemFont itemFromString:@"Start Game!"
												   target:self selector:@selector(startGame:)];
		
		[CCMenuItemFont setFontSize:20];
		
		CCMenuItem *About = [CCMenuItemFont itemFromString:@"How to Play"
													target:self selector:@selector(GoToHelp:)];
		
		CCMenuItem *Scores = [CCMenuItemFont itemFromString:@"High Scores"
													 target:self selector:@selector(GoToHighScore:)];
		
		CCMenuItem *Credits = [CCMenuItemFont itemFromString:@"Credits"
													  target:self selector:@selector(GoToCredits:)];
		
		CCMenu *menu = [CCMenu menuWithItems: Play, About, Scores, Credits, nil];
		
		menu.position = ccp(240, 80);
		[menu alignItemsVerticallyWithPadding:4];
		[self addChild:menu];
		
	}
	return self;
}

- (void)onEnter {
    [super onEnter];
	CGSize winSize = [CCDirector sharedDirector].winSize;
	int ufoY = winSize.height / 2;
	_ufo.position = ccp(-_ufo.contentSize.width, ufoY);
	_ufo.scale = 1.0f;
	_ufo.targetScale = 3.0f;
	[_ufo runAction:[CCSequence actions:
					[CCMoveTo actionWithDuration:2.0f position:ccp(winSize.width/2 - 10, ufoY)],
					[CCMoveTo actionWithDuration:1.0f position:ccp(winSize.width + (_ufo.contentSize.width * _ufo.targetScale), ufoY)],
					[CCCallFuncN actionWithTarget:_ufo selector:@selector(magnify:)],
					[CCDelayTime actionWithDuration:1.0f],
					[CCMoveTo actionWithDuration:0.1f position:ccp(winSize.width + (_ufo.contentSize.width * _ufo.targetScale), ufoY + (_ufo.contentSize.height * _ufo.scale * 2) + 20)],
					[CCMoveTo actionWithDuration:2.0f position:ccp(350, ufoY + (_ufo.contentSize.height * _ufo.scale * 2) + 20)],
					nil]];
}

-(void) resume: (id) sender {	
	[[CCDirector sharedDirector] popScene];
}

-(void) startGame: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate*) [UIApplication sharedApplication].delegate;
	[delegate launchNewGame];
	
}

-(void) GoToHighScore: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate*) [UIApplication sharedApplication].delegate;
	[delegate launchHighScore];
	
}

-(void) GoToCredits: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate*) [UIApplication sharedApplication].delegate;
	[delegate launchCredits];

}

-(void) GoToHelp: (id) sender {
	/*
	 [[CCDirector sharedDirector] sendCleanupToScene];
	 [[CCDirector sharedDirector] popScene];
	 [[CCDirector sharedDirector] replaceScene:[CCFadeTransition
	 transitionWithDuration:1 scene:[MenuScene node]]];
	 */
	
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate*) [UIApplication sharedApplication].delegate;
	[delegate launchHelp];
}

- (void) dealloc {
    [super dealloc];
}
@end
