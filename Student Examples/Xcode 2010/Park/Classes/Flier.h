//
//  Flier.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 3..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Flier : CCSprite {
	float _velocityX;
	float _velocityY;
	float _acceleration;
	float _targetX;
	float _targetY;
	float _targetScale;
}
+ (Flier*)getFlier;


@property (nonatomic, assign) float velocityX;
@property (nonatomic, assign) float velocityY;
@property (nonatomic, assign) float targetScale;
@property (nonatomic, assign) float acceleration;

@end
