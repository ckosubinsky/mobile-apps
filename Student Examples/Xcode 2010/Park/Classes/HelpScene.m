//
//  HelpScene.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 8..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "HelpScene.h"
#import "Doodle_RaiderAppDelegate.h"

@implementation HelpScene
@synthesize layer = _layer;

- (id)init {
    if ((self = [super init])) {
        self.layer = [[[HelpLayer alloc] init] autorelease];
        [self addChild:_layer];
	}
    return self;
}
@end

@implementation HelpLayer
@synthesize spriteSheet = _spriteSheet;

- (id) init {
	if( (self=[super init] )) {
		
		CGSize winSize = [CCDirector sharedDirector].winSize;
        
		self.spriteSheet = [CCSpriteSheet spriteSheetWithTexture:[[CCTextureCache sharedTextureCache] addImage:@"sprite.png"]];
		[self addChild:_spriteSheet z:-1];
		
		//set background
		CCSprite *_main_bkgrnd = [CCSprite spriteWithSpriteFrameName:@"instruction.png"];
        _main_bkgrnd.position = ccp(winSize.width/2, winSize.height / 2);
        [_spriteSheet addChild:_main_bkgrnd];
		
		//set menus
		[CCMenuItemFont setFontName:@"Marker Felt"];
		[CCMenuItemFont setFontSize:25];
		
		CCMenuItem *mainMenu = [CCMenuItemFont itemFromString:@"<< Back"
													   target:self selector:@selector(GoToMainMenu:)];
		CCMenu *menu = [CCMenu menuWithItems: mainMenu, nil];
		
		menu.position = ccp(50, 20);
		[menu alignItemsVerticallyWithPadding:4];
		[self addChild:menu];
	}
	
	return self;
}		

-(void) GoToMainMenu: (id) sender {
	Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate *) [UIApplication sharedApplication].delegate;
	[delegate launchMainMenu];
}
@end

