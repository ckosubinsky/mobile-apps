//
//  LoadingScene.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"


@interface LoadingScene : CCScene {
	
}

@end


@interface LoadingLayer : CCLayer {
    CCSprite *_defaultImage;
    CCSpriteSheet *_spriteSheet;
    CCSprite *_main_bkgrnd;
    CCSprite *_main_title;
    CCSprite *_ufo;
    CCSprite *_tapToCont;
    CCSprite *_loading;
	
	CCLabel *_label;
	
    BOOL _isLoading;
    BOOL _imagesLoaded;
    BOOL _scenesLoaded;
}

@property (nonatomic, assign) CCSprite *defaultImage;
@property (nonatomic, assign) CCSpriteSheet *spriteSheet;
@property (nonatomic, assign) CCSprite *main_bkgrnd;
@property (nonatomic, assign) CCSprite *main_title;
@property (nonatomic, assign) CCSprite *ufo;
@property (nonatomic, assign) CCSprite *tapToCont;
@property (nonatomic, assign) CCSprite *loading;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL imagesLoaded;
@property (nonatomic, assign) BOOL scenesLoaded;
@property (nonatomic, assign) CCLabel *label;


@end
