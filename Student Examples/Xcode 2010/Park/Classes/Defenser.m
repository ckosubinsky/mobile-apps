//
//  Defenser.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 3..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "Defenser.h"


@implementation Defenser
@synthesize actions = _actions;
@synthesize currentAction = _currentAction;
@synthesize enabled = _enabled;
@synthesize numberOfBeam = _numberOfBeam;
@synthesize reloadTime = _reloadTime;

-(id) init{
	if((self=[super init])){
		[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
		_reloadTime = 0.5f;
		_reloaded = true;
		_currentTime = 0.0f;
	}
	return self;
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint touchPoint = [touch locationInView:[touch view]];
	touchPoint = [[CCDirector sharedDirector] convertToGL:touchPoint];
	if(_reloaded && [self isTouchOnSprite:touchPoint]){
		[self attack];
		return YES;
	}
	return NO;
}

-(BOOL) isTouchOnSprite:(CGPoint)touch{
	if(CGRectContainsPoint(CGRectMake(self.position.x - ((self.contentSize.width/2)*self.scale), self.position.y - ((self.contentSize.height/2)*self.scale), self.contentSize.width*self.scale, self.contentSize.height*self.scale), touch))
		return YES;
	else 
		return NO;
}


- (void)onEnter {
    [super onEnter];
	[self schedule:@selector(update:)];
}

-(void)update:(ccTime)dt {
	if(!_reloaded) {
		_currentTime += dt;
		if(_currentTime >= _reloadTime) {
			_currentTime = 0.0f;
			_reloaded = true;
		} 
	}
}

-(void)switchAction:(int)targetAction {
	if(_currentAction != targetAction) {
		[self stopAction:[_actions objectAtIndex:_currentAction]];
		_currentAction = targetAction;
		[self runAction:[_actions objectAtIndex:_currentAction]];
	}
}

-(bool)isFacingLeft {
	return (_currentAction == DefenserFacingLeft || 
			_currentAction == DefenserAttackFacingLeft ||
			_currentAction == DefenserWalkFacingLeft);
}

-(void)attack {	
	_reloaded = false;
	_numberOfBeam++;
	if([self isFacingLeft])
		[self switchAction:DefenserAttackFacingLeft];
	else 
		[self switchAction:DefenserAttackFacingRight];
}
-(void)restore:(id)sender {
	if([self isFacingLeft])
		[self switchAction:DefenserFacingLeft];
	else	
		[self switchAction:DefenserFacingRight];
}

+(Defenser*)getDefenser {
	
	Defenser *defenser = [[[Defenser alloc] initWithSpriteFrameName:@"defenser1_01_01.png" ] autorelease];
	defenser.actions = [[NSMutableArray alloc] init];
	
	NSMutableArray *leftAnimFrames = [NSMutableArray array];    
	[leftAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_01_01.png"]];
	CCAnimation *leftAnimation = [CCAnimation animationWithName:@"left" delay:0.2f frames:leftAnimFrames];
	id leftAction = [CCRepeatForever actionWithAction:
					 [CCAnimate actionWithAnimation:leftAnimation restoreOriginalFrame:NO]];
	[defenser runAction:leftAction];
	defenser.currentAction = DefenserFacingLeft;
	[defenser.actions addObject:leftAction];
	
	NSMutableArray *rightAnimFrames = [NSMutableArray array];    
	[rightAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_02_01.png"]];
	CCAnimation *rightAnimation = [CCAnimation animationWithName:@"right" delay:0.2f frames:rightAnimFrames];
	id rightAction = [CCRepeatForever actionWithAction:
						  [CCAnimate actionWithAnimation:rightAnimation restoreOriginalFrame:NO]];
	
	[defenser.actions addObject:rightAction];

	NSMutableArray *walkLeftAnimFrames = [NSMutableArray array];    
	[walkLeftAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_01_01.png"]];
	[walkLeftAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_01_02.png"]];
	CCAnimation *walkLeftAnimation = [CCAnimation animationWithName:@"right" delay:0.2f frames:walkLeftAnimFrames];
	id walkLeftAction = [CCRepeatForever actionWithAction:
						  [CCAnimate actionWithAnimation:walkLeftAnimation restoreOriginalFrame:NO]];
	
	[defenser.actions addObject:walkLeftAction];
	
	
	NSMutableArray *walkRightAnimFrames = [NSMutableArray array];    
	[walkRightAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_02_01.png"]];
	[walkRightAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_02_02.png"]];
	CCAnimation *walkRightAnimation = [CCAnimation animationWithName:@"right" delay:0.2f frames:walkRightAnimFrames];
	id walkRightAction = [CCRepeatForever actionWithAction:
						 [CCAnimate actionWithAnimation:walkRightAnimation restoreOriginalFrame:NO]];
	
	[defenser.actions addObject:walkRightAction];
	
	NSMutableArray *attackLeftAnimFrames = [NSMutableArray array];    
	[attackLeftAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_03_01.png"]];
	[attackLeftAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_03_02.png"]];
	[attackLeftAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_03_03.png"]];
	CCAnimation *attackLeftAnimation = [CCAnimation animationWithName:@"attkLeft" delay:0.1f frames:attackLeftAnimFrames];
	id attackLeftAction = [CCSequence actions:
						   [CCAnimate actionWithAnimation:attackLeftAnimation restoreOriginalFrame:NO],
						   [CCCallFuncN actionWithTarget:defenser selector:@selector(restore:)],
						   nil];
	[defenser.actions addObject:attackLeftAction];
	
	NSMutableArray *attackRightAnimFrames = [NSMutableArray array];    
	[attackRightAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_04_01.png"]];
	[attackRightAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_04_02.png"]];
	[attackRightAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"defenser1_04_03.png"]];
	CCAnimation *attackRightAnimation = [CCAnimation animationWithName:@"attkLeft" delay:0.1f frames:attackRightAnimFrames];
	id attackRightAction = [CCSequence actions:
							[CCAnimate actionWithAnimation:attackRightAnimation restoreOriginalFrame:NO],
							[CCCallFuncN actionWithTarget:defenser selector:@selector(restore:)],
							nil];
	[defenser.actions addObject:attackRightAction];	
	return defenser;
}
@end
