//
//  Defenser.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 3..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

enum {
	DefenserFacingLeft = 0,
	DefenserFacingRight = 1,
	DefenserWalkFacingLeft = 2,
	DefenserWalkFacingRight = 3,
	DefenserAttackFacingLeft = 4,
	DefenserAttackFacingRight = 5
};


@interface Defenser : CCSprite {
	int _currentAction;
	NSMutableArray *_actions;
	bool _enabled;
	bool _reloaded;
	int _numberOfBeam;
	float _reloadTime;
	float _currentTime;
}

@property (nonatomic, assign) NSMutableArray *actions;
@property (nonatomic, assign) int currentAction;
@property (nonatomic, assign) int numberOfBeam;
@property (nonatomic, assign) float reloadTime;
@property (nonatomic, assign) bool enabled;


+(Defenser*)getDefenser;

-(void)switchAction:(int)targetAction;
-(void)attack;

@end
