//
//  Doodle_RaiderAppDelegate.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright Stony Brook University 2010. All rights reserved.
//

#import <UIKit/UIKit.h>


@class LoadingScene;
@class MainMenuScene;
@class GameScene;
@class HighScoresScene;
@class GameData;
@class CreditsScene;
@class HelpScene;

@interface Doodle_RaiderAppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow *window;
	LoadingScene *_loadingScene;
    MainMenuScene *_mainMenuScene;
    GameScene *_gameScene;
    CreditsScene *_creditsScene;
    HighScoresScene *_scoreScene;
    HelpScene *_helpScene;
	GameData *_gameData;
    
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) LoadingScene *loadingScene;
@property (nonatomic, retain) MainMenuScene *mainMenuScene;
@property (nonatomic, retain) GameScene *gameScene;
@property (nonatomic, retain) CreditsScene *creditsScene;
@property (nonatomic, retain) HighScoresScene *scoreScene;
@property (nonatomic, retain) HelpScene *helpScene;
@property (nonatomic, retain) GameData *gameData;

- (void)loadScenes;
- (void)launchMainMenu;
- (void)launchNewGame;
- (void)launchHighScore;
- (void)launchNextLevel;
- (void)launchKillEnding;
- (void)launchSuicideEnding;
- (void)launchLoseEnding;
- (void)launchRestartGame;
- (void)launchCredits;
- (void) launchHelp;
- (void)launchIngameMenu:(BOOL)pop;

@end
