//
//  HelpScene.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 8..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface HelpLayer : CCLayer {
    CCSpriteSheet *_spriteSheet;	
}

@property (nonatomic, assign) CCSpriteSheet *spriteSheet;
@end



@interface HelpScene : CCScene {

	HelpLayer *_layer;
}

@property (nonatomic, retain) HelpLayer *layer;
@end
