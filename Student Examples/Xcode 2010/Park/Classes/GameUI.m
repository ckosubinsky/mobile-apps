//
//  GameUI.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 3..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "GameUI.h"
#import "Flier.h"
#import "GameData.h"
#import "Doodle_RaiderAppDelegate.h"

@implementation GameUI
@synthesize energyGauge = _energyGauge;
@synthesize rightGauge = _rightGauge;
@synthesize legNumberLabel = _legNumberLabel;
@synthesize progressGauge = _progressGauge;
@synthesize progressGaugeBar = _progressGaugeBar;
- (id) init {
	if(self = [super init]) {
		CGSize winSize = [CCDirector sharedDirector].winSize;
		
		static int PROGRESS_LEFT_MARGIN = 20;
		static int PROGRESS_TOP_MARGIN = 30;
		_progressGauge = [CCSprite spriteWithSpriteFrameName:@"ui_stage_progress.png"];
		_progressGauge.position = ccp(PROGRESS_LEFT_MARGIN, 
								 winSize.height - PROGRESS_TOP_MARGIN - (_progressGauge.contentSize.height / 2));
		[self addChild:_progressGauge];
		
		_progressGaugeBar = [CCSprite spriteWithSpriteFrameName:@"ui_stage_progress_bar.png"];
		[self addChild:_progressGaugeBar];
		
		
		Flier *flier = [Flier getFlier];
		flier.scale = 0.5f;
		flier.position = ccp(PROGRESS_LEFT_MARGIN, _progressGauge.position.y - 5 + ((flier.contentSize.height + _progressGauge.contentSize.height) / 2));
		[self addChild:flier];
		
		static int ENERGY_RIGHT_MARGIN = 20;
		static int ENERGY_TOP_MARGIN = 24;
		self.rightGauge = [CCSprite spriteWithSpriteFrameName:@"ui_energy.png"];
		self.rightGauge.position = ccp(winSize.width - ENERGY_RIGHT_MARGIN,
								  winSize.height - ENERGY_TOP_MARGIN - (self.rightGauge.contentSize.height / 2));
		[self addChild:_rightGauge];
		
		//
		self.energyGauge = [CCSprite spriteWithSpriteFrameName:@"ui_energy_bar.png"];
		
		self.energyGauge.scaleY = 0.1f;
		self.energyGauge.position = ccp(self.rightGauge.position.x, self.rightGauge.position.y);
		[self addChild:_energyGauge];
		
		
		CCSprite *pauseSprite = [CCSprite spriteWithSpriteFrameName:@"button_pause.png"];
		
		CCMenuItemSprite *pauseMenu = [CCMenuItemSprite itemFromNormalSprite:pauseSprite selectedSprite:pauseSprite target:self selector:@selector(launchInGameMenu:)];
        CCMenu *menu = [CCMenu menuWithItems: pauseMenu, nil];
		
		menu.position = ccp(winSize.width - pauseSprite.contentSize.width - 7, 
							winSize.height - pauseSprite.contentSize.height);

		[self addChild:menu];
		
		self.legNumberLabel = [CCLabel labelWithString:@"" dimensions:CGSizeMake(40, 40) alignment:UITextAlignmentCenter fontName:@"Marker Felt" fontSize:28];
		self.legNumberLabel.position = ccp(35,152);
		[self addChild:_legNumberLabel];
	}
	
	return self;
}

- (void)launchInGameMenu:(id)sender {
	if([GameData sharedData].gameState == PLAYING_STATE) {
		[GameData sharedData].gameState = PAUSE_STATE;
		Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate *) [UIApplication sharedApplication].delegate;
		[delegate launchIngameMenu:YES];
	} else {
		[GameData sharedData].gameState = PLAYING_STATE;
		Doodle_RaiderAppDelegate *delegate = (Doodle_RaiderAppDelegate *) [UIApplication sharedApplication].delegate;
		[delegate launchIngameMenu:NO];
	}
}

- (void)onEnter {
    [super onEnter];	
    [self schedule:@selector(update:)];
}

-(void)update:(ccTime)dt {
	float currentHP = [GameData sharedData].currentHP;
	float maxHP = [GameData sharedData].maxHP;
	float progress = ([GameData sharedData].endingTime - [GameData sharedData].currentTimeSpend) / [GameData sharedData].endingTime;
	progress = (progress < 0.0f) ? 0.0f : progress;
	float gaugeScale = currentHP / maxHP;
	
	static int ENERGY_TOP_HEART_HEIGHT = 16;	
	self.energyGauge.scaleY = (float) ((self.rightGauge.contentSize.height - ENERGY_TOP_HEART_HEIGHT) * gaugeScale);
	self.energyGauge.position = ccp(self.rightGauge.position.x, 
									self.rightGauge.position.y - ((self.rightGauge.contentSize.height - self.energyGauge.scaleY) / 2) + 1);
	
	
	self.progressGaugeBar.position = ccp(self.progressGauge.position.x, 
										 self.progressGauge.position.y + (self.progressGauge.contentSize.height * progress)- (self.progressGauge.contentSize.height / 2) ); 
	if(_currentNumber != [GameData sharedData].numberOfLegs - [[GameData sharedData].activeLegs count]) {
		_currentNumber = [GameData sharedData].numberOfLegs - [[GameData sharedData].activeLegs count];
	}
	[self.legNumberLabel setString:[NSString stringWithFormat:@"%d", _currentNumber]];
	
}
@end
