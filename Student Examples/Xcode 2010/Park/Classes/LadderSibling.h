//
//  LadderSibling.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class Ladder;

@interface LadderSibling : CCSprite {
	int _posX;
	int _posY;
	int _length;
	bool _connected;
	Ladder *_leftLadder;
	Ladder *_rightLadder;
}

@property (nonatomic, assign) int posX;
@property (nonatomic, assign) int posY;
@property (nonatomic, assign) int length;
@property (nonatomic, assign) bool connected;
@property (nonatomic, assign) Ladder *leftLadder;
@property (nonatomic, assign) Ladder *rightLadder;

+ (LadderSibling *)ladderSiblingWithPos:(int)pos;

- (void) setWidth:(float)width;
- (void) connectLeftLadder:(Ladder*)lLadder rightLadder:(Ladder*)rLadder;
- (Ladder*) nextLadder:(Ladder*)currentLadder;

@end
