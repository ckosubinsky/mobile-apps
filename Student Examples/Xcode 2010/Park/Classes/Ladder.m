//
//  Ladder.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "Ladder.h"
#import "LadderSibling.h"
#import "Alien.h"
#import "Defenser.h"
#import "Flier.h"

@implementation Ladder
@synthesize posX = _posX;
@synthesize posY = _posY;
@synthesize border = _border;
@synthesize length = _length;
@synthesize siblings = _siblings;
@synthesize aliens = _aliens;
@synthesize aliensToRemove = _aliensToRemove;
@synthesize defenser = _defenser;
@synthesize flier = _flier;
@synthesize active = _active;
@synthesize beams = _beams;

- (void)addAlien:(Alien *)alien {
	
	static int STARTING_POINT_TOP_MARGIN = 5;
	
	int alienX = _posX;
	int alienY = _posY;
	alien.spawning = true;
	[alien setPosition:ccp(alienX, alienY - STARTING_POINT_TOP_MARGIN + ((_length - alien.contentSize.height) / 2))];
	[alien setOpacity:0];
	[alien runAction:[CCSequence actions:
					  [CCFadeIn actionWithDuration:1.0f],
					  [CCCallFuncN actionWithTarget:self selector:@selector(activateAlien:)],
					  nil]];
	
	[_aliens addObject:alien];
}

- (void)addBeam:(Alien *)beam {
	static int STARTING_POINT_BOTTOM_MARGIN = 25;
	
	int beamX = _posX;
	int beamY = _posY;
	beam.spawning = false;
	[beam setPosition:ccp(beamX, beamY + STARTING_POINT_BOTTOM_MARGIN - ((_length + beam.contentSize.height) / 2))];
	[_beams addObject:beam];
}

- (void)activateAlien:(id)sender {
	Alien *alien = (Alien *)sender;
	alien.spawning = false;
}

- (void)removeAlien:(CCSprite*)alien {
	[_aliensToRemove addObject:alien];
	[_aliens removeObject:alien];
}

- (void)removeBeam:(CCSprite*)beam {
	[_aliensToRemove addObject:beam];
	[_beams removeObject:beam];
}

- (void)addLadderSibbling:(LadderSibling *)sibling {
	[_siblings addObject:sibling];
}

/*
- (void)onEnter {
    [super onEnter];	
    [self schedule:@selector(update:)];
}
 */

-(void)updateObject:(Alien*)alien {
	if(!alien.spawning) {
		alien.crossing = false;
		Ladder *nextLadder = nil;
		int alienX = (int)(alien.position.x);
		float alienY = (float)(alien.position.y);
		int targetX = (int)(self.position.x);
		for(LadderSibling *ls in _siblings) {
			float siblingY = (float)(ls.position.y);
			
			if((alien.speedY > 0 && alienY - siblingY < alien.speedY && alienY - siblingY >= 0) ||
			   (alien.speedY < 0 && alienY - siblingY > alien.speedY && siblingY - alienY >= 0)) {
				nextLadder = [ls nextLadder:self];
				targetX = (int)(nextLadder.position.x);
				if(alienX < targetX)
					[alien moveRight];
				else
					[alien moveLeft];
				alien.crossing = true;
				break;
			}	
		}
		
		if(alien.crossing) {
			if (abs(alienX - targetX) < alien.speedX) {
				if(alien.speedY > 0) {
					[alien moveDown];
					[nextLadder.aliens addObject:alien];
					[self.aliens removeObject:alien];
				} else if (alien.speedY < 0) {
					[alien moveDown];
					[nextLadder.beams addObject:alien];
					[self.beams removeObject:alien];
				} 
				 
			} 
		} else {
			static int LADDER_MARGIN_TOP = 10;
			static int LADDER_MARGIN_BOTTOM = 25;
			if ((alien.speedY > 0 && alien.position.y <= _posY - (_length / 2) + LADDER_MARGIN_BOTTOM) || 
				(alien.speedY < 0 && alien.position.y >= _posY + (_length / 2) - LADDER_MARGIN_TOP) ) {
				//alien reach to the platform
				/*if (_defenser.enabled) {
					[_defenser attack];
					[alien switchAction:AlienAnimateDead];
				} else {
					[alien switchAction:AlienAnimateAttack];
				}*/
				if(alien.speedY > 0) {
					[alien switchAction:AlienAnimateAttack];
					[self removeAlien:alien];
				} else {
					alien.currentAction = -1;
					[self removeBeam:alien];	
				}
			} else {
				[alien moveDown];
			}
		}
	}
}

-(void)update:(ccTime)dt {
	if(_active) {		
		for(Alien *alien in _aliens) {
			[self updateObject:alien];
		}
		
		for(Alien *beam in _beams) {
			[self updateObject:beam];
			for(Alien *alien in _aliens) {
				CGRect alienBox = [alien getCollisionRect];
				CGRect beamBox = [beam getCollisionRect];
				if (CGRectIntersectsRect(alienBox, beamBox)) {
					[alien switchAction:AlienAnimateDead];
					[self removeAlien:alien];
					
					beam.currentAction = -1;
					[self removeBeam:beam];					
				}
				
			}
		}
	}
}

-(void) initDefenser {
	[_defenser setPosition:ccp(0, -(_defenser.contentSize.height))];
	CGSize winSize = [CCDirector sharedDirector].winSize;
	
	if(self.position.x > winSize.width / 2)
		[_defenser switchAction:DefenserWalkFacingRight];
	else
		[_defenser switchAction:DefenserWalkFacingLeft];
}

-(void) resetDefenser:(id)sender {
	if(_defenser.enabled) {
		[_defenser setPosition:ccp(self.position.x, 
								   -(_defenser.contentSize.height))];
		[_defenser runAction:[CCSequence actions:
							  [CCMoveTo actionWithDuration:1.0f position:ccp(self.position.x, 
																			 self.position.y - ((self.length) / 2)
																			 )],
							  [CCCallFuncN actionWithTarget:_defenser selector:@selector(restore:)],
							  nil]];	
	}
}

-(void) activateLadder:(id)sender {
	[self runAction:[CCSequence actions:
					  [CCFadeIn actionWithDuration:1.0f],
		//			  [CCCallFuncN actionWithTarget:self selector:@selector(activateAlien:)],
					  nil]];
	

}

-(void) initFlier {
	[_flier setPosition:ccp(-(_flier.contentSize.width), 0)];
	[self setOpacity:0];
}

-(void) placeFlier:(float)delay {
	[_flier setPosition:ccp(-(_flier.contentSize.width), self.position.y + ((self.length + _flier.contentSize.height) / 2))];
	float duration = self.position.x / 120.0f;
	
	[_flier runAction:[CCSequence actions:
					   [CCDelayTime actionWithDuration:delay],
					   [CCMoveTo actionWithDuration:duration position:ccp(self.position.x, 
																	  self.position.y + ((self.length + _flier.contentSize.height) / 2))],
					   [CCCallFuncN actionWithTarget:self selector:@selector(activateLadder:)],
					   [CCCallFuncN actionWithTarget:self selector:@selector(resetDefenser:)],
					   
					 nil]];	
}


+ (Ladder *)ladderWithPos:(int)pos length:(int)len {
	CGSize winSize = [CCDirector sharedDirector].winSize;
	Ladder *ladder = [[Ladder alloc] initWithFile:@"aurora.png" rect:CGRectMake(0,0,36,len)];
	
	ladder.aliens = [[NSMutableArray alloc] init];
	ladder.aliensToRemove = [[NSMutableArray alloc] init];
	ladder.siblings = [[NSMutableArray alloc] init];
	ladder.beams = [[NSMutableArray alloc] init];
	
	ccTexParams params = {GL_LINEAR,GL_LINEAR,GL_LINEAR, GL_LINEAR};
	[ladder.texture setTexParameters:&params];
	ladder.posX = pos;
	ladder.posY = winSize.height / 2;
	ladder.length = len;
	[ladder setPosition:ccp(ladder.posX, ladder.posY)];
	return ladder;
}

@end
