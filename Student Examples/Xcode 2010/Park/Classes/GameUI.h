//
//  GameUI.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 3..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GameUI : CCLayer {
	CCSprite *_energyGauge;
	CCSprite *_progressGauge;
	CCSprite *_progressGaugeBar;
	CCSprite *_rightGauge;
	CCLabel *_legNumberLabel;
	int _currentNumber;
}

@property (nonatomic, retain) CCSprite *energyGauge;
@property (nonatomic, retain) CCSprite *rightGauge;
@property (nonatomic, retain) CCSprite *progressGauge;
@property (nonatomic, retain) CCSprite *progressGaugeBar;
@property (nonatomic, retain) CCLabel *legNumberLabel;

@end
