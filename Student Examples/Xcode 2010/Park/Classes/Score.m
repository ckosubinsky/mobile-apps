//
//  Score.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 7..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "Score.h"


@implementation Score
@synthesize record = _record;
@synthesize level = _level;
@synthesize recordDate = _recordDate;


-(id)initWithCoder:(NSCoder*)coder
{
    
	if(self = [super init]) {
		_record = [coder decodeIntForKey:@"record"];
		_level = [coder decodeIntForKey:@"level"];
		_recordDate = [[coder decodeObjectForKey:@"date"] retain];
	}
		
	return self;
	
}
	
-(void)encodeWithCoder:(NSCoder*)coder
{
	//[super encodeWithCoder:coder];
    [coder encodeInt:_record forKey:@"record"];
    [coder encodeInt:_level forKey:@"level"];
    [coder encodeObject: _recordDate forKey:@"date"];
}
@end
