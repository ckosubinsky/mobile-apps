//
//  GameScene.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "GameScene.h"
#import "LadderSibling.h"
#import "Ladder.h"
#import "GameData.h"
#import "Alien.h"
#import "Flier.h"
#import "Defenser.h"
#import "GameUI.h"
#import "GameMenu.h"
#import "AlienData.h"

@implementation GameScene
@synthesize layer = _layer;
@synthesize menu = _menu;

-(id) init {
	if(self = [super init]) {
		//self.gameData = data;
	 
		self.layer = [[[GameLayer alloc] init] autorelease];
        [self addChild:_layer];
		
		self.menu = [[GameMenu alloc] init];
        
		CCLayer *uiLayer = [[[GameUI alloc] init] autorelease];	
		[self addChild:uiLayer];
	}
	return self;
}

- (void) popMenu {
	[self addChild:_menu];
}

- (void) closeMenu {
	[GameData sharedData].gameState = PLAYING_STATE;
	[self removeChild:_menu cleanup:YES];
}

- (void) restartMenu {
	[self removeChild:_menu cleanup:YES];
	[_layer initLevel];
}

@end

@implementation GameLayer
@synthesize main_bkgrnd = _main_bkgrnd;
@synthesize label = _label;
@synthesize spriteSheet = _spriteSheet;

-(id) init {
	if(self = [super init]) {
		self.isTouchEnabled = YES;
		
		self.spriteSheet = [CCSpriteSheet spriteSheetWithTexture:[[CCTextureCache sharedTextureCache] addImage:@"sprite.png"]];
        [self addChild:_spriteSheet z:-1];
        
		//set background
		CGSize winSize = [CCDirector sharedDirector].winSize;
		self.main_bkgrnd = [CCSprite spriteWithSpriteFrameName:@"stage_background.png"];
		_main_bkgrnd.position = ccp(winSize.width/2, winSize.height/2);
		[_spriteSheet addChild:_main_bkgrnd];
		
		//add ufos
		for(Flier *flier in [GameData sharedData].fliers) {
			[self addChild:flier];
		}
		for(Defenser *defenser in [GameData sharedData].defensers) {
			[self addChild:defenser z:1];
			DEFENSER_TOUCH_AREA_HEIGHT = defenser.contentSize.height;
		}
		
		//add ladders
		for(Ladder *ladder in [GameData sharedData].ladders) {
			[self addChild:ladder]; 
		}
				
		//initialize current ladder sibling
		_currentSibling = 0;
		
		//add ladderSiblings
		for(LadderSibling *ls in [GameData sharedData].ladderSiblings) {
			[self addChild:ls]; 
			ls.connected = false;
		}
		
_label = [CCLabel labelWithString:@"" dimensions:CGSizeMake(200, 100) alignment:UITextAlignmentCenter fontName:@"Marker Felt" fontSize:12];
		_labelMessage = [CCLabel labelWithString:@"" dimensions:CGSizeMake(200, 100) alignment:UITextAlignmentCenter fontName:@"Marker Felt" fontSize:40];
		_labelMessage.color = ccc3(255,255,255);
		_labelMessage.position = ccp(winSize.width / 2, winSize.height / 2);
		
		_label.position = ccp(100, 0);
		_label.color = ccc3(255, 255, 255);
		[self addChild:_label];
		[self addChild:_labelMessage];
	
	}
	
	return self;
    
}

- (void)onEnter {
    [super onEnter];	
    [self initLevel];
	[self schedule:@selector(update:)];
	
}

- (void)initLevel {
	//delete all loaded sprites
	for(Ladder *ladder in [GameData sharedData].ladders) {
		for(Alien *alien in ladder.aliens)
			[self removeChild:alien cleanup:YES];
		
		for(Alien *beam in ladder.beams)
			[self removeChild:beam cleanup:YES];
		
	}
	
	[[GameData sharedData] loadLevel];
	
	int j = 0;
	float delay = 1.0f;
	for(int i = [GameData sharedData].numberOfLadders - 1; i >= 0; i--) {
		Ladder *targetLadder = [[GameData sharedData].ladders objectAtIndex:i];
		
		if(targetLadder.defenser.enabled)
			j++;
		
		[targetLadder initDefenser];
		[targetLadder initFlier];
		
		[targetLadder placeFlier:delay];
		delay += 1.0f;
	}
	delay += 1.0f;
	CGSize winSize = [CCDirector sharedDirector].winSize;
	_labelMessage.position = ccp(- (_labelMessage.contentSize.width / 2), winSize.height / 2);
	[_labelMessage setString:[NSString stringWithFormat:@"Level %d", [GameData sharedData].currentLevel + 1]];
	
	[_labelMessage runAction:[CCSequence actions:
							  [CCDelayTime actionWithDuration:delay],
							  [CCMoveTo actionWithDuration:0.5f position:ccp(winSize.width / 2,winSize.height / 2)],
							  [CCDelayTime actionWithDuration:1.0f],
							  [CCMoveTo actionWithDuration:0.5f position:ccp(winSize.width + (_labelMessage.contentSize.width / 2), winSize.height / 2)],
							  [CCCallFuncN actionWithTarget:self selector:@selector(finishOpening:)],
							  nil]];
	
	
}

-(void)finishOpening:(id)sender {
	[GameData sharedData].gameState = PLAYING_STATE;
}

-(void)update:(ccTime)dt {
	
	if([GameData sharedData].gameState == PLAYING_STATE) {
		for(Ladder *ladder in [GameData sharedData].ladders) {
			
			[ladder update:dt];
			
			for(Alien *sprite in ladder.aliensToRemove) {
				if(sprite.currentAction == AlienDestroy) {
					[ladder.aliensToRemove removeObject:sprite];
					[self removeChild:sprite cleanup:YES];
				}
			}
			
			if(ladder.defenser.numberOfBeam > 0) {
				[self addBeam:ladder];
				ladder.defenser.numberOfBeam = 0;
			}
		}
		
		if([GameData sharedData].damaging) {
			CGSize winSize = [CCDirector sharedDirector].winSize;
			float delay = 0.05f;
			[_main_bkgrnd runAction:[CCSequence actions:
									 [CCMoveTo actionWithDuration:delay position:ccp(winSize.width/2 - 10, winSize.height/2)],
									 [CCMoveTo actionWithDuration:delay position:ccp(winSize.width/2 + 10, winSize.height/2)],
									 [CCMoveTo actionWithDuration:delay position:ccp(winSize.width/2 - 10, winSize.height/2)],
									 [CCMoveTo actionWithDuration:delay position:ccp(winSize.width/2 + 10, winSize.height/2)],
									 [CCMoveTo actionWithDuration:delay position:ccp(winSize.width/2 - 10, winSize.height/2)],
									 [CCMoveTo actionWithDuration:delay position:ccp(winSize.width/2 + 10, winSize.height/2)],
									 [CCMoveTo actionWithDuration:delay position:ccp(winSize.width/2 - 10, winSize.height/2)],
									 [CCMoveTo actionWithDuration:delay position:ccp(winSize.width/2, winSize.height/2)],
									 nil]];
			[GameData sharedData].damaging = false;
		}
		
		NSMutableArray *spawnAliens = [[GameData sharedData] getAliens:dt];
		for(AlienData *spawnAlien in spawnAliens) {
			[self spawn:spawnAlien.alienType];
		}
		[spawnAliens removeAllObjects];
		
		if([[GameData sharedData] levelComplete]) {
			[self initLevel];
		}
	}
	
}

- (void)spawn:(int)alienType {
	[self addAlien:[Alien alienWithType:alienType]];
}

-(void)addAlien:(Alien*)alien {
	int location = (arc4random() % [GameData sharedData].numberOfLadders);
	[[[GameData sharedData].ladders objectAtIndex:location] addAlien:alien];
	[self addChild:alien];
}

- (void)addBeam:(Ladder*)ladder {
	Alien *beam = [[[Alien alloc] initWithSpriteFrameName:@"beam01_03.png"] autorelease];
	beam.actions = [[NSMutableArray alloc] init];
	
	NSMutableArray *walkAnimFrames = [NSMutableArray array];    
	[walkAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"beam01_01.png"]];
	[walkAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"beam01_02.png"]];
	[walkAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"beam01_03.png"]];
	[walkAnimFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"beam01_02.png"]];
	CCAnimation *walkAnimation = [CCAnimation animationWithName:@"rotate" delay:0.15f frames:walkAnimFrames];
	id walkAction = [CCRepeatForever actionWithAction:
					 [CCAnimate actionWithAnimation:walkAnimation restoreOriginalFrame:NO]];
	[beam runAction:walkAction];
	[beam.actions addObject:walkAction];
	[beam.actions addObject:walkAction];
	[beam.actions addObject:walkAction];
	id stopAction = [CCAnimate actionWithAnimation:walkAnimation restoreOriginalFrame:NO];
	
	[beam.actions addObject:stopAction];
	[beam.actions addObject:stopAction];
	
	beam.speedX = 1.0f;
	beam.speedY = -1.1f;
	
	[self addChild:beam];
	[ladder addBeam:beam];
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	for(UITouch *touch in touches) {
		//UITouch *touch = [touches anyObject];
		CGPoint location = [touch locationInView:[touch view]];
		location = [[CCDirector sharedDirector] convertToGL:location];
		
		int i = 0;
		if(location.y < DEFENSER_TOUCH_AREA_HEIGHT) {
			//defenser touch process
		} else {
			_selectedLeg = [[GameData sharedData] selectedLeg:location];
			
			if(_selectedLeg == nil) {
				
				//LadderSibling *ls = [[GameData sharedData].ladderSiblings objectAtIndex:_currentSibling];
				LadderSibling *ls = [[GameData sharedData].inactiveLegs objectAtIndex:i++];
				
				if(ls.leftLadder != nil) {
					[ls.leftLadder.siblings removeObject:ls];
				}
				
				if(ls.rightLadder != nil) {
					[ls.rightLadder.siblings removeObject:ls];
				}
				
				
				[ls setOpacity:100];
				[ls setScale:1.0f];
				[ls setPosition:location];
				ls.posX = location.x;
				[ls setWidth:8];
			} else {
				[_selectedLeg setColor:ccc3(200, 0, 0)];
			}

		}
	}
}

- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	int i = 0;
	for(UITouch *touch in touches) {
		//UITouch *touch = [touches anyObject];
		CGPoint location = [touch locationInView:[touch view]];
		location = [[CCDirector sharedDirector] convertToGL:location];
		
		if(location.y < DEFENSER_TOUCH_AREA_HEIGHT) {
			//defenser touch process
		} else {
			//LadderSibling *ls = [[GameData sharedData].ladderSiblings objectAtIndex:_currentSibling];
			LadderSibling *ls = [[GameData sharedData].inactiveLegs objectAtIndex:i++];
			float width = (float)(ls.posX) - (float)(location.x);
			[ls setWidth:width];
		}
	}
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	// Get current touch location
	int i = 0;
	for(UITouch *touch in touches) {
		//UITouch *touch = [touches anyObject];
		_currentSibling--;
		CGPoint location = [touch locationInView:[touch view]];
		location = [[CCDirector sharedDirector] convertToGL:location];
		
		if(location.y < DEFENSER_TOUCH_AREA_HEIGHT) {
			//defenser touch process
		} else {
			//LadderSibling *ls = [[GameData sharedData].ladderSiblings objectAtIndex:0];
			if(_selectedLeg == nil) {
				
				LadderSibling *ls = [[GameData sharedData].inactiveLegs objectAtIndex:i++];
				[ls setOpacity:255];
				
				
				float leftPos = (ls.posX < location.x) ? ls.posX : location.x;
				float rightPos = (ls.posX > location.x) ? ls.posX : location.x;
				
				float width = abs((float)(ls.posX) - (float)(location.x));
				
				Ladder *leftLadder = nil;	
				Ladder *rightLadder = nil;
				
				if(ls.position.x < location.x) { //starting point is on the left
					
					for(int i = 0; i < [GameData sharedData].numberOfLadders - 1;i++) {
						leftLadder = [[GameData sharedData].ladders objectAtIndex:i];
						rightLadder = [[GameData sharedData].ladders objectAtIndex:i+1];
						
						float median = ((float)(rightLadder.position.x) + (float)(leftLadder.position.x)) / 2;
						float minLength = ((float)(rightLadder.position.x) - (float)(leftLadder.position.x)) / 3;
						
						if(width < minLength) { //length too short to connect
							[ls setOpacity:0];
							break;
						}
						
						if(leftPos < median) {
							
							if(![[GameData sharedData] isLegAvailable]) {
								//if legs are not enough, disconnect oldest connection
								LadderSibling *oldLeg = [[GameData sharedData].activeLegs objectAtIndex:0];
								[[GameData sharedData] cutConnection:oldLeg];
								
							}
							
							
							[ls connectLeftLadder:leftLadder
									  rightLadder:rightLadder];
							[leftLadder.siblings addObject:ls];
							[rightLadder.siblings addObject:ls];
							ls.connected = true;
							[[GameData sharedData].inactiveLegs removeObject:ls];
							[[GameData sharedData].activeLegs addObject:ls];
							break;
						}
						
					}
				} else { //starting point is on the right
					for(int i = [GameData sharedData].numberOfLadders - 1; i > 0;i--) {
						leftLadder = [[GameData sharedData].ladders objectAtIndex:i-1];
						rightLadder = [[GameData sharedData].ladders objectAtIndex:i];
						
						float median = ((float)(rightLadder.position.x) + (float)(leftLadder.position.x)) / 2;
						float minLength = ((float)(rightLadder.position.x) - (float)(leftLadder.position.x)) / 3;
						
						if(width < minLength) { //length too short to connect
							[ls setOpacity:0];
							break;
						}
						
						if(rightPos > median) {
							
							if(![[GameData sharedData] isLegAvailable]) {
								//if legs are not enough, disconnect oldest connection
								LadderSibling *oldLeg = [[GameData sharedData].activeLegs objectAtIndex:0];
								[[GameData sharedData] cutConnection:oldLeg];
								
							}
							
							
							[ls connectLeftLadder:leftLadder
									  rightLadder:rightLadder];
							[leftLadder.siblings addObject:ls];
							[rightLadder.siblings addObject:ls];
							ls.connected = true;
							[[GameData sharedData].inactiveLegs removeObject:ls];
							[[GameData sharedData].activeLegs addObject:ls];
							break;
						}
						
					}
				}
			} else {
				_currentSibling--;				
				[_selectedLeg setColor:ccc3(255, 255, 255)];
				LadderSibling *selectedLeg = [[GameData sharedData] selectedLeg:location];
				
				if (_selectedLeg == selectedLeg)
					[[GameData sharedData] cutConnection:selectedLeg];
			}
		}
	}
}

@end

