//
//  Doodle_RaiderAppDelegate.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright Stony Brook University 2010. All rights reserved.
//

#import "Doodle_RaiderAppDelegate.h"
#import "cocos2d.h"
#import "GameScene.h"
#import "GameData.h"
#import "HighScoresScene.h"
#import "CreditsScene.h"
#import "MainMenuScene.h"
#import "LoadingScene.h"

@implementation Doodle_RaiderAppDelegate

@synthesize window;
@synthesize loadingScene = _loadingScene;
@synthesize mainMenuScene = _mainMenuScene;
@synthesize gameData = _gameData;
@synthesize gameScene = _gameScene;
@synthesize scoreScene = _scoreScene;
@synthesize helpScene = _helpScene;
@synthesize creditsScene = _creditsScene;

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
	// CC_DIRECTOR_INIT()
	//
	// 1. Initializes an EAGLView with 0-bit depth format, and RGB565 render buffer
	// 2. EAGLView multiple touches: disabled
	// 3. creates a UIWindow, and assign it to the "window" var (it must already be declared)
	// 4. Parents EAGLView to the newly created window
	// 5. Creates Display Link Director
	// 5a. If it fails, it will use an NSTimer director
	// 6. It will try to run at 60 FPS
	// 7. Display FPS: NO
	// 8. Device orientation: Portrait
	// 9. Connects the director to the EAGLView
	//
	CC_DIRECTOR_INIT();
	
	// Obtain the shared director in order to...
	CCDirector *director = [CCDirector sharedDirector];
	
	// Sets landscape mode
	[director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
	
	// Turn on display FPS
	[director setDisplayFPS:NO];
	
	// Turn on multiple touches
	EAGLView *view = [director openGLView];
	[view setMultipleTouchEnabled:YES];
	
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kTexture2DPixelFormat_RGBA8888];	
	
		
	self.loadingScene = [[[LoadingScene alloc] init] autorelease];
	[[CCDirector sharedDirector] runWithScene: _loadingScene];
}


- (void) loadScenes {
	EAGLContext *k_context = [[[EAGLContext alloc]
                               initWithAPI:kEAGLRenderingAPIOpenGLES1
                               sharegroup:[[[[CCDirector sharedDirector] openGLView] context] sharegroup]] autorelease];    
    [EAGLContext setCurrentContext:k_context];
    
	self.mainMenuScene = [[[MainMenuScene alloc] init] autorelease];
	self.scoreScene = [[[HighScoresScene alloc] init] autorelease];
	self.creditsScene = [[[CreditsScene alloc] init] autorelease];
	self.helpScene = [[[HelpScene alloc] init] autorelease];
	self.gameScene = [[[GameScene alloc] init] autorelease];
	
}

- (void) launchMainMenu {	
	[[CCDirector sharedDirector] replaceScene:[CCFadeTransition 
											   transitionWithDuration:1.0f scene:_mainMenuScene]];
	 
}

- (void) launchNewGame {
	[[CCDirector sharedDirector] replaceScene:[CCFadeTransition
											   transitionWithDuration:1.0f scene:_gameScene]];
}

- (void) launchHighScore {
	[[CCDirector sharedDirector] replaceScene:[CCFadeTransition
											   transitionWithDuration:1.0f scene:_scoreScene]];
}

- (void) launchHelp {
	[[CCDirector sharedDirector] replaceScene:[CCFadeTransition
											   transitionWithDuration:1.0f scene:_helpScene]];
}

- (void) launchCredits {
	[[CCDirector sharedDirector] replaceScene:[CCFadeTransition
											   transitionWithDuration:1.0f scene:_creditsScene]];

}

- (void) launchIngameMenu:(BOOL)pop {
	if(pop)
		[_gameScene popMenu];
	else
		[_gameScene closeMenu];

}

- (void) launchRestartGame {
	[_gameScene restartMenu];
}

- (void)applicationWillResignActive:(UIApplication *)application {
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	[[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application {
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
	[[CCDirector sharedDirector] startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application {
	[[CCDirector sharedDirector] end];
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc {
	self.loadingScene = nil;
	//self.menuScene = nil;
	//self.gameScene = nil;
	
	
	[[CCDirector sharedDirector] release];
	[window release];
	[super dealloc];
}

@end
