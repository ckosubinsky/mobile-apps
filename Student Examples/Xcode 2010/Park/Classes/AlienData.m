//
//  AlienData.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 12. 8..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import "AlienData.h"


@implementation AlienData
@synthesize alienType = _alienType;
@synthesize spawnTime = _spawnTime;
@end
