//
//  Ladder.h
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright 2010 Stony Brook University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class Alien;
@class LadderSibling;
@class Defenser;
@class Flier;

@interface Ladder : CCSprite {
	int _posX;
	int _posY;
	int _length;
	int _border;
	BOOL _active;
	NSMutableArray *_siblings;
	NSMutableArray *_aliens;
	NSMutableArray *_aliensToRemove;
	NSMutableArray *_beams;
	Defenser *_defenser;
	Flier *_flier;
}


@property (nonatomic, assign) int posX;
@property (nonatomic, assign) int posY;
@property (nonatomic, assign) int border;
@property (nonatomic, assign) int length;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, assign) Defenser *defenser;
@property (nonatomic, assign) Flier *flier;
@property (nonatomic, assign) NSMutableArray *siblings;
@property (nonatomic, assign) NSMutableArray *aliens;
@property (nonatomic, assign) NSMutableArray *aliensToRemove;
@property (nonatomic, assign) NSMutableArray *beams;

- (void)addLadderSibbling:(LadderSibling *)sibling;
- (void)addAlien:(Alien *)alien;
- (void)addBeam:(Alien *)beam;
- (void)removeAlien:(CCSprite*)alien;
- (void)initDefenser;
- (void)initFlier;
- (void)placeFlier:(float)delay;
- (void)update:(ccTime)dt;
+ (Ladder *)ladderWithPos:(int)pos length:(int)len;

@end
