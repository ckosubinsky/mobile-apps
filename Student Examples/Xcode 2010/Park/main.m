//
//  main.m
//  Doodle Raider
//
//  Created by Justin Park on 10. 11. 21..
//  Copyright Stony Brook University 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	int retVal = UIApplicationMain(argc, argv, nil, @"Doodle_RaiderAppDelegate");
	[pool release];
	return retVal;
}
