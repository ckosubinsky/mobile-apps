
#|
$Properties
$YaVersion 16
$Source $Form
$Define Screen1 $As Form $Version 3
Title = "Computer History Quiz"
Uuid = 0
$Define Image1 $As Image $Version 1
Picture = "babbage_sm.jpg"
Uuid = 693541589
$End $Define
$Define QuestionLabel $As Label $Version 1
Alignment = 1
FontBold = True
Text = "Question"
Uuid = 1497723986
Width = -2
$End $Define
$Define HorizontalArrangement1 $As HorizontalArrangement $Version 1
Uuid = -39297712
Width = -2
$Define AnswerPromptLabel $As Label $Version 1
Alignment = 1
Text = "Your Answer:"
Uuid = -710649457
Width = -2
$End $Define
$Define AnswerText $As TextBox $Version 2
Hint = "Type your answer here."
Uuid = 1638600132
Width = -2
$End $Define
$End $Define
$Define RightWrongLabel $As Label $Version 1
Alignment = 1
FontBold = True
Text = "correct/incorrect"
Uuid = -1091147610
Width = -2
$End $Define
$Define HorizontalArrangement2 $As HorizontalArrangement $Version 1
Uuid = 579685109
Width = -2
$Define AnswerButton $As Button $Version 1
Text = "Submit"
Uuid = 62346047
Width = -2
$End $Define
$Define NextButton $As Button $Version 1
Text = "Next Question"
Uuid = 377773236
Width = -2
$End $Define
$End $Define
$Define Cheer $As Sound $Version 2
Source = "cheer.wav"
Uuid = -1483936128
$End $Define
$Define Boo $As Sound $Version 2
Source = "boo.wav"
Uuid = 19655249
$End $Define
$End $Define
$End $Properties

|#
#|
$JSON
{"YaVersion":"16","Source":"Form","Properties":{"$Name":"Screen1","$Type":"Form","$Version":"3","Uuid":"0","Title":"\"Computer History Quiz\"","$Components":[{"$Name":"Image1","$Type":"Image","$Version":"1","Uuid":"693541589","Picture":"\"babbage_sm.jpg\""},{"$Name":"QuestionLabel","$Type":"Label","$Version":"1","Uuid":"1497723986","Alignment":"1","FontBold":"True","Text":"\"Question\"","Width":"-2"},{"$Name":"HorizontalArrangement1","$Type":"HorizontalArrangement","$Version":"1","Uuid":"-39297712","Width":"-2","$Components":[{"$Name":"AnswerPromptLabel","$Type":"Label","$Version":"1","Uuid":"-710649457","Alignment":"1","Text":"\"Your Answer:\"","Width":"-2"},{"$Name":"AnswerText","$Type":"TextBox","$Version":"2","Uuid":"1638600132","Hint":"\"Type your answer here.\"","Width":"-2"}]},{"$Name":"RightWrongLabel","$Type":"Label","$Version":"1","Uuid":"-1091147610","Alignment":"1","FontBold":"True","Text":"\"correct\/incorrect\"","Width":"-2"},{"$Name":"HorizontalArrangement2","$Type":"HorizontalArrangement","$Version":"1","Uuid":"579685109","Width":"-2","$Components":[{"$Name":"AnswerButton","$Type":"Button","$Version":"1","Uuid":"62346047","Text":"\"Submit\"","Width":"-2"},{"$Name":"NextButton","$Type":"Button","$Version":"1","Uuid":"377773236","Text":"\"Next Question\"","Width":"-2"}]},{"$Name":"Cheer","$Type":"Sound","$Version":"2","Uuid":"-1483936128","Source":"\"cheer.wav\""},{"$Name":"Boo","$Type":"Sound","$Version":"2","Uuid":"19655249","Source":"\"boo.wav\""}]}}
|#